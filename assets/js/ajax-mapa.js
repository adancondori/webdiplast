var map;
var marker = null;
var setRegion = null;

function mostrarMapa()
{
    var center = new google.maps.LatLng(-17.783057284741652, -63.182616122066975);
    var mapOptions = {
        center: center,
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
}

function insertPoligono(locations) {
//        alert(poligono);
//    var locations = google.maps.geometry.encoding.decodePath(poligono);
//    alert(locations.length);
    var setRegion = new google.maps.Polygon({
        paths: locations,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35
    });

    setRegion.setMap(map);
}

function verZona(poligonoEncode) {
    if (setRegion) {
        setRegion.setMap(null);
    }
    var locations = google.maps.geometry.encoding.decodePath(poligonoEncode);
    setRegion = new google.maps.Polygon({
        paths: locations,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35
    });
    setRegion.setMap(map);
}


/* Application Negocio
 ------------------------------------------------ */
var Mapa = function() {
    "use strict";

    return {
        mostrarMapa: function() {
            mostrarMapa();
        },
        insertPoligono: function(poligono) {
            insertPoligono(poligono);
        },
        verZona: function(poligonoEncode) {
            verZona(poligonoEncode);
        },
    };
}();