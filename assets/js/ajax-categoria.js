function listarCategoria() {
    var parametros = {
        "modo": "listar"
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/categoria/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_categoria();
        }
    });
}
function listarconimagenCategoria() {
    var parametros = {
        "modo": "listarconimage"
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/categoria/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            
            document.getElementById('content').innerHTML = response;
            Gallery.init();
        }
    });
}

function nuevoCategoria() {

    var parametros = {
        "modo": "nuevo"
    };
    $.ajax({
        data: parametros,
        url: '../../indexyii.php/categoria/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_validarform();
            // Ejecucion de Parsley  para validar el formulario, si es valido se ejecuta el metodo "guardarpersonal"
            $('#formulario').parsley();
            $(document).ready(function() {
                $('#formulario').parsley().subscribe('parsley:form:validate', function(formInstance) {

                    if (formInstance.isValid('block1', true)) {
                        guardarCategoria();
                        formInstance.submitEvent.preventDefault();
                        return;
                    }
                    // else stop form submission
                    formInstance.submitEvent.preventDefault();
                });
            });
        }
    });
}

function guardarCategoria() {

    var nombre = document.getElementById('nombre').value;
    var descripcion = document.getElementById('descripcion').value;

    var parametros = {
        "modo": "guardar",
        "Categoria[nombre]": nombre,
        "Categoria[descripcion]": descripcion,
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/categoria/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            alert('Guardado');
            listarCategoria();
        }
    });
    return false;
}

function cargarCategoria(id) {
    var parametros = {
        "modo": "cargar",
        "id": id
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/categoria/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_validarform();
            // Ejecucion de Parsley  para validar el formulario, si es valido se ejecuta el metodo "modificarpersonal"
            $('#formulario').parsley();
            $(document).ready(function() {
                $('#formulario').parsley().subscribe('parsley:form:validate', function(formInstance) {
                    if (formInstance.isValid('block1', true)) {
                        modificarCategoria(id);
                        formInstance.submitEvent.preventDefault();
                        return;
                    }
                    // else stop form submission
                    formInstance.submitEvent.preventDefault();
                });
            });
        }
    });
}

function modificarCategoria(id) {

    var nombre = document.getElementById('nombre').value;
    var descripcion = document.getElementById('descripcion').value;

    var parametros = {
        "modo": "modificar",
        "id": id,
        "Categoria[nombre]": nombre,
        "Categoria[descripcion]": descripcion
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/categoria/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            alert('Modificado');
            listarCategoria();
        }
    });
    return false;
}
/* Application Categoria
 ------------------------------------------------ */
var Categoria = function() {
    "use strict";

    return {
        listar: function() {
            listarCategoria();
        },
        listarconimagen: function() {
            listarconimagenCategoria();
        },
        nuevo: function() {
            nuevoCategoria();
        },
        guardar: function() {
            return guardarCategoria();
        },
        cargar: function(id) {
            cargarCategoria(id);
        },
        modificar: function(id) {
            return modificarCategoria(id);
        }
    };
}();