function listarPedido() {
    var parametros = {
        "modo": "listar"
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/pedido/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_tabla();
        }
    });
}

function nuevoPedido(id) {

    var parametros = {
        "modo": "nuevo",
        "idcategoria": id
    };

    $.ajax({
        data: parametros,
        url: 'indexyii.php/pedido/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_validarform();
            // Ejecucion de Parsley  para validar el formulario, si es valido se ejecuta el metodo "guardarPedido"
            $('#formulario').parsley();
            $(document).ready(function() {
                $('#formulario').parsley().subscribe('parsley:form:validate', function(formInstance) {
                    if (formInstance.isValid('block1', true)) {
                        guardarPedido();
                        formInstance.submitEvent.preventDefault();
                        return;
                    }
                    // else stop form submission
                    formInstance.submitEvent.preventDefault();
                });
            });
        }
    });
}

function guardarPedido() {

    var nombre = document.getElementById('nombre').value;
    var descripcion = document.getElementById('descripcion').value;
    var medida = document.getElementById('medida').value;
    var precio = document.getElementById('precio').value;
    var stock = document.getElementById('stock').value;
    var id_categoria = document.getElementById('id_categoria').value;
    var parametros = {
        "modo": "guardar",
        "Pedido[nombre]": nombre,
        "Pedido[descripcion]": descripcion,
        "Pedido[medida]": medida,
        "Pedido[precio]": precio,
        "Pedido[stock]": stock,
        "Pedido[id_categoria]": id_categoria
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/pedido/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            alert('Guardado');
            listarPedido();
        }
    });
    return false;
}

function cargarPedido(id) {
    var parametros = {
        "modo": "cargar",
        "id": id
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/pedido/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_validarform();
            // Ejecucion de Parsley  para validar el formulario, si es valido se ejecuta el metodo "modificarpedido"
            $('#formulario').parsley();
            $(document).ready(function() {
                $('#formulario').parsley().subscribe('parsley:form:validate', function(formInstance) {
                    if (formInstance.isValid('block1', true)) {
                        modificarPedido(id);
                        formInstance.submitEvent.preventDefault();
                        return;
                    }
                    // else stop form submission
                    formInstance.submitEvent.preventDefault();
                });
            });
        }
    });
}

function modificarPedido(id) {

    var nombre = document.getElementById('nombre').value;
    var descripcion = document.getElementById('descripcion').value;
    var medida = document.getElementById('medida').value;
    var precio = document.getElementById('precio').value;
    var stock = document.getElementById('stock').value;

    var parametros = {
        "modo": "modificar",
        "id": id,
        "Pedido[nombre]": nombre,
        "Pedido[descripcion]": descripcion,
        "Pedido[medida]": medida,
        "Pedido[precio]": precio,
        "Pedido[stock]": stock
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/pedido/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            alert('Modificado');
            listarPedido();
        }
    });
    return false;
}
function listardetallePedido(id_pedido) {

    var parametros = {
        "modo": "listardetalle",
        "id_pedido": id_pedido
    };
    alert(id_pedido);
    $.ajax({
        data: parametros,
        url: 'indexyii.php/pedido/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            alert(response);
            document.getElementById('content').innerHTML = response;
            script_tabla();
        }
    });
    return false;
}
/* Application Pedido
 ------------------------------------------------ */
var Pedido = function() {
    "use strict";

    return {
        listar: function() {
            listarPedido();
        },
        nuevo: function(id) {
            nuevoPedido(id);
        },
        guardar: function() {
            return guardarPedido();
        },
        cargar: function(id) {
            cargarPedido(id);
        },
        modificar: function(id) {
            return modificarPedido(id);
        },
        listardetalle: function(id) {
            return listardetallePedido(id);
        }
    };
}();