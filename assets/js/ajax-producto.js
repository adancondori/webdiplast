function listarProducto() {
    var parametros = {
        "modo": "listar"
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/producto/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_tabla();
        }
    });
}

function nuevoProducto(id) {

    var parametros = {
        "modo": "nuevo",
        "idcategoria": id
    };

    $.ajax({
        data: parametros,
        url: 'indexyii.php/producto/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_validarform();
            // Ejecucion de Parsley  para validar el formulario, si es valido se ejecuta el metodo "guardarProducto"
            $('#formulario').parsley();
            $(document).ready(function() {
                $('#formulario').parsley().subscribe('parsley:form:validate', function(formInstance) {
                    if (formInstance.isValid('block1', true)) {
                        guardarProducto();
                        formInstance.submitEvent.preventDefault();
                        return;
                    }
                    // else stop form submission
                    formInstance.submitEvent.preventDefault();
                });
            });
        }
    });
}

function guardarProducto() {

    var nombre = document.getElementById('nombre').value;
    var descripcion = document.getElementById('descripcion').value;
    var medida = document.getElementById('medida').value;
    var precio = document.getElementById('precio').value;
    var stock = document.getElementById('stock').value;
    var id_categoria = document.getElementById('id_categoria').value;
    var parametros = {
        "modo": "guardar",
        "Producto[nombre]": nombre,
        "Producto[descripcion]": descripcion,
        "Producto[medida]": medida,
        "Producto[precio]": precio,
        "Producto[stock]": stock,
        "Producto[id_categoria]": id_categoria
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/producto/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            alert('Guardado');
            listarProducto();
        }
    });
    return false;
}

function cargarProducto(id) {
    var parametros = {
        "modo": "cargar",
        "id": id
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/producto/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_validarform();
            // Ejecucion de Parsley  para validar el formulario, si es valido se ejecuta el metodo "modificarproducto"
            $('#formulario').parsley();
            $(document).ready(function() {
                $('#formulario').parsley().subscribe('parsley:form:validate', function(formInstance) {
                    if (formInstance.isValid('block1', true)) {
                        modificarProducto(id);
                        formInstance.submitEvent.preventDefault();
                        return;
                    }
                    // else stop form submission
                    formInstance.submitEvent.preventDefault();
                });
            });
        }
    });
}

function modificarProducto(id) {

    var nombre = document.getElementById('nombre').value;
    var descripcion = document.getElementById('descripcion').value;
    var medida = document.getElementById('medida').value;
    var precio = document.getElementById('precio').value;
    var stock = document.getElementById('stock').value;

    var parametros = {
        "modo": "modificar",
        "id": id,
        "Producto[nombre]": nombre,
        "Producto[descripcion]": descripcion,
        "Producto[medida]": medida,
        "Producto[precio]": precio,
        "Producto[stock]": stock
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/producto/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            alert('Modificado');
            listarProducto();
        }
    });
    return false;
}
/* Application Producto
 ------------------------------------------------ */
var Producto = function() {
    "use strict";

    return {
        listar: function() {
            listarProducto();
        },
        nuevo: function(id) {
            nuevoProducto(id);
        },
        guardar: function() {
            return guardarProducto();
        },
        cargar: function(id) {
            cargarProducto(id);
        },
        modificar: function(id) {
            return modificarProducto(id);
        }
    };
}();