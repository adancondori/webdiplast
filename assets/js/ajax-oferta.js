function listarOferta() {
    var parametros = {
        "modo": "listar"
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/oferta/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_tabla();
        }
    });
}

function nuevoOferta() {

    var parametros = {
        "modo": "nuevo"
    };

    $.ajax({
        data: parametros,
        url: 'indexyii.php/oferta/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_validarform();
            handleDatepicker();//solo es para el Date 
            // Ejecucion de Parsley  para validar el formulario, si es valido se ejecuta el metodo "guardarOferta"
            $('#formulario').parsley();
            $(document).ready(function() {
                $('#formulario').parsley().subscribe('parsley:form:validate', function(formInstance) {
                    if (formInstance.isValid('block1', true)) {
                        guardarOferta();
                        formInstance.submitEvent.preventDefault();
                        return;
                    }
                    // else stop form submission
                    formInstance.submitEvent.preventDefault();
                });
            });
        }
    });
}

function guardarOferta() {
    var nombre = document.getElementById('nombre').value;
    var descripcion = document.getElementById('descripcion').value;
    var fecha = document.getElementById('fecha').value;
    var tipo = document.getElementById('tipo').value;
    var id_producto = document.getElementById('id_producto').value;

    var parametros = {
        "modo": "guardar",
        "Oferta[nombre]": nombre,
        "Oferta[descripcion]": descripcion,
        "Oferta[fecha]": fecha,
        "Oferta[tipo]": tipo,
        "Oferta[id_producto]": id_producto
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/oferta/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            alert('Guardado');
            listarOferta();
        }
    });
    return false;
}

function cargarOferta(id) {
    var parametros = {
        "modo": "cargar",
        "id": id
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/oferta/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_validarform();
            handleDatepicker();//solo es para el Date 
            // Ejecucion de Parsley  para validar el formulario, si es valido se ejecuta el metodo "modificaroferta"
            $('#formulario').parsley();
            $(document).ready(function() {
                $('#formulario').parsley().subscribe('parsley:form:validate', function(formInstance) {
                    if (formInstance.isValid('block1', true)) {
                        modificarOferta(id);
                        formInstance.submitEvent.preventDefault();
                        return;
                    }
                    // else stop form submission
                    formInstance.submitEvent.preventDefault();
                });
            });
        }
    });
}

function modificarOferta(id) {

    var nombre = document.getElementById('nombre').value;
    var descripcion = document.getElementById('descripcion').value;
    var fecha = document.getElementById('fecha').value;
    var tipo = document.getElementById('tipo').value;
    var id_producto = document.getElementById('id_producto').value;

    var parametros = {
        "modo": "modificar",
        "id": id,
        "Oferta[nombre]": nombre,
        "Oferta[descripcion]": descripcion,
        "Oferta[fecha]": fecha,
        "Oferta[tipo]": tipo,
        "Oferta[id_producto]": id_producto
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/oferta/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            alert('Modificado');
            listarOferta();
        }
    });
    return false;
}
/* Application Oferta
 ------------------------------------------------ */
var Oferta = function() {
    "use strict";

    return {
        listar: function() {
            listarOferta();
        },
        nuevo: function(id) {
            nuevoOferta(id);
        },
        guardar: function() {
            return guardarOferta();
        },
        cargar: function(id) {
            cargarOferta(id);
        },
        modificar: function(id) {
            return modificarOferta(id);
        }
    };
}();