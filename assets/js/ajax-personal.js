function listarPersonal() {
    var parametros = {
        "modo": "listar"
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/personal/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_tabla();
        }
    });
}

function nuevoPersonal() {

    var parametros = {
        "modo": "nuevo"
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/personal/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_validarform();
            script_mapa();
            // Ejecucion de Parsley  para validar el formulario, si es valido se ejecuta el metodo "guardarpersonal"
            $('#formulario').parsley();
            $(document).ready(function() {
                $('#formulario').parsley().subscribe('parsley:form:validate', function(formInstance) {
                    if (formInstance.isValid('block1', true)) {
                        guardarPersonal();
                        formInstance.submitEvent.preventDefault();
                        return;
                    }
                    // else stop form submission
                    formInstance.submitEvent.preventDefault();
                });
            });
        }
    });
}

function guardarPersonal() {

    var nombre = document.getElementById('nombre').value;
    var apellidos = document.getElementById('apellidos').value;
    var direccion = document.getElementById('direccion').value;
    var telefono = document.getElementById('telefono').value;
    var cargo = document.getElementById('cargo').value;
    var ci = document.getElementById('ci').value;
    var email = document.getElementById('email').value;

    var parametros = {
        "modo": "guardar",
        "Personal[nombre]": nombre,
        "Personal[apellidos]": apellidos,
        "Personal[direccion]": direccion,
        "Personal[telefono]": telefono,
        "Personal[cargo]": cargo,
        "Personal[ci]": ci,
        "Personal[email]": email
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/personal/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            alert('Guardado');
            listarPersonal();
        }
    });
    return false;
}

function cargarPersonal(id) {
    var parametros = {
        "modo": "cargar",
        "id": id
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/personal/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_validarform();
            // Ejecucion de Parsley  para validar el formulario, si es valido se ejecuta el metodo "modificarpersonal"
            $('#formulario').parsley();
            $(document).ready(function() {
                $('#formulario').parsley().subscribe('parsley:form:validate', function(formInstance) {
                    if (formInstance.isValid('block1', true)) {
                        modificarPersonal(id);
                        formInstance.submitEvent.preventDefault();
                        return;
                    }
                    // else stop form submission
                    formInstance.submitEvent.preventDefault();
                });
            });
        }
    });
}

function modificarPersonal(id) {

    var nombre = document.getElementById('nombre').value;
    var apellidos = document.getElementById('apellidos').value;
    var direccion = document.getElementById('direccion').value;
    var telefono = document.getElementById('telefono').value;
    var cargo = document.getElementById('cargo').value;
    var ci = document.getElementById('ci').value;
    var email = document.getElementById('email').value;

    var parametros = {
        "modo": "modificar",
        "id": id,
        "Personal[nombre]": nombre,
        "Personal[apellidos]": apellidos,
        "Personal[direccion]": direccion,
        "Personal[telefono]": telefono,
        "Personal[cargo]": cargo,
        "Personal[ci]": ci,
        "Personal[email]": email
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/personal/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            alert('Modificado');
            listarPersonal();
        }
    });
    return false;
}

function monitoreoPersonal() {
    var parametros = {
        "modo": "monitoreo"
    };

    $.ajax({
        data: parametros,
        url: 'indexyii.php/personal/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_mapa();
        }
    });
}


/* Application Personal
 ------------------------------------------------ */
var Personal = function() {
    "use strict";

    return {
        listar: function() {
            listarPersonal();
        },
        nuevo: function() {
            nuevoPersonal();
        },
        guardar: function() {
            return guardarPersonal();
        },
        cargar: function(id) {
            cargarPersonal(id);
        },
        modificar: function(id) {
            return modificarPersonal(id);
        },
        monitoreo: function(id) {
            return monitoreoPersonal(id);
        }
    };
}();