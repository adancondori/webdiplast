function loadScript(url) {
    var script = document.createElement('script');

    if (script.readyState) { // IE
        script.onreadystatechange = function() {
            if (script.readyState === 'loaded' || script.readyState === 'complete') {
                script.onreadystatechange = null;
            }
        };
    } else { // Others
        script.onload = function() {
        };
    }
    script.src = url;
//    document.getElementsByTagName('head')[0].appendChild(script);
    $(".content").append(script);
}

function script_tabla() {
    var myLibrary = 'assets/plugins/DataTables-1.9.4/js/jquery.dataTables.js';
    loadScript(myLibrary);
    myLibrary = 'assets/plugins/DataTables-1.9.4/js/data-table.js';
    loadScript(myLibrary);
    $(document).ready(function() {
        handlePanelAction(); //  expandir y colapsar
    });
}

function script_validarform() {
//    var myLibrary = 'http://localhost/webdiplast/assets/plugins/parsley/parsley.js';
//    loadScript(myLibrary);
    $(document).ready(function() {
        handlePanelAction();
    });
}

function script_categoria() {
    myLibrary = 'assets/plugins/slimscroll/jquery.slimscroll.min.js';
    loadScript(myLibrary);
    $(document).ready(function() {
        handelTooltipPopoverActivation();
    });
}


function script_galeria() {
//    myLibrary = 'assets/plugins/isotope/jquery.isotope.min.js';
//    loadScript(myLibrary);
    alert("123");
//    $(document).ready(function() {
//        $('#content').load(toLoad, showNewContent());}
//        alert("123456");
//        $.getScript("gallery.demo.js");
//
//    });
    Gallery.init();
}

function script_mapa() {
//    var myLibrary = 'https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false';
//    loadScript(myLibrary);
    var myLibrary = 'assets/js/map-google.demo.js';
    loadScript(myLibrary);
    var myLibrary2 = 'assets/plugins/bootstrap-select/bootstrap-select.min.js';
    loadScript(myLibrary2);
    handleDatepicker();//solo es para el Date 
    setTimeout(MapGoogle.init(), 2000);
}

function script_wizard() {
    var myLibrary = 'assets/plugins/bootstrap-wizard/js/bwizard.js';
    loadScript(myLibrary);
    myLibrary = 'assets/js/form-wizards.demo.min.js';
    loadScript(myLibrary);
    $(document).ready(function() {
        FormWizard.init();
    });
}

function script_mapsPoint() {
//    var myLibrary = 'http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false';
//    loadScript(myLibrary);

    setTimeout(Negocio.iniciarMapa, 4000);
}