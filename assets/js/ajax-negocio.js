var map;
var marker = null;

function iniciarMapa() {
    setTimeout(iniciarMapa1, 400);
}
function iniciarMapa1()
{
    var center = new google.maps.LatLng(-17.783057284741652, -63.182616122066975);
    var mapOptions = {
        center: center,
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);

    google.maps.event.addListener(map, 'click', function(event) {
        setLatLong(event.latLng);
    });
    google.maps.event.trigger(map, 'resize');
}

function mostrarMapa()
{
    var center = new google.maps.LatLng(-17.783057284741652, -63.182616122066975);
    var mapOptions = {
        center: center,
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);
}

function setLatLong(location) {
    if (marker != null) {
        marker.setMap(null);
    }
    marker = new google.maps.Marker({
        position: location,
        map: map
    });
    document.getElementById("localizacion").value = location.lat() + "," + location.lng();
}

function setLatLong2(lat, long) {

    if (marker != null) {
        marker.setMap(null);
    }
    var location = new google.maps.LatLng(lat, long);

    marker = new google.maps.Marker({
        position: location,
        map: map
    });
}

function setLatLong3(lat, long) {

    setTimeout(function() {
        setLatLong2(lat, long);
    }, 500);
}



/* Application Negocio
 ------------------------------------------------ */
var Negocio = function() {
    "use strict";

    return {
        iniciarMapa: function() {
            iniciarMapa();
        },
        setLatLong2: function(lat, long) {
            setLatLong3(lat, long);
        },
        mostrarMapa: function() {
            mostrarMapa();
        }
    };
}();