function listarCliente() {
    var parametros = {
        "modo": "listar"
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/cliente/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_tabla();
        }
    });
}

function nuevoCliente() {

    var parametros = {
        "modo": "nuevo"
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/cliente/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_validarform();
            script_wizard();
            // Ejecucion de Parsley  para validar el formulario, si es valido se ejecuta el metodo "guardarcliente"
            $('#formulario').parsley();
            $(document).ready(function() {
                $('#formulario').parsley().subscribe('parsley:form:validate', function(formInstance) {
                    if (formInstance.isValid('block1', true)) {
                        guardarCliente();
                        formInstance.submitEvent.preventDefault();
                        return;
                    }
                    // else stop form submission
                    formInstance.submitEvent.preventDefault();
                });
            });
            script_mapsPoint();
        }
    });
}

function guardarCliente() {

    var codigo = document.getElementById('codigo').value;
    var nombre = document.getElementById('nombre').value;
    var apellidos = document.getElementById('apellidos').value;
    var direccion = document.getElementById('direccion').value;
    var sexo = document.getElementById('sexo').value;
    var fechanacimiento = document.getElementById('fechanacimiento').value;
    var telefono = document.getElementById('telefono').value;
    var celular = document.getElementById('celular').value;
    var ci = document.getElementById('ci').value;
    var email = document.getElementById('email').value;

    var localizacion = document.getElementById('localizacion').value;
    var loc = localizacion.split(',');

    var nombre1 = document.getElementById('nombre1').value;
    var razonsocial = document.getElementById('razonsocial').value;
    var nit = document.getElementById('nit').value;
    var direccion1 = document.getElementById('direccion1').value;
    var latitud = loc[0];
    var longitud = loc[1];
    var observacion = document.getElementById('observacion').value;
    var id_zona = document.getElementById('zona').value;

    var parametros = {
        "modo": "guardar",
        "Cliente[codigo]": codigo,
        "Cliente[nombre]": nombre,
        "Cliente[apellidos]": apellidos,
        "Cliente[direccion]": direccion,
        "Cliente[sexo]": sexo,
        "Cliente[fechanacimiento]": fechanacimiento,
        "Cliente[telefono]": telefono,
        "Cliente[celular]": celular,
        "Cliente[ci]": ci,
        "Cliente[email]": email,
        "Negocio[nombre]": nombre1,
        "Negocio[razonsocial]": razonsocial,
        "Negocio[nit]": nit,
        "Negocio[direccion]": direccion1,
        "Negocio[latitud]": latitud,
        "Negocio[longitud]": longitud,
        "Negocio[observacion]": observacion,
        "Negocio[id_cliente]": id_zona,
        "Negocio[id_zona]": id_zona
    };

    var frame = document.getElementById('imagenes');
    var ar = frame.contentWindow.document.getElementsByName('imagen0');
    var len = ar.length;

    if (len > 0) {
        parametros['Imagen0[direccion]'] = ar[0].title;
    }
    if (len > 1) {
        parametros['Imagen1[direccion]'] = ar[1].title;
    }
    if (len > 2) {
        parametros['Imagen2[direccion]'] = ar[2].title;
    }

    $.ajax({
        data: parametros,
        url: 'indexyii.php/cliente/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            if (response != 'error') {

                alert('Guardado');
                listarCliente();
            } else {
                alert('Ocurrio un error al guardar los datos.');
            }
        }
    });
    return false;
}

function cargarCliente(id) {
    var parametros = {
        "modo": "cargar",
        "id": id
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/cliente/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            document.getElementById('content').innerHTML = response;
            script_validarform();
            // Ejecucion de Parsley  para validar el formulario, si es valido se ejecuta el metodo "modificarcliente"
            $('#formulario').parsley();
            $(document).ready(function() {
                $('#formulario').parsley().subscribe('parsley:form:validate', function(formInstance) {
                    if (formInstance.isValid('block1', true)) {
                        modificarCliente(id);
                        formInstance.submitEvent.preventDefault();
                        return;
                    }
                    // else stop form submission
                    formInstance.submitEvent.preventDefault();
                });
            });
        }
    });
}

function modificarCliente(id) {

    var codigo = document.getElementById('codigo').value;
    var nombre = document.getElementById('nombre').value;
    var apellidos = document.getElementById('apellidos').value;
    var direccion = document.getElementById('direccion').value;
    var sexo = document.getElementById('sexo').value;
    var fechanacimiento = document.getElementById('fechanacimiento').value;
    var telefono = document.getElementById('telefono').value;
    var celular = document.getElementById('celular').value;
    var ci = document.getElementById('ci').value;
    var email = document.getElementById('email').value;

    var parametros = {
        "modo": "modificar",
        "id": id,
        "Cliente[codigo]": codigo,
        "Cliente[nombre]": nombre,
        "Cliente[apellidos]": apellidos,
        "Cliente[direccion]": direccion,
        "Cliente[sexo]": sexo,
        "Cliente[fechanacimiento]": fechanacimiento,
        "Cliente[telefono]": telefono,
        "Cliente[celular]": celular,
        "Cliente[ci]": ci,
        "Cliente[email]": email
    };
    $.ajax({
        data: parametros,
        url: 'indexyii.php/cliente/ajax',
        type: 'post',
        beforeSend: function() {
            $("#content").html('<div id="page-loader" class="fade in"><span class="spinner"></span></div>');
        },
        success: function(response) {
            alert('Modificado');
            listarCliente();
        }
    });
    return false;
}
/* Application Cliente
 ------------------------------------------------ */
var idN = "0";

var Negocio = function() {
    "use strict";

    return {
        obtenerPK: function() {
            return idN;
        }
    };
}();

var Cliente = function() {
    "use strict";

    return {
        listar: function() {
            listarCliente();
        },
        nuevo: function() {
            nuevoCliente();
        },
        guardar: function() {
            return guardarCliente();
        },
        cargar: function(id) {
            cargarCliente(id);
        },
        modificar: function(id) {
            return modificarCliente(id);
        }
    };
}();