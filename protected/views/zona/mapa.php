<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $zonas  :  Lista de Personales */

function decodePolylineToArray($encoded) {
    $length = strlen($encoded);
    $index = 0;
    $points = array();
    $lat = 0;
    $lng = 0;

    while ($index < $length) {
        $b = 0;
        $shift = 0;
        $result = 0;
        do {
            $b = ord(substr($encoded, $index++)) - 63;
            $result |= ($b & 0x1f) << $shift;
            $shift += 5;
        } while ($b >= 0x20);
        $dlat = (($result & 1) ? ~($result >> 1) : ($result >> 1));
        $lat += $dlat;
        $shift = 0;
        $result = 0;
        do {
            $b = ord(substr($encoded, $index++)) - 63;
            $result |= ($b & 0x1f) << $shift;
            $shift += 5;
        } while ($b >= 0x20);

        $dlng = (($result & 1) ? ~($result >> 1) : ($result >> 1));
        $lng += $dlng;
        $points[] = array($lat * 1e-5, $lng * 1e-5);
    }

    return $points;
}
?>

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->

<link href="../../assets/plugins/DataTables-1.9.4/css/data-table.css" rel="stylesheet" />
<script type="text/javascript" src="../../assets/plugins/DataTables-1.9.4/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../assets/plugins/DataTables-1.9.4/js/data-table.js"></script>
<link href="../../assets/plugins/bootstrap-wizard/css/bwizard.min.css" rel="stylesheet" />
<link href="../../assets/plugins/parsley/parsley.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL STYLE ================== -->
<script type="text/javascript" src="../../assets/plugins/bootstrap-wizard/js/bwizard.js"></script>
<script type="text/javascript" src="../../assets/js/form-wizards.demo.min.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>-->
<script src="../../assets/js/map-google.demo.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>

<script src="../../assets/js/ajax-mapa.js"></script>
<!-- ================== END PAGE LEVEL STYLE ================== -->
<script type="text/javascript">
    function verZona1(poligonoEncode, nombre) {
        Mapa.verZona(poligonoEncode);
        document.getElementById('nombrezona').innerHTML = "Zona: " + nombre;
        return false;
    }
</script>
<div id="content" class="content">

    <h1 class="page-header">Zonas</h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-4">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">Lista de Zonas</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Nro</th>
                                    <th>Nombre</th>
                                    <th>Ver</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($zonas as $zona) { ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $zona->id; ?></td>
                                        <td><?php echo $zona->nombre; ?></td>
                                        <td>
                                            <a href="#" onclick="verZona1('<?php echo $zona->poligono ?>', '<?php echo $zona->nombre ?>')">Ver</a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <div class="col-md-8">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 id="nombrezona" class="panel-title">Zona</h4>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 col-sm-12">
                        <div id="googleMap" class="col-md-12 col-sm-12" style="height: 500px"></div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>

<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->

<script src="../../assets/plugins/jquery-ui-1.10.4/ui/minified/jquery-ui.min.js"></script>
<script src="../../assets/plugins/bootstrap-3.1.1/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="../../assets/crossbrowserjs/html5shiv.js"></script>
        <script src="../../assets/crossbrowserjs/respond.min.js"></script>
        <script src="../../assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="../../assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="../../assets/plugins/gritter/js/jquery.gritter.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.time.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="../../assets/plugins/sparkline/jquery.sparkline.js"></script>
<script src="../../assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../../assets/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="../../assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../../assets/js/dashboard.min.js"></script>
<script src="../../assets/js/form-plugins.demo.js"></script>
<script src="../../assets/js/apps.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=geometry&sensor=false"></script>

<!-- ================== END PAGE LEVEL JS ================== -->
<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<!--            <script src="../../assets/plugins/isotope/jquery.isotope.min.js"></script>
<script src="../../assets/plugins/lightbox/js/lightbox-2.6.min.js"></script>
<script src="../../assets/js/gallery.demo.min.js"></script>-->
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function() {
        App.init();
        Dashboard.init();
        FormWizard.init();
        Mapa.mostrarMapa();

    });
</script>
</body>
</html>
