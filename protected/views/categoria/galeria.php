<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $personales  :  Lista de Personales */
$PATH = "../../assets/upload/server/php/files/";
?>

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->

<script type="text/javascript" src="../../assets/js/ajax-categoria.js"></script>
<link href="../../assets/plugins/DataTables-1.9.4/css/data-table.css" rel="stylesheet" />
<script type="text/javascript" src="../../assets/js/map-google.demo.js"></script>
<script type="text/javascript" src="../../assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<!-- ================== END PAGE LEVEL STYLE ================== -->
<div id="content" class="content">

    <h1 class="page-header">Galeria de Productos</h1>

    <div id="options" class="m-b-10">
        <span class="gallery-option-set" id="filter" data-option-key="filter">
            <a href="#show-all" class="btn btn-default btn-xs active" data-option-value="*">
                Todo
            </a>
            <?php
            foreach ($categorias as $cat) {
                ?>
                <a href="#gallery-group-<?php echo $cat->id; ?>" class="btn btn-default btn-xs" data-option-value=".gallery-group-<?php echo $cat->id; ?>">
                    <?php echo $cat->nombre; ?>
                </a>
                <?php
            }
            ?>
        </span>
    </div>
    <div id="gallery" class="gallery">
        <?php
        for ($j = 0; $j < sizeof($productos); $j++) {
            ?>
            <div class="image gallery-group-<?php echo $productos[$j]->id_categoria; ?>">
                <div style="background-color: white" class="image-inner">
                    <center>
                        <img style="width: inherit" src="<?php echo $PATH . $imagenes[$j]; ?>" alt="" />
                    </center>
                    <p class="image-caption">
                        Codigo: <?php echo $productos[$j]->codigo; ?>
                    </p>
                </div>
                <div class="image-info">
                    <h5 class="title"><?php echo $productos[$j]->nombre; ?></h5>
                    <div class="desc">
                        <?php echo $productos[$j]->descripcion; ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<?php
include_once 'protected/views/layouts/pie.php';
?>
<script>
    $(document).ready(function() {
        Gallery.init();
    });
</script>