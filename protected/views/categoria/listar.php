<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $personales  :  Lista de Personales */
$PATH = "../../assets/upload/server/php/files/";
?>

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->

<script type="text/javascript" src="../../assets/js/ajax-categoria.js"></script>
<link href="../../assets/plugins/DataTables-1.9.4/css/data-table.css" rel="stylesheet" />
<script type="text/javascript" src="../../assets/js/map-google.demo.js"></script>
<script type="text/javascript" src="../../assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="../../assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- ================== END PAGE LEVEL STYLE ================== -->
<div id="content" class="content">
    <?php
    /* @var $categoria  :  Lista de Categorias */
    $tam = sizeof($categorias);
    if ($tam % 2 == 0) {
        $j = $tam / 2;
    } else {
        $j = (int) (($tam / 2) + 1);
    }
    ?>
    <!-- begin page-header -->
    <h1 class="page-header">CATEGORIAS</h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="col-md-12">
<!--        <div class="input-group m-b-20">
            <input type="text" class="form-control input-white" placeholder="Enter keywords here..." />
            <div class="input-group-btn">
                <button type="button" class="btn btn-inverse"><i class="fa fa-search"></i> Search</button>
            </div>
        </div>-->
        <div>
            <a href="../../indexyii.php/categoria/ajax?modo=nuevo" ><button type="button" class="btn btn-warning m-r-5 m-b-5" >Añadir Nueva Categoria</button></a>
        </div>
    </div>
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12 result-container">
            <div class="col-md-6">
                <ul class="result-list">
                    <br>
                    <?php
                    for ($i = 0; $i < $j; $i++) {
                        ?>
                        <li style="height: 180px">
                            <div style="background-color: white" class="result-image">
                                <center>
                                    <img style="height: 180px; width: inherit" src="<?php echo $PATH . $imagenes[$i]; ?>" alt="" />
                                </center>
                            </div>
                            <div class="result-info">
                                <h4 class="title"><a><?php echo $categorias[$i]->nombre; ?></a></h4>
                                <p class="desc">
                                    <?php echo $categorias[$i]->descripcion; ?>
                                </p>
                                <div class="btn-row">
                                    <a href="../../indexyii.php/categoria/ajax?modo=cargar&id=<?php echo $categorias[$i]->getAttribute('id'); ?>" data-toggle="tooltip" data-container="body" data-title="Editar"><i class="fa fa-fw fa-pencil fa-lg"></i></a>
                                    <a href="../../indexyii.php/producto/ajax?modo=nuevo&idcategoria=<?php echo $categorias[$i]->getAttribute('id'); ?>" data-toggle="tooltip" data-container="body" data-title="Añadir Productos"><i class="fa fa-fw fa-plus-circle fa-lg"></i></a>
                                    <a href="../../indexyii.php/producto/ajax?modo=listar&id_categoria=<?php echo $categorias[$i]->getAttribute('id'); ?>" data-toggle="tooltip" data-container="body" data-title="Lista de Productos"><i class="fa fa-fw fa-list-ol fa-lg"></i></a>
                                </div>
                            </div>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="result-list">
                    <br>
                    <?php
                    for ($i = $j; $i < $tam; $i++) {
                        ?>
                        <li style="height: 180px">
                            <div style="background-color: white" class="result-image">
                                <center>
                                    <img style="height: 180px; width: inherit" src="<?php echo $PATH . $imagenes[$i]; ?>" alt="" />
                                </center>
                            </div>
                            <div class="result-info">
                                <h4 class="title"><a><?php echo $categorias[$i]->nombre; ?></a></h4>
                                <p class="desc">
                                    <?php echo $categorias[$i]->descripcion; ?>
                                </p>
                                <div class="btn-row">
                                    <a href="../../indexyii.php/categoria/ajax?modo=cargar&id=<?php echo $categorias[$i]->getAttribute('id'); ?>" data-toggle="tooltip" data-container="body" data-title="Editar"><i class="fa fa-fw fa-pencil fa-lg"></i></a>
                                    <a href="../../indexyii.php/producto/ajax?modo=nuevo&idcategoria=<?php echo $categorias[$i]->getAttribute('id'); ?>;" data-toggle="tooltip" data-container="body" data-title="Añadir Productos"><i class="fa fa-fw fa-plus-circle fa-lg"></i></a>
                                    <a href="../../indexyii.php/producto/ajax?modo=listar&id_categoria=<?php echo $categorias[$i]->getAttribute('id'); ?>" data-toggle="tooltip" data-container="body" data-title="Lista de Productos"><i class="fa fa-fw fa-list-ol fa-lg"></i></a>
                                </div>
                            </div>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <!-- end col-12 -->
    </div>

</div>
<?php
include_once 'protected/views/layouts/pie.php';
?>

<script>
    $(document).ready(function() {
        handelTooltipPopoverActivation();
    });
</script>