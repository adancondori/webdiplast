<?php
/* @var $this OfertamovilController */
/* @var $data Ofertamovil */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_movil')); ?>:</b>
	<?php echo CHtml::encode($data->id_movil); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_oferta')); ?>:</b>
	<?php echo CHtml::encode($data->id_oferta); ?>
	<br />


</div>