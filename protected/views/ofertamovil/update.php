<?php
/* @var $this OfertamovilController */
/* @var $model Ofertamovil */

$this->breadcrumbs=array(
	'Ofertamovils'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Ofertamovil', 'url'=>array('index')),
	array('label'=>'Create Ofertamovil', 'url'=>array('create')),
	array('label'=>'View Ofertamovil', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Ofertamovil', 'url'=>array('admin')),
);
?>

<h1>Update Ofertamovil <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>