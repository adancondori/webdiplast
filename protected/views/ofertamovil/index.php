<?php
/* @var $this OfertamovilController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ofertamovils',
);

$this->menu=array(
	array('label'=>'Create Ofertamovil', 'url'=>array('create')),
	array('label'=>'Manage Ofertamovil', 'url'=>array('admin')),
);
?>

<h1>Ofertamovils</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
