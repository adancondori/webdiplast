<?php
/* @var $this OfertamovilController */
/* @var $model Ofertamovil */

$this->breadcrumbs=array(
	'Ofertamovils'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Ofertamovil', 'url'=>array('index')),
	array('label'=>'Manage Ofertamovil', 'url'=>array('admin')),
);
?>

<h1>Create Ofertamovil</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>