<?php
/* @var $this UsuarioplaystoreController */
/* @var $model Usuarioplaystore */

$this->breadcrumbs=array(
	'Usuarioplaystores'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Usuarioplaystore', 'url'=>array('index')),
	array('label'=>'Manage Usuarioplaystore', 'url'=>array('admin')),
);
?>

<h1>Create Usuarioplaystore</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>