<?php
/* @var $this UsuarioplaystoreController */
/* @var $model Usuarioplaystore */

$this->breadcrumbs=array(
	'Usuarioplaystores'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Usuarioplaystore', 'url'=>array('index')),
	array('label'=>'Create Usuarioplaystore', 'url'=>array('create')),
	array('label'=>'View Usuarioplaystore', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Usuarioplaystore', 'url'=>array('admin')),
);
?>

<h1>Update Usuarioplaystore <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>