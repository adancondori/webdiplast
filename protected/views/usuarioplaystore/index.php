<?php
/* @var $this UsuarioplaystoreController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Usuarioplaystores',
);

$this->menu=array(
	array('label'=>'Create Usuarioplaystore', 'url'=>array('create')),
	array('label'=>'Manage Usuarioplaystore', 'url'=>array('admin')),
);
?>

<h1>Usuarioplaystores</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
