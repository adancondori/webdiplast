<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $personales  :  Lista de Personales */
?>

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="assets/plugins/parsley/parsley.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL STYLE ================== -->
<script>
    $(function() {
        $("#fechanacimiento").datepicker("option", "dateFormat", "yy-mm-dd");
    });
</script>
<div id="content" class="content">

    <h1 class="page-header">PERSONAL</h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">Todos los campos con (*) son requeridos</h4>
                </div>
                <div class="panel-body panel-form">
                    <form id="formulario" class="form-horizontal form-bordered" action="../../indexyii.php/personal/ajax?modo=guardar" method="POST" >
                        <input type="text" hidden="hidden" name="id" value="<?php echo $personal->getAttribute('id'); ?>" >
                        <input type="text" hidden="hidden" name="Usuario[id]" value="<?php echo $usuario->getAttribute('id'); ?>" >
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Nombre * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="nombre" name="Personal[nombre]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $personal->getAttribute('nombre'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Apellidos * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="apellidos" name="Personal[apellidos]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $personal->getAttribute('apellidos'); ?>">
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">Sexo * :</label>
                            <div class="col-md-6 col-sm-6">
                                <select class="form-control parsley-validated" id="sexo" name="Personal[sexo]" required="true" data-parsley-group="block1" >
                                    <option value="">Please choose</option>
                                    <option value="H" 
                                    <?php if ($personal->sexo == 'H') echo 'selected'; ?>
                                            >Hombre</option>
                                    <option value="M"
                                    <?php if ($personal->sexo == 'M') echo 'selected'; ?>
                                            >Mujer</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fechanacimiento">Fecha de Nacimiento :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" data-date-format="yyyy-mm-dd" id="fechanacimiento" name="Personal[fechanacimiento]" placeholder="XXXX-XX-XX" required="true" data-parsley-group="block1" value="<?php echo $personal->fechanacimiento; ?>">
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Dirección * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="direccion" name="Personal[direccion]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $personal->getAttribute('direccion'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="message">Teléfono :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="telefono" placeholder="(XXX) XXXX XXX" name="Personal[telefono]" required="true" data-parsley-group="block1" value="<?php echo $personal->getAttribute('telefono'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">Cargo * :</label>
                            <div class="col-md-6 col-sm-6">
                                <select class="form-control parsley-validated" id="cargo" name="Personal[cargo]" name="cargo" required="true" data-parsley-group="block1" >
                                    <option value="">Please choose</option>
                                    <option value="Administrador" 
                                    <?php if ($personal->getAttribute('cargo') == 'Administrador') echo 'selected'; ?>
                                            >Administrador</option>
                                    <option value="Usuario"
                                    <?php if ($personal->getAttribute('cargo') == 'Usuario') echo 'selected'; ?>
                                            >Usuario</option>
                                    <option value="Promotor"
                                    <?php if ($personal->getAttribute('cargo') == 'Promotor') echo 'selected'; ?>
                                            >Promotor</option>
                                    <option value="Entregador"
                                    <?php if ($personal->getAttribute('cargo') == 'Entregador') echo 'selected'; ?>
                                            >Entregador</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">CI :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="ci" name="Personal[ci]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $personal->getAttribute('ci'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="cuentausuario"> <b>Cuenta de Usuario</b></label> 
                        </div>    
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="email">Email * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="email" name="Usuario[email]" data-trigger="change" required="true" placeholder="email" data-parsley-group="block1" value="<?php echo $usuario->getAttribute('email'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="email">Usuario * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="usuario" name="Usuario[usuario]" data-trigger="change" required="true" placeholder="usuario" data-parsley-group="block1" value="<?php echo $usuario->getAttribute('usuario'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="email">Contraseña * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="contrasena" name="Usuario[contrasena]" data-trigger="change" required="true"  placeholder="contraseña" data-parsley-group="block1" value="<?php echo $usuario->getAttribute('contrasena'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="email">Repita Contraseña * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="contrasena1" name="Usuario[contrasena2]" data-trigger="change" required="true" placeholder="repita contraseña" data-parsley-group="block1" value="<?php echo $usuario->getAttribute('contrasena'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4"></label>
                            <div class="col-md-6 col-sm-6">
                                <button class="btn btn-primary">Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
    </div>
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
<?php
include_once 'protected/views/layouts/pie.php';
?>