<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $zonas  :  Lista de Personales */

function decodePolylineToArray($encoded) {
    $length = strlen($encoded);
    $index = 0;
    $points = array();
    $lat = 0;
    $lng = 0;

    while ($index < $length) {
        $b = 0;
        $shift = 0;
        $result = 0;
        do {
            $b = ord(substr($encoded, $index++)) - 63;
            $result |= ($b & 0x1f) << $shift;
            $shift += 5;
        } while ($b >= 0x20);
        $dlat = (($result & 1) ? ~($result >> 1) : ($result >> 1));
        $lat += $dlat;
        $shift = 0;
        $result = 0;
        do {
            $b = ord(substr($encoded, $index++)) - 63;
            $result |= ($b & 0x1f) << $shift;
            $shift += 5;
        } while ($b >= 0x20);

        $dlng = (($result & 1) ? ~($result >> 1) : ($result >> 1));
        $lng += $dlng;
        $points[] = array($lat * 1e-5, $lng * 1e-5);
    }

    return $points;
}
?>
<link href="../../assets/plugins/DataTables-1.9.4/css/data-table.css" rel="stylesheet" />
<script type="text/javascript" src="../../assets/plugins/DataTables-1.9.4/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../assets/plugins/DataTables-1.9.4/js/data-table.js"></script>
<link href="../../assets/plugins/bootstrap-wizard/css/bwizard.min.css" rel="stylesheet" />
<link href="../../assets/plugins/parsley/parsley.css" rel="stylesheet" />
<script type="text/javascript" src="../../assets/plugins/bootstrap-wizard/js/bwizard.js"></script>
<script type="text/javascript" src="../../assets/js/form-wizards.demo.min.js"></script>
<script src="../../assets/js/map-google.demo.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>

<script type="text/javascript">
    $(function() {
        $("#fecha").datepicker("option", "dateFormat", "yy-mm-dd");
    });
</script>
<div id="content" class="content">
    <h1 class="page-header">Monitoreo</h1>
    <div class="row">
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 id="nombrezona" class="panel-title">Monitoreo</h4>
                </div>
                <div class="panel-body">
                    <form action="../../indexyii.php/personal/ajax?modo=monitoreo" method="post">
                        <div class="col-md-3 col-sm-3">
                            <select required="true" name="idpersonal" id="idpersonal" class="form-control parsley-validated" >
                                <option value="">Personal</option>
                                <?php
                                foreach ($personales as $personal) {
                                    ?>
                                    <option value="<?php echo $personal->id; ?>"<?php echo isset($_POST['idpersonal']) && $_POST['idpersonal']==$personal->id ? 'selected' : '' ?>>
                                        <?php echo $personal->nombre . ' ' . $personal->apellidos; ?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control parsley-validated" data-date-format="yyyy-mm-dd" id="fecha" name="fecha" placeholder="Fecha" required="true" data-parsley-group="block1" value="<?php if (isset($_POST['fecha'])) echo $_POST['fecha']; ?>">
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control parsley-validated btn-success" type="submit" value="Buscar">
                        </div>
                    </form>
                    <br><br>
                    <div class="col-md-12 col-sm-12">
                        <div id="map" class="col-md-12 col-sm-12" style="height: 500px"></div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
    </div>
</div>

<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<script src="../../assets/plugins/jquery-ui-1.10.4/ui/minified/jquery-ui.min.js"></script>
<script src="../../assets/plugins/bootstrap-3.1.1/js/bootstrap.min.js"></script>
<script src="../../assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="../../assets/plugins/gritter/js/jquery.gritter.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.time.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="../../assets/plugins/sparkline/jquery.sparkline.js"></script>
<script src="../../assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../../assets/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="../../assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../../assets/js/dashboard.min.js"></script>
<script src="../../assets/js/form-plugins.demo.js"></script>
<script src="../../assets/js/apps.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?libraries=geometry&sensor=false"></script>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="../../assets/js/ajax-tracking.js"></script>

<script>
    $(document).ready(function() {
        App.init();
        Dashboard.init();
        FormWizard.init();
    });
    jQuery(function() {
        var stops = [
            <?php
            foreach ($posiciones as $posicion) {
            ?>
            {"Geometry": {"Latitude": <?php echo $posicion->latitud; ?>, "Longitude": <?php echo $posicion->longitud; ?>, "time": "<?php echo yii::app()->locale->dateFormatter->format("HH:MM", $posicion->fechahora); ?>"}},
            <?php
            }
            ?>
        ];

        var map = new window.google.maps.Map(document.getElementById("map"));

        // new up complex objects before passing them around
        var directionsDisplay = new window.google.maps.DirectionsRenderer({suppressMarkers: true});
        var directionsService = new window.google.maps.DirectionsService();

        Tour_startUp(stops);

        window.tour.loadMap(map, directionsDisplay);
        window.tour.fitBounds(map);

        if (stops.length > 1)
            window.tour.calcRoute(directionsService, directionsDisplay);
    });
</script>
</body>
</html>
