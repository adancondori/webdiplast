<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $oferta  :  Oferta */
?>

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="../../assets/plugins/parsley/parsley.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL STYLE ================== -->
<link href="../../assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
<link href="../../assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" />
<link href="../../assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet" />
<link href="../../assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" />

<script type="text/javascript">
    function guardarEnviar() {
        var form = document.getElementById("formulario");
        form.action = "../../indexyii.php/oferta/ajax?modo=guardarenviar";
        return true;
    }
</script>
<div id="content" class="content">

    <h1 class="page-header">OFERTA</h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">Nueva Oferta</h4>
                </div>
                <div class="panel-body panel-form">
                    <form id="formulario" class="form-horizontal form-bordered" action="../../indexyii.php/oferta/ajax?modo=guardar" method="POST" >
                        <input type="text" hidden="hidden" name="id" value="<?php echo $oferta->getAttribute('id'); ?>" >
                        <input type="text" hidden="hidden" name="Oferta[id]" value="<?php echo $oferta->getAttribute('id'); ?>" >
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Nombre * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="nombre" name="Oferta[nombre]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $oferta->getAttribute('nombre'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Descripción * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="descripcion" name="Oferta[descripcion]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $oferta->getAttribute('descripcion'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Fecha * :</label>
                            <div class="col-md-6 col-sm-6">
                                <div class="input-group date" id="datepicker-disabled-past"  data-date-format="yyyy-mm-dd" data-date-start-date="Date.default">
                                    <input type="text" class="form-control" placeholder="Select Date" id="fecha" name="Oferta[fecha]" required="true" data-parsley-group="block1" value="<?php echo $oferta->getAttribute('fecha'); ?>"/>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">ID Producto :</label>
                            <div class="col-md-6 col-sm-6">
                                <select class="form-control parsley-validated" id="id_producto" name="Oferta[id_producto]" data-parsley-group="block1" >
                                    <option value="">Please choose</option>
                                    <?php
                                    for ($i = 0; $i < sizeof($productos); $i++) {
                                        echo '<option value="' . $productos[$i]->id . '">' . $productos[$i]->nombre . '</nombre>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4"></label>
                            <div class="col-md-6 col-sm-6">
                                <button class="btn btn-primary" >Guardar</button>
                                <button class="btn btn-primary" onclick="guardarEnviar()">Guardar y Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
    </div>
</div>


<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="../../assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../../assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function() {

        handleDatepicker();
        handlePanelAction();
    });
</script>
<?php
include_once 'protected/views/layouts/pie.php';
?>
<script>
    $(document).ready(function() {

    });
</script>