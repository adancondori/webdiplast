<?php
/* @var $this TrazoController */
/* @var $model Trazo */

$this->breadcrumbs=array(
	'Trazos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Trazo', 'url'=>array('index')),
	array('label'=>'Create Trazo', 'url'=>array('create')),
	array('label'=>'Update Trazo', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Trazo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Trazo', 'url'=>array('admin')),
);
?>

<h1>View Trazo #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'latitud',
		'longitud',
		'indice',
		'id_zona',
	),
)); ?>
