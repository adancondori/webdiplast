<?php
/* @var $this TrazoController */
/* @var $model Trazo */

$this->breadcrumbs=array(
	'Trazos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Trazo', 'url'=>array('index')),
	array('label'=>'Create Trazo', 'url'=>array('create')),
	array('label'=>'View Trazo', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Trazo', 'url'=>array('admin')),
);
?>

<h1>Update Trazo <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>