<?php
/* @var $this TrazoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Trazos',
);

$this->menu=array(
	array('label'=>'Create Trazo', 'url'=>array('create')),
	array('label'=>'Manage Trazo', 'url'=>array('admin')),
);
?>

<h1>Trazos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
