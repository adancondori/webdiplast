<?php
/* @var $this TrazoController */
/* @var $model Trazo */

$this->breadcrumbs=array(
	'Trazos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Trazo', 'url'=>array('index')),
	array('label'=>'Manage Trazo', 'url'=>array('admin')),
);
?>

<h1>Create Trazo</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>