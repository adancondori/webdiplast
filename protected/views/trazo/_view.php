<?php
/* @var $this TrazoController */
/* @var $data Trazo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('latitud')); ?>:</b>
	<?php echo CHtml::encode($data->latitud); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('longitud')); ?>:</b>
	<?php echo CHtml::encode($data->longitud); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('indice')); ?>:</b>
	<?php echo CHtml::encode($data->indice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_zona')); ?>:</b>
	<?php echo CHtml::encode($data->id_zona); ?>
	<br />


</div>