<?php
/* @var $this NegocioController */
/* @var $model Negocio */

$this->breadcrumbs=array(
	'Negocios'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Negocio', 'url'=>array('index')),
	array('label'=>'Create Negocio', 'url'=>array('create')),
	array('label'=>'View Negocio', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Negocio', 'url'=>array('admin')),
);
?>

<h1>Update Negocio <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>