<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $personales  :  Lista de Personales */

$PATH = "../../assets/upload/server/php/files/";
?>

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->

<link href="../../assets/plugins/bootstrap-wizard/css/bwizard.min.css" rel="stylesheet" />
<link href="../../assets/plugins/parsley/parsley.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL STYLE ================== -->
<script type="text/javascript" src="../../assets/plugins/bootstrap-wizard/js/bwizard.js"></script>
<script type="text/javascript" src="../../assets/js/form-wizards.demo.min.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>-->
<script src="../../assets/js/map-google.demo.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>
<script src="../../assets/js/ajax-negocio.js"></script>
<!-- ================== END PAGE LEVEL STYLE ================== -->
<div id="content" class="content">

    <h1 class="page-header">Negocio</h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Todos los campos (*) son requeridos</h4>
                </div>
                <div class="panel-body">
                    <form id="formulario" method="POST" action="../../indexyii.php/negocio/ajax?modo=modificar">
                        <div id="wizard">
                            <fieldset>
                                <legend class="pull-left width-full">Datos del Negocio</legend>
                                <!-- begin row -->
                                <input hidden="hidden" name="id" value="<?php echo $negocio->id; ?>">
                                <div class="row form-horizontal form-bordered">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4" for="nombre">Nombre * :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control parsley-validated" type="text" id="nombre1" name="Negocio[nombre]" data-trigger="change" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $negocio->nombre; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4" for="razonsocial">Razón Social * :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control parsley-validated" type="text" id="razonsocial" name="Negocio[razonsocial]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $negocio->razonsocial; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4" for="nit">NIT * :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control parsley-validated" type="text" id="nit" name="Negocio[nit]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $negocio->nit; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4" for="direccion">Dirección * :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control parsley-validated" type="text" id="direccion1" name="Negocio[direccion]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $negocio->direccion; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4" for="observacion">Observación :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control parsley-validated" type="text" id="observacion" name="Negocio[observacion]" data-parsley-group="block1" value="<?php echo $negocio->observacion; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4">Zona * :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <select class="form-control parsley-validated" id="zona" name="Negocio[id_zona]" required="true" data-parsley-group="block1" >
                                                <option value="">Please choose</option>
                                                <?php
                                                for ($i = 0; $i < sizeof($zonas); $i++) {
                                                    if ($zonas[$i]->id == $negocio->id_zona) {
                                                        echo '<option value="' . $zonas[$i]->id . '" selected>' . $zonas[$i]->nombre . '</nombre>';
                                                    } else {
                                                        echo '<option value="' . $zonas[$i]->id . '">' . $zonas[$i]->nombre . '</nombre>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4" for="localizacion">Localización :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control parsley-validated" type="text" id="localizacion" name="localizacion" required="true" readonly="readonly" data-parsley-group="block1" value="<?php echo $negocio->latitud . "; " . $negocio->longitud; ?>">
                                            <div id="googleMap" class="col-md-12 col-sm-12" style="height: 300px"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4" for="localizacion">Fotos :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <?php
                                            foreach ($imagenes as $img) {
                                                ?>
                                                <img width="150" height="120" src="<?php echo $PATH . $img->direccion; ?>">&nbsp;
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4"></label>
                                        <div class="col-md-6 col-sm-6">
                                            <button class="btn btn-primary" >Guardar Cambios</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>
                            <!-- end wizard step-4 -->
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>

<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->

<script src="../../assets/plugins/jquery-ui-1.10.4/ui/minified/jquery-ui.min.js"></script>
<script src="../../assets/plugins/bootstrap-3.1.1/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="../../assets/crossbrowserjs/html5shiv.js"></script>
        <script src="../../assets/crossbrowserjs/respond.min.js"></script>
        <script src="../../assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="../../assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="../../assets/plugins/gritter/js/jquery.gritter.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.time.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="../../assets/plugins/sparkline/jquery.sparkline.js"></script>
<script src="../../assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../../assets/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="../../assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../../assets/js/dashboard.min.js"></script>
<script src="../../assets/js/form-plugins.demo.js"></script>
<script src="../../assets/js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->
<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<!--            <script src="../../assets/plugins/isotope/jquery.isotope.min.js"></script>
<script src="../../assets/plugins/lightbox/js/lightbox-2.6.min.js"></script>
<script src="../../assets/js/gallery.demo.min.js"></script>-->
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function() {
        App.init();
        Dashboard.init();
        FormWizard.init();
        Negocio.iniciarMapa();

<?php
if (isset($negocio)) {
    echo "Negocio.setLatLong2(" . $negocio->latitud . "," . $negocio->longitud . ");";
}
?>
    });
</script>
</body>
</html>
