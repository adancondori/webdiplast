<?php
/* @var $this NegocioController */
/* @var $model Negocio */

$this->breadcrumbs=array(
	'Negocios'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Negocio', 'url'=>array('index')),
	array('label'=>'Create Negocio', 'url'=>array('create')),
	array('label'=>'Update Negocio', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Negocio', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Negocio', 'url'=>array('admin')),
);
?>

<h1>View Negocio #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'uuid',
		'nombre',
		'razonsocial',
		'nit',
		'direccion',
		'latitud',
		'longitud',
		'observacion',
		'id_cliente',
		'id_zona',
		'fechacreacion',
		'fechamodificacion',
		'estado',
	),
)); ?>
