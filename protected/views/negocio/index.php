<?php
/* @var $this NegocioController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Negocios',
);

$this->menu=array(
	array('label'=>'Create Negocio', 'url'=>array('create')),
	array('label'=>'Manage Negocio', 'url'=>array('admin')),
);
?>

<h1>Negocios</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
