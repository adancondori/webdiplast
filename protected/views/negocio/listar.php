<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $personales  :  Lista de Personales */
?>

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->

<!--<script type="text/javascript" src="../../assets/js/ajax-personal.js"></script>-->
<link href="../../assets/plugins/DataTables-1.9.4/css/data-table.css" rel="stylesheet" />
<script type="text/javascript" src="../../assets/plugins/DataTables-1.9.4/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../assets/plugins/DataTables-1.9.4/js/data-table.js"></script>
<!-- begin breadcrumb -->
<div id="content" class="content">

    <h1 class="page-header">NEGOCIOS</h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->

            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">Lista de Negocios</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Razón Social</th>
                                    <th>NIT</th>
                                    <th>Dirección</th>
                                    <th>Cliente</th>
                                    <th>Zona</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($negocios); $i++) {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $negocios[$i]->nombre; ?></td>
                                        <td><?php echo $negocios[$i]->razonsocial; ?></td>
                                        <td><?php echo $negocios[$i]->nit; ?></td>
                                        <td><?php echo $negocios[$i]->direccion; ?></td>
                                        <td><?php echo $negocios[$i]->idCliente->nombre." ".$negocios[$i]->idCliente->apellidos; ?></td>
                                        <td><?php echo $negocios[$i]->idZona->nombre; ?></td>
                                        <td>
                                            <a href="../../indexyii.php/negocio/ajax?modo=cargar&id=<?php echo $negocios[$i]->id; ?>">
                                                Editar</a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
</div>
<?php
include_once 'protected/views/layouts/pie.php';
?>
