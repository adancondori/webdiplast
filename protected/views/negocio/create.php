<?php
/* @var $this NegocioController */
/* @var $model Negocio */

$this->breadcrumbs=array(
	'Negocios'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Negocio', 'url'=>array('index')),
	array('label'=>'Manage Negocio', 'url'=>array('admin')),
);
?>

<h1>Create Negocio</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>