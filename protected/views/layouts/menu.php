<div id="sidebar" class="sidebar">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        <ul class="nav">
            <li class="nav-profile">
                <div class="btn-group-lg">
                    <i class="fa fa-user"></i> 
                    <?php echo $_SESSION['usuario'] ?>
                </div>
            </li>
        </ul>
        <!-- end sidebar user -->
        <!-- begin sidebar nav -->
        <ul class="nav">
            <li class="nav-header">Men&uacute;</li>
            <li class="active">
                <a href="../../indexyii.php/categoria/ajax?modo=listar"><i class="fa fa-laptop"></i> <span>Inicio</span></a>
            </li>
            <li class="has-sub">
                <a href="javascript:;">
                    <i class="fa fa-suitcase"></i> 
                    <b class="caret pull-right"></b>
                    <span>ADM Productos</span> 
                </a>
                <ul class="sub-menu">
                    <li><a href="../../indexyii.php/categoria/ajax?modo=listar">Categorias</a></li>
                    <li><a href="../../indexyii.php/categoria/ajax?modo=galeria">Galeria</a></li>
                    <li><a href="../../indexyii.php/producto/ajax?modo=listar">Productos</a></li>
                </ul>
            </li>
            <li class="has-sub">
                <a href="javascript:;">
                    <i class="fa fa-file-o"></i> 
                    <b class="caret pull-right"></b>
                    <span>ADM Clientes</span> 
                </a>
                <ul class="sub-menu">
                    <li><a href="../../indexyii.php/cliente/ajax?modo=listar">Clientes</a></li>
                    <li><a href="../../indexyii.php/negocio/ajax?modo=listar">Negocios</a></li>
                </ul>
            </li>
            <li class="has-sub">
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-th"></i> 
                    <span>ADM Zonas</span> 
                </a>
                <ul class="sub-menu">
                    <li><a href="../../indexyii.php/zona/ajax?modo=mapa">Zonas</a></li>
                    <li><a href="../../indexyii.php/asignacionzona/ajax?modo=listar">Asignación de Zonas</a></li>
                </ul>
            </li>
            <li class="has-sub">
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-th"></i> 
                    <span>ADM Personal</span> 
                </a>
                <ul class="sub-menu">
                    <li><a href="../../indexyii.php/personal/ajax?modo=listar">Personales</a></li>
                    <li><a href="../../indexyii.php/personal/ajax?modo=nuevo">Nuevo Personal</a></li>
                    <li><a href="../../indexyii.php/personal/ajax?modo=monitoreo">Seguimiento</a></li>
                </ul>
            </li>
            <li class="has-sub">
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-th"></i> 
                    <span>ADM Ventas</span> 
                </a>
                <ul class="sub-menu">
                    <li><a href="../../indexyii.php/pedido/ajax?modo=listar">Historial de Pedidos</a></li>
                </ul>
            </li>
            <li class="has-sub">
                <a href="javascript:;">
                    <i class="fa fa-envelope"></i>
                    <b class="caret pull-right"></b>
                    <span>
                        ADM Ofertas 
                        <span class="label label-success m-l-5">NEW</span>
                    </span>
                </a>
                <ul class="sub-menu">
                    <li><a href="../../indexyii.php/oferta/ajax?modo=nuevo">Nueva Oferta <i class="fa fa-paper-plane text-success m-l-5"></i></a></li>
                    <li><a href="../../indexyii.php/oferta/ajax?modo=listar">Historial de Ofertas</a></li>
                </ul>
            </li>
            <li class="has-sub">
                <a href="javascript:;">
                    <i class="fa fa-envelope"></i>
                    <b class="caret pull-right"></b>
                    <span>
                        Reportes 
                        <span class="label label-warning m-l-5">NEW</span>
                    </span>
                </a>
                <ul class="sub-menu">
                    <li><a href="../../indexyii.php/reporte/ajax?modo=reporte">Productos <i class="fa fa-paper-plane text-success m-l-5"></i></a></li>
                    <li><a href="../../indexyii.php/reporte/ajax?modo=reporte2">Negocios <i class="fa fa-paper-plane text-success m-l-5"></i></a></li>
                </ul>
            </li>
            <li class="has-sub">
                <a href="javascript:;">
                    <i class="fa fa-envelope"></i>
                    <b class="caret pull-right"></b>
                    <span>
                        ADM Planillas
                    </span>
                </a>
                <ul class="sub-menu">
                    <li><a href="../../indexyii.php/producto/ajax?modo=planilla">Importar / Exportar</a></li>
                </ul>
            </li>
            <!-- begin sidebar minify button -->
            <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
            <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>