<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>Diplast</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

        <!-- ================== BEGIN BASE CSS STYLE ================== -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
        <link href="../../assets/plugins/jquery-ui-1.10.4/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
        <link href="../../assets/plugins/bootstrap-3.1.1/css/bootstrap.min.css" rel="stylesheet" />
        <link href="../../assets/plugins/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" />
        <link href="../../assets/css/animate.min.css" rel="stylesheet" />
        <link href="../../assets/css/style.min.css" rel="stylesheet" />
        <link href="../../assets/css/style-responsive.min.css" rel="stylesheet" />
        <!-- ================== END BASE CSS STYLE ================== -->

        <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
        <link href="../../assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" />
        <link href="../../assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
        <link href="../../assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" />
        <link href="../../assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />	
        <!-- ================== END PAGE LEVEL STYLE ================== -->

        <!-- ================== BEGIN UTIL ================== -->
        <script src="../../assets/plugins/jquery-1.8.2/jquery-1.8.2.min.js"></script>
        <script type="text/javascript" src="../../assets/plugins/parsley/parsley.min.js"></script>
        <!-- ================== END JQUERY ================== -->

        <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
        <link href="../../assets/plugins/isotope/isotope.css" rel="stylesheet" />
        <link href="../../assets/plugins/lightbox/css/lightbox.css" rel="stylesheet" />
        <script src="../../assets/plugins/isotope/jquery.isotope.min.js"></script>
        <script src="../../assets/plugins/lightbox/js/lightbox-2.6.min.js"></script>
        <script src="../../assets/js/gallery.demo.js"></script>
        <!-- ================== END PAGE LEVEL STYLE ================== -->

        <!-- ================== BEGIN DATE ================== -->
        <link href="../../assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
        <script src="../../assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <!-- ================== END DATE ================== -->
        
        <!-- ================== BEGIN GOOGLEMAP ================== -->
<!--        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
        <script src="../../assets/js/map-google.demo.js"></script>-->
        <!--<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>-->
        <!-- ================== END PAGE LEVEL JS ================== -->

    </head>
    <body>
        <script type="text/javascript" >

            function cerrarsesion() {

                var parametros = {
                    "modo": "cerrarsesion"
                };
                $.ajax({
                    data: parametros,
                    url: '../../indexyii.php/usuario/ajax',
                    type: 'post',
                    beforeSend: function() {
                    },
                    success: function(response) {
                        var obj = JSON.parse(response);
                        if (obj['msg'] === 'ok') {
                            location.href = '../../index.php';
                        } else {
                            alert(obj['msg']);
                        }
                    }
                });
            }

        </script>
        <!-- begin #page-loader -->
        <div id="page-loader" class="fade in"><span class="spinner"></span></div>
        <!-- end #page-loader -->

        <!-- begin #page-container -->
        <div id="page-container" class="fade">
            <!-- begin #header -->
            <div id="header" class="header navbar navbar-default navbar-fixed-top">
                <!-- begin container-fluid -->
                <div class="container-fluid">
                    <!-- begin mobile sidebar expand / collapse button -->
                    <div class="navbar-header">
                        <a href="index.php" class="navbar-brand"><span class="navbar-logo"></span> DIPLAST S.R.L.</a>
                        <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- end mobile sidebar expand / collapse button -->

                    <!-- begin header navigation right -->
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown navbar-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="../../assets/img/user-11.jpg" alt="" /> 
                                <span class="hidden-xs"><?php echo $_SESSION['usuario'] ?></span> <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu animated fadeInLeft">
                                <li class="divider"></li>
                                <li><a href="javascript: cerrarsesion();">Cerrar Sesi&oacute;n</a></li>
                            </ul>
                        </li>
                    </ul>
                    <!-- end header navigation right -->
                </div>
                <!-- end container-fluid -->
            </div>
            <!-- end #header -->