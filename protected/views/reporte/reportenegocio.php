<?php
//include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $personales  :  Lista de Personales */
?>

<!-- begin #content -->
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Inicio</a></li>
        <li class="active">Reporte</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Negocios con más pedidos </h1>
    <!-- end page-header -->
    <!-- begin row -->
    <div class="row">
        <div class="col-md-1"></div>
        <!-- begin col-4 -->
        <div class="col-md-10">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">Ordenado de mayor a menor</h4>
                </div>
                <div class="panel-body">
                    <div id="bar-chart" class="height-sm"></div>
                </div>
            </div>
        </div>
        <!-- end col-4 -->
        <div class="col-md-1"></div>
    </div>
    
    <h1 class="page-header">Negocios con monto de pedidos más elevados </h1>
    <!-- end page-header -->
    <!-- begin row -->
    <div class="row">
        <div class="col-md-1"></div>
        <!-- begin col-4 -->
        <div class="col-md-10">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">Ordenado de mayor a menor</h4>
                </div>
                <div class="panel-body">
                    <div id="bar-chart2" class="height-sm"></div>
                </div>
            </div>
        </div>
        <!-- end col-4 -->
        <div class="col-md-1"></div>
    </div>
    <!-- end row -->
</div>
<!-- end #content -->

<!-- ================== END PAGE LEVEL JS ================== -->
<!-- ================== BEGIN BASE JS ================== -->
<script src="../../assets/plugins/jquery-1.8.2/jquery-1.8.2.min.js"></script>
<script src="../../assets/plugins/jquery-ui-1.10.4/ui/minified/jquery-ui.min.js"></script>
<script src="../../assets/plugins/bootstrap-3.1.1/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="assets/crossbrowserjs/html5shiv.js"></script>
        <script src="assets/crossbrowserjs/respond.min.js"></script>
        <script src="assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="../../assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="../../assets/plugins/flot/jquery.flot.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.time.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.stack.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.crosshair.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.categories.js"></script>
<script src="../../assets/js/charts.demo.js"></script>
<script src="../../assets/js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function() {
        App.init();
        //Chart.init();
        reporteProductosMasVendidos();
        reporteProductosMasAlto();
    });
    function reporteProductosMasVendidos() {
        var parametros = {
            "otro": ""
        };
        $.ajax({
            data: parametros,
            url: 'reportenegociomaspedidos',
            type: 'post',
            beforeSend: function() {

            },
            success: function(response) {
                //alert(response);
                handleBarChart(response);
            }
        });
    }
    function reporteProductosMasAlto() {
        var parametros = {
            "otro": ""
        };
        $.ajax({
            data: parametros,
            url: 'reportenegociomasalto',
            type: 'post',
            beforeSend: function() {

            },
            success: function(response) {
                //alert(response);
                handleBarChart2(response);
            }
        });
    }
    function handleBarChart(message) {

        if ($('#bar-chart').length !== 0) {
            var jsonData = JSON.parse(message);
            //alert(jsonData);
            var data = new Array;
            for (var i = 0; i < jsonData.length; i++) {
                var counter = jsonData[i];
                data[i] = [counter.nombre, counter.cantidad];
            }
            //var data = [["January", 10], ["February", 48], ["March", 4], ["April", 13], ["May", 17], ["June", 9]];
            $.plot("#bar-chart", [{data: data, color: purple}], {
                series: {
                    bars: {
                        show: true,
                        barWidth: 0.4,
                        align: 'center',
                        fill: true,
                        fillColor: purple,
                        zero: true
                    }
                },
                xaxis: {
                    mode: "categories",
                    tickColor: '#ddd',
                    tickLength: 0
                },
                grid: {
                    borderWidth: 0
                }
            });
        }
    }
    function handleBarChart2(message) {

        if ($('#bar-chart2').length !== 0) {
            var jsonData = JSON.parse(message);
            //alert(jsonData);
            var data = new Array;
            for (var i = 0; i < jsonData.length; i++) {
                var counter = jsonData[i];
                data[i] = [counter.nombre, counter.monto];
            }
            //var data = [["January", 10], ["February", 48], ["March", 4], ["April", 13], ["May", 17], ["June", 9]];
            $.plot("#bar-chart2", [{data: data, color: purple}], {
                series: {
                    bars: {
                        show: true,
                        barWidth: 0.4,
                        align: 'center',
                        fill: true,
                        fillColor: purple,
                        zero: true
                    }
                },
                xaxis: {
                    mode: "categories",
                    tickColor: '#ddd',
                    tickLength: 0
                },
                grid: {
                    borderWidth: 0
                }
            });
        }
    }

    var handleInteractiveChart = function() {
        "use strict";
        function showTooltip(x, y, contents) {
            $('<div id="tooltip" class="flot-tooltip">' + contents + '</div>').css({
                top: y - 45,
                left: x - 55
            }).appendTo("body").fadeIn(200);
        }
        if ($('#interactive-chart').length !== 0) {
            var d1 = [[0, 42], [1, 53], [2, 66], [3, 60], [4, 68], [5, 66], [6, 71], [7, 75], [8, 69], [9, 70], [10, 68], [11, 72], [12, 78], [13, 86]];
            var d2 = [[0, 12], [1, 26], [2, 13], [3, 18], [4, 35], [5, 23], [6, 18], [7, 35], [8, 24], [9, 14], [10, 14], [11, 29], [12, 30], [13, 43]];

            $.plot($("#interactive-chart"), [
                {
                    data: d1,
                    label: "Page Views",
                    color: purple,
                    lines: {show: true, fill: false, lineWidth: 2},
                    points: {show: false, radius: 5, fillColor: '#fff'},
                    shadowSize: 0
                }, {
                    data: d2,
                    label: 'Visitors',
                    color: green,
                    lines: {show: true, fill: false, lineWidth: 2, fillColor: ''},
                    points: {show: false, radius: 3, fillColor: '#fff'},
                    shadowSize: 0
                }
            ],
                    {
                        xaxis: {tickColor: '#ddd', tickSize: 2},
                        yaxis: {tickColor: '#ddd', tickSize: 20},
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#ccc",
                            borderWidth: 1,
                            borderColor: '#ddd'
                        },
                        legend: {
                            labelBoxBorderColor: '#ddd',
                            margin: 0,
                            noColumns: 1,
                            show: true
                        }
                    }
            );
            var previousPoint = null;
            $("#interactive-chart").bind("plothover", function(event, pos, item) {
                $("#x").text(pos.x.toFixed(2));
                $("#y").text(pos.y.toFixed(2));
                if (item) {
                    if (previousPoint !== item.dataIndex) {
                        previousPoint = item.dataIndex;
                        $("#tooltip").remove();
                        var y = item.datapoint[1].toFixed(2);

                        var content = item.series.label + " " + y;
                        showTooltip(item.pageX, item.pageY, content);
                    }
                } else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
                event.preventDefault();
            });
        }
    };

</script>

</body>
</html>
