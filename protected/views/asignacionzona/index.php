<?php
/* @var $this AsignacionzonaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Asignacionzonas',
);

$this->menu=array(
	array('label'=>'Create Asignacionzona', 'url'=>array('create')),
	array('label'=>'Manage Asignacionzona', 'url'=>array('admin')),
);
?>

<h1>Asignacionzonas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
