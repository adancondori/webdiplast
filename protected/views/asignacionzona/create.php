<?php
/* @var $this AsignacionzonaController */
/* @var $model Asignacionzona */

$this->breadcrumbs=array(
	'Asignacionzonas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Asignacionzona', 'url'=>array('index')),
	array('label'=>'Manage Asignacionzona', 'url'=>array('admin')),
);
?>

<h1>Create Asignacionzona</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>