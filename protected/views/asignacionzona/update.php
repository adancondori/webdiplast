<?php
/* @var $this AsignacionzonaController */
/* @var $model Asignacionzona */

$this->breadcrumbs=array(
	'Asignacionzonas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Asignacionzona', 'url'=>array('index')),
	array('label'=>'Create Asignacionzona', 'url'=>array('create')),
	array('label'=>'View Asignacionzona', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Asignacionzona', 'url'=>array('admin')),
);
?>

<h1>Update Asignacionzona <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>