<?php
/* @var $this AsignacionzonaController */
/* @var $model Asignacionzona */

$this->breadcrumbs=array(
	'Asignacionzonas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Asignacionzona', 'url'=>array('index')),
	array('label'=>'Create Asignacionzona', 'url'=>array('create')),
	array('label'=>'Update Asignacionzona', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Asignacionzona', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Asignacionzona', 'url'=>array('admin')),
);
?>

<h1>View Asignacionzona #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_zona',
		'id_personal',
		'dia',
	),
)); ?>
