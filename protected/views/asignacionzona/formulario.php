<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $personales  :  Lista de Personales */
?>

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->

<?php
/* @var $categoria  :  Objeto categoria */
?>
<lib></lib>
<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="../../assets/plugins/parsley/parsley.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL STYLE ================== -->
<script>
    function agregar() {
        var dia = $('#dias').val();
        var zona = $("#zonas option:selected").val();
        var zonatext = $("#zonas option:selected").text();
        if (dia == "" || zona == "") {
            alert("Debe escoger una opción");
            return false;
        }
        var i = $('#tbrows').children().length;
        var row = '<tr id="' + i + '" class="odd gradeX"><td>' + zonatext + '<input hidden="hidden" name="idzonas[]" value="' + zona + '"></td>' +
                '<td>' + dia + '<input hidden="hidden" name="dias[]" value="' + dia + '"></td>' +
                '<td><a href="javascript:$(' + "'#" + i + "'" + ').remove()">Quitar</a></td>' +
                '</tr>';
        $('#tbrows').append(row);

        return false;
    }
</script>
<div id="content" class="content">
    <!-- begin page-header -->
    <h1 class="page-header">ASIGNACI&Oacute; DE ZONA</h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">Todos los campos (*) son requeridos</h4>
                </div>
                <div class="panel-body panel-form">
                    <form id="formulario" class="form-horizontal form-bordered" action="../../indexyii.php/asignacionzona/ajax?modo=guardar" method="POST" >
                        <input type="text" name="id" value="<?php echo $personal->id; ?>" hidden="hidden">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Personal * :</label>
                            <div class="col-md-6 col-sm-6">
                                <?php echo $personal->nombre; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Asignaciones * :</label>
                            <div class="col-md-6 col-sm-6">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <td>
                                                <select class="form-control parsley-validated" id="zonas" data-parsley-group="block1" >
                                                    <option value="">Please choose</option>
                                                    <?php
                                                    for ($i = 0; $i < sizeof($zonas); $i++) {
                                                        echo '<option value="' . $zonas[$i]->id . '">' . $zonas[$i]->nombre . '</nombre>';
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select class="form-control parsley-validated" id="dias" data-parsley-group="block1" >
                                                    <option value="">Please choose</option>
                                                    <option value="Lunes">Lunes</option>
                                                    <option value="Martes">Martes</option>
                                                    <option value="Miercoles">Miercoles</option>
                                                    <option value="Jueves">Jueves</option>
                                                    <option value="Viernes">Viernes</option>
                                                    <option value="Sabado">S&aacute;bado</option>
                                                </select>
                                            </td>
                                            <td>
                                                <button onclick="return agregar()" class="form-control parsley-validated" >Agregar</button>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody id="tbrows">
                                        <?php
                                        for ($i = 0; $i < count($asignaciones); $i++) {
                                            ?>
                                            <tr id="<?php echo $i ?>" class="odd gradeX">
                                                <td><?php echo $asignaciones[$i]->idZona->nombre ?><input hidden="hidden" name="idzonas[]" value="<?php echo $asignaciones[$i]->id_zona ?>"></td>
                                                <td><?php echo $asignaciones[$i]->dia ?><input hidden="hidden" name="dias[]" value="<?php echo $asignaciones[$i]->dia ?>"></td>
                                                <td><a href="javascript:$('#<?php echo $i ?>').remove()">Quitar</a></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4"></label>
                            <div class="col-md-6 col-sm-6">
                                <button class="btn btn-primary" >Guardar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
    </div>

</div>
<?php
include_once 'protected/views/layouts/pie.php';
?>