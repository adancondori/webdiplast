<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $proveedores  :  Lista de Proveedores */
?>

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->

<!--<script type="text/javascript" src="../../assets/js/ajax-personal.js"></script>-->
<link href="../../assets/plugins/DataTables-1.9.4/css/data-table.css" rel="stylesheet" />
<script type="text/javascript" src="../../assets/plugins/DataTables-1.9.4/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../assets/plugins/DataTables-1.9.4/js/data-table.js"></script>
<!-- ================== END PAGE LEVEL STYLE ================== -->
<div id="content" class="content">

    <h1 class="page-header">PROVEEDORES</h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <a href="../../indexyii.php/proveedor/ajax?modo=nuevo" ><button type="button" class="btn btn-warning m-r-5 m-b-5">Añadir Nuevo Proveedor</button></a>
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">Lista del Personal</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>NIT</th>
                                    <th>Dirección</th>
                                    <th>Teléfono</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($proveedores); $i++) {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $proveedores[$i]->getAttribute('nombre'); ?></td>
                                        <td><?php echo $proveedores[$i]->getAttribute('nit'); ?></td>
                                        <td><?php echo $proveedores[$i]->getAttribute('direccion'); ?></td>
                                        <td><?php echo $proveedores[$i]->getAttribute('telefono'); ?></td>
                                        <td>
                                            <a href="../../indexyii.php/proveedor/ajax?modo=cargar&id=<?php echo $proveedores[$i]->getAttribute('id'); ?>">
                                                Editar</a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
</div>
<?php
include_once 'protected/views/layouts/pie.php';
?>
