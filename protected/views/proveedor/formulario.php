<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $personales  :  Lista de Personales */
?>

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="assets/plugins/parsley/parsley.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL STYLE ================== -->

<div id="content" class="content">

    <h1 class="page-header">PROVEEDOR</h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">Datos Proveedor</h4>
                </div>
                <div class="panel-body panel-form">
                    <form id="formulario" class="form-horizontal form-bordered" action="../../indexyii.php/proveedor/ajax?modo=guardar" method="POST" >
                        <input type="text" hidden="hidden" name="id" value="<?php echo $proveedor->getAttribute('id'); ?>" >
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Nombre * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="nombre" name="Proveedor[nombre]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $proveedor->getAttribute('nombre'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Razon Social * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="razonsocial" name="Proveedor[razonsocial]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $proveedor->getAttribute('razonsocial'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">NIT * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="nit" name="Proveedor[nit]"  data-parsley-group="block1" value="<?php echo $proveedor->getAttribute('nit'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="message">Dirección :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="direccion" name="Proveedor[direccion]" data-parsley-group="block1" value="<?php echo $proveedor->getAttribute('direccion'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="message">Teléfono :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="telefono" placeholder="(XXX) XXXX XXX" name="Proveedor[telefono]" data-parsley-group="block1" value="<?php echo $proveedor->getAttribute('telefono'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4"></label>
                            <div class="col-md-6 col-sm-6">
                                <button class="btn btn-primary" >Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
    </div>
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
<?php
include_once 'protected/views/layouts/pie.php';
?>