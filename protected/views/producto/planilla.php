<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
?>
<script type="text/javascript">
    function collapse(index) {
        for (var i = 1; i <= 5; i++) {
            if (i == index) {
                $('#planilla' + i).slideDown();
            } else {
                $('#planilla' + i).slideUp();
            }
        }
    }
    $(document).ready(function() {
        $('#planilla2').slideUp();
        $('#planilla3').slideUp();
        $('#planilla4').slideUp();
        $('#planilla5').slideUp();
    });
</script>
<div id="content" class="content">
    <link href="assets/plugins/parsley/parsley.css" rel="stylesheet" />
    <h1 class="page-header">PLANILLAS</h1>
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-6">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" onclick="collapse(1)" class="btn btn-xs btn-icon btn-circle btn-warning" ><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">Productos</h4>
                </div>
                <div id="planilla1" class="panel-body panel-form">
                    <form id="formulario" class="form-horizontal form-bordered" action="../../indexyii.php/producto/ajax?modo=importar" method="POST" enctype="multipart/form-data" >
                        <input type="text" name="id" value="" hidden="hidden">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Importar CSV * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="file" id="archivo" name="archivo" required="true" data-parsley-group="block1" accept=".csv">
                                <button class="btn btn-primary" >Importar</button>
                            </div>
                        </div>
                    </form>
                    <form id="formulario" class="form-horizontal form-bordered" action="../../indexyii.php/producto/ajax?modo=exportar" method="POST" enctype="multipart/form-data" >
                        <input type="text" name="id" value="" hidden="hidden">   
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Exportar a CSV :</label>
                            <div class="col-md-6 col-sm-6">
                                <button class="btn btn-primary" >Exportar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" onclick="collapse(2)" class="btn btn-xs btn-icon btn-circle btn-warning" ><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">Clientes</h4>
                </div>
                <div id="planilla2" class="panel-body panel-form">
                    <form id="formulario" class="form-horizontal form-bordered" action="../../indexyii.php/cliente/ajax?modo=importar" method="POST" enctype="multipart/form-data" >
                        <input type="text" name="id" value="" hidden="hidden">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Importar CSV * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="file" id="archivo" name="archivo" required="true" data-parsley-group="block1" accept=".csv">
                                <button class="btn btn-primary" >Importar</button>
                            </div>
                        </div>
                    </form>
                    <form id="formulario" class="form-horizontal form-bordered" action="../../indexyii.php/cliente/ajax?modo=exportar" method="POST" enctype="multipart/form-data" >
                        <input type="text" name="id" value="" hidden="hidden">   
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Exportar a CSV :</label>
                            <div class="col-md-6 col-sm-6">
                                <button class="btn btn-primary" >Exportar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" onclick="collapse(3)" class="btn btn-xs btn-icon btn-circle btn-warning" ><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">Negocios</h4>
                </div>
                <div id="planilla3" class="panel-body panel-form">
                    <form id="formulario" class="form-horizontal form-bordered" action="../../indexyii.php/negocio/ajax?modo=importar" method="POST" enctype="multipart/form-data" >
                        <input type="text" name="id" value="" hidden="hidden">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Importar CSV * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="file" id="archivo" name="archivo" required="true" data-parsley-group="block1" accept=".csv">
                                <button class="btn btn-primary" >Importar</button>
                            </div>
                        </div>
                    </form>
                    <form id="formulario" class="form-horizontal form-bordered" action="../../indexyii.php/negocio/ajax?modo=exportar" method="POST" enctype="multipart/form-data" >
                        <input type="text" name="id" value="" hidden="hidden">   
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Exportar a CSV :</label>
                            <div class="col-md-6 col-sm-6">
                                <button class="btn btn-primary" >Exportar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" onclick="collapse(4)" class="btn btn-xs btn-icon btn-circle btn-warning" ><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">Productos - Actualizar Stock</h4>
                </div>
                <div id="planilla4" class="panel-body panel-form">
                    <form id="formulario" class="form-horizontal form-bordered" action="../../indexyii.php/producto/ajax?modo=stock" method="POST" enctype="multipart/form-data" >
                        <input type="text" name="id" value="" hidden="hidden">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Importar CSV * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="file" id="archivo" name="archivo" required="true" data-parsley-group="block1" accept=".csv">
                                <button class="btn btn-primary" >Importar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" onclick="collapse(5)" class="btn btn-xs btn-icon btn-circle btn-warning" ><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">Productos - Actualizar Costo</h4>
                </div>
                <div id="planilla5" class="panel-body panel-form">
                    <form id="formulario" class="form-horizontal form-bordered" action="../../indexyii.php/producto/ajax?modo=costo" method="POST" enctype="multipart/form-data" >
                        <input type="text" name="id" value="" hidden="hidden">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Importar CSV * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="file" id="archivo" name="archivo" required="true" data-parsley-group="block1" accept=".csv">
                                <button class="btn btn-primary" >Importar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <div class="col-md-6">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">Resultados</h4>
                </div>
                <div id="resultados" class="panel-body panel-form">
                    <div class="table-responsive">
                        <?php
                        if (isset($productos)) {
                            ?>
                            <table id="data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Nombre</th>
                                        <th>Precio</th>
                                        <th>Stock</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($productos as $producto) {
                                        ?>
                                        <tr class="odd gradeX"> 
                                            <td><?php echo $producto->codigo; ?></td>
                                            <td><?php echo $producto->nombre; ?></td>
                                            <td><?php echo $producto->precio; ?></td>
                                            <td><?php echo $producto->stock; ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php
                        }

                        if (isset($clientes)) {
                            ?>
                            <table id="data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>CI</th>
                                        <th>Tel&eacute;fono</th>
                                        <th>Email</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($clientes as $cliente) {
                                        ?>
                                        <tr class="odd gradeX"> 
                                            <td><?php echo $cliente->nombre . " " . $cliente->apellidos; ?></td>
                                            <td><?php echo $cliente->ci; ?></td>
                                            <td><?php echo $cliente->telefono; ?></td>
                                            <td><?php echo $cliente->email; ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php
                        }

                        if (isset($negocios)) {
                            ?>
                            <table id="data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>NIT</th>
                                        <th>Direcci&oacute;n</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($negocios as $negocio) {
                                        ?>
                                        <tr class="odd gradeX"> 
                                            <td><?php echo $negocio->nombre; ?></td>
                                            <td><?php echo $negocio->nit; ?></td>
                                            <td><?php echo $negocio->direccion; ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php
                        }

                        if (isset($productosStock)) {
                            ?>
                            <table id="data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Nombre</th>
                                        <th>Stock</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($productosStock as $producto) {
                                        ?>
                                        <tr class="odd gradeX"> 
                                            <td><?php echo $producto->codigo; ?></td>
                                            <td><?php echo $producto->nombre; ?></td>
                                            <td><?php echo $producto->stock; ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php
                        }

                        if (isset($productosCosto)) {
                            ?>
                            <table id="data-table" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Nombre</th>
                                        <th>Costo</th>
                                        <th>Precio</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($productosCosto as $producto) {
                                        ?>
                                        <tr class="odd gradeX"> 
                                            <td><?php echo $producto->codigo; ?></td>
                                            <td><?php echo $producto->nombre; ?></td>
                                            <td><?php echo $producto->costo; ?></td>
                                            <td><?php echo $producto->precio; ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
    </div>
</div>
<?php
include_once 'protected/views/layouts/pie.php';
?>

