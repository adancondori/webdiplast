<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $personales  :  Lista de Personales */
?>
<div id="content" class="content">
    <!-- begin breadcrumb -->

    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->

    <?php
    /* @var $producto  :  Lista de Personales */
    ?>
    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="assets/plugins/parsley/parsley.css" rel="stylesheet" />
    <!-- ================== END PAGE LEVEL STYLE ================== -->

    <!-- begin page-header -->
    <h1 class="page-header">PRODUCTOS</h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title"><?php echo $categoria->nombre; ?></h4>
                </div>
                <div class="panel-body panel-form">
                    <form id="formulario" class="form-horizontal form-bordered" action="../../indexyii.php/producto/ajax?modo=guardar" method="POST" enctype="multipart/form-data" >
                        <input type="text" name="id" value="<?php echo $producto->id; ?>" hidden="hidden">
                        <input type="text" name="Producto[id_categoria]" value="<?php echo $categoria->id; ?>" hidden="hidden">
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Codigo * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" <?php echo $producto->nombre ? 'readonly="readonly"' : '' ?> type="text" id="codigo" name="Producto[codigo]" required="true" placeholder="Ingrese Datos" data-parsley-group="block1" value="<?php echo $producto->getAttribute('codigo'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Nombre * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="nombre" name="Producto[nombre]" required="true" placeholder="Ingrese Datos" data-parsley-group="block1" value="<?php echo $producto->getAttribute('nombre'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Descripción * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="descripcion" name="Producto[descripcion]" required="true" placeholder="Ingrese Datos" data-parsley-group="block1" value="<?php echo $producto->getAttribute('descripcion'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Medida * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="medida" name="Producto[medida]" required="true" placeholder="Ingrese Datos" data-parsley-group="block1" value="<?php echo $producto->getAttribute('medida'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Stock * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="text" id="stock" name="Producto[stock]" required="true" placeholder="Ingrese Datos" data-parsley-group="block1" value="<?php echo $producto->getAttribute('stock'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4" for="fullname">Imagen * :</label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control parsley-validated" type="file" id="imagen" name="imagen" data-parsley-group="block1" value="<?php echo '' ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4"></label>
                            <div class="col-md-6 col-sm-6">
                                <button class="btn btn-primary" >Aceptar</button>
                            </div>
                        </div>
                        <input type="text" id="id_categoria" value="<?php echo $producto->getAttribute('id_categoria'); ?>" hidden="true" <input/> 
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
    </div>
</div>
<?php
include_once 'protected/views/layouts/pie.php';
?>

