<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $personales  :  Lista de Personales */
?>

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->

<script type="text/javascript" src="../../assets/js/ajax-categoria.js"></script>
<link href="../../assets/plugins/DataTables-1.9.4/css/data-table.css" rel="stylesheet" />
<script type="text/javascript" src="../../assets/js/map-google.demo.js"></script>
<script type="text/javascript" src="../../assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="../../assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- ================== END PAGE LEVEL STYLE ================== -->

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->

<link href="../../assets/plugins/DataTables-1.9.4/css/data-table.css" rel="stylesheet" />
<script type="text/javascript" src="../../assets/plugins/DataTables-1.9.4/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../assets/plugins/DataTables-1.9.4/js/data-table.js"></script>
<!-- ================== END PAGE LEVEL STYLE ================== -->
<script type="text/javascript">
    function eliminar(id) {
        if (confirm("¿Está seguro que desea Eliminar este Producto?")) {
            window.location = "../../indexyii.php/producto/ajax?modo=eliminar&id=" + id;
        }
    }
</script>
<div id="content" class="content">
    <?php
    /* @var $productos  :  Lista de Productos */
    ?>

    <h1 class="page-header">Lista de Productos</h1>

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">Productos</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Descripción</th>
                                    <th>Medida</th>
                                    <th>Precio</th>
                                    <th>Stock</th>
                                    <th>Reserva</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($productos); $i++) {
                                    $stock = (int) $productos[$i]->getAttribute('stock');
                                    $reserva = 0;
                                    if ($stock < 0) {
                                        $reserva = $stock * (-1);
                                        $stock = 0;
                                    }
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $productos[$i]->getAttribute('nombre'); ?></td>
                                        <td><?php echo $productos[$i]->getAttribute('descripcion'); ?></td>
                                        <td><?php echo $productos[$i]->getAttribute('medida'); ?></td>
                                        <td><?php echo $productos[$i]->getAttribute('precio'); ?></td>
                                        <td><?php echo $stock; ?></td>
                                        <td><?php echo $reserva; ?></td>
                                        <td>
                                            <a href="../../indexyii.php/producto/ajax?modo=cargar&id=<?php echo $productos[$i]->id; ?>">
                                                Editar</a>&nbsp;&nbsp;
                                            <a href="#" onclick="eliminar(<?php echo $productos[$i]->id; ?>)">
                                                Eliminar</a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
</div>
<?php
include_once 'protected/views/layouts/pie.php';
?>
