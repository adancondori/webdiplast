<?php
/* @var $this UsuarioController */
/* @var $model Usuario */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'usuario-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'usuario'); ?>
        <?php echo $form->textField($model, 'usuario', array('size' => 45, 'maxlength' => 45)); ?>
        <?php echo $form->error($model, 'usuario'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'contrasena'); ?>
        <?php echo $form->textField($model, 'contrasena', array('size' => 45, 'maxlength' => 45)); ?>
        <?php echo $form->error($model, 'contrasena'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email', array('size' => 45, 'maxlength' => 45)); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>
    <!--
            <div class="row">
    <?php // echo $form->labelEx($model,'conectado'); ?>
    <?php // echo $form->textField($model,'conectado',array('size'=>1,'maxlength'=>1));  ?>
    <?php // echo $form->error($model,'conectado');  ?>
            </div>
    -->
    <div class="row">
        <?php echo $form->labelEx($model, 'id_personal'); ?>
        <?php echo $form->dropDownList($model, 'id_personal', Personal::model()->listNombreCompleto(), array('empty' => 'Seleccione Personal')); ?>
        <?php echo $form->error($model, 'id_personal'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->