<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->

<?php
//$_SESSION['usuario'] = 'Freddy';
?>

<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>Color Admin | Login Page</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />

        <!-- ================== BEGIN BASE CSS STYLE ================== -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <link href="../../assets/plugins/jquery-ui-1.10.4/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
        <link href="../../assets/plugins/bootstrap-3.1.1/css/bootstrap.min.css" rel="stylesheet" />
        <link href="../../assets/plugins/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" />
        <link href="../../assets/css/animate.min.css" rel="stylesheet" />
        <link href="../../assets/css/style.css" rel="stylesheet" />
        <link href="../../assets/css/style-responsive.css" rel="stylesheet" />
        <!-- ================== END BASE CSS STYLE ================== -->

        <!-- ================== BEGIN JQUERY ================== -->
        <script type="text/javascript" src="../../assets/plugins/jquery-1.8.2/jquery-1.8.2.min.js"></script>
        <!-- ================== END JQUERY ================== -->
    </head>
    <body>
        <script type="text/javascript" >

            function iniciarsesion() {
                var email = document.getElementById('email');
                var contrasena = document.getElementById('contrasena');

                var parametros = {
                    "modo": "iniciarsesion",
                    "email": email.value,
                    "contrasena": contrasena.value
                };

                var xmlhttp;

                if (window.XMLHttpRequest) {
                    // code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                } else {
                    // code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }

                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState == 4) {
                        if (xmlhttp.status == 200) {
                            alert('hola123');
                        }
                        else if (xmlhttp.status == 400) {
                            alert('There was an error 400')
                        }
                        else {
                            alert('something else other than 200 was returned')
                        }
                    }
                }

                xmlhttp.open("POST", "../../indexyii.php/usuario/ajax", true);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.send("modo=index&email=" + email.value + "&contrasena=" + contrasena.value);


//                $.ajax({
//                    data: parametros,
//                    url: '../../indexyii.php/usuario/ajax',
//                    type: 'post',
//                    beforeSend: function() {
//                        //$("#resultado").html("Procesando, espere por favor...");
//                        alert('sss');
//                    },
//                    success: function(response) {
//
//                        alert('holaa');
//
//                        alert(response);
//                        var obj = JSON.parse(response);
//                        if (obj['msg'] === 'ok') {
//                            location.href = '../../indexyii.php/usuario/ajax?modo=index';
//                        } else {
//                            document.getElementById('msg').innerHTML = obj['msg'];
//                        }
//                    }
//                });
                return false;
            }

        </script>
        <!-- begin #page-loader -->
        <div id="page-loader" class="fade in"><span class="spinner"></span></div>
        <!-- end #page-loader -->

        <!-- begin #page-container -->
        <div id="page-container" class="fade">
            <!-- begin login -->
            <div class="login bg-black animated fadeInDown">
                <!-- begin brand -->
                <div class="login-header">
                    <div class="brand">
                        <span class="logo"></span> DIPLAST S.R.L.
                        <small>Representante autorizado</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-sign-in"></i>
                    </div>
                </div>
                <!-- end brand -->
                <div class="login-content">
                    <form action="index.php" method="POST" class="margin-bottom-0" onsubmit="return iniciarsesion();">
                        <div class="form-group m-b-20" id="msg">
                        </div>
                        <div class="form-group m-b-20">
                            <input id="email" type="text" class="form-control input-lg" placeholder="Correo Electrónico" />
                        </div>
                        <div class="form-group m-b-20">
                            <input id="contrasena" type="password" class="form-control input-lg" placeholder="Contraseña" />
                        </div>
                        <label class="checkbox m-b-20">
                            <input type="checkbox" /> Recordarme
                        </label>
                        <div class="login-buttons">
                            <button class="btn btn-success btn-block btn-lg" >Iniciar Sesión</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end login -->
        </div>
        <!-- end page container -->

        <!-- ================== BEGIN BASE JS ================== -->
        <script src="../../assets/plugins/jquery-1.8.2/jquery-1.8.2.min.js"></script>
        <script src="../../assets/plugins/jquery-ui-1.10.4/ui/minified/jquery-ui.min.js"></script>
        <script src="../../assets/plugins/bootstrap-3.1.1/js/bootstrap.min.js"></script>
        <!--[if lt IE 9]>
                <script src="../../assets/crossbrowserjs/html5shiv.js"></script>
                <script src="../../assets/crossbrowserjs/respond.min.js"></script>
                <script src="../../assets/crossbrowserjs/excanvas.min.js"></script>
        <![endif]-->
        <script src="../../assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
        <!-- ================== END BASE JS ================== -->

        <!-- ================== BEGIN PAGE LEVEL JS ================== -->
        <script src="../../assets/js/apps.min.js"></script>
        <!-- ================== END PAGE LEVEL JS ================== -->
        <script>
                        $(document).ready(function() {
                            App.init();
                        });
        </script>
    </body>
</html>
