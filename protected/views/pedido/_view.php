<?php
/* @var $this PedidoController */
/* @var $data Pedido */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numero')); ?>:</b>
	<?php echo CHtml::encode($data->numero); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechapedido')); ?>:</b>
	<?php echo CHtml::encode($data->fechapedido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaentrega')); ?>:</b>
	<?php echo CHtml::encode($data->fechaentrega); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('montototal')); ?>:</b>
	<?php echo CHtml::encode($data->montototal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create')); ?>:</b>
	<?php echo CHtml::encode($data->create); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('change')); ?>:</b>
	<?php echo CHtml::encode($data->change); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('createby')); ?>:</b>
	<?php echo CHtml::encode($data->createby); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('changeby')); ?>:</b>
	<?php echo CHtml::encode($data->changeby); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_negocio')); ?>:</b>
	<?php echo CHtml::encode($data->id_negocio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_promotor')); ?>:</b>
	<?php echo CHtml::encode($data->id_promotor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_entregador')); ?>:</b>
	<?php echo CHtml::encode($data->id_entregador); ?>
	<br />

	*/ ?>

</div>