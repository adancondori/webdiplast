<?php
/* @var $this PedidoController */
/* @var $model Pedido */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pedido-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'numero'); ?>
		<?php echo $form->textField($model,'numero',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'numero'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fechapedido'); ?>
		<?php echo $form->textField($model,'fechapedido'); ?>
		<?php echo $form->error($model,'fechapedido'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fechaentrega'); ?>
		<?php echo $form->textField($model,'fechaentrega'); ?>
		<?php echo $form->error($model,'fechaentrega'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php echo $form->textField($model,'estado',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'montototal'); ?>
		<?php echo $form->textField($model,'montototal'); ?>
		<?php echo $form->error($model,'montototal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'create'); ?>
		<?php echo $form->textField($model,'create'); ?>
		<?php echo $form->error($model,'create'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'change'); ?>
		<?php echo $form->textField($model,'change'); ?>
		<?php echo $form->error($model,'change'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'createby'); ?>
		<?php echo $form->textField($model,'createby'); ?>
		<?php echo $form->error($model,'createby'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'changeby'); ?>
		<?php echo $form->textField($model,'changeby'); ?>
		<?php echo $form->error($model,'changeby'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_negocio'); ?>
		<?php echo $form->textField($model,'id_negocio'); ?>
		<?php echo $form->error($model,'id_negocio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_promotor'); ?>
		<?php echo $form->textField($model,'id_promotor'); ?>
		<?php echo $form->error($model,'id_promotor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_entregador'); ?>
		<?php echo $form->textField($model,'id_entregador'); ?>
		<?php echo $form->error($model,'id_entregador'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->