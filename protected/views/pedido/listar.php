<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $personales  :  Lista de Personales */
?>

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<script type="text/javascript" src="../../assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="../../assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- ================== END PAGE LEVEL STYLE ================== -->

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="../../assets/plugins/DataTables-1.9.4/css/data-table.css" rel="stylesheet" />
<script type="text/javascript" src="../../assets/plugins/DataTables-1.9.4/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../assets/plugins/DataTables-1.9.4/js/data-table.js"></script>
<!-- ================== END PAGE LEVEL STYLE ================== -->
<div id="content" class="content">
    <?php
    /* @var $pedidos  :  Lista de Pedidos */
    //$pedidos->setPagination(false);
    //echo count($pedidos);
    //var_dump($pedidos[0]->idNegocio->nombre);
    ?>

    <h1 class="page-header">PEDIDOS</h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">Lista de Pedidos</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Fecha de Pedido</th>
                                    <th>Fecha de Entrega</th>
                                    <th>Estado de Pedido</th>
                                    <th>Monto Total</th>
                                    <th>Negocio</th>
                                    <th>Promotor</th>
                                    <th>Entregador</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for ($i = 0; $i < count($pedidos); $i++) { ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $pedidos[$i]->fechapedido; ?></td>
                                        <td><?php echo $pedidos[$i]->fechaentrega; ?></td>
                                        <td><?php echo $pedidos[$i]->estado; ?></td>
                                        <td><p align="right"><?php echo number_format($pedidos[$i]->montototal,2,",","."); ?></p></td>
                                        <td><?php echo $pedidos[$i]->idNegocio->nombre; ?></td>
                                        <td><?php echo $pedidos[$i]->idPromotor->nombre . ' ' . $pedidos[$i]->idPromotor->apellidos; ?></td>
                                        <td>
                                            <?php
                                            if (isset($pedidos[$i]->idEntregador->nombre)) {
                                                echo $pedidos[$i]->idEntregador->nombre;
                                            } else {
                                                echo 'NO ENTREGADO';
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <a href="../../indexyii.php/pedido/ajax?modo=listardetalle&id_pedido=<?php echo $pedidos[$i]->id; ?>">
                                                Ver Detalle</a>&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
</div>
<?php
include_once 'protected/views/layouts/pie.php';
?>
