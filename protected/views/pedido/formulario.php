<?php
/* @var $pedido  :  Lista de Personales */
?>
<lib></lib>
<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="assets/plugins/parsley/parsley.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL STYLE ================== -->


<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Home</a></li>
    <li><a href="javascript:;">Form Stuff</a></li>
    <li class="active">Form Validation</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Form Validation <small>header small text goes here...</small></h1>
<!-- end page-header -->

<!-- begin row -->
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                </div>
                <h4 class="panel-title">Basic Form Validation</h4>
            </div>
            <div class="panel-body panel-form">
                <form id="formulario" class="form-horizontal form-bordered" method="POST" >
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="fullname">Codigo * :</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control parsley-validated" type="text" id="nombre" name="nombre" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $pedido->getAttribute('numero'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="fullname">Fecha del Pedido * :</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control parsley-validated" type="text" id="fechapedido" name="fechapedido" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $pedido->getAttribute('fechapedido'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="fullname">Fecha de Entrega * :</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control parsley-validated" type="text" id="fechaentrega" name="fechaentrega" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $pedido->getAttribute('fechaentrega'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="fullname">Estado * :</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control parsley-validated" type="text" id="estado" name="estado" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $pedido->getAttribute('estado'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="fullname">Monto Total :</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control parsley-validated" type="text" id="montototal" name="montototal" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $pedido->getAttribute('montototal'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="fullname">Negocio * :</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control parsley-validated" type="text" id="id_negocio" name="id_negocio" data-trigger="change" required="true"  placeholder="Email" data-parsley-group="block1" value="<?php echo $pedido->getAttribute('id_negocio'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="fullname">Promotor * :</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control parsley-validated" type="text" id="id_promotor" name="id_promotor" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $pedido->getAttribute('id_promotor'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="fullname">Entregador * :</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control parsley-validated" type="text" id="id_entregador" name="id_entregador" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $pedido->getAttribute('id_entregador'); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4"></label>
                        <div class="col-md-6 col-sm-6">
                            <button class="btn btn-primary" >Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-6 -->
</div>
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>