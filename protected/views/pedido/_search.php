<?php
/* @var $this PedidoController */
/* @var $model Pedido */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'numero'); ?>
		<?php echo $form->textField($model,'numero',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fechapedido'); ?>
		<?php echo $form->textField($model,'fechapedido'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fechaentrega'); ?>
		<?php echo $form->textField($model,'fechaentrega'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estado'); ?>
		<?php echo $form->textField($model,'estado',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'montototal'); ?>
		<?php echo $form->textField($model,'montototal'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'create'); ?>
		<?php echo $form->textField($model,'create'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'change'); ?>
		<?php echo $form->textField($model,'change'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'createby'); ?>
		<?php echo $form->textField($model,'createby'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'changeby'); ?>
		<?php echo $form->textField($model,'changeby'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_negocio'); ?>
		<?php echo $form->textField($model,'id_negocio'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_promotor'); ?>
		<?php echo $form->textField($model,'id_promotor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_entregador'); ?>
		<?php echo $form->textField($model,'id_entregador'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->