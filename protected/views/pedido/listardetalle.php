<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $personales  :  Lista de Personales */
?>

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->

<script type="text/javascript" src="../../assets/js/ajax-categoria.js"></script>
<script type="text/javascript" src="../../assets/js/map-google.demo.js"></script>
<script type="text/javascript" src="../../assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="../../assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- ================== END PAGE LEVEL STYLE ================== -->

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="../../assets/plugins/DataTables-1.9.4/css/data-table.css" rel="stylesheet" />
<script type="text/javascript" src="../../assets/plugins/DataTables-1.9.4/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../assets/plugins/DataTables-1.9.4/js/data-table.js"></script>
<!-- ================== END PAGE LEVEL STYLE ================== -->
<div id="content" class="content">
    <?php
    /* @var $detalles  :  Lista de Detalles */
    ?>

    <h1 class="page-header">PEDIDO</h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <!--<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>-->
                    </div>
                    <h4 class="panel-title">Detalles del Pedido</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th>Precio Unitario (Bs.)</th>
                                    <th>Cantidad</th>
                                    <th>Monto Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $suma = 0;
                                for ($i = 0; $i < count($detalles); $i++) {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $detalles[$i]->idProducto->nombre; ?></td>
                                        <td><?php echo $detalles[$i]->idProducto->precio; ?></td>
                                        <td><?php echo $detalles[$i]->attributes['cantidad']; ?></td>
                                        <td><?php echo $detalles[$i]->attributes['montototal'];
                                $suma += floatval($detalles[$i]->attributes['montototal'])
                                    ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                            <tfoot>

                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td align="right"><b>Suma Total: Bs.</b></td>
                                    <td><b><?php echo $suma; ?></b></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
</div>
<?php
include_once 'protected/views/layouts/pie.php';
?>