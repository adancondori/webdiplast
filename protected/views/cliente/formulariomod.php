<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $personales  :  Lista de Personales */
?>

<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->

<!--<script type="text/javascript" src="../../assets/js/ajax-personal.js"></script>-->
<link href="../../assets/plugins/DataTables-1.9.4/css/data-table.css" rel="stylesheet" />
<script type="text/javascript" src="../../assets/plugins/DataTables-1.9.4/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../assets/plugins/DataTables-1.9.4/js/data-table.js"></script>
<!-- ================== END PAGE LEVEL STYLE ================== -->
<script>
    $(function() {
        $("#fechanacimiento").datepicker("option", "dateFormat", "yy-mm-dd");
    });
</script>
<div id="content" class="content">

    <h1 class="page-header">Actualizar Cliente</h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Cliente</h4>
                </div>
                <div class="panel-body">
                    <form id="formulario" method="POST" action="../../indexyii.php/cliente/ajax?modo=modificar">
                        <div id="wizard">
                            <fieldset>
                                <legend class="pull-left width-full">Datos del Cliente</legend>
                                <!-- begin row -->
                                <input name="id" hidden="hidden" value="<?php echo $cliente->id; ?>">
                                <div class="row form-horizontal form-bordered">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4" for="nombre">Nombre * :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control parsley-validated" type="text" id="nombre" name="Cliente[nombre]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $cliente->nombre; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4" for="apellidos">Apellidos * :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control parsley-validated" type="text" id="apellidos" name="Cliente[apellidos]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $cliente->apellidos; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4" for="direccion">Dirección * :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control parsley-validated" type="text" id="direccion" name="Cliente[direccion]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $cliente->direccion; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4">Sexo * :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <select class="form-control parsley-validated" id="sexo" name="Cliente[sexo]" required="true" data-parsley-group="block1" >
                                                <option value="">Please choose</option>
                                                <option value="H" 
                                                <?php if ($cliente->sexo == 'H') echo 'selected'; ?>
                                                        >Hombre</option>
                                                <option value="M"
                                                <?php if ($cliente->sexo == 'M') echo 'selected'; ?>
                                                        >Mujer</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4" for="fechanacimiento">Fecha de Nacimiento :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control parsley-validated" data-date-format="yyyy-mm-dd" id="fechanacimiento" name="Cliente[fechanacimiento]" placeholder="XXXX-XX-XX" required="true" data-parsley-group="block1" value="<?php echo $cliente->fechanacimiento; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4" for="telefono">Teléfono :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control parsley-validated" type="text" id="telefono" name="Cliente[telefono]" required="true" placeholder="(XXX) XXXX XXX" data-parsley-group="block1" value="<?php echo $cliente->telefono; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4" for="celular">Celular :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control parsley-validated" type="text" id="celular" name="Cliente[celular]" data-trigger="change" required="true" placeholder="(XXX) XXXX XXX" data-parsley-group="block1" value="<?php echo $cliente->celular; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4" for="ci">CI :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control parsley-validated" type="text" id="ci" name="Cliente[ci]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $cliente->ci; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4" for="email">Email :</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control parsley-validated" type="text" id="email" name="Cliente[email]" data-trigger="change" required="true" data-parsley-type="email" placeholder="Email" data-parsley-group="block1" value="<?php echo $cliente->email; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-4"></label>
                                        <div class="col-md-6 col-sm-6">
                                            <button class="btn btn-primary" >Guardar Cambios</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- end row -->
                            </fieldset>
                            <!-- end wizard step-3 -->
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<?php
include_once 'protected/views/layouts/pie.php';
?>
