<?php
include_once 'protected/views/layouts/login.php';
include_once 'protected/views/layouts/cabecera.php';
include_once 'protected/views/layouts/menu.php';
/* @var $cliente  :  Objeto Cliente */
/* @var $zonas  :  Lista de Zonas */
?>
<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="../../assets/plugins/bootstrap-wizard/css/bwizard.min.css" rel="stylesheet" />
<link href="../../assets/plugins/parsley/parsley.css" rel="stylesheet" />
<!-- ================== END PAGE LEVEL STYLE ================== -->
<script type="text/javascript" src="../../assets/plugins/bootstrap-wizard/js/bwizard.js"></script>
<script type="text/javascript" src="../../assets/js/form-wizards.demo.min.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>-->
<script src="../../assets/js/map-google.demo.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>
<script src="../../assets/js/ajax-negocio.js"></script>

<script>
    function setimagenes() {
        var frame = document.getElementById('frameimagenes');
        var ar = frame.contentWindow.document.getElementsByName('imagen0');
        var len = ar.length;

        var name_imagenes = "";
        if (len > 0) {
            name_imagenes = ar[0].title;
        }
        if (len > 1) {
            name_imagenes += "," + ar[1].title;
        }
        if (len > 2) {
            name_imagenes += "," + ar[2].title;
        }
        document.getElementById('imagenes').value = name_imagenes;
        return true;
    }
    $(function() {
        $("#fechanacimiento").datepicker("option", "dateFormat", "yy-mm-dd");
    });
</script>

<div id="content" class="content">

    <h1 class="page-header">Nuevo Cliente</h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">Cliente</h4>
                </div>
                <div class="panel-body">
                    <form id="formulario" action="../../indexyii.php/cliente/ajax?modo=guardar" method="POST" onsubmit="setimagenes()" >
                        <div id="wizard">
                            <ol>
                                <li>
                                    Cliente
                                </li>
                                <li onclick="iniciarMapa();">
                                    Negocio
                                </li>
                            </ol>
                            <div>
                                <fieldset>
                                    <legend class="pull-left width-full">Datos del Cliente</legend>
                                    <!-- begin row -->
                                    <div class="row form-horizontal form-bordered">
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4" for="nombre">Nombre * :</label>
                                            <div class="col-md-6 col-sm-6">
                                                <input class="form-control parsley-validated" type="text" id="nombre" name="Cliente[nombre]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $cliente->nombre; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4" for="apellidos">Apellidos * :</label>
                                            <div class="col-md-6 col-sm-6">
                                                <input class="form-control parsley-validated" type="text" id="apellidos" name="Cliente[apellidos]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $cliente->apellidos; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4" for="direccion">Dirección * :</label>
                                            <div class="col-md-6 col-sm-6">
                                                <input class="form-control parsley-validated" type="text" id="direccion" name="Cliente[direccion]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $cliente->direccion; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4">Sexo * :</label>
                                            <div class="col-md-6 col-sm-6">
                                                <select class="form-control parsley-validated" id="sexo" name="Cliente[sexo]" required="true" data-parsley-group="block1" >
                                                    <option value="">Please choose</option>
                                                    <option value="H" 
                                                    <?php if ($cliente->sexo == 'H') echo 'selected'; ?>
                                                            >Hombre</option>
                                                    <option value="M"
                                                    <?php if ($cliente->sexo == 'M') echo 'selected'; ?>
                                                            >Mujer</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4" for="fechanacimiento">Fecha de Nacimiento :</label>
                                            <div class="col-md-6 col-sm-6">
                                                <input class="form-control parsley-validated" data-date-format="yyyy-mm-dd" id="fechanacimiento" name="Cliente[fechanacimiento]" placeholder="XXXX-XX-XX" required="true" data-parsley-group="block1" value="<?php echo $cliente->fechanacimiento; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4" for="telefono">Teléfono :</label>
                                            <div class="col-md-6 col-sm-6">
                                                <input class="form-control parsley-validated" type="text" id="telefono" name="Cliente[telefono]" required="true" placeholder="(XXX) XXXX XXX" data-parsley-group="block1" value="<?php echo $cliente->telefono; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4" for="celular">Celular :</label>
                                            <div class="col-md-6 col-sm-6">
                                                <input class="form-control parsley-validated" type="text" id="celular" name="Cliente[celular]" data-trigger="change" required="true" placeholder="(XXX) XXXX XXX" data-parsley-group="block1" value="<?php echo $cliente->celular; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4" for="ci">CI :</label>
                                            <div class="col-md-6 col-sm-6">
                                                <input class="form-control parsley-validated" type="text" id="ci" name="Cliente[ci]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $cliente->ci; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4" for="email">Email :</label>
                                            <div class="col-md-6 col-sm-6">
                                                <input class="form-control parsley-validated" type="text" id="email" name="Cliente[email]" data-trigger="change" required="true" data-parsley-type="email" placeholder="Email" data-parsley-group="block1" value="<?php echo $cliente->email; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>
                            </div>
                            <!-- end wizard step-3 -->
                            <!-- begin wizard step-4 -->
                            <div>
                                <fieldset>
                                    <legend class="pull-left width-full">Datos del Negocio</legend>
                                    <!-- begin row -->
                                    <div class="row form-horizontal form-bordered">
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4" for="nombre">Nombre * :</label>
                                            <div class="col-md-6 col-sm-6">
                                                <input class="form-control parsley-validated" type="text" id="nombre1" name="Negocio[nombre]" data-trigger="change" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $cliente->celular; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4" for="razonsocial">Razón Social * :</label>
                                            <div class="col-md-6 col-sm-6">
                                                <input class="form-control parsley-validated" type="text" id="razonsocial" name="Negocio[razonsocial]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $cliente->nombre; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4" for="nit">NIT * :</label>
                                            <div class="col-md-6 col-sm-6">
                                                <input class="form-control parsley-validated" type="text" id="nit" name="Negocio[nit]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $cliente->apellidos; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4" for="direccion">Dirección * :</label>
                                            <div class="col-md-6 col-sm-6">
                                                <input class="form-control parsley-validated" type="text" id="direccion1" name="Negocio[direccion]" required="true" placeholder="Requerido" data-parsley-group="block1" value="<?php echo $cliente->direccion; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4" for="observacion">Observación :</label>
                                            <div class="col-md-6 col-sm-6">
                                                <input class="form-control parsley-validated" type="text" id="observacion" name="Negocio[observacion]" required="true" data-parsley-group="block1" value="<?php echo $cliente->telefono; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4">Zona * :</label>
                                            <div class="col-md-6 col-sm-6">
                                                <select class="form-control parsley-validated" id="zona" name="Negocio[id_zona]" required="true" data-parsley-group="block1" >
                                                    <option value="">Please choose</option>
                                                    <?php
                                                    for ($i = 0; $i < sizeof($zonas); $i++) {
                                                        ?>
                                                        <option value="<?php echo $zonas[$i]->id ?>">
                                                            <?php echo $zonas[$i]->nombre ?>
                                                        </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4" for="localizacion">Localización :</label>
                                            <div class="col-md-6 col-sm-6">
                                                <input class="form-control parsley-validated" type="text" id="localizacion" name="localizacion" required="true" readonly="readonly" data-parsley-group="block1" value="<?php echo $cliente->telefono; ?>">
                                                <div id="googleMap" class="col-md-12 col-sm-12" style="height: 300px; width: max-content"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4" for="localizacion">Fotos :</label>
                                            <iframe id="frameimagenes" src="../../indexyii.php/cliente/ajax?modo=upload" class="col-md-6 col-sm-6" frameBorder="0"  style="height: 400px">

                                            </iframe>
                                            <input hidden="hidden" id="imagenes" name="imagenes" >
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-4 col-sm-4"></label>
                                            <div class="col-md-6 col-sm-6">
                                                <button class="btn btn-primary" >Guardar</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </fieldset>
                            </div>
                            <!-- end wizard step-4 -->
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
    <!-- end row -->
</div>
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->

<script src="../../assets/plugins/jquery-ui-1.10.4/ui/minified/jquery-ui.min.js"></script>
<script src="../../assets/plugins/bootstrap-3.1.1/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
        <script src="../../assets/crossbrowserjs/html5shiv.js"></script>
        <script src="../../assets/crossbrowserjs/respond.min.js"></script>
        <script src="../../assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="../../assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="../../assets/plugins/gritter/js/jquery.gritter.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.time.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="../../assets/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="../../assets/plugins/sparkline/jquery.sparkline.js"></script>
<script src="../../assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../../assets/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="../../assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="../../assets/js/dashboard.min.js"></script>
<script src="../../assets/js/form-plugins.demo.js"></script>
<script src="../../assets/js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->
<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<!--            <script src="../../assets/plugins/isotope/jquery.isotope.min.js"></script>
<script src="../../assets/plugins/lightbox/js/lightbox-2.6.min.js"></script>
<script src="../../assets/js/gallery.demo.min.js"></script>-->
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
                        $(document).ready(function() {
                            App.init();
                            Dashboard.init();
                            FormWizard.init();
                            Negocio.iniciarMapa();
                            
                        });
</script>
</body>
</html>