<?php
/* @var $this MovilController */
/* @var $model Movil */

$this->breadcrumbs=array(
	'Movils'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Movil', 'url'=>array('index')),
	array('label'=>'Create Movil', 'url'=>array('create')),
	array('label'=>'Update Movil', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Movil', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Movil', 'url'=>array('admin')),
);
?>

<h1>View Movil #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'imei',
		'gcmid',
		'id_personal',
		'id_usuarioplaystore',
	),
)); ?>
