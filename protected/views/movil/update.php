<?php
/* @var $this MovilController */
/* @var $model Movil */

$this->breadcrumbs=array(
	'Movils'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Movil', 'url'=>array('index')),
	array('label'=>'Create Movil', 'url'=>array('create')),
	array('label'=>'View Movil', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Movil', 'url'=>array('admin')),
);
?>

<h1>Update Movil <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>