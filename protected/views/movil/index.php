<?php
/* @var $this MovilController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Movils',
);

$this->menu=array(
	array('label'=>'Create Movil', 'url'=>array('create')),
	array('label'=>'Manage Movil', 'url'=>array('admin')),
);
?>

<h1>Movils</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
