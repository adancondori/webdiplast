<?php
/* @var $this MovilController */
/* @var $model Movil */

$this->breadcrumbs=array(
	'Movils'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Movil', 'url'=>array('index')),
	array('label'=>'Manage Movil', 'url'=>array('admin')),
);
?>

<h1>Create Movil</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>