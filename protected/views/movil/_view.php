<?php
/* @var $this MovilController */
/* @var $data Movil */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imei')); ?>:</b>
	<?php echo CHtml::encode($data->imei); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gcmid')); ?>:</b>
	<?php echo CHtml::encode($data->gcmid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_personal')); ?>:</b>
	<?php echo CHtml::encode($data->id_personal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_usuarioplaystore')); ?>:</b>
	<?php echo CHtml::encode($data->id_usuarioplaystore); ?>
	<br />


</div>