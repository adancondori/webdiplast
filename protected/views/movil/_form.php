<?php
/* @var $this MovilController */
/* @var $model Movil */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'movil-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'imei'); ?>
		<?php echo $form->textField($model,'imei',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'imei'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gcmid'); ?>
		<?php echo $form->textField($model,'gcmid',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'gcmid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_personal'); ?>
		<?php echo $form->textField($model,'id_personal'); ?>
		<?php echo $form->error($model,'id_personal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_usuarioplaystore'); ?>
		<?php echo $form->textField($model,'id_usuarioplaystore'); ?>
		<?php echo $form->error($model,'id_usuarioplaystore'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->