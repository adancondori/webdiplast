<?php

/**
 * This is the model class for table "{{imagen}}".
 *
 * The followings are the available columns in table '{{imagen}}':
 * @property integer $id
 * @property string $direccion
 * @property integer $id_producto
 * @property integer $id_categoria
 * @property integer $id_negocio
 * @property integer $id_oferta
 *
 * The followings are the available model relations:
 * @property Producto $idProducto
 * @property Categoria $idCategoria
 * @property Negocio $idNegocio
 * @property Oferta $idOferta
 */
class Imagen extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{imagen}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('direccion', 'required'),
			array('id_producto, id_categoria, id_negocio, id_oferta', 'numerical', 'integerOnly'=>true),
			array('direccion', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, direccion, id_producto, id_categoria, id_negocio, id_oferta', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idProducto' => array(self::BELONGS_TO, 'Producto', 'id_producto'),
			'idCategoria' => array(self::BELONGS_TO, 'Categoria', 'id_categoria'),
			'idNegocio' => array(self::BELONGS_TO, 'Negocio', 'id_negocio'),
			'idOferta' => array(self::BELONGS_TO, 'Oferta', 'id_oferta'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'direccion' => 'Direccion',
			'id_producto' => 'Id Producto',
			'id_categoria' => 'Id Categoria',
			'id_negocio' => 'Id Negocio',
			'id_oferta' => 'Id Oferta',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('id_producto',$this->id_producto);
		$criteria->compare('id_categoria',$this->id_categoria);
		$criteria->compare('id_negocio',$this->id_negocio);
		$criteria->compare('id_oferta',$this->id_oferta);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Imagen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
