<?php

/**
 * This is the model class for table "{{personal}}".
 *
 * The followings are the available columns in table '{{personal}}':
 * @property integer $id
 * @property string $nombre
 * @property string $apellidos
 * @property string $sexo
 * @property string $fechanacimiento
 * @property string $direccion
 * @property string $telefono
 * @property string $cargo
 * @property string $ci
 * @property string $email
 * @property string $tipo
 * @property string $create
 * @property string $change
 * @property integer $createby
 * @property integer $changeby
 * @property integer $estado
 *
 * The followings are the available model relations:
 * @property Asignacionzona[] $asignacionzonas
 * @property Movil[] $movils
 * @property Pedido[] $pedidos
 * @property Pedido[] $pedidos1
 * @property Posicion[] $posicions
 * @property Usuario[] $usuarios
 */
class Personal extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{personal}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre, apellidos', 'required'),
            array('createby, changeby, estado', 'numerical', 'integerOnly' => true),
            array('nombre, apellidos', 'length', 'max' => 50),
            array('sexo, tipo', 'length', 'max' => 1),
            array('direccion', 'length', 'max' => 100),
            array('telefono', 'length', 'max' => 30),
            array('cargo, email', 'length', 'max' => 45),
            array('ci', 'length', 'max' => 15),
            array('fechanacimiento, create, change', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nombre, apellidos, sexo, fechanacimiento, direccion, telefono, cargo, ci, email, tipo, create, change, createby, changeby, estado', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'asignacionzonas' => array(self::HAS_MANY, 'Asignacionzona', 'id_personal'),
            'movils' => array(self::HAS_MANY, 'Movil', 'id_personal'),
            'pedidos' => array(self::HAS_MANY, 'Pedido', 'id_promotor'),
            'pedidos1' => array(self::HAS_MANY, 'Pedido', 'id_entregador'),
            'posicions' => array(self::HAS_MANY, 'Posicion', 'id_personal'),
            'usuarios' => array(self::HAS_MANY, 'Usuario', 'id_personal'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'sexo' => 'Sexo',
            'fechanacimiento' => 'Fechanacimiento',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'cargo' => 'Cargo',
            'ci' => 'Ci',
            'email' => 'Email',
            'tipo' => 'Tipo',
            'create' => 'Create',
            'change' => 'Change',
            'createby' => 'Createby',
            'changeby' => 'Changeby',
            'estado' => 'Estado',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nombre', $this->nombre, true);
        $criteria->compare('apellidos', $this->apellidos, true);
        $criteria->compare('sexo', $this->sexo, true);
        $criteria->compare('fechanacimiento', $this->fechanacimiento, true);
        $criteria->compare('direccion', $this->direccion, true);
        $criteria->compare('telefono', $this->telefono, true);
        $criteria->compare('cargo', $this->cargo, true);
        $criteria->compare('ci', $this->ci, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('tipo', $this->tipo, true);
        $criteria->compare('create', $this->create, true);
        $criteria->compare('change', $this->change, true);
        $criteria->compare('createby', $this->createby);
        $criteria->compare('changeby', $this->changeby);
        $criteria->compare('estado', $this->estado);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Personal the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function listNombreCompleto() {
        $LC = $this->findAll();
        $array = array();
        for ($i = 0; $i < sizeof($LC); $i++) {
            $C = $LC[$i];
            $array[$C->attributes['id']] = $C->attributes['nombre'] . ' ' . $C->attributes['apellidos'];
        }
        return $array;
    }

}
