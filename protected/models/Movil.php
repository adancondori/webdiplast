<?php

/**
 * This is the model class for table "{{movil}}".
 *
 * The followings are the available columns in table '{{movil}}':
 * @property integer $id
 * @property string $imei
 * @property string $gcmid
 * @property integer $id_personal
 * @property integer $id_usuarioplaystore
 *
 * The followings are the available model relations:
 * @property Personal $idPersonal
 * @property Usuarioplaystore $idUsuarioplaystore
 * @property Ofertamovil[] $ofertamovils
 */
class Movil extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{movil}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('imei, gcmid', 'required'),
            array('id_personal, id_usuarioplaystore', 'numerical', 'integerOnly' => true),
            array('imei', 'length', 'max' => 50),
            array('gcmid', 'length', 'max' => 300),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, imei, gcmid, id_personal, id_usuarioplaystore', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idPersonal' => array(self::BELONGS_TO, 'Personal', 'id_personal'),
            'idUsuarioplaystore' => array(self::BELONGS_TO, 'Usuarioplaystore', 'id_usuarioplaystore'),
            'ofertamovils' => array(self::HAS_MANY, 'Ofertamovil', 'id_movil'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'imei' => 'Imei',
            'gcmid' => 'Gcmid',
            'id_personal' => 'Id Personal',
            'id_usuarioplaystore' => 'Id Usuarioplaystore',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('imei', $this->imei, true);
        $criteria->compare('gcmid', $this->gcmid, true);
        $criteria->compare('id_personal', $this->id_personal);
        $criteria->compare('id_usuarioplaystore', $this->id_usuarioplaystore);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Movil the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function behaviors() {
        return array(
            'RestModelBehavior' => array(
                'class' => 'application.extensions.wrest.behaviors.WRestModelBehavior',
            )
        );
    }

}
