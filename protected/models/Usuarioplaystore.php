<?php

/**
 * This is the model class for table "{{usuarioplaystore}}".
 *
 * The followings are the available columns in table '{{usuarioplaystore}}':
 * @property integer $id
 * @property string $nombre
 * @property string $fechanacimiento
 * @property string $sexo
 * @property string $email
 * @property string $telefono
 *
 * The followings are the available model relations:
 * @property Movil[] $movils
 */
class Usuarioplaystore extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{usuarioplaystore}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre', 'length', 'max' => 200),
            array('sexo', 'length', 'max' => 1),
            array('email', 'length', 'max' => 50),
            array('telefono', 'length', 'max' => 20),
            array('fechanacimiento', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nombre, fechanacimiento, sexo, email, telefono', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'movils' => array(self::HAS_MANY, 'Movil', 'id_usuarioplaystore'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nombre' => 'Nombre',
            'fechanacimiento' => 'Fechanacimiento',
            'sexo' => 'Sexo',
            'email' => 'Email',
            'telefono' => 'Telefono',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nombre', $this->nombre, true);
        $criteria->compare('fechanacimiento', $this->fechanacimiento, true);
        $criteria->compare('sexo', $this->sexo, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('telefono', $this->telefono, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Usuarioplaystore the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function behaviors() {
        return array(
            'RestModelBehavior' => array(
                'class' => 'application.extensions.wrest.behaviors.WRestModelBehavior',
            )
        );
    }

}
