<?php

/**
 * This is the model class for table "{{pedido}}".
 *
 * The followings are the available columns in table '{{pedido}}':
 * @property integer $id
 * @property string $numero
 * @property string $fechapedido
 * @property string $fechaentrega
 * @property string $estado
 * @property double $montototal
 * @property string $create
 * @property string $change
 * @property integer $createby
 * @property integer $changeby
 * @property integer $id_negocio
 * @property integer $id_promotor
 * @property integer $id_entregador
 *
 * The followings are the available model relations:
 * @property Detallepedido[] $detallepedidos
 * @property Negocio $idNegocio
 * @property Personal $idPromotor
 * @property Personal $idEntregador
 */
class Pedido extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    const PENDIENTE = 'PENDIENTE';
    const CANCELADO = 'CANCELADO';
    const ENTREGADO = 'ENTREGADO';

    public function tableName() {
        return '{{pedido}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('numero, estado, montototal, create, id_negocio', 'required'),
            array('createby, changeby, id_negocio, id_promotor, id_entregador', 'numerical', 'integerOnly' => true),
            array('montototal', 'numerical'),
            array('numero', 'length', 'max' => 50),
            array('estado', 'length', 'max' => 10),
            array('fechapedido, fechaentrega, change', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, numero, fechapedido, fechaentrega, estado, montototal, create, change, createby, changeby, id_negocio, id_promotor, id_entregador', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'detallepedidos' => array(self::HAS_MANY, 'Detallepedido', 'id_pedido'),
            'idNegocio' => array(self::BELONGS_TO, 'Negocio', 'id_negocio'),
            'idPromotor' => array(self::BELONGS_TO, 'Personal', 'id_promotor'),
            'idEntregador' => array(self::BELONGS_TO, 'Personal', 'id_entregador'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'numero' => 'Numero',
            'fechapedido' => 'Fechapedido',
            'fechaentrega' => 'Fechaentrega',
            'estado' => 'Estado',
            'montototal' => 'Montototal',
            'create' => 'Create',
            'change' => 'Change',
            'createby' => 'Createby',
            'changeby' => 'Changeby',
            'id_negocio' => 'Negocio',
            'id_promotor' => 'Promotor',
            'id_entregador' => 'Entregador',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('numero', $this->numero, true);
        $criteria->compare('fechapedido', $this->fechapedido, true);
        $criteria->compare('fechaentrega', $this->fechaentrega, true);
        $criteria->compare('estado', $this->estado, true);
        $criteria->compare('montototal', $this->montototal);
        $criteria->compare('create', $this->create, true);
        $criteria->compare('change', $this->change, true);
        $criteria->compare('createby', $this->createby);
        $criteria->compare('changeby', $this->changeby);
        $criteria->compare('id_negocio', $this->id_negocio);
        $criteria->compare('id_promotor', $this->id_promotor);
        $criteria->compare('id_entregador', $this->id_entregador);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Pedido the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function cargarObject($objeto) {

        $this->setAttribute('numero', $objeto->numero);
        $this->setAttribute('fechapedido', $objeto->fechapedido);
        $this->setAttribute('montototal', $objeto->montototal);
        $this->setAttribute('id_negocio', $objeto->id_negocio);
        $this->setAttribute('id_promotor', $objeto->id_promotor);
    }

    public function behaviors() {
        return array(
            'RestModelBehavior' => array(
                'class' => 'application.extensions.wrest.behaviors.WRestModelBehavior',
            )
        );
    }
    /*public function scopes() {
        return array(
            'bystatus' => array('order' => 'fechapedido ASC'),
        );
    }*/
}
