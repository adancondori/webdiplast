<?php

/**
 * This is the model class for table "{{detallecompra}}".
 *
 * The followings are the available columns in table '{{detallecompra}}':
 * @property integer $id
 * @property integer $id_producto
 * @property integer $id_compra
 * @property integer $cantidad
 * @property double $costo
 *
 * The followings are the available model relations:
 * @property Producto $idProducto
 * @property Compra $idCompra
 */
class Detallecompra extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{detallecompra}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_producto, id_compra', 'required'),
            array('id_producto, id_compra, cantidad', 'numerical', 'integerOnly' => true),
            array('costo', 'numerical'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, id_producto, id_compra, cantidad, costo', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idProducto' => array(self::BELONGS_TO, 'Producto', 'id_producto'),
            'idCompra' => array(self::BELONGS_TO, 'Compra', 'id_compra'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'id_producto' => 'Id Producto',
            'id_compra' => 'Id Compra',
            'cantidad' => 'Cantidad',
            'costo' => 'Costo',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('id_producto', $this->id_producto);
        $criteria->compare('id_compra', $this->id_compra);
        $criteria->compare('cantidad', $this->cantidad);
        $criteria->compare('costo', $this->costo);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Detallecompra the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function behaviors() {
        return array(
            'RestModelBehavior' => array(
                'class' => 'application.extensions.wrest.behaviors.WRestModelBehavior',
            )
        );
    }

}
