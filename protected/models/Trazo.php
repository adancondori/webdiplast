<?php

/**
 * This is the model class for table "{{trazo}}".
 *
 * The followings are the available columns in table '{{trazo}}':
 * @property integer $id
 * @property double $latitud
 * @property double $longitud
 * @property integer $indice
 * @property integer $id_zona
 *
 * The followings are the available model relations:
 * @property Zona $idZona
 */
class Trazo extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{trazo}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('latitud, longitud, indice, id_zona', 'required'),
            array('indice, id_zona', 'numerical', 'integerOnly' => true),
            array('latitud, longitud', 'numerical'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, latitud, longitud, indice, id_zona', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idZona' => array(self::BELONGS_TO, 'Zona', 'id_zona'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'latitud' => 'Latitud',
            'longitud' => 'Longitud',
            'indice' => 'Indice',
            'id_zona' => 'Id Zona',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('latitud', $this->latitud);
        $criteria->compare('longitud', $this->longitud);
        $criteria->compare('indice', $this->indice);
        $criteria->compare('id_zona', $this->id_zona);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Trazo the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function behaviors() {
        return array(
            'RestModelBehavior' => array(
                'class' => 'application.extensions.wrest.behaviors.WRestModelBehavior',
            )
        );
    }

}
