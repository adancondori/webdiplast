<?php

/**
 * This is the model class for table "{{cliente}}".
 *
 * The followings are the available columns in table '{{cliente}}':
 * @property integer $id
 * @property string $uuid
 * @property integer $codigo
 * @property string $nombre
 * @property string $apellidos
 * @property string $sexo
 * @property string $fechanacimiento
 * @property string $ci
 * @property string $direccion
 * @property string $telefono
 * @property string $celular
 * @property string $email
 * @property string $fechacreacion
 * @property string $fechamodificacion
 * @property integer $estado
 *
 * The followings are the available model relations:
 * @property Negocio[] $negocios
 */
class Cliente extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{cliente}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre, apellidos, sexo, fechacreacion, fechamodificacion', 'required'),
            array('codigo, estado', 'numerical', 'integerOnly' => true),
            array('uuid, direccion', 'length', 'max' => 300),
            array('nombre, apellidos', 'length', 'max' => 50),
            array('sexo', 'length', 'max' => 1),
            array('ci', 'length', 'max' => 15),
            array('telefono, celular', 'length', 'max' => 20),
            array('email', 'length', 'max' => 45),
            array('fechanacimiento', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, uuid, codigo, nombre, apellidos, sexo, fechanacimiento, ci, direccion, telefono, celular, email, fechacreacion, fechamodificacion, estado', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'negocios' => array(self::HAS_MANY, 'Negocio', 'id_cliente'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'uuid' => 'Uuid',
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'sexo' => 'Sexo',
            'fechanacimiento' => 'Fechanacimiento',
            'ci' => 'Ci',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'celular' => 'Celular',
            'email' => 'Email',
            'fechacreacion' => 'Fechacreacion',
            'fechamodificacion' => 'Fechamodificacion',
            'estado' => 'Estado',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('uuid', $this->uuid, true);
        $criteria->compare('codigo', $this->codigo);
        $criteria->compare('nombre', $this->nombre, true);
        $criteria->compare('apellidos', $this->apellidos, true);
        $criteria->compare('sexo', $this->sexo, true);
        $criteria->compare('fechanacimiento', $this->fechanacimiento, true);
        $criteria->compare('ci', $this->ci, true);
        $criteria->compare('direccion', $this->direccion, true);
        $criteria->compare('telefono', $this->telefono, true);
        $criteria->compare('celular', $this->celular, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('fechacreacion', $this->fechacreacion, true);
        $criteria->compare('fechamodificacion', $this->fechamodificacion, true);
        $criteria->compare('estado', $this->estado);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Cliente the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function listNombreCompleto() {
        $LC = $this->findAll();
        $array = array();
        for ($i = 0; $i < sizeof($LC); $i++) {
            $C = $LC[$i];
            $array[$C->attributes['id']] = $C->attributes['nombre'] . ' ' . $C->attributes['apellidos'];
        }
        return $array;
    }

    public function cargarObject($objeto) {
        if ($objeto->id != -1) {
            $this->setAttribute('id', $objeto->id);
        }
        $this->setAttribute('uuid', $objeto->uuid);
        $this->setAttribute('codigo', $objeto->codigo);
        $this->setAttribute('nombre', $objeto->nombre);
        $this->setAttribute('apellidos', $objeto->apellidos);
        $this->setAttribute('sexo', $objeto->sexo);
        $this->setAttribute('fechanacimiento', $objeto->fechanacimiento);
        $this->setAttribute('ci', $objeto->ci);
        $this->setAttribute('direccion', $objeto->direccion);
        $this->setAttribute('telefono', $objeto->telefono);
        $this->setAttribute('celular', $objeto->celular);
        $this->setAttribute('email', $objeto->email);
    }

    public function behaviors() {
        return array(
            'RestModelBehavior' => array(
                'class' => 'application.extensions.wrest.behaviors.WRestModelBehavior',
            )
        );
    }

    public function cargar($row) {
        $this->setAttribute('codigo', $row[0]);
        $this->setAttribute('uuid', $row[1]);
        $this->setAttribute('nombre', $row[2]);
        $this->setAttribute('apellidos', $row[3]);
        $this->setAttribute('sexo', $row[4]);
        $this->setAttribute('fechanacimiento', $row[5]);
        $this->setAttribute('ci', $row[6]);
        $this->setAttribute('direccion', $row[7]);
        $this->setAttribute('telefono', $row[8]);
        $this->setAttribute('celular', $row[9]);
        $this->setAttribute('email', $row[10]);
    }

}
