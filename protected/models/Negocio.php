<?php

/**
 * This is the model class for table "{{negocio}}".
 *
 * The followings are the available columns in table '{{negocio}}':
 * @property integer $id
 * @property string $uuid
 * @property string $nombre
 * @property string $razonsocial
 * @property string $nit
 * @property string $direccion
 * @property double $latitud
 * @property double $longitud
 * @property string $observacion
 * @property integer $id_cliente
 * @property integer $id_zona
 * @property string $fechacreacion
 * @property string $fechamodificacion
 * @property integer $estado
 *
 * The followings are the available model relations:
 * @property Imagen[] $imagens
 * @property Cliente $idCliente
 * @property Zona $idZona
 * @property Pedido[] $pedidos
 */
class Negocio extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{negocio}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre, latitud, longitud, id_cliente, id_zona, fechacreacion, fechamodificacion', 'required'),
            array('id_cliente, id_zona, estado', 'numerical', 'integerOnly' => true),
            array('latitud, longitud', 'numerical'),
            array('uuid, direccion', 'length', 'max' => 300),
            array('nombre, razonsocial', 'length', 'max' => 45),
            array('nit', 'length', 'max' => 15),
            array('observacion', 'length', 'max' => 200),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, uuid, nombre, razonsocial, nit, direccion, latitud, longitud, observacion, id_cliente, id_zona, fechacreacion, fechamodificacion, estado', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'imagens' => array(self::HAS_MANY, 'Imagen', 'id_negocio'),
            'idCliente' => array(self::BELONGS_TO, 'Cliente', 'id_cliente'),
            'idZona' => array(self::BELONGS_TO, 'Zona', 'id_zona'),
            'pedidos' => array(self::HAS_MANY, 'Pedido', 'id_negocio'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'uuid' => 'Uuid',
            'nombre' => 'Nombre',
            'razonsocial' => 'Razonsocial',
            'nit' => 'Nit',
            'direccion' => 'Direccion',
            'latitud' => 'Latitud',
            'longitud' => 'Longitud',
            'observacion' => 'Observacion',
            'id_cliente' => 'Id Cliente',
            'id_zona' => 'Id Zona',
            'fechacreacion' => 'Fechacreacion',
            'fechamodificacion' => 'Fechamodificacion',
            'estado' => 'Estado',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('uuid', $this->uuid, true);
        $criteria->compare('nombre', $this->nombre, true);
        $criteria->compare('razonsocial', $this->razonsocial, true);
        $criteria->compare('nit', $this->nit, true);
        $criteria->compare('direccion', $this->direccion, true);
        $criteria->compare('latitud', $this->latitud);
        $criteria->compare('longitud', $this->longitud);
        $criteria->compare('observacion', $this->observacion, true);
        $criteria->compare('id_cliente', $this->id_cliente);
        $criteria->compare('id_zona', $this->id_zona);
        $criteria->compare('fechacreacion', $this->fechacreacion, true);
        $criteria->compare('fechamodificacion', $this->fechamodificacion, true);
        $criteria->compare('estado', $this->estado);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Negocio the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function cargarObject($objeto) {
        if ($objeto->id != -1) {
            $this->setAttribute('id', $objeto->id);
        }
        $this->setAttribute('uuid', $objeto->uuid);
        $this->setAttribute('nombre', $objeto->nombre);
        $this->setAttribute('razonsocial', $objeto->razonsocial);
        $this->setAttribute('nit', $objeto->nit);
        $this->setAttribute('direccion', $objeto->direccion);
        $this->setAttribute('latitud', $objeto->latitud);
        $this->setAttribute('longitud', $objeto->longitud);
        $this->setAttribute('observacion', $objeto->observacion);
        $this->setAttribute('id_cliente', $objeto->id_cliente);
        $this->setAttribute('id_zona', $objeto->id_zona);
    }

    public function behaviors() {
        return array(
            'RestModelBehavior' => array(
                'class' => 'application.extensions.wrest.behaviors.WRestModelBehavior',
            )
        );
    }

    public function cargar($row) {
        $this->setAttribute('uuid', $row[0]);
        $this->setAttribute('nombre', $row[1]);
        $this->setAttribute('razonsocial', $row[2]);
        $this->setAttribute('nit', $row[3]);
        $this->setAttribute('direccion', $row[4]);
        $this->setAttribute('latitud', $row[5]);
        $this->setAttribute('longitud', $row[6]);
        $this->setAttribute('observacion', $row[7]);
        $uuid = $row[8];
        $C = Cliente::model()->findByAttributes(array("uuid" => $uuid));
        $this->setAttribute('id_cliente', $C->id);
        $this->setAttribute('id_zona', $row[9]);
        $this->setAttribute('fechacreacion', $row[10]);
        $this->setAttribute('fechamodificacion', $row[11]);
        $this->setAttribute('estado', $row[12]);
    }

}
