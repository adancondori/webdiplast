<?php

/**
 * This is the model class for table "{{posicion}}".
 *
 * The followings are the available columns in table '{{posicion}}':
 * @property integer $id
 * @property double $latitud
 * @property double $longitud
 * @property string $fechahora
 * @property string $texto
 * @property integer $id_personal
 *
 * The followings are the available model relations:
 * @property Personal $idPersonal
 */
class Posicion extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{posicion}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('latitud, longitud, fechahora, id_personal', 'required'),
            array('id_personal', 'numerical', 'integerOnly' => true),
            array('latitud, longitud', 'numerical'),
            array('texto', 'length', 'max' => 200),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, latitud, longitud, fechahora, texto, id_personal', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idPersonal' => array(self::BELONGS_TO, 'Personal', 'id_personal'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'latitud' => 'Latitud',
            'longitud' => 'Longitud',
            'fechahora' => 'Fechahora',
            'texto' => 'Texto',
            'id_personal' => 'Id Personal',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('latitud', $this->latitud);
        $criteria->compare('longitud', $this->longitud);
        $criteria->compare('fechahora', $this->fechahora, true);
        $criteria->compare('texto', $this->texto, true);
        $criteria->compare('id_personal', $this->id_personal);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Posicion the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function cargarObject($objeto) {

        $this->setAttribute('latitud', $objeto->latitud);
        $this->setAttribute('longitud', $objeto->longitud);
        $this->setAttribute('fechahora', $objeto->fechahora);
        $this->setAttribute('texto', $objeto->texto);
        $this->setAttribute('id_personal', $objeto->id_personal);
    }

    public function behaviors() {
        return array(
            'RestModelBehavior' => array(
                'class' => 'application.extensions.wrest.behaviors.WRestModelBehavior',
            )
        );
    }

}
