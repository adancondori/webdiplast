<?php

/**
 * This is the model class for table "{{asignacionzona}}".
 *
 * The followings are the available columns in table '{{asignacionzona}}':
 * @property integer $id
 * @property integer $id_zona
 * @property integer $id_personal
 * @property string $dia
 *
 * The followings are the available model relations:
 * @property Personal $idPersonal
 * @property Zona $idZona
 */
class Asignacionzona extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{asignacionzona}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_zona, id_personal, dia', 'required'),
            array('id_zona, id_personal', 'numerical', 'integerOnly' => true),
            array('dia', 'length', 'max' => 10),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, id_zona, id_personal, dia', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idPersonal' => array(self::BELONGS_TO, 'Personal', 'id_personal'),
            'idZona' => array(self::BELONGS_TO, 'Zona', 'id_zona'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'id_zona' => 'Zona',
            'id_personal' => 'Personal',
            'dia' => 'Día',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('id_zona', $this->id_zona);
        $criteria->compare('id_personal', $this->id_personal);
        $criteria->compare('dia', $this->dia, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Asignacionzona the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function behaviors() {
        return array(
            'RestModelBehavior' => array(
                'class' => 'application.extensions.wrest.behaviors.WRestModelBehavior',
            )
        );
    }

}
