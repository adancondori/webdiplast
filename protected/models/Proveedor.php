<?php

/**
 * This is the model class for table "{{proveedor}}".
 *
 * The followings are the available columns in table '{{proveedor}}':
 * @property integer $id
 * @property string $nombre
 * @property string $razonsocial
 * @property string $nit
 * @property string $direccion
 * @property string $telefono
 * @property integer $estado
 */
class Proveedor extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{proveedor}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('estado', 'numerical', 'integerOnly' => true),
            array('nombre, razonsocial, nit', 'length', 'max' => 50),
            array('direccion', 'length', 'max' => 100),
            array('telefono', 'length', 'max' => 30),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nombre, razonsocial, nit, direccion, telefono, estado', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nombre' => 'Nombre',
            'razonsocial' => 'Razonsocial',
            'nit' => 'Nit',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'estado' => 'Estado',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nombre', $this->nombre, true);
        $criteria->compare('razonsocial', $this->razonsocial, true);
        $criteria->compare('nit', $this->nit, true);
        $criteria->compare('direccion', $this->direccion, true);
        $criteria->compare('telefono', $this->telefono, true);
        $criteria->compare('estado', $this->estado);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Proveedor the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function behaviors() {
        return array(
            'RestModelBehavior' => array(
                'class' => 'application.extensions.wrest.behaviors.WRestModelBehavior',
            )
        );
    }

}
