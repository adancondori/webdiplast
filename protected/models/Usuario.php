<?php

/**
 * This is the model class for table "{{usuario}}".
 *
 * The followings are the available columns in table '{{usuario}}':
 * @property integer $id
 * @property string $usuario
 * @property string $contrasena
 * @property string $email
 * @property string $conectado
 * @property integer $id_personal
 *
 * The followings are the available model relations:
 * @property Personal $idPersonal
 */
class Usuario extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{usuario}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('usuario, contrasena, id_personal', 'required'),
            array('id_personal', 'numerical', 'integerOnly' => true),
            array('usuario, contrasena, email', 'length', 'max' => 45),
            array('conectado', 'length', 'max' => 1),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, usuario, contrasena, email, conectado, id_personal', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idPersonal' => array(self::BELONGS_TO, 'Personal', 'id_personal'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'usuario' => 'Usuario',
            'contrasena' => 'Contraseña',
            'email' => 'Email',
            'conectado' => 'Conectado',
            'id_personal' => 'Personal',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('usuario', $this->usuario, true);
        $criteria->compare('contrasena', $this->contrasena, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('conectado', $this->conectado, true);
        $criteria->compare('id_personal', $this->id_personal);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Usuario the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function behaviors() {
        return array(
            'RestModelBehavior' => array(
                'class' => 'application.extensions.wrest.behaviors.WRestModelBehavior',
            )
        );
    }

}
