<?php

/**
 * This is the model class for table "{{producto}}".
 *
 * The followings are the available columns in table '{{producto}}':
 * @property integer $id
 * @property string $codigo
 * @property string $nombre
 * @property string $descripcion
 * @property string $medida
 * @property double $costo
 * @property double $precio
 * @property integer $stock
 * @property double $porcentage
 * @property integer $id_categoria
 * @property string $create
 * @property string $change
 * @property integer $createby
 * @property integer $changeby
 * @property integer $estado
 *
 * The followings are the available model relations:
 * @property Detallecompra[] $detallecompras
 * @property Detallepedido[] $detallepedidos
 * @property Imagen[] $imagens
 * @property Oferta[] $ofertas
 * @property Categoria $idCategoria
 */
class Producto extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{producto}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre, stock, id_categoria', 'required'),
            array('stock, id_categoria, createby, changeby, estado', 'numerical', 'integerOnly' => true),
            array('costo, precio, porcentage', 'numerical'),
            array('codigo', 'length', 'max' => 50),
            array('nombre', 'length', 'max' => 45),
            array('descripcion', 'length', 'max' => 200),
            array('medida', 'length', 'max' => 30),
            array('create, change', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, codigo, nombre, descripcion, medida, costo, precio, stock, porcentage, id_categoria, create, change, createby, changeby, estado', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'detallecompras' => array(self::HAS_MANY, 'Detallecompra', 'id_producto'),
            'detallepedidos' => array(self::HAS_MANY, 'Detallepedido', 'id_producto'),
            'imagens' => array(self::HAS_MANY, 'Imagen', 'id_producto'),
            'ofertas' => array(self::HAS_MANY, 'Oferta', 'id_producto'),
            'idCategoria' => array(self::BELONGS_TO, 'Categoria', 'id_categoria'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'codigo' => 'Código',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'medida' => 'Medida',
            'costo' => 'Costo',
            'precio' => 'Precio',
            'stock' => 'Stock',
            'porcentage' => 'Porcentage',
            'id_categoria' => 'Id Categoria',
            'create' => 'Create',
            'change' => 'Change',
            'createby' => 'Createby',
            'changeby' => 'Changeby',
            'estado' => 'Estado',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('codigo', $this->codigo, true);
        $criteria->compare('nombre', $this->nombre, true);
        $criteria->compare('descripcion', $this->descripcion, true);
        $criteria->compare('medida', $this->medida, true);
        $criteria->compare('costo', $this->costo);
        $criteria->compare('precio', $this->precio);
        $criteria->compare('stock', $this->stock);
        $criteria->compare('porcentage', $this->porcentage);
        $criteria->compare('id_categoria', $this->id_categoria);
        $criteria->compare('create', $this->create, true);
        $criteria->compare('change', $this->change, true);
        $criteria->compare('createby', $this->createby);
        $criteria->compare('changeby', $this->changeby);
        $criteria->compare('estado', $this->estado);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Producto the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function behaviors() {
        return array(
            'RestModelBehavior' => array(
                'class' => 'application.extensions.wrest.behaviors.WRestModelBehavior',
            )
        );
    }

    public function cargar($row) {
        $this->setAttribute('codigo', $row[0]);
        $this->setAttribute('nombre', $row[1]);
        $this->setAttribute('descripcion', $row[2]);
        $this->setAttribute('medida', $row[3]);
        $this->setAttribute('precio', $row[4]);
        $this->setAttribute('stock', $row[5]);
        $this->setAttribute('id_categoria', $row[6]);
    }

}
