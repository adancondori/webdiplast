<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/webdiplast/protected/controllers/SincController.php';

class WRestServiciosAction extends CAction {

    public $scenario = '';
    public $PATH_IMAGE = 'assets/upload/server/php/files/';
    public $sincController;

    public function run() {

        $this->sincController = new SincController;
        try {

            switch ($_GET['modo']) {
                case 'monitoreo':

                    $this->sincController->monitoreo();
                    break;
                case 'login':

                    $this->sincController->login();
                    break;
                case 'sinccategoria':

                    $this->sincController->sinccategoria();
                    break;
                case 'sincproducto':

                    $this->sincController->sincproducto();
                    break;
                case 'sinczona':

                    $this->sincController->sinczona();
                    break;
                case 'sinccliente':

                    $this->sincController->sinccliente();
                    break;
                case 'sincnegocio':

                    $this->sincController->sincnegocio();
                    break;
                case 'sincimagen':

                    $this->sincController->sincimagen();
                    break;
                case 'sincimagenproducto':

                    $this->sincController->sincimagenproducto();
                    break;
                case 'sincimagennegocio':

                    $this->sincController->sincimagennegocio();
                    break;
                case 'enviarpedido':

                    $this->sincController->enviarpedido();
                    break;
                case 'modificarpedido':

                    $this->sincController->modificarpedido();
                    break;
                case 'cancelarpedido':

                    $this->sincController->cancelarpedido();
                    break;
                case 'registrarcliente':

                    $this->sincController->registrarcliente();
                    break;
                case 'registrarnegocio':

                    $this->sincController->registrarnegocio();
                    break;
                case 'sincpedido':

                    $this->sincController->sincpedido();
                    break;
                case 'registrarmovil':

                    $this->sincController->registrarmovil();
                    break;
                case 'registrarimagennegocio':

                    $this->sincController->registrarimagennegocio();
                    break;
                case 'entregarpedido':

                    $this->sincController->entregarpedido();
                    break;
                default:
                    break;
            }
        } catch (Exception $e) {
            $resp['msg'] = 'error';
            echo json_encode($resp);
        }
    }

}
