SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `tbl_asignacionzona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_zona` int(11) NOT NULL,
  `id_personal` int(11) NOT NULL,
  `dia` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_asignacionzona_tbl_zona1_idx` (`id_zona`),
  KEY `fk_tbl_asignacionzona_tbl_personal1_idx` (`id_personal`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

INSERT INTO `tbl_asignacionzona` (`id`, `id_zona`, `id_personal`, `dia`) VALUES
(3, 1, 2, 'Lunes'),
(4, 131, 2, 'Miercoles'),
(5, 2, 1, 'Lunes'),
(6, 29, 1, 'Martes');

CREATE TABLE IF NOT EXISTS `tbl_categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `fechacreacion` date NOT NULL,
  `fechamodificacion` date NOT NULL,
  `estado` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

INSERT INTO `tbl_categoria` (`id`, `nombre`, `descripcion`, `fechacreacion`, `fechamodificacion`, `estado`) VALUES
(17, 'Cubiertos123', 'Se presenta Cucharas, Cuhillos, Cubiertos', '0000-00-00', '2015-03-13', 1),
(18, 'Envases', 'Vasos y otros', '0000-00-00', '0000-00-00', 1),
(19, 'Popotes', 'Bombillas plásticas', '0000-00-00', '0000-00-00', 1),
(20, 'Vasos desechables', 'Vasos variados en forma y tamaño', '0000-00-00', '0000-00-00', 1),
(23, 'Plato Termico', 'Platos de plastofor', '0000-00-00', '0000-00-00', 1),
(24, 'Charolas', 'Material Plástico', '0000-00-00', '0000-00-00', 1),
(25, 'Producto Termicos', 'Productos Termicos con con Diseño:  se presentan Productos con Diseños en vasos platos y otros', '0000-00-00', '0000-00-00', 1),
(26, 'Contenedores', 'Material Plástico u Plastofor', '0000-00-00', '0000-00-00', 1),
(27, 'Bolsas', 'De distintos materiales', '0000-00-00', '0000-00-00', 1),
(28, 'Papel', 'Distintos tamaños', '0000-00-00', '0000-00-00', 1),
(29, 'Varios', 'De distintos tipos', '0000-00-00', '0000-00-00', 1),
(30, 'Botellas', 'Distintos tamaños', '0000-00-00', '0000-00-00', 1),
(31, 'Vita film', 'Distintas medidas', '0000-00-00', '0000-00-00', 1),
(32, 'Zoom', 'zoom bolivia', '0000-00-00', '0000-00-00', 1),
(33, 'Bolsas´'' *á', 'Bolsascon simbolos', '0000-00-00', '0000-00-00', 1);

CREATE TABLE IF NOT EXISTS `tbl_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(300) NOT NULL DEFAULT '0',
  `codigo` int(11) DEFAULT '0',
  `nombre` varchar(50) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `sexo` char(1) NOT NULL,
  `fechanacimiento` date DEFAULT NULL,
  `ci` varchar(15) DEFAULT NULL,
  `direccion` varchar(300) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `celular` varchar(20) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `fechacreacion` date NOT NULL,
  `fechamodificacion` date NOT NULL,
  `estado` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=161 ;

INSERT INTO `tbl_cliente` (`id`, `uuid`, `codigo`, `nombre`, `apellidos`, `sexo`, `fechanacimiento`, `ci`, `direccion`, `telefono`, `celular`, `email`, `fechacreacion`, `fechamodificacion`, `estado`) VALUES
(142, '2017c2fd-9e9b-4d22-84ac-eded8290e4f3', 0, 'Joel', 'Condori', 'H', '2000-11-19', '133456', 'Los Lotes', '72180342', '72180342', '2@2.com', '0000-00-00', '0000-00-00', 1),
(143, '9f739216-d79d-49c7-bac8-d93956f6b358', 0, 'Alán', 'Cosmos', 'H', '1989-11-19', '7728987', 'Los Lotes', '79050606', '76050606', '2@2.com', '0000-00-00', '0000-00-00', 1),
(144, 'ecd3dfef-c307-4677-aa46-c1803fe488ba', 0, 'Pepe', 'Pedreza', 'H', '2000-11-19', '123546', 'Los PENOCOS', '123546', '123546', '2@2.com', '0000-00-00', '0000-00-00', 1),
(145, '9df318c5-a4a8-4093-a3d0-7f9c7615c8de', 0, 'Jacinto', 'Pachacalla', 'H', '2014-11-26', '123546', 'Los PENOCOS', '72180342', '7728987', '2@2.com', '0000-00-00', '0000-00-00', 1),
(146, '128974e7-beb2-4c74-bd9c-1cad1eca7b14', 0, 'Pancho', 'Lopez', 'H', '1990-12-31', '7705655123', 'B/ Villa Mercedez', '555', '7985', 'pancho@gmail.com', '0000-00-00', '2015-03-13', 1),
(147, '1f4bfc71-79fa-4d43-87ee-c715d172062d', 0, 'Juan', 'Quiroga', 'H', '2015-01-31', '7728987', 'Tutmaso', '79050606', '72180342', '2@2.com', '0000-00-00', '0000-00-00', 1),
(148, 'f609e282-580f-4554-94a9-4752b9d43f94', 0, 'Wilson', 'Ramírez', 'H', '2014-02-04', '7728987', 'Los Lotes', '3340439', '79050606', '2@2.com', '0000-00-00', '0000-00-00', 1),
(149, 'c8785594-60ea-4516-86c1-2ab6569be895', 0, 'Augusto', 'Morales', 'H', '2005-02-04', '7585675', 'B/ Villa', '5386858', '43868568', 'augusto@hotmail.com', '0000-00-00', '0000-00-00', 1),
(150, 'bac76d5e-9e15-4aaa-9b20-a3e698217546', 0, 'Pachacho', 'Peres', 'H', '1925-08-06', '84568565', 'B/ Hfgvg', '75577555', '568588', 'freddy553g@jk.com', '0000-00-00', '0000-00-00', 1),
(151, '0', 1341, 'percy', 'fernandez', 'H', '2015-02-27', '43453543', 'direccion', '543456', '356345', 'freddy@gmai.com', '0000-00-00', '0000-00-00', 1),
(152, '0', 1000, 'Dayan', 'Antazana Maldonado', 'M', '2015-03-15', '7728987', 'Villa Flor', '75060606', '79050606', '2@2.com', '0000-00-00', '0000-00-00', 1),
(153, 'af14da3c-2b2f-4afd-9486-e034c3705a42', 0, 'Pedro', 'Pericles', 'H', '2015-03-03', '1785', 'Villa Flor', '79050606', '7728987', '2@2.com', '0000-00-00', '0000-00-00', 1),
(154, '0', 0, 'Julito', 'Peresco', 'H', '2015-03-25', '345234', 'B/ jenecheru', '74358349', '3456234', 'freddy@gmai.com', '0000-00-00', '0000-00-00', 1),
(155, '0', 0, 'Juanito', 'Portugues', 'H', '2015-03-17', '234523', 'B/ Potugal', '3452342234', '34523423', 'freddy@gmai.com', '0000-00-00', '0000-00-00', 1),
(156, '0', 0, 'Oscar', 'Ñandu', 'H', '2015-03-17', '23456234', 'nasduces', '45234', '32452342', 'freddy@gmai.com', '0000-00-00', '0000-00-00', 1),
(157, '0', 0, 'jutntiote', 'chita', 'H', '2015-03-18', '345643', 'B/fiurend', '34563', '546345', 'freddy@gmai.com', '2015-03-13', '2015-03-13', 1),
(158, '0', 0, 'topoyiyo', 'coimbra', 'H', '2015-03-18', '4123412', 'dfaadf', '134123', '2134123', 'freddy@gmai.com', '2015-03-13', '2015-03-13', 1),
(159, '0', 0, 'sdfgsdfg', 'sdfgsd', 'H', '2015-03-17', '5234532', 'fgsd', '435234', '5234', 'freddy@gmai.com', '2015-03-13', '2015-03-13', 1),
(160, '0', 0, 'sdfgsdfg', 'sdfgsd', 'H', '2015-03-17', '5234532', 'fgsd', '435234', '5234', 'freddy@gmai.com', '2015-03-13', '2015-03-13', 1);

CREATE TABLE IF NOT EXISTS `tbl_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `observacion` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tbl_detallecompra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `id_compra` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `costo` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_producto` (`id_producto`),
  KEY `id_compra` (`id_compra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tbl_detallepedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descuento` double DEFAULT NULL,
  `cantidad` int(11) NOT NULL,
  `montototal` double NOT NULL,
  `id_pedido` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_detallepedido_tbl_pedido1_idx` (`id_pedido`),
  KEY `fk_tbl_detallepedido_tbl_producto1_idx` (`id_producto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=180 ;

INSERT INTO `tbl_detallepedido` (`id`, `descuento`, `cantidad`, `montototal`, `id_pedido`, `id_producto`) VALUES
(60, 0, 10, 120, 2, 23),
(61, 0, 8, 200, 2, 87),
(62, 0, 6, 336, 2, 33),
(63, 0, 4, 180, 2, 54),
(120, 0, 7, 182, 4, 42),
(121, 0, 5, 295, 4, 76),
(125, 0, 30, 4500, 5, 22),
(126, 0, 10, 2500, 5, 23),
(127, 0, 50, 1250, 5, 29),
(128, 0, 10, 3300, 5, 24),
(129, 0, 2, 300, 6, 22),
(130, 0, 2, 660, 6, 24),
(135, 0, 9, 1350, 7, 22),
(136, 0, 3, 990, 7, 24),
(137, 0, 4, 1000, 7, 23),
(138, 0, 4, 112, 7, 40),
(139, 0, 2, 50, 7, 31),
(140, 0, 5, 90, 8, 58),
(141, 0, 4, 200, 8, 36),
(145, 0, 4, 60, 3, 24),
(146, 0, 9, 234, 3, 41),
(147, 0, 2, 106, 3, 93),
(148, 0, 4, 88, 3, 25),
(149, 0, 5, 400, 3, 63),
(150, 0, 4, 60, 3, 22),
(151, 0, 3, 36, 3, 23),
(152, 0, 4, 100, 3, 29),
(153, 0, 6, 150, 3, 31),
(154, 0, 3, 87, 3, 55),
(155, 0, 4, 0, 3, 97),
(156, 0, 3, 450, 9, 22),
(162, 0, 3, 450, 11, 22),
(163, 0, 3, 990, 11, 24),
(164, 0, 5, 0, 11, 97),
(165, 0, 2, 60, 11, 44),
(166, 0, 4, 600, 12, 22),
(167, 0, 3, 105, 12, 78),
(168, 0, 4, 168, 12, 86),
(169, 0, 8, 1200, 1, 22),
(170, 0, 10, 120, 1, 23),
(171, 0, 4, 1320, 1, 24),
(172, 0, 3, 75, 1, 29),
(173, 0, 1, 150, 13, 22),
(174, 0, 5, 125, 13, 38),
(175, 0, 3, 75, 13, 39),
(176, 0, 8, 1200, 10, 22),
(177, 0, 2, 44, 10, 25),
(178, 0, 4, 1320, 10, 24),
(179, 0, 6, 0, 10, 97);
DROP TRIGGER IF EXISTS `cancelarpedido`;
DELIMITER //
CREATE TRIGGER `cancelarpedido` AFTER UPDATE ON `tbl_detallepedido`
 FOR EACH ROW begin
update tbl_producto set stock=stock+old.cantidad where id=old.id_producto;
end
//
DELIMITER ;
DROP TRIGGER IF EXISTS `creardetalle`;
DELIMITER //
CREATE TRIGGER `creardetalle` AFTER INSERT ON `tbl_detallepedido`
 FOR EACH ROW BEGIN
UPDATE tbl_producto SET stock=stock - NEW.cantidad WHERE id=NEW.id_producto;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `eliminardetalle`;
DELIMITER //
CREATE TRIGGER `eliminardetalle` AFTER DELETE ON `tbl_detallepedido`
 FOR EACH ROW begin
update tbl_producto set stock=stock+OLD.cantidad where id=OLD.id_producto;
end
//
DELIMITER ;

CREATE TABLE IF NOT EXISTS `tbl_imagen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `direccion` varchar(200) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `id_negocio` int(11) DEFAULT NULL,
  `id_oferta` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_producto` (`id_producto`),
  KEY `id_categoria` (`id_categoria`),
  KEY `id_negocio` (`id_negocio`),
  KEY `id_oferta` (`id_oferta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=168 ;

INSERT INTO `tbl_imagen` (`id`, `direccion`, `id_producto`, `id_categoria`, `id_negocio`, `id_oferta`) VALUES
(48, 'categoria17.jpg', NULL, 17, NULL, NULL),
(49, 'categoria18.jpg', NULL, 18, NULL, NULL),
(50, 'categoria19.jpg', NULL, 19, NULL, NULL),
(51, 'producto22.jpg', 22, NULL, NULL, NULL),
(52, 'categoria20.jpg', NULL, 20, NULL, NULL),
(53, 'producto23.jpg', 23, NULL, NULL, NULL),
(56, 'producto24.jpg', 24, NULL, NULL, NULL),
(57, 'categoria23.jpg', NULL, 23, NULL, NULL),
(58, 'categoria24.png', NULL, 24, NULL, NULL),
(59, 'categoria25.jpg', NULL, 25, NULL, NULL),
(60, 'categoria26.png', NULL, 26, NULL, NULL),
(61, 'producto25.jpg', 25, NULL, NULL, NULL),
(62, 'categoria27.png', NULL, 27, NULL, NULL),
(63, 'producto26.jpg', 26, NULL, NULL, NULL),
(64, 'categoria28.png', NULL, 28, NULL, NULL),
(65, 'producto27.jpg', 27, NULL, NULL, NULL),
(66, 'categoria29.jpg', NULL, 29, NULL, NULL),
(67, 'categoria30.jpg', NULL, 30, NULL, NULL),
(68, 'categoria31.jpg', NULL, 31, NULL, NULL),
(69, 'producto28.jpg', 28, NULL, NULL, NULL),
(70, 'producto29.png', 29, NULL, NULL, NULL),
(71, 'producto30.jpg', 30, NULL, NULL, NULL),
(72, 'producto31.png', 31, NULL, NULL, NULL),
(73, 'producto32.jpg', 32, NULL, NULL, NULL),
(74, 'producto33.jpg', 33, NULL, NULL, NULL),
(75, 'producto34.png', 34, NULL, NULL, NULL),
(76, 'producto35.png', 35, NULL, NULL, NULL),
(77, 'producto36.jpg', 36, NULL, NULL, NULL),
(78, 'producto37.jpg', 37, NULL, NULL, NULL),
(79, 'producto38.jpg', 38, NULL, NULL, NULL),
(80, 'producto39.jpg', 39, NULL, NULL, NULL),
(81, 'producto40.png', 40, NULL, NULL, NULL),
(82, 'producto41.png', 41, NULL, NULL, NULL),
(83, 'producto42.png', 42, NULL, NULL, NULL),
(84, 'producto43.png', 43, NULL, NULL, NULL),
(85, 'producto44.jpg', 44, NULL, NULL, NULL),
(86, 'producto45.jpg', 45, NULL, NULL, NULL),
(87, 'producto46.jpg', 46, NULL, NULL, NULL),
(88, 'producto47.jpg', 47, NULL, NULL, NULL),
(89, 'producto48.png', 48, NULL, NULL, NULL),
(90, 'producto49.png', 49, NULL, NULL, NULL),
(91, 'producto50.jpg', 50, NULL, NULL, NULL),
(92, 'producto51.png', 51, NULL, NULL, NULL),
(93, 'producto52.jpg', 52, NULL, NULL, NULL),
(94, 'producto53.png', 53, NULL, NULL, NULL),
(95, 'producto54.jpg', 54, NULL, NULL, NULL),
(96, 'producto55.png', 55, NULL, NULL, NULL),
(97, 'producto56.png', 56, NULL, NULL, NULL),
(98, 'producto57.jpg', 57, NULL, NULL, NULL),
(99, 'producto58.jpg', 58, NULL, NULL, NULL),
(100, 'producto59.jpg', 59, NULL, NULL, NULL),
(101, 'producto60.jpg', 60, NULL, NULL, NULL),
(102, 'producto61.jpg', 61, NULL, NULL, NULL),
(103, 'producto62.jpg', 62, NULL, NULL, NULL),
(104, 'producto63.jpg', 63, NULL, NULL, NULL),
(105, 'producto64.jpg', 64, NULL, NULL, NULL),
(106, 'producto65.png', 65, NULL, NULL, NULL),
(107, 'producto.jpg', NULL, NULL, NULL, NULL),
(108, 'producto66.png', 66, NULL, NULL, NULL),
(109, 'producto67.jpg', 67, NULL, NULL, NULL),
(110, 'producto68.png', 68, NULL, NULL, NULL),
(111, 'producto69.png', 69, NULL, NULL, NULL),
(112, 'producto70.png', 70, NULL, NULL, NULL),
(113, 'producto71.jpg', 71, NULL, NULL, NULL),
(114, 'producto.png', NULL, NULL, NULL, NULL),
(115, 'producto72.png', 72, NULL, NULL, NULL),
(116, 'producto73.png', 73, NULL, NULL, NULL),
(117, 'producto74.png', 74, NULL, NULL, NULL),
(118, 'producto75.png', 75, NULL, NULL, NULL),
(119, 'producto76.png', 76, NULL, NULL, NULL),
(120, 'producto77.png', 77, NULL, NULL, NULL),
(121, 'producto78.png', 78, NULL, NULL, NULL),
(122, 'producto79.png', 79, NULL, NULL, NULL),
(123, 'producto80.png', 80, NULL, NULL, NULL),
(124, 'producto81.png', 81, NULL, NULL, NULL),
(125, 'producto82.jpg', 82, NULL, NULL, NULL),
(126, 'producto83.jpg', 83, NULL, NULL, NULL),
(127, 'producto84.jpg', 84, NULL, NULL, NULL),
(128, 'producto85.png', 85, NULL, NULL, NULL),
(129, 'producto86.png', 86, NULL, NULL, NULL),
(130, 'producto87.png', 87, NULL, NULL, NULL),
(131, 'producto88.png', 88, NULL, NULL, NULL),
(132, 'producto89.png', 89, NULL, NULL, NULL),
(133, 'producto90.png', 90, NULL, NULL, NULL),
(134, 'producto91.png', 91, NULL, NULL, NULL),
(135, 'producto92.png', 92, NULL, NULL, NULL),
(136, 'producto93.png', 93, NULL, NULL, NULL),
(137, 'producto94.png', 94, NULL, NULL, NULL),
(138, 'producto95.png', 95, NULL, NULL, NULL),
(139, 'negocio94_0.png', NULL, NULL, 94, NULL),
(140, 'negocio95_0.png', NULL, NULL, 95, NULL),
(141, 'negocio95_1.png', NULL, NULL, 95, NULL),
(142, 'negocio95_2.png', NULL, NULL, 95, NULL),
(143, 'negocio96_0.png', NULL, NULL, 96, NULL),
(144, 'negocio96_1.png', NULL, NULL, 96, NULL),
(145, 'negocio96_2.png', NULL, NULL, 96, NULL),
(146, 'negocio97_0.png', NULL, NULL, 97, NULL),
(147, 'producto.jpg', NULL, NULL, NULL, NULL),
(148, 'producto.jpg', NULL, NULL, NULL, NULL),
(149, 'producto.jpg', NULL, NULL, NULL, NULL),
(150, 'negocio98_0.png', NULL, NULL, 98, NULL),
(151, 'imagen (2).jpg', NULL, NULL, NULL, NULL),
(152, 'imagen (3).jpg', NULL, NULL, 99, NULL),
(153, 'categoria32.png', NULL, 32, NULL, NULL),
(154, 'producto96.png', 96, NULL, NULL, NULL),
(155, 'categoria33.jpg', NULL, 33, NULL, NULL),
(156, 'producto97.jpg', 97, NULL, NULL, NULL),
(157, 'imagen (8).jpg', NULL, NULL, 100, NULL),
(158, 'negocio101_0.png', NULL, NULL, 101, NULL),
(159, 'negocio101_1.png', NULL, NULL, 101, NULL),
(160, 'negocio101_2.png', NULL, NULL, 101, NULL),
(161, 'negocio102_0.png', NULL, NULL, 102, NULL),
(162, 'imagen (9).jpg', NULL, NULL, 104, NULL),
(163, 'imagen (10).jpg', NULL, NULL, 105, NULL),
(164, 'imagen (11).jpg', NULL, NULL, 106, NULL),
(165, 'imagen (24).jpg', NULL, NULL, NULL, NULL),
(166, 'imagen (25).jpg', NULL, NULL, NULL, NULL),
(167, 'producto100.jpg', 100, NULL, NULL, NULL);

CREATE TABLE IF NOT EXISTS `tbl_movil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imei` varchar(50) NOT NULL,
  `gcmid` varchar(300) NOT NULL,
  `id_personal` int(11) DEFAULT NULL,
  `id_usuarioplaystore` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_personal` (`id_personal`),
  KEY `id_usuarioplaystore` (`id_usuarioplaystore`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

INSERT INTO `tbl_movil` (`id`, `imei`, `gcmid`, `id_personal`, `id_usuarioplaystore`) VALUES
(1, 'SADFASGFDSGSADHHAFSDGSDF', 'APA91bEaVkqy969m6RePa_HbQOuunurRLZUcD4_XhN0IJ-N_eZpZ1euklasUkNSUjejjG9TgcsYzhdvmkSXgOqFLQ1ku6vyiAzKXOb-5a4FOCI7Gzc3KYwojzQwMkFO0qGbmCRw9VY16H8C3p7GEHnD01SYdBjqUW82a4bV0WwvB3X_waqbCS48', 1, NULL),
(2, 'SADFASGFDSGSADHHAFSDGSDF', 'APA91bFxFbCZn3w9g-JXeH6md67YFCdLfKmHBWbnoPdBi78ZTk5bFcROj6kh1jFZszxmGBqstfcbiz_9S9DcOV2F7P6qnl9jKFIFUAoD7Vt0bWUZhIOyLop3bpdV5-rKgkYGicnKMJgRsfhEQkx0DZHl7GlPMXRJ-rOdeupkuuUHn9pnfj4LYH0', 1, NULL),
(3, 'SADFASGFDSGSADHHAFSDGSDF', 'APA91bEJPnQJO92GL1RUD3fRH3K1d1OGVT_AIpozdGt4TR5tTM6vfVpyX_Xo-H81KJm6TiJEQ0yuFhcWrf0GiwdJ3sFwroXXC9cYM5ksiqgr6w75ZYHxQhvqaf9OWqELXC36LLsdrMlHC86SnKuXLD4d92fyqq9DPBFCHaASdFPEcXUdXu7jc4c', 1, NULL),
(4, 'SADFASGFDSGSADHHAFSDGSDF', 'APA91bEIwf6rQfCD40jIGk-I_xThbhkKfoHgN_0R1BKnSQ2UOOxEs9n62tDQn4kJa8wGMROpEO_uQEZ0AS7VwPIBLzgwSPC7U1JWEl2joBq-Vbrt_zU2qvCUUdaJWGnWD9GaP1DgERwcrp4-pgb4dvM7jXCpjLXNzjJApsR988cLzsGSygc4Zqs', 1, NULL),
(5, 'SADFASGFDSGSADHHAFSDGSDF', 'APA91bElfCrOi0UH0aLaSV-zzQKaEwiq1XdqG4cqOaP9POVSjxDX7Nre8oK5836xmfwLsL9pljsHnDXSEijYiXM4MnrcTmBKJIBtzoAegaBKvPZ4QEk4_7LB2tKjwFHZBHd3zAAjAfjqdei6MQGBkkFCOVXFqzl_AfrICncRmxOWkZAAy0D_iMc', 2, NULL),
(6, 'SADFASGFDSGSADHHAFSDGSDF', 'APA91bElfCrOi0UH0aLaSV-zzQKaEwiq1XdqG4cqOaP9POVSjxDX7Nre8oK5836xmfwLsL9pljsHnDXSEijYiXM4MnrcTmBKJIBtzoAegaBKvPZ4QEk4_7LB2tKjwFHZBHd3zAAjAfjqdei6MQGBkkFCOVXFqzl_AfrICncRmxOWkZAAy0D_iMc', 2, NULL),
(7, 'SADFASGFDSGSADHHAFSDGSDF', 'APA91bFu0oCVW35GgVFzXnVQJAedjCM2MSF443O8cKluyLxeEBy2H3NcRxVKJcRraWd3b-pJEfexWDNrPbBcL_ks4zTJuP76-k4OZKj8S9lhc7zTaArslrBBtksB6oyr_42efHDkxZ9aRMoDidEIQ4xq985DieOgweFOmtYGxWo6qYbEubqLQfk', 1, NULL),
(8, 'SADFASGFDSGSADHHAFSDGSDF', 'APA91bFYabrKTefXpy62yXygLAxjUiLMexHgExArQMDUjokHkKj4IP9EUEMAyz6n2VVv2y5okI9E5k3UkWnYEwNq51UGUNo6kSOJIz-qXhdB87WKwkN0NXr7ZiF4hSDH-qC-AzSTr97Et-zPoZXRW-imIH6din0aya5mK0ZbcVI0QmQ92NXcR18', 2, NULL),
(9, 'SADFASGFDSGSADHHAFSDGSDF', 'APA91bF53Lz_yCT3cXlEagqvmA8cIriTFlcY5n7CtE2f813PVuzs7817OPhOXa9WogUVxlpYxrJzKiWplkhtS-INpehDH0QYohc3xz4dIFuGhwDKxOuXQAyiL2emhPdoI2_VCZtRz0D3VEt6VM6JmwTPEkG12Tbw3LURD75FVWdEWtsVReKK_5U', 1, NULL),
(10, 'SADFASGFDSGSADHHAFSDGSDF', 'APA91bEbE7O8KtHmK8eA01AW8aE_DpTLFOJZh-6zS_gGplPSdPrW7asCjEHBSCRtfX5qiPjXw21EcEWpWCSQ3u0PbHSGvkeng9wdsT4xBL5rhG0P2v76a_bzUlzpSsZ-GeNDCi4uJqQICpYVEhyqpehNvxEu3yix47EG7Yc4m-bqDso0T63VGcc', 2, NULL),
(11, 'SADFASGFDSGSADHHAFSDGSDF', 'APA91bFYabrKTefXpy62yXygLAxjUiLMexHgExArQMDUjokHkKj4IP9EUEMAyz6n2VVv2y5okI9E5k3UkWnYEwNq51UGUNo6kSOJIz-qXhdB87WKwkN0NXr7ZiF4hSDH-qC-AzSTr97Et-zPoZXRW-imIH6din0aya5mK0ZbcVI0QmQ92NXcR18', 2, NULL);

CREATE TABLE IF NOT EXISTS `tbl_negocio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(300) NOT NULL DEFAULT '0',
  `nombre` varchar(45) NOT NULL,
  `razonsocial` varchar(45) DEFAULT NULL,
  `nit` varchar(15) DEFAULT NULL,
  `direccion` varchar(300) DEFAULT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `observacion` varchar(200) DEFAULT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_zona` int(11) NOT NULL,
  `fechacreacion` date NOT NULL,
  `fechamodificacion` date NOT NULL,
  `estado` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_tbl_negocio_tbl_cliente1_idx` (`id_cliente`),
  KEY `fk_tbl_negocio_tbl_zona1_idx` (`id_zona`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=107 ;

INSERT INTO `tbl_negocio` (`id`, `uuid`, `nombre`, `razonsocial`, `nit`, `direccion`, `latitud`, `longitud`, `observacion`, `id_cliente`, `id_zona`, `fechacreacion`, `fechamodificacion`, `estado`) VALUES
(90, '4ce8e214-67a9-4531-bd1b-be1cdf510f8f', 'Baratillo', 'Venta', '3340439', 'LOS LOTES ', -17.753591307747165, -63.223223090171814, 'porton negro ', 142, 1, '0000-00-00', '0000-00-00', 1),
(91, 'e6d9d9cf-7cb8-4ba7-b2ba-c0de4a2f75ca', 'Brítanny', 'Venta', '3352689MINO', 'LOS LOTED', -17.851422231777, -63.177818320692, 'portón ', 143, 1, '0000-00-00', '0000-00-00', 1),
(92, '4f64d4dd-62ff-4c81-abb1-8af3c4e6d4a7', 'Killa', 'Venta', '1671829NBBBJJ', 'IRALA', -17.792794024811, -63.178008422256, 'avenida las Américas ', 144, 2, '0000-00-00', '0000-00-00', 1),
(93, 'df4723af-4fc9-476c-8f07-fecdc1bdd1ba', 'Baratos ', 'Ninguno ', '135899', 'LOS TITAD', -17.84572495788, -63.174331784248, 'nnjjjj', 145, 2, '0000-00-00', '0000-00-00', 1),
(94, '937b07a9-504f-42c4-908f-4a827a0b9e47', 'Lengua', 'Lengua Srl', '24697543235', 'B/ FYHGG', -17.788918419555, -63.182984925807, '', 146, 1, '0000-00-00', '0000-00-00', 1),
(95, 'a08752b0-c691-43b7-8ce1-20e318558e74', 'Cainco', 'Abarrotes', '173849949H44', 'BRANIFF', -17.783308539469, -63.182149082422, 'portón negro', 147, 2, '0000-00-00', '0000-00-00', 1),
(96, '87ca46aa-20cf-45e0-a17b-d238be0b81a2', 'Tienda Yacuiba', 'Venta De Abarrotes ', '7728980ACC', 'Av Yacuiba ', -17.807723257393, -63.185444846749, 'portón negro \n', 148, 29, '0000-00-00', '0000-00-00', 1),
(97, '9b8fb80a-81c8-4195-8cb2-5d5235b289a7', 'Augusto Srl', 'Auditoria Srk', '28568585', 'B/Villa ', -17.855307647512, -63.18323135376, '', 149, 131, '0000-00-00', '0000-00-00', 1),
(98, 'f1cefa05-da71-4d64-a3f4-bf2e96db6afb', 'Los Chichuriros', 'Chichito Srl', '4268424', 'B/ Vilka', -17.856660103317832, -63.18511946126819, '', 150, 131, '0000-00-00', '0000-00-00', 1),
(99, '0', 'percy sa', 'Venta', '2345234', 'direccion', -17.78007412664324, -63.19792985916138, 'ninguna', 151, 6, '0000-00-00', '0000-00-00', 1),
(100, '0', 'Perla', 'Venta de Abarratos', '7897897898789', 'Los Lotes', -17.785304836705016, -63.17916512489319, 'pporton negro', 152, 138, '0000-00-00', '0000-00-00', 1),
(101, '89e134a9-ad8a-4917-b322-e134bb361db7', 'Chariton ', 'Abarrotes ', '1344JEINDN', 'Cañaveral', -17.780465541279, -63.200768642128, 'portón negro ', 153, 33, '0000-00-00', '0000-00-00', 1),
(102, '5ebe4393-ae48-4bcd-a2a0-7a4f668a63b2', 'Fufi', 'Fufi Srl', '274738', 'Centro', -17.783315243847, -63.181963339448, 'ninguna', 142, 8, '0000-00-00', '0000-00-00', 1),
(103, '2f8751be-3497-4cbf-bd37-63175ca54d08', 'Adananin', 'Nan Srl', '2516171', 'B/ Dadi', -17.78299534896, -63.182036094368, 'sjshjs', 142, 8, '0000-00-00', '0000-00-00', 1),
(104, '0', 'julito 1', 'julito sa', '23452345', 'B/ los los', -17.845446982925584, -63.287644386291504, 'ninguna', 154, 16, '0000-00-00', '0000-00-00', 1),
(105, '0', 'Portugal', 'Portugal SRL', '3452345234', 'B/ Portugal', -17.798380941620916, -63.17595720291138, 'ninguna', 155, 7, '0000-00-00', '0000-00-00', 1),
(106, '0', 'Oscar', 'Oscar SRL', '2345234', 'B/ Oscar', -17.78661249028795, -63.20067644119263, 'ninguna', 156, 15, '0000-00-00', '0000-00-00', 1);

CREATE TABLE IF NOT EXISTS `tbl_oferta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `fecha` date DEFAULT NULL,
  `tipo` char(1) DEFAULT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `fechacreacion` date NOT NULL,
  `fechamodificacion` date NOT NULL,
  `estado` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_tbl_oferta_tbl_producto_idx` (`id_producto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `tbl_oferta` (`id`, `nombre`, `descripcion`, `fecha`, `tipo`, `id_producto`, `fechacreacion`, `fechamodificacion`, `estado`) VALUES
(1, 'Oferta 1', 'precios rebajados', '2015-03-11', 'P', 43, '0000-00-00', '0000-00-00', 1),
(2, 'Oferta 2', 'nueva rebaja', '2015-03-17', 'D', 26, '0000-00-00', '0000-00-00', 1);

CREATE TABLE IF NOT EXISTS `tbl_ofertamovil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` varchar(45) NOT NULL,
  `id_movil` int(11) NOT NULL,
  `id_oferta` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_movil` (`id_movil`),
  KEY `id_oferta` (`id_oferta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tbl_pedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` varchar(50) NOT NULL,
  `fechapedido` datetime DEFAULT NULL,
  `fechaentrega` datetime DEFAULT NULL,
  `estado` varchar(10) NOT NULL,
  `montototal` double NOT NULL,
  `create` datetime NOT NULL,
  `change` datetime DEFAULT NULL,
  `createby` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `id_negocio` int(11) NOT NULL,
  `id_promotor` int(11) DEFAULT NULL,
  `id_entregador` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_pedido_tbl_negocio1_idx` (`id_negocio`),
  KEY `fk_tbl_pedido_tbl_personal1_idx` (`id_promotor`),
  KEY `fk_tbl_pedido_tbl_personal2_idx` (`id_entregador`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

INSERT INTO `tbl_pedido` (`id`, `numero`, `fechapedido`, `fechaentrega`, `estado`, `montototal`, `create`, `change`, `createby`, `changeby`, `id_negocio`, `id_promotor`, `id_entregador`) VALUES
(1, '8e682883-58db-4c55-ae94-8d6bf96a6bd5', '2015-01-31 11:31:49', NULL, 'PENDIENTE', 2715, '2015-01-31 11:31:41', '2015-03-07 09:17:27', 2, 2, 94, 2, NULL),
(2, '3bc34961-5cb3-4ac5-bd06-7a5a21429f1e', '2014-11-26 22:18:23', NULL, 'PENDIENTE', 836, '2015-01-31 11:41:01', '2015-01-31 12:10:55', 2, 2, 93, 2, NULL),
(3, '43ae8b7a-26c5-460e-bba7-14ce91ba49ae', '2015-01-31 11:32:15', NULL, 'PENDIENTE', 1321, '2015-01-31 13:02:36', '2015-03-06 23:31:44', 1, 1, 95, 1, NULL),
(4, '24e52442-3936-415c-af2e-ab9da260890f', '2015-02-04 00:31:07', NULL, 'CANCELADO', 477, '2015-02-04 00:30:54', '2015-02-04 00:31:33', 1, 1, 96, 1, NULL),
(5, '515a3c12-37be-442e-be9c-6818343c5009', '2015-02-04 00:32:42', NULL, 'ENTREGADO', 11550, '2015-02-04 00:32:27', '2015-02-04 00:33:55', 2, 2, 97, 2, NULL),
(6, '41a6e9b5-f703-4e5b-a272-d537b873567f', '2015-02-04 00:37:11', NULL, 'ENTREGADO', 960, '2015-02-04 00:43:30', '2015-03-06 23:19:12', 1, 1, 96, 1, NULL),
(7, 'a01c7e23-20e1-416b-8088-94caa19c0fe6', '2015-02-04 01:02:11', NULL, 'CANCELADO', 3502, '2015-02-04 01:01:59', '2015-03-06 23:20:18', 1, 1, 96, 1, NULL),
(8, '8f6defb2-29e0-4986-98c5-1428679e76cc', '2015-02-07 01:08:13', NULL, 'PENDIENTE', 290, '2015-02-07 01:08:50', NULL, 2, NULL, 94, 2, NULL),
(9, 'dc5fa0b8-2605-4329-bc17-1553af1bb771', '2015-03-06 23:56:16', NULL, 'PENDIENTE', 450, '2015-03-06 23:56:09', NULL, 2, NULL, 101, 2, NULL),
(10, '8acd17fb-44ad-44b0-9dc4-aaf09f28d779', '2015-03-07 00:26:17', NULL, 'PENDIENTE', 2564, '2015-03-07 00:26:13', '2015-03-07 09:21:36', 2, 2, 90, 2, NULL),
(11, '75630087-296d-4629-a712-7c81de10955d', '2015-03-07 09:13:21', NULL, 'PENDIENTE', 1500, '2015-03-07 09:13:13', '2015-03-07 09:14:13', 2, 2, 98, 2, NULL),
(12, '72faaaeb-e3d8-4b4a-bbf6-151cd0aed1fd', '2015-03-07 09:15:05', NULL, 'PENDIENTE', 873, '2015-03-07 09:16:04', NULL, 2, NULL, 98, 2, NULL),
(13, '96c117e6-6969-4809-a570-a17e71b96fde', '2015-03-07 09:16:59', NULL, 'PENDIENTE', 350, '2015-03-07 09:17:32', NULL, 2, NULL, 94, 2, NULL);

CREATE TABLE IF NOT EXISTS `tbl_personal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `sexo` char(1) DEFAULT NULL,
  `fechanacimiento` date DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(30) DEFAULT NULL,
  `cargo` varchar(45) DEFAULT NULL,
  `ci` varchar(15) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `tipo` char(1) DEFAULT NULL,
  `create` datetime DEFAULT NULL,
  `change` datetime DEFAULT NULL,
  `createby` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `estado` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `tbl_personal` (`id`, `nombre`, `apellidos`, `sexo`, `fechanacimiento`, `direccion`, `telefono`, `cargo`, `ci`, `email`, `tipo`, `create`, `change`, `createby`, `changeby`, `estado`) VALUES
(1, 'Freddy', 'Quispe Fernandez', NULL, NULL, 'B/ Villa Mercedez', '75625642', 'Entregador', '654321', 'freddy@gmail.com', 'P', NULL, NULL, NULL, NULL, 0),
(2, 'Adan', 'Condori Callisaya', NULL, NULL, 'B/ Pedro Diez', '65431', 'Administrador', '6549872', 'adan@gmail.com', 'P', NULL, NULL, NULL, NULL, 0);

CREATE TABLE IF NOT EXISTS `tbl_posicion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `fechahora` datetime NOT NULL,
  `texto` varchar(200) DEFAULT NULL,
  `id_personal` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_personal` (`id_personal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tbl_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(50) DEFAULT NULL,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `medida` varchar(30) DEFAULT NULL,
  `costo` double DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `stock` int(11) NOT NULL,
  `porcentage` double DEFAULT NULL,
  `id_categoria` int(11) NOT NULL,
  `create` datetime DEFAULT NULL,
  `change` datetime DEFAULT NULL,
  `createby` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `estado` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_tbl_producto_tbl_categoria1_idx` (`id_categoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=101 ;

INSERT INTO `tbl_producto` (`id`, `codigo`, `nombre`, `descripcion`, `medida`, `costo`, `precio`, `stock`, `porcentage`, `id_categoria`, `create`, `change`, `createby`, `changeby`, `estado`) VALUES
(22, '1000', 'Cubierto INIX', 'Material: Poliestireno INIX Medidas: Largo 16.5 Marca: Color: Negro Presentación: Caja 1  100 Unidades', '16 cm', 100, 150, 160, 0.5, 17, NULL, NULL, NULL, NULL, 0),
(23, '1001', 'Cucharas', 'Material: Poliestireno Medidas: Largo 13.5cm Marca: Fantasy Color: Blanco Presentación: Caja 1.  24 Unidades ', '13 cm', 200, 250, 667, 0.2, 17, NULL, NULL, NULL, NULL, 0),
(24, '1002', 'Cuchillo', 'Material: Poliestireno Medidas: Largo 17 cm Marca: Fantasy Color: Blanco Presentación: Caja 1  24 Unidades', '17 cm', 300, 330, 773, 0.1, 17, NULL, NULL, NULL, NULL, 0),
(25, '1005', 'Vaso Termico L', 'Vaso térmico de 20 cm con diseño de Frutas', '20 cm x 10 cm', 0, 22, 244, 0, 25, NULL, NULL, NULL, NULL, 0),
(26, '1006', 'Vaso Termico M', 'Vaso termico de Frutas de 10 cm x 5 cm', '10 cm x 5 cm', 0, 18, 350, 0, 25, NULL, NULL, NULL, NULL, 0),
(27, '1007', 'Tapas ', 'Tapas para vasos Termicos', '5 cm', 0, 8, 500, 0, 25, NULL, NULL, NULL, NULL, 0),
(28, '1009', 'Botella Cooler', 'Botella con tomador para liquidos, con formas. de 30 cm :  1 Caja  contiene 12 Unidades ', '30 cm', 0, 25, 400, 0, 25, NULL, NULL, NULL, NULL, 0),
(29, '2001', 'Envase 1 litro Reyma', 'Piezas por paquete: 25; Paquete por caja: 12; Unidad por caja: 300; Capacidad de ml: 1065', '1065 ml', 0, 25, 243, 0, 18, NULL, NULL, NULL, NULL, 0),
(30, '1011', 'Vita Film L', 'Cinta de plastico de 50 cm  transparente  1 caja tiene 6 Unidades', '50 cm', 0, 80, 300, 0, 31, NULL, NULL, NULL, NULL, 0),
(31, '2002', 'Envase 1 Lt. Vamsa', 'Piezas por paquete: 25; Paquete por caja: 12; Unidad por caja:	300; Capacidad de ml:	1065', '1065 ml', 0, 25, 294, 0, 18, NULL, NULL, NULL, NULL, 0),
(32, '1012', 'Vita Film M.  EGA ', 'Materia: Película plástica grado alimenticio. Medidas:  38cm,  1200 Mts. ( 5 Kg ) Marca: EGA PAC Color: Transparente. Presentación: 1 CAJA  6 Unidades', '38 cm', 0, 85, 350, 0, 31, NULL, NULL, NULL, NULL, 0),
(33, '1013', 'Vita Film S.', 'Vita Film pequeño de 15 cm', '15 cm', 0, 56, 294, 0, 31, NULL, NULL, NULL, NULL, 0),
(34, '2003', 'Envase 1/2 Lt. Reyma', 'Piezas por paquete: 25 Paquete por caja: 20 Unidad por caja: 500 Capacidad de ml: 500', '500 ml', 0, 15, 300, 0, 18, NULL, NULL, NULL, NULL, 0),
(35, '2004', 'Envase 1/2 Lt. Vamsa', 'Piezas por paquete: 25 Paquete por caja: 12 Unidad por caja: 300 Capacidad de ml: 600', '600 ml', 0, 15, 300, 0, 18, NULL, NULL, NULL, NULL, 0),
(36, '1014', 'Botella S', 'Botella Pequeña, tranparente de 15 cm', '15 cm', 0, 50, 346, 0, 30, NULL, NULL, NULL, NULL, 0),
(37, '1015', 'Botellon', 'Botellon de 20 litros con medida de 50 cm  por unidad ', '50 cm', 0, 80, 380, 0, 30, NULL, NULL, NULL, NULL, 0),
(38, '1016', 'Botellon Mediano', 'Botellon de 5 Litros de 30  cm . venta por unidad', '30 cm', 0, 25, 345, 0, 30, NULL, NULL, NULL, NULL, 0),
(39, '1016', 'Botellon Mediano', 'Botellon de 5 Litros de 30  cm . venta por unidad', '30 cm', 0, 25, 347, 0, 30, NULL, NULL, NULL, NULL, 0),
(40, '2005', 'Popote corrugado estuchado', '2000 piezas por caja, blanco y natural.', '22 cm', 0, 28, 300, 0, 19, NULL, NULL, NULL, NULL, 0),
(41, '2006', 'Popote corto granel estuchado', '2000 piezas por caja, blanco y natural.', '21 cm', 0, 26, 291, 0, 19, NULL, NULL, NULL, NULL, 0),
(42, '2007', 'Popote en caja corrugado estuchado', '500 piezas por caja / 4 cajas por caja de empaque, blanco y natural.', '22 cm', 0, 26, 300, 0, 19, NULL, NULL, NULL, NULL, 0),
(43, '2008', 'Popote en caja corto estuchado', '500 piezas por caja / 4 cajas por caja de empaque, blanco y natural.', '21 cm', 0, 24, 300, 0, 19, NULL, NULL, NULL, NULL, 0),
(44, '1019', 'Hoja de Papel Aluminio', 'Hojas de papel aluminio de 30 de ancho', '30 cm', 0, 30, 498, 0, 29, NULL, NULL, NULL, NULL, 0),
(45, '1020', 'Hojas de papel aluminio', 'Capacillos rojos No. 72', '5 cm', 0, 5, 350, 0, 29, NULL, NULL, NULL, NULL, 0),
(46, '1021', 'Blondas', 'Blondas para mesa venta por caja. 1 Caja tiene 100 unidades ', '10 cm', 0, 100, 300, 0, 29, NULL, NULL, NULL, NULL, 0),
(47, '1023', 'Tapas para bebidas calientes', 'Material: POLIPRO Medidas: Varias Marca: SOLO CUP y DART Color: Blanca Presentacón:  Caja 10/100   1000 PZ', '8 cm', 0, 60, 300, 0, 29, NULL, NULL, NULL, NULL, 0),
(48, '2009', 'Popote en caja largo estuchado', '500 piezas por caja / 4 cajas por caja de empaque, blanco y natural.', '26 cm', 0, 21, 300, 0, 19, NULL, NULL, NULL, NULL, 0),
(49, '2010', 'Popote en caja rayado y natural', '150 piezas por caja, rayado y natural.', '21 cm', 0, 28, 300, 0, 19, NULL, NULL, NULL, NULL, 0),
(50, '1025', 'Red para cabello Negro', 'Red para cabello de color  negro', '5 cm', 0, 10, 300, 0, 29, NULL, NULL, NULL, NULL, 0),
(51, '2011', 'Popote en caja rayado y natural', '150 piezas por caja, rayado y natural.', '26 cm', 0, 26, 300, 0, 19, NULL, NULL, NULL, NULL, 0),
(52, '2012', 'Popote estuchado papel celofan PRIMO', 'Material: Plástico virgen. Medidas: 21 cm Marca: PRIMO Color: Natural. Presentación: Caja 2000 piezas.', '21 cm', 0, 35, 300, 0, 19, NULL, NULL, NULL, NULL, 0),
(53, '2013', 'Popote largo estuchado', '2000 piezas por caja, blanco y natural.', '26 cm', 0, 32, 300, 0, 19, NULL, NULL, NULL, NULL, 0),
(54, '1027', 'Agitador para café', 'Agitador para café de color marron de 15  cm. 1 Bolsa contiene 100 unidades', '15 cm', 0, 45, 346, 0, 29, NULL, NULL, NULL, NULL, 0),
(55, '2014', 'Popote Neon granel corto', '1,700 pzas. Aproximadamente por paquete. color: verde, amarillo, naranja, rojo y azul', '21 cm', 0, 29, 297, 0, 19, NULL, NULL, NULL, NULL, 0),
(56, '2015', 'Popote Neon granel largo', '1,350 pzas. Aproximadamente por paquete. color: verde, amarillo, naranja, rojo y azul', '26 cm', 0, 32, 300, 0, 19, NULL, NULL, NULL, NULL, 0),
(57, '2016', 'Popote Sipp', 'Popote Sipp', '25 cm', 0, 34, 300, 0, 19, NULL, NULL, NULL, NULL, 0),
(58, '2017', 'Vaso 10 Oz JAGUAR', 'Material: Polipropileno. Medidas: 10 oz. Marca: JAGUAR Color: Natural. Presentación: Caja 20/50   1000 piezas.', '10 oz', 0, 18, 295, 0, 20, NULL, NULL, NULL, NULL, 0),
(59, '1029', 'Higiénico fapsa hd360', 'Higiénico fapsa hd360', '10 cm', 0, 30, 300, 0, 28, NULL, NULL, NULL, NULL, 0),
(60, '2018', 'Vaso 10 Oz REYMA', 'Material: Polipropileno. Medidas: 10 oz. Marca: REYMA Color: Natural. Presentación: Caja 20/50   1000 piezas.', '10 oz', 0, 19, 300, 0, 20, NULL, NULL, NULL, NULL, 0),
(61, '1030', 'Higiénico predut hd 200', 'Higiénico predut hd 200 1 Caja contiene 6 Rollos', '20 cm', 0, 60, 350, 0, 28, NULL, NULL, NULL, NULL, 0),
(62, '2019', 'Vaso 12 Oz REYMA', 'Material: Polipropileno. Medidas: 12 oz. Marca: REYMA Color: Natural. Presentación: Caja 20/50   1000 piezas.', '12 oz', 0, 34, 300, 0, 20, NULL, NULL, NULL, NULL, 0),
(63, '1031', 'Bolsa baja densidad rollo', 'MATERIAL: BAJA DENSIDAD MEDIDAS: 40 X 60 CALIBRES :  150 COLOR: NATURAL PRESENTACION:  ROLLO', '40 cm x 60 cm', 0, 80, 345, 0, 27, NULL, NULL, NULL, NULL, 0),
(64, '2020', 'Vaso para bebidas calientes 4 oz', 'Material: Papel encerado. Medidas: 4 oz. Marca: SOLO CUP Color: Tonalidades en cafe. Presentación:  Caja 20/50   1000 PZ', '4 oz', 0, 29, 300, 0, 20, NULL, NULL, NULL, NULL, 0),
(65, '2021', 'Vaso Poliestireno Molde Cristal 10', 'Vaso transparente, ideal para gelatina en tus fiestas infantiles.  Piezas por paquete	50 Paquete por caja	20 Unidad por caja	1000 Capacidad de ml	75', '10 cm', 0, 26, 300, 0, 20, NULL, NULL, NULL, NULL, 0),
(66, '2022', 'Vaso Poliestireno no. 5 A', 'Piezas por paquete	50 Paquete por caja	20 Unidad por caja	1000 Capacidad de ml	84', '5 cm', 0, 22, 300, 0, 20, NULL, NULL, NULL, NULL, 0),
(67, '1034', 'Bolsa Fuelle Alta Densidad Kilogramos 25×50', ' En fuelle de alta densidad se pueden manejar colores y medidas especiales sobre pedido, consultarlo con su agente de ventas * Aplica restricciones.', '25cm x 50cm', 0, 35, 350, 0, 27, NULL, NULL, NULL, NULL, 0),
(68, '2023', 'Vaso Polipropileno Bicolor No. 16', 'Piezas por paquete	25 Paquete por caja	20 Unidad por caja	500 Capacidad de ml	525', '10 cm', 0, 29, 300, 0, 20, NULL, NULL, NULL, NULL, 0),
(69, '2024', 'Vaso Polipropileno no. 12', 'Piezas por paquete	50 Paquete por caja	20 Unidad por caja	1000 Capacidad de ml	311', '12 cm', 0, 35, 300, 0, 20, NULL, NULL, NULL, NULL, 0),
(70, '1035', 'Bolsa Fuelle Baja Densidad Negro Kilogramos', 'Bolsa Fuelle Baja Densidad Negro Kilogramos', '50 cm', 0, 18, 500, 0, 27, NULL, NULL, NULL, NULL, 0),
(71, '1038', 'Bolsa negra alta densidad', '40 x 48 Pulgada (NEGRA) 24 X 24 Pulgadas ( NATURAL) CALIBRE: 100 COLOR: NEGRO Y NATURAL PRESENTACION: ROLLO SELLO ESTRELLA', '40 x 48 Pulgada', 0, 50, 500, 0, 27, NULL, NULL, NULL, NULL, 0),
(72, '2025', 'Plato pastelero 006', 'Piezas por paquete	25 Paquete por caja	20 Unidad por caja	500 Capacidad de ml	200', '25 cm', 0, 47, 300, 0, 23, NULL, NULL, NULL, NULL, 0),
(73, '2026', 'Plato pozolero PH10', 'Piezas por paquete	25 Paquete por caja	20 Unidad por caja	500 Capacidad de ml	630', '22 cm', 0, 54, 300, 0, 23, NULL, NULL, NULL, NULL, 0),
(74, '2027', 'Plato Térmico 10 división', 'Piezas por paquete	25 Paquete por caja	20 Unidad por caja	500 Capacidad de ml	600', '24 cm', 0, 52, 300, 0, 23, NULL, NULL, NULL, NULL, 0),
(75, '1040', 'Contenedor de poliestireno R500', ' Piezas por paquete	50 Paquete por caja	2 Unidad por caja	100 Capacidad de ml	4000', '30 cm', 0, 150, 500, 0, 26, NULL, NULL, NULL, NULL, 0),
(76, '2028', 'Plato Térmico 9 c2 división', 'Piezas por paquete	25 Paquete por caja	20 Unidad por caja	500 Capacidad de ml	700', '24x28', 0, 59, 300, 0, 23, NULL, NULL, NULL, NULL, 0),
(77, '1041', 'Contenedor 7×7 div doble bisagra', 'Contenedor 7×7 div doble bisagra', '10 cm x 8 cm', 0, 15, 500, 0, 26, NULL, NULL, NULL, NULL, 0),
(78, '1043', 'Contenedor 7×7 liso doble bisagra', 'Contenedor 7×7 liso doble bisagra', '10cm x 8cm', 0, 35, 497, 0, 26, NULL, NULL, NULL, NULL, 0),
(79, '1044', 'Contenedor 8×8 div', 'Contenedor 8×8 div', '8cm x 8cm', 0, 35, 500, 0, 27, NULL, NULL, NULL, NULL, 0),
(80, '1045', 'Contenedor 8×8 liso', 'Contenedor 8×8 liso color Blanco', '8cm x 8cm', 0, 26, 500, 0, 26, NULL, NULL, NULL, NULL, 0),
(81, '1047', 'Contenedor 9×9 div', 'Contenedor 9×9 div color blanco liso', '8cm x 8cm', 0, 28, 500, 0, 26, NULL, NULL, NULL, NULL, 0),
(82, '1049', 'Contenedor Base Negra CPP2415N', 'MATERIAL: POLIESTRILENO CONTENEDOR BASE NEGRA CPP2415N MEDIDAS: LARGO: 24m ANCHO: 15cm ALTO: 6cm CALIBRE: 120 COLOR: BASE NEGRA /DOMO NATURAL PRESENTACION: CAJA 100 PZ AS', '24 cm x 8cm', 0, 45, 500, 0, 26, NULL, NULL, NULL, NULL, 0),
(83, '1050', 'Contenedor Base Negra FH 1613-23N', 'MATERIAL: POLIESTRILENO CONTENEDOR BASE NEGRA FH 1613-23N MEDIDAS: LARGO: 16m ANCHO: 13cm ALTO: 5cm CALIBRE: 120 COLOR: BASE NEGRA /DOMO NATURAL PRESENTACION: CAJA 250 PZ AS', '16cm x 8cm', 0, 35, 500, 0, 26, NULL, NULL, NULL, NULL, 0),
(84, '1051', 'Contenedor Base Negra Redondo', 'MATERIAL: POLIESTRILENO CONTENEDOR BASE NEGRA  FH17N MEDIDAS: DIAMETRO: 17 cm CALIBRE: 120 COLOR: BASE NEGRA /DOMO NATURAL PRESENTACION: CAJA 150 PZ AS', '10cm', 0, 25, 500, 0, 26, NULL, NULL, NULL, NULL, 0),
(85, '1052', 'Contenedor de poliestireno R10', 'piezas por paquete	25 Paquete por caja	4 Unidad por caja	100 Capacidad de ml	1500', '20cm', 0, 56, 500, 0, 26, NULL, NULL, NULL, NULL, 0),
(86, '1053', 'Contenedor de poliestireno R12', ' Piezas por paquete	250 Paquete por caja	1 Unidad por caja	250 Capacidad de ml	650', '15cm', 0, 42, 496, 0, 26, NULL, NULL, NULL, NULL, 0),
(87, '1054', 'Charola 007 reyma', ' Piezas por paquete	50 Paquete por caja	10 Unidad por caja	500 Capacidad de ml	600', '10cm', 0, 25, 492, 0, 24, NULL, NULL, NULL, NULL, 0),
(88, '1055', 'Charola 1014', ' Piezas por paquete	200 Paquete por caja	1 Unidad por caja	200 Capacidad de ml	1060', '10cm', 0, 23, 500, 0, 24, NULL, NULL, NULL, NULL, 0),
(89, '1056', 'Charola 4P', ' Piezas por paquete	300 Paquete por caja	1 Unidad por caja	300 Capacidad de ml	700', '10cm', 0, 53, 500, 0, 24, NULL, NULL, NULL, NULL, 0),
(90, '1057', 'Charola 5D', ' Piezas por paquete	125 Paquete por caja	4 Unidad por caja	500 Capacidad de ml	850', '10cm', 0, 42, 500, 0, 24, NULL, NULL, NULL, NULL, 0),
(91, '1058', 'Charola 8S', ' Piezas por paquete	500 Paquete por caja	1 Unidad por caja	500 Capacidad de ml	520', '10cm', 0, 35, 500, 0, 24, NULL, NULL, NULL, NULL, 0),
(92, '1059', 'Charola 9H', ' Piezas por paquete	200 Paquete por caja	1 Unidad por caja	200 Capacidad de ml	1350', '10cm', 0, 15, 500, 0, 24, NULL, NULL, NULL, NULL, 0),
(93, '2029', 'Plato Térmico 8 división', 'Piezas por paquete	20 Paquete por caja	25 Unidad por caja	500 Capacidad de ml	500', '25 cm', 0, 53, 298, 0, 23, NULL, NULL, NULL, NULL, 0),
(94, '1060', 'Charola 9', ' Piezas por paquete	200 Paquete por caja	1 Unidad por caja	200 Capacidad de ml	1220', '10cm', 0, 42, 500, 0, 24, NULL, NULL, NULL, NULL, 0),
(95, '2030', 'Plato Térmico 9 división', 'Piezas por paquete	25 Paquete por caja	20 Unidad por caja	500 Capacidad de ml	700', '32 cm', 0, 64, 300, 0, 23, NULL, NULL, NULL, NULL, 0),
(96, '150022', 'Preuba', 'nuevo plato compralo ok', '50 x 50', NULL, NULL, 800, NULL, 32, NULL, NULL, NULL, NULL, 0),
(97, '1202', 'Cubierto grandininisimo', 'muy grandototote', '10x14', NULL, NULL, 385, NULL, 17, NULL, NULL, NULL, NULL, 0),
(98, '23423', 'poducto', 'poducto sin r', '12', NULL, NULL, 2332, NULL, 17, '2015-03-13 00:00:00', '2015-03-13 00:00:00', NULL, NULL, 1),
(99, '2345234', 'producto 123', 'descripcion', '123', NULL, NULL, 2432, NULL, 17, '2015-03-13 00:00:00', '2015-03-13 00:00:00', NULL, NULL, 1),
(100, '3546345', 'poducto', 'poducto sin r', '23434', NULL, NULL, 23423, NULL, 17, '2015-03-13 00:00:00', '2015-03-13 00:00:00', NULL, NULL, 1);

CREATE TABLE IF NOT EXISTS `tbl_proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `razonsocial` varchar(50) DEFAULT NULL,
  `nit` varchar(50) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(30) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

INSERT INTO `tbl_proveedor` (`id`, `nombre`, `razonsocial`, `nit`, `direccion`, `telefono`, `estado`) VALUES
(1, 'Proveedor 12', 'pro srtl', '24352342', 'dir', '2435234', NULL);

CREATE TABLE IF NOT EXISTS `tbl_trazo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `indice` int(11) NOT NULL,
  `id_zona` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_trazo_tbl_zona1_idx` (`id_zona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tbl_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) NOT NULL,
  `contrasena` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `conectado` char(1) DEFAULT NULL,
  `id_personal` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_usuario_tbl_personal1_idx` (`id_personal`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `tbl_usuario` (`id`, `usuario`, `contrasena`, `email`, `conectado`, `id_personal`) VALUES
(1, 'freddy', '123456', 'freddy', 'S', 1),
(2, 'adan', '123456', 'adan@gmail.com', 'S', 2);

CREATE TABLE IF NOT EXISTS `tbl_usuarioplaystore` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  `fechanacimiento` date DEFAULT NULL,
  `sexo` char(1) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tbl_zona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `poligono` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=187 ;

INSERT INTO `tbl_zona` (`id`, `nombre`, `descripcion`, `poligono`) VALUES
(1, 'UV-001', '', 'lymkBzhs`Km@ocAhQbPpDvEnBpEe@jH[tEBvEn@jLgL`@'),
(2, 'UV-002', '', 'nsnkB|br`KsBuEeDaEuEiEsEmE_DsD`@wDz@oD~BoFnDiEfE_DdC_Ax@`@v@rEjI|P~EjF`A|A|@rDcEhAsDjBwBtCc@HQPCN@L'),
(3, 'UV-003', '', '|gokB|lq`Ku@uCeB}CcDoD_A_BqG{Mu@eEOgAv@w@|AOzMgB`J_A`FUrFUhAHb@d@jClj@i@n@gI`@aIr@gCXeBU'),
(4, 'UV-004', '', 'lfpkBtgq`K}B_j@Rw@r@YxQUhHC}@dEqAnHk@tCmArDiChEY|AJpAdAhEU^oL|@UU'),
(5, 'UV-005', '', 'vupkBxeq`KiAeGhA}CxBiE`BeEhAqG|@kFNcAp@MpBPdG|ArFxAdFdApD`@bCJf@d@w@dLaBvQmAbLOXa@Gk@i@e@w@[w@k@YiBkBoBoByCcA'),
(6, 'PU-C', '', 'larkBztq`KnFon@`AUhB`@hAZ`BIx@@h@JuFpn@}Bf@}BOmAYm@Q'),
(7, 'UV-006', '', 'rorkBrvq`KrFao@zCXpB~@`BdA|AbCx@dCb@zDShCgC~ZwAi@qAw@sBm@wBEwBN}@L'),
(8, 'UV-C', '', '|mpkB|pt`KeSFoJmAgJmEcEcHqCgJyAaKqAaK?uLhBmLxAuEbFwFdGuAjPoBfWwBvUgBrDGhClAzBlBfA`CXdEvEjg@fEve@iA`@qM`@cFz@gJpC{EtA'),
(9, 'UV-007', '', '`brkB`bs`KsFwn@f@Yn@X~ClCzDbArEQ|Ea@nDCpChAtBdBd@nBfFhX[`A'),
(10, 'UV-008', '', 'lgrkBfdt`KkDu`@~f@wDnATjHv^wl@hF'),
(11, 'UV-009', '', 'hdskBrgu`Kc[q_@Ve@pk@mEp@rD]zJiCxHoFjG'),
(12, 'UV-010', '', '|rqkBvsu`Kw@}Lq@sHqCeEy@sH~JwBpNk@`AXzZv_@uEhDsFtA_IHyOp@'),
(13, 'UV-011', '', 'lspkBhwu`KSa@uAcb@l@UvESpFaA|GmBbAvFx@pC`BrBb@lG_@z@b@Th@fJKn@oUjA'),
(14, 'UV-012', '', 'zqokBnou`Kc@g@dDcZNQVExLJvGCPFPZ|Ar`@Mh@e@RuDY'),
(15, 'UV-013', '', 'h|nkB~hu`KwHcByBsAu@cArDup@xAOzClNhBfEzCfD|BfBjDnAhCr@vBLb@`@_@|CuA`Mi@`Gc@PqL}B'),
(16, 'UV-014', '', 'tdnkBhtt`Ku@yCCm@[o@{DkSeAaGKoDr@UrVa@hA`E_BFuDll@YjBkCsD'),
(17, 'UV-015', '', 'txlkB~as`KwVmHcHuBoBmCSwKHuFbAc@^@fZvG`HrBrX`J`AfBPlQc@ZkE?'),
(18, 'UV-016', '', 'rwmkBlrr`Koa@aNu]{HmAiAd@cJhBoQpByJx@Yfu@lTd@ZZz@'),
(19, 'UV-017', '', 'dvlkB`up`K_WkHi@{@tGyQtGyLpC{CnA@hHhGvJzIvQrUJlA}B`Gq@nD[dC'),
(20, 'UV-018', '', 'dvlkB`co`KImAlF_FfG_EbHuDlHsClAj@xEvIvDlLlFfZDv@Kj@a@j@kE~BeFvEe@@'),
(21, 'UV-019', '', 'tpnkBz`p`KeGo[qDwMsFyJR_AnQ}DzMyBtDr]pDl^cCX'),
(22, 'UV-023', '', '|cqkBpvo`K?SFY|@uE^w@VSb@Un@K~@AbFz@nB`@lBXtBLrBEnFu@`Cm@Z?XNb@pDH`BD|AC`DKrBK^OZa@PSBS?sAQmKgAyCo@yCs@uI{BwCo@QKGK'),
(23, 'UV-020', '', 'djokBv{o`K_Ju|@lEa@lFW`FE`F?~An]vAt[Sd@YV'),
(24, 'UV-021', '', 'rbpkBtwo`KsDuz@|BLrAj@fA|@dAnA~@vAxBjDx@nBC^F^NPZJfAdIvAnDfDnD`FhFh@|@DrBWpAmAv@}FIeGT}CHmFd@'),
(25, 'UV-022', '', 'nipkBhcn`KiB}BcBqAyAe@rReI`@H^`@@bCEfEGjEW`Du@nB_@Ls@?]O'),
(26, 'UV-025', '', '`fskBtwq`KfAqMfAiLCkDT]~EgCnGmDpJmFtCaB|@En@`@rAfCfCdIxEdOzAhFOj@g@^eNtDcQtEsGdBoGz@}AHe@w@'),
(27, 'UV-024', '', 'xhskBzqp`KgAcCiAyAqCiBmDkAsFy@Ke@R_KmAkQPoEz@}H`AmDhA]zJvE~K`IdG`GdGxGtIhM]pAqGjDqHdE{D|B_EjB}@D'),
(28, 'UV-026', '', 'hmskB`yr`KsBcLmBaLVm@zKeBdNsDxI}BnQwEt@p@jErIjFfLhFvOlEbPmAVeMmEyE_B}EiA}TCyWR'),
(29, 'UV-027', '', 'v{skBrzt`KkC{Ez@uKi@aGoDmRsB{MdGUpKKxWG|GtApTzHpAbAdAfB`@rHWxDg@vDgHrTwAr@eSNc\\L'),
(30, 'UV-028', '', 'xfskBniu`KzGoGdCeDjBqEtCrEf@QrTOlNCfPKJj@_CpHuDdJuG`L}K~IuGdFcAb@m@]aFeG_IeJuMqO'),
(31, 'UV-029', '', 'rlrkB~mw`K}GyS}EgOwFuPTg@fVcArHc@zCw@rCiA|CqB`EzEhN`P|KtMWjAgKxG_MbGqMxDqMxC'),
(32, 'UV-030', '', 'bvpkBjsw`Km@k[a@gWEiDdCYvJa@xLi@x@^nBtFbBdF~A`F|AzEnEzMdDpKc@fAsEp@wNv@mFDkFK'),
(33, 'UV-031', '', 'dfokBfkw`KhCgYjBcSfAwJzAXzNtCzEx@|EZ`@p@JvDRbKPxLf@rUI`AcFI_e@sD'),
(34, 'UV-032', '', 'pdnkBxdw`K|AyVxBc]`@{G^YjFxBfGnA`LvBpCr@U~GuAjOaBvNqBbPeA`@qIs@mJsAsEa@'),
(35, 'UV-033', '', 'hankBlfw`KeE{@sDgAmGqD}I_KsGcKwBmERs@dMkH|HiExFgDdI}GfCuBbBnCdBvBg@|LwA|SgAjPu@pL'),
(36, 'UV-034', '', 'pslkBf{u`K_EoIeCiGm@iBVy@vAg@zAKdLc@|EWhHwAjHsCvIqFh@JnBhHuJbIkN|HmLzGaEvB'),
(37, 'UV-035', '', '|dlkB~}t`KwA{FiCoJTeA~Am@`CWjBPpG`AnGlApAJxAItA[jAi@nAoAbA}AfAaCzBeBfCq@tDq@d@^jA|FrAnGRhA{@r@eGxDsGdDaGzAgPx@uK\\'),
(38, 'UV-036', '', 't|kkBret`KiBsLyAsNa@iJAqEx@?pJlCrQbFnP~EjEh@`ITd@dFd@bE{FjAoDnAeAtAiBtDiBnBeCn@gBH{FgAmIaBaDOqDv@'),
(39, 'UV-037', '', 'lzikBbxq`K|@cL~@sNh@m@p@AlJbFjK`GzHfEnFfDMrHFpO]F_IyB}LwDqJoCeIuC'),
(40, 'UV-038', '', 'fjkkBnrq`K}MoH_LkGaJaFs@kAg@qBW}@P_D^gEl@sG`@wBdBw@zKtC~MzD|NhEmApH}AbLsAnP'),
(41, 'UV-039', '', 'rfjkBnpo`KnAuFfBsFbGiNfH{K`BuBjBKzMpHfNnI`HhEAv@cGtImGtLkFxOi@ReSqFqMsDaGuC'),
(42, 'UV-040', '', 'fklkB|yn`KqM_IsNoIwFsDAsBrLqK`KsH~LuG`GqBbC^hE`HnKbOxEjIEv@uNdHkIvF}H`H'),
(43, 'UV-041', '', 'xsmkBt}m`K_HcLiGsI}AqB}CeGDcBdCeAhMkDlCc@pMWhS@~AfAb@~Mr@nJZvGc@`@uLdBqHbBmKdC'),
(44, 'UV-042', '', 'r}nkBrrm`KaAsLk@eHIgJr@eAnF@dIZlCbB~ApCbArEt@fF^rFL`DmMVkELwER'),
(45, 'UV-043', '', 't|okBdzm`Kq@wMcAeLoCwKb@g@fP|@rFNtCJx@d@x@vAB|AE~EIbGm@vCiBbCiHtCcHzC'),
(46, 'UV-043B', '', 'zmrkBlgo`KsH{Y_HuWiJi]VClRb@zOlBhLbCr@^`@~@@z@sFzQ_G|QmBbGe@fCWfCa@zEUxE'),
(47, 'UV-044', '', '`wrkB~hn`KvDmLrDoLfCeI|@u@`BM`E|@xJlDnJ|EvHhF@xASjAuGzIyH`LoHpKkAQ{E}DsHeFuJkF'),
(48, 'UV-045', '', 'nttkB`wo`K{BgKsBkF_GgGaDuBH}@pF_IrE}GxDkFnA@pEbEpE|FxFbJtCvFVxBGlCi@fBmA`BcClAuBbA{EdCuF`C'),
(49, 'UV-046', '', 'hlukBxeq`K_GuJqBgGmFkPuAcEbAq@~NkHdLyFvBTzBnCnJhM|FvHN~Ae@vAgLzHcRtM'),
(50, 'UV-047', '', 'v}ukBjhr`KoD}LaAaDgAmCtC{BpLqItLgI`GgEnGbJpDvFxDfHbDpJbBdGExA]l@oOZyOXaJNiIv@'),
(51, 'UV-048', '', 'tivkBvis`KaCaLwBmIaAoDNq@fHu@rP]dPYrGGzA^jAjAz@nFVrDZdK?xKo@p@s@ZmKu@sOmAaHk@{Fc@'),
(52, 'UV-049', '', 'zavkBbvt`KjDsK~AiHb@_P`@SdKt@lS~A`L`Av@r@d@bAHtAUhFm@fJwClCgBZwI|@yMpAuP`BkDQWmA'),
(53, 'UV-050', '', '`sukBzvu`KrEqKfEkMv@[fCf@nJs@rO{ArLiAdGg@b@XP`AsAbKmDtPsFvQqEdKsBl@gBW}MuIeJgGgF_DgFeC'),
(54, 'UV-051', '', '~wukBvex`KyKwMaPgRaGcHJc@rHkH|JyJvGmHnBMjCx@vN`J|IzFfG~D`AlB?zBsEvIkHfLcJhKiErDeA@'),
(55, 'UV-052', '', 'd`skBpvy`KmFc`@mCyQk@aFb@u@zIgBdNqDbM{FpQqKn@AzCdDzG|HfL|MjM~NgNtJsJpGqJdF_KfEgKnDgVrF'),
(56, 'UV-053', '', 'l}qkBzzy`KkB{UsBuVe@wHf@w@fHw@|I{AnDk@r@l@tBjOzBhPrB|N|@jGk@|B_A^_AR_Jp@cIPsAA{@]'),
(57, 'UV-054', '', 'hypkB|xy`Kg@wY_@wTKuE`@u@`KVhGLxD?`A`AlAnNbBlTz@tKXxDi@rAw@NwF?{JCgG?_BcA'),
(58, 'UV-055', '', 'pfokB|zy`KOaTUuXMmQl@w@zJh@|QfB|LbBz@jAZhMb@pV^`Si@t@gPIgMGwN_@'),
(59, 'UV-056', '', 't}mkBlwx`KdA}Nt@kKl@sFp@a@`Dt@dD|@nBj@pAHrHD`C@d@d@JlJFtMNjOLdMDvGcALsGmCiImGaG}GsFiGyAgB'),
(60, 'UV-057', '', 'j}lkBhxw`KvD}D`DuC`A]l@a@vF|@nEr@hEj@V^Nd@EzAWxCm@jHmAjQQVa@A_IeImG_GyJsI'),
(61, 'UV-058', '', '|ylkBlww`KwIeFsH}EuGqBeHmAkIkCcB{@@YrQmKhK}FbDiB`B@vEpIlFpIhFdHtCtDFvA_DnDsFdF'),
(62, 'UV-059', '', 'j`kkBlzv`KwIoQwGkPcDeMTiBpAgBlCkAbEo@~EJfMtAhJdAnAN|C~FvFzKjDnGShAsPnJeMdHoDtBqAG'),
(63, 'UV-059A', '', 'j{jkBtzv`KcXcHiUgFwPmM`CaBjKeBtGaAzLeHnAkAn@`@hAdElBjGjFzN|HhQ'),
(64, 'UV-060', '', 'ddjkB`_u`KoByJgA_H?qAt@k@rQ~A`LfArQbBxA`Ah@pEr@vEQPeCSeKiAwMmAyEVwBb@gBv@eDdB'),
(65, 'UV-061', '', 'j|ikBh`t`Ky@iHs@cKMgE@}Av@o@|Eb@xN~AlMzApHz@pBXf@p@j@rFr@pH|@pI^nDe@PkPwAuLeAgIy@kGo@wAmA'),
(66, 'UV-062', '', 'xwikBzxr`KIkEN}G`@oF`@{AzAF~GpBhHpBtHzBhG`BtEzA~@^IpA_@fCRvBt@dDn@nC@v@_@`@cC]mMwA{KkAgH{@oFw@yAkA'),
(67, 'UV-063', '', 'lxikBtdt`KeQvDkOdDcEz@CyA[iTJoCMyH@{DG}DpB{@t@Bm@cFKgCScGAkEZ}BhAuHn@oDjAX~KtCtJvCfBbA~@fDe@rIE|Ki@bBc@`@`A`CZj@XpDrBbWA`A'),
(68, 'UV-064', '', 'nohkBvqt`KeJfByCj@aAg@Y_Cw@sKBaMXqMmBgE_BsC}Bw@qEaB`@yAzD_O~E}PdDqKlD~@nMrDfBj@c@xE_@zCaAvFCbCNpFZhGf@jDi@HgCx@ATPlB@fHH`FAx@UrARjDJlKJpE'),
(69, 'UV-065', '', 'fsikBryq`K_LaDyM_EoOgE_GkBGc@zCaLbCmJpAkEt@S|K~CxPbFpLnDbAfAn@hBq@lLu@jJg@vCcA`@'),
(70, 'UV-066', '', 'zyikB|sp`KqMsDeMuDsNiEYcADiAfBcHlCkJn@gBn@KvQfFnJfClH`Cl@hAPdAu@fFg@rFo@~G_@z@'),
(71, 'UV-067', '', 'x_jkBjso`KwPaFuMoD_GiBKe@r@oC`DcMlEuOpDyK~A}EvB{HhAsD|InFrL`HdLlHfEvCGlDcE`GyFzJmEtKsD|Lo@pB'),
(72, 'UV-331', '', 'zbgkBp`u`KaNrCwDx@_Oe@iHIoImBcNmAgEcA}E_CwOuGSmRc@yBKyMp@{I|B^`HHrBdBpMP~CBtAn@zACnS[h[J|FtAnAtCVna@HtK'),
(73, 'UV-068', '', '~hgkBbwr`KgMaFyRuHoMiET{@pAsEzDuNnCkJ`CaItDlAbQtEpV|G~An@O~AiGvSgCbJ{BzIu@bB'),
(74, 'UV-069', '', 'xzgkBdcq`K_SsFgSaG}FeBLw@hDyPzCuM|@cDbAZ`MtEhLdEvI|EbG`DF|@kCzJaDtLcAtC'),
(75, 'UV-070', '', 'tghkBbbp`KcJ}E}F_DmG_CkOsFmCiAbAkDpD}LdEoO`CkHzKlK~IbJbH~Gt@fAfBpAbCfBDv@eCdJwCxKs@b@'),
(76, 'UV-071', '', '`thkBpao`KqEeDoM}MiLkL_FgFBWhJyQtKeTdFgJjA}AhAr@pIbFjI~EfLzGn@j@kC`FcGzQmDnKiClJaFtR'),
(77, 'UV-073', '', 'rhfkBxop`KePsEmPcFoGcBdA_D`H{TzD{LfBcJlLlEzPzG`GdCmAzDaExOkDhPgApF'),
(78, 'UV-072', '', '|uekBl_r`KqReFqQwE{Ag@fDyNzFuVxA_GdSjFfQfFnChAgFtQeE|OeB~G'),
(79, 'UV-074', '', 'lyfkBz}n`KmRqHyNuFeCiAlDoL~HsXt@a@xIzEfK~FlLjG_@rA{BrH_CnI{CtK'),
(80, 'UV-075', '', 'zggkB|vm`KgPyI}KoG{D{B@{@hEqI`IoOzC{FfA}AWm@n@y@dB[|B}B~DoA~F~ChBCpC|AtOjIqA`DeGvL}HjOeK`S'),
(81, 'UV-332', '', 'd}ckBxpt`K]kTUkAUyUmHC_ONgLNnCjId@vETvEKlFtGpApHfDjJdD'),
(82, 'UV-333', '', 'd{ckBtas`K_RV{MNgDBwBkGE}Qv@wJZ{E|QrBtU~BtHzAaCjKmC`Ky@zAFlD'),
(83, 'UV-076', '', 'xjdkBfpq`KaUsE_LsAqNmA~AeGdA}GtDoXhBkMlNfD`RxFhRxFcGlR}FbW'),
(84, 'UV-077', '', 'p{dkBb~o`KmY{IwRkF_EoAh@aC~AoGzDaNbF}Q`PxFzVrJlJrDKl@uE~NiEhNeB|F'),
(85, 'UV-078', '', 'xoekBpin`K{Y{K}MsF}GeCbDiOfFaObEaOnDnBtO~IbOlI`ItECr@wDzMaEvMsBnH'),
(86, 'UV-079', '', 'bafkB|}l`KmUcM{PkJkG}DhKcSrLwT`DwFn@uDbX|OxZ`RyBpCiCrBkCdB_AxA}GvMyGzM}BnE'),
(87, 'UV-079A', '', 'fjgkBjjk`K{TyMeU{MoKoG~AwFzEsL`DoHj^nR|KdGk@p@mEfINZTjHn@rFXzCZhB'),
(88, 'UV-106', '', 'b{wkBfbr`KoEkOaHyNmHwJSi@tBaBvLuI|HsFdEuCbC|ChDpElFbJlFzHnKxN`CpDnCtGd@jBa@~@cPjCsKlAaMXoFCo@w@'),
(89, 'UV-107', '', 'pyykBh|s`K}WqBmR{AkIs@eAiBGiKQeIu@iJ]{C^y@pNYtEO`QyB`IyA`Ab@hBzObCzT`BhOw@vA'),
(90, 'UV-108', '', 'vvwkBvmx`KmNqKaMkJmDqC`@y@bFkJnGcL`DsGbIiTpBbA`UdLrOfIvHzDeA~CoDdIBfAoB`EaCpBkK`ImG|EwJlH'),
(91, 'UV-109', '', '~tvkBtfy`KeJwK{IsKM{AVgAvFgGbGaHdCeD`K|H|N`LlFhEaKpH}JnHgFnDw@?'),
(92, 'UV-110', '', 'bzukBxnz`KuGqHkI{JgKgL}BuCnL_HnKkHdG}ExBDfF|FlI~JrDhEG`AuC`CkEbIaKbTc@b@'),
(93, 'UV-111', '', 'nltkBzx{`KiFaOaDqH{E~CuCfBiEoPsDyKeDoId@mAzP{DhPiFzJcE|@N|QnShKjMpCdDIf@wOnMyOjM}E`E'),
(94, 'UV-112', '', 'byskBfw{`KkR`BwCHaJgD_DwBiLAd@kB{@mHEwFqAuOe@iG^kAx@_@nMSnKy@dCh@vAjBhExKxCxKhGrU'),
(95, 'UV-113', '', 'zarkBzq{`K_UIOo@}GuAwEeAYwO_@cSAqBh@{@fLIlPHpAh@~@~AxAvOj@dHDhFr@bF]jB'),
(96, 'UV-114', '', '~rykBb|q`KgDyHcEeHwPsU_DwFgDkF_KqMsDsE_Be@zBkAzFiEzKqHrEgDjEpDdK`JnQbQlKvI|EdElNfLrIrGvHxGcHpCoOzFuDpAcEp@iP|BwEj@'),
(97, 'UV-115', '', 'h_zkB|xs`KoEoa@cBqOc@aExB_@|IuAjK{AhFoAvLqEzKgErIfHdKvIjErDkBlKcB~Ig@zCuJ{BsHuA_C`@wJlG{RbNcHrE}Bx@'),
(98, 'UV-116', '', 'tr{kBn|u`KwVa@}CQwQgDuFaAG]dCqRh@kM@aOl@_BlBcBbHsE|O_LpLaItCq@nLnB`FvAaCxUiDn[oBlOcEpU'),
(99, 'UV-117', '', '~{zkB~lw`KeHwBqD}@wFfA}HpAg@a@cEcFkEkFkBcCbDeGhDyIvCgKj@cBlCZdNvBjFz@hD^|LJdFR`@`AmCzH{EhMyDfKyBvF'),
(100, 'UV-118A', '', 'tczkBr_y`KmMqKcTmPwIwH`B}AfDkDxGcO~@qBxAP|GtIxEtFtEtFtEhClItCEf@yDzJiDtJ_BfE'),
(101, 'UV-118', '', '`czkBz`y`KsHzF{DxB}IlFgCtA{FUwCgDsB{DsJmM}H_GuDaD|@iAhJqHxIsGjBoAlI|GfNfLhHtFrIfH'),
(102, 'UV-119', '', 'j|wkBdsz`KaL_NgKmLuHmJCw@`AcAjJ_HrImGbE{Cv@^bKxHhCtBjGbItAvBG^dDdFMv@qBfCq@~BmAvBeJlH}E|Ds@X'),
(103, 'UV-120', '', 't|vkB~t{`K}NmP_NwOIg@b@gAlFcLdHiOrBeB|AgAbAJjJxKxJpLpJrLE`AmIlJmIlJiFlF'),
(104, 'UV-121', '', '~{vkBxw{`KeMnNaLnL_L|L{HiNaHgMgIu@wKuA}BaAdNeKvKyIpB]~KeJvGkFf@VjDvDnIlJbMnN'),
(105, 'UV-122', '', '|o|kBxev`KwKeC}GeBcEcAlB}IzDaVjE}_@tCsV|K`CxOnD~IfCvDrAoHlSoJjYeIpT}DhK'),
(106, 'UV-123', '', 'bx{kBdxw`KuKaDiIwBwAk@pFyOdHsRpDyIrA`@nKfCfItBN^sEzL_HnR_DxI'),
(107, 'UV-124', '', 'tu{kBx|y`KkPwGaNyHyJuHmBeB`EaKnDoHzBqFvHvAxQxCtOzCpKhCuExJiFrKyIvQ'),
(108, 'UV-125', '', 'lszkBp`{`KoKaJgMwKuJoImFeFdJyFlFoClGmEzCoBt@oBlDdDjEnDlGbFfJvFlMnFiAjDoB`DmCdC}GnF_KdI'),
(109, 'UV-126', '', 'vgykBpd|`KiN}P_MaNuIkKJm@zK}JlGwFH{Bx@aBxDkCbEF~E~D`NzLxJxIxH`H~AzAwI~GcLvIqMvJ'),
(110, 'UV-127', '', 'nfxkBfd}`KaMaOiM{NwIwKlFgGvIoJtJqKjIjJfLdNxI~JtFnGoJzHiLjKmFjF'),
(111, 'UV-129', '', '~s}kBlmv`KoNiBkKqAkDg@lIuVxIkVrJmX`CaHdAc@fKxCnMfElJfDsJpRmLbU_NpX'),
(112, 'UV-130', '', 'fv|kBpdx`KqQ{FsHsBtBeGToCjE{L|E_NtBoFbAHbJdAnNdBnGz@E|@yLlViHfOkDvG'),
(113, 'UV-131', '', 'z__lB~yv`KeM{B_IuAaFcAyHgBCk@rLsUvJ}RzJ{RbE{H|N~EhMzDdInCqFzKiSb_@mIhO'),
(114, 'UV-132', '', 'pw}kBbtx`KaG{BoEo@cFaCgGsC_Be@lBqAdCeFtDiIpFyK~FkLtBcE`P|CfKnBpI~A}HjMgJfOqPvW'),
(115, 'UV-133', '', 'fk`lBhdw`KuRyC{OqB}Cu@tHeObKkRrIwOtHkNnWdIxRvG}OfUeJhNmLpP'),
(116, 'UV-134', '', 'by~kBjcy`KcRyGcL_ElK{O`MmS`LwQpTpClRvC_MlQoM~Q}NrR'),
(117, 'UV-135', '', 'ti}kBn{z`KwF{DeG_BqLaFkLmF}GsD|GqOpIkPlEgJxZlK`YfKvG~BeIrHwIfMaJlN'),
(118, 'UV-136', '', '|c|kBxo|`K{IeIkBuDkIkHgHoGeHiGiD{C|HcGxQwNtDqH~JnEhNvGlJjDvEbBhEhCeHlLqJhOqLdQ'),
(119, 'UV-137', '', 'b}zkBz`~`K{PoSaT{VeMyNlTsP`TiPrPdO|E|Ej@[jDzCvCnEbExDbF|E{LrP{QnX'),
(120, 'UV-138', '', 'tuykBty~`K{JuLaN}O_SmUnKaKvIaIfHyFlWjZxJpLzInKvDfEoG|EeKvEoLjF'),
(121, 'UV-186', '', 'xnalBzow`KeM_EmKiDoCcAvJwNtKkPpIaMtEeHbIpC`Cz@aDtLmGnTqHtV'),
(122, 'UV-187', '', 'tn`lB`cy`KwMuA{Fo@aIgDiFiBjIcLpHqKbH{JnJwLvMbE|NzEmHrPcK~R'),
(123, 'UV-188', '', 'ry_lB|bz`K}GwCaHoCmK{DcK}DbIwHrIgItAeB~GtBnGrCnJx@hLpAkGrNsEzKaDfD'),
(124, 'UV-188A', '', 'jo~kB~b{`KeF}CuIoFkIeFjF_IlDoFxDcF`HiGrMzEhIzCdNzFnDbBIv@kEfKyMdEyQlF'),
(125, 'UV-166', '', 'vhzkB|~o`KiSyPgTaRiEuDdXaSfPeL`IcG|InTpGhNnBzD`BpAbCtBnAxCwJnHgIlG{JnH'),
(126, 'UV-170', '', '~z{kBrkq`KcNqLaP_NmIcHgFwEvKgIvJqHnKqIfBnFGbFbDlIvGtOhHfRwCzC{DbE'),
(127, 'UV-167', '', 'h~zkB`jn`KcFyLcEgJ}AiErRiN|NiK|K{H~A`MdBlNtArLf@lEeHjDkH`D{EtBqJpB'),
(128, 'UV-171', '', 'th|kB`}p`K}FmNmMc]RwEyDoK}DgDaCmCsCcGvOaDfGiChPuHxHfRh@QxH|Qz@|Ef@rD~@pDzC~HhDnJcGrC_LxFwFrCeDhD'),
(129, 'UV-175', '', 'hk|kBbzq`KqIeH{CgChD_EbNwN~OaIrLcGfBrEjHdQzBpI}ExCeSpLcMdH'),
(130, 'UV-176', '', '~v}kBv`s`KmF_EaFkEyIcIsEuDdBwAvHoEpMmHlL_H~FnOtJrLoIlI}KvI'),
(131, 'UV-180', '', '|t~kB~ys`KuDyAkFkEcH}GaEmDlJmIfJoIjDcD`HlInArAjBiAlFxEhJxIoK|GmQ|K'),
(132, 'UV-184', '', 'lm`lBlrt`KmZsJwPuF}G_CxOuJlF}ClFmDjFfElOxG`IpC{HlU'),
(133, 'UV-184A', '', 'l|alBdgu`KoSuG}NkEkIsClDaLzDoKdBiEhIjC~H~B~FSpJs@kAlNq@~Gw@lJSpC'),
(134, 'UV-168', '', 'zu|kBbln`K{EiIyDwIcCqTcByNoAcL|K}HnKyHhRgNfCnT~A`QbCrTzAbMxAdNkK`DcUfH'),
(135, 'UV-172', '', '~n}kBdcp`KiHsPsDqPoH{QpN}EbNwDfOuFvJ_C|E`PlFpP`DdKiCp@_Cz@oGlDiIvCoEfB}QhM'),
(136, 'UV-173', '', 'le_lBjco`KgGaRaGeRqBmGpEyAfNkBdDI~Bj@bDm@tC`BrBtCfHzO~ErKz@xBgCxAmBz@uL|AiOjB'),
(137, 'UV-174', '', 'rp`lB~vn`KeIuQcG}MoCmDcCgAtLkF`KqEzDeCrI|LtJlNhE`IsLjG}QhJ'),
(138, 'UV-092', '', 'hqukBlxn`K{LyRcIwIQw@|CaGzK{MbHeItBuEvA}OT_DrEzFtGdJ~ExGbCfEjHfHLjAlC`CYt@sL|KmSzQyKvJ'),
(139, 'UV-091A', '', '|utkBfvm`KkKeHeG{CrFkNjHkQ~FsNfE{K`DZdFxAtF`D`CtCKrDo@`KShD}AtDmC|DqDnDaK~LaEfG'),
(140, 'UV-080', '', 'rdskB`|l`KaQ_DyOkB}N]_@g@KiCd@_GfAeDpBwAjDu@`Ho@jQqB`OoBpHq@OhBeFz[oAxGq@\\'),
(141, 'UV-091', '', 'n_tkBhgm`KeIwCmHcCy@qBtAiJfC}OpB}LbASnLqAtIeAlEg@xFST^a@tAmJlVuF|M_G|N'),
(142, 'UV-104', '', 'xawkB`jm`KoAsAEoB_MaTcIeL{GkJ}AaCTcNLgJLyIP[lER|C`@tCvAdCzBlFhGzC`D|ChD|HfIdF~EfGhEx@Zl@~@hBxCyHpKgLlOcBvAsBR{@pA'),
(143, 'UV-150', '', 'xbxkBhel`K}C_EBg@cJcHcOgOyCuDzKyLfJuJ~L_NvG_Hn@ERUrB|FnGxOUl@nD`JlCrGlA`DeLfMoNlO'),
(144, 'UV-149', '', '|hykBh_k`KoEmLwFmMoHkRkAmCbLcF~J}EnJeEbDjHnFtMzD`JfCpF{JvH{NdN'),
(145, 'UV-148', '', '|nxkBjki`Ki@Q[FQGmFsMyFsOqCiH~MiGdJqEnJkEnEdKzDdKbH~OcJ~DoIfEwJzE'),
(146, 'UV-147', '', '`uwkBtej`K{MiPgL_NaIoJ|FiCbIqDtL{FxG_D|DfJjGvOrCvGnA`DkI`IaIzI_AjA'),
(147, 'UV-145', '', 'pnvkBnxh`KjFaNrIsRlDeJtAkCbCzF|DhJhCvGiNpGiQdI'),
(148, 'UV-146', '', 'ptwkBlej`KiNcPcN{OuFkHkHMiCES`@m@fXe@xOGbDXJnCHnD^rCjAlCfBfKlLzDgEtJqK'),
(149, 'UV-101', '', 'fmvkBbyh`KkFQyCKK[^qNTkI~@u`@~KKhP]vD~IwLxXoDdJeEfKa@|@'),
(150, 'UV-097', '', '`yukB`pk`KiGyDgDqAoGkA_DWPuIZ}JLyDfIb@jH^jF`@OtHa@tS'),
(151, 'UV-098', '', 'vzukB|ij`KkJe@{Im@qCQNmNZuMZoMHmAf@MbL^jFTxDTI`Ee@rRe@rO'),
(152, 'UV-099', '', 'p~ukB`xh`KcL_@qG[kEUXmKb@sPb@oQLqEpGI|JMpFCSfJa@nPo@`W'),
(153, 'UV-100', '', 'jbvkBb{f`K}MVgLNwI_Bl@}Ub@wTT_JvILbM^dMZQbHa@zOg@hQ'),
(154, 'UV-102', '', 'lbwkB~yf`KqNRiML^wSb@iQf@wLhE|JbHzPfGfO'),
(155, 'UV-093', '', 'd}tkBtdk`KmHRsLvA}N`CKOp@wEb@gH`@qJb@wJP[pLn@xIj@xJt@UhH_@dM'),
(156, 'UV-094', '', 'h_ukBvfj`KqOcA}Kq@_F]LiEpAwVn@qNd@WvLr@zOf@bAj@KxPm@`Y'),
(157, 'UV-095', '', 'z`ukBnuh`KiQo@yLy@g@w@?aSDgNpJqA@w@VmFb@}FxGd@lN~BYnNa@hQk@|R'),
(158, 'UV-081', '', 'jsskBzkk`KqNbBgKlAgSbCeJz@qBQq@[@aAr@mEpAqBrAkBPmBT_FpJXzJd@hGZ|If@nFRlDPp@bAVdA'),
(159, 'UV-082', '', 'j|qkBh}i`KyFYkHa@oF]wDOvAdFpCfKjDtMhCpJzBzITgAOo@v@aCvBsCVwCZcJj@eMT}F'),
(160, 'UV-083', '', 'j|qkBr|i`KoFSgFWaFWqG]OUwAiFmBmHqBmH{AqFe@qBLGvGa@dFUfJa@xFUrGWfBEZP?hASvI[fKa@fJWbF'),
(161, 'UV-086', '', 'ntskBz`k`KyHWcKe@mI_@eHa@gIYwCQUc@n@sNn@uNr@gQv@wXz@o@hACfKh@dKh@tJb@dLl@dDTd@XBh@WtG]zG]nI]xIc@jJk@hMY~FM|@'),
(162, 'UV-084B', '', 'tvpkBhqh`KyCJmADUUu@yCy@wC{@aDq@mCqBqGkBwFeAsC[mAe@qBe@sCGi@R_Az@WxE]xD]hE]hGg@`@COjOc@bQs@~P'),
(163, 'UV-084', '', 'p~qkBlmh`KsFXgGXsGXiFR{ETY]ZsGb@iL`@yLZcPLaDrDBdFNbFJ|EFxJPK|EIxJOnMSbQU`@'),
(164, 'UV-087', '', 't|rkBfnh`KqEQeEQ_FUaEUe@q@HqGHaJNyNHcIFyBd@QpFRjPf@~@FItFU`L_@nPU|J'),
(165, 'UV-089', '', 't|skB~qh`K_H[eIe@}Ig@JuK`@kKNeIT}JLuCTI`GNxGLdELhCHRPAnGAdKAjJEzK@xE'),
(166, 'UV-085B', '', 'pvokB~of`KmCyJcCyIeCkJqBaHqBkHI{APy@zFRrJZlKHjKLvKJT^KzFSjLSzMWzJaDVgGh@_Kz@'),
(167, 'UV-085', '', 'rbrkBflf`KyGKcIMuGMwEGgCCRiMP}KNiMJ_FfILxIVfIPxDNT^ItIIzLMtP'),
(168, 'UV-088', '', '``skBznf`KqGGyFKyD[_EUJyJF_MJ_JNoJtGLhIPdIVDlAYlLWxL]jO'),
(169, 'UV-090', '', 'j_tkB~ef`KyEGuFOiGQuDIReIPuKPcIDaD~FHlIR`ITSdNWpN'),
(170, 'UV-096', '', '~ztkBhnf`K_F_@oDe@cFiBgEqAPoKTkKTiLrEFzGPjFLpBJQhJYvL]dP'),
(171, 'UV-142', '', 'l~xkBflg`KcFzByHnDoHjDgJdEiFmM{CqHaDgIaAaClF_CrGwCnGwC|KgFjDlIhDfI|EvL'),
(172, 'UV-143', '', 'njxkB`ef`KeI|DkFbCeF~BeGnCaEdBmEyK_C_G_EqL_EqJ}BmGxEGxID~HC|HC|DLjAXrAt@lA`AzAvDhEdK~BlF'),
(173, 'UV-226', '', 'fypkBfwd`KwLEwMOyJOkK]qAWkEDmFDsAA{A_H{B}HkB_H{BkIiBkGsAiEb@MfFIjLElIIvDErG?pJEtKCzLMjFECtIC`NApOG~At@~F`@`B'),
(174, 'UV-227', '', 'tcrkBvyd`KkIQeJSuGSuFSyAqJGgHG{MFoNAyE~JMjLSzBCzEz@bF~@IjGIxIYpUIrK'),
(175, 'UV-228', '', 'zbskBr{d`KoFKoGO}FOkDKDyKB{IReKJ_MJ_GrDNdITnJZrAFS`J]fKMfQEvL'),
(176, 'UV-229', '', 'dqskBj|d`KsFGmDOHqMBoLPaJXuJRsEtER|FTfFHGzCUvHI`D[xCa@hAuAtBsAfDkA~P'),
(177, 'UV-229A', '', 'r}tkBb_e`KiGOcHM_HOqK]}COn@yHp@sJtC_GhA}Al@kNf@{OrISpHUrHMfCG^|@w@jYQlNSnKWjI'),
(178, 'UV-100A', '', 'jevkBpae`KiFGeHOmFQqHM_DQTqITuLPcIRqIPmKPqG|CxHpCnG|FfNjGhOpE~KdAlC'),
(179, 'UV-234', '', '~_ukBvxb`KcFN_FPyENuFJwCH@{IE{DXwD|@iGjBiM`AKjAGzCbHbDfIv@dArEvKfCdG'),
(180, 'UV-233', '', 'p|skBrbc`K}EOiFO_GUf@gNTuKXmIXwJN{HfCi@jEZlIh@bEZo@~BaAdE{@`Gw@nGMnAW|G@rIQ~H'),
(181, 'UV-232', '', 'feskB~`c`KqFQwEOsEMaEKaAGZ}BHqKDiLDqFjEaC|GiEjImEjDaBFrA]lMc@vOa@vNKjD'),
(182, 'UV-231', '', 'zfrkBv~b`KiEm@oEw@y@SmGHoFFyABHmAtCw@vDcF~DgGzAiC~EyBlGqDAvGGdJEdKS|A'),
(183, 'UV-263', '', 'xqpkB|zb`KuDd@eG@_GDgHDgFBoEByB@kAeEoCeGaCwDr@o@zDm@jEo@dEs@dEq@vDi@lEy@xEs@zFeAxApJ`AbG~AhI'),
(184, 'UV-230', '', 'xiqkBpub`K{B`BqDDiJ^c@mEyBmKgBuKjDu@tF}@hGaAxC_B|AgCn@r@dC`FjDbHrAvCkBlCuBhD'),
(185, 'UV-276', '', 'petkBbfa`KqHg@}F]wBQC{@sFr@eAhAsH|DmE~B}EjCiEbCgFpCwEnC}@j@]OkC}E}DuHkAwBj@uBX_Be@_Do@eE_C}DdHaE`HgEh@~@nDmBlEoCvBgAxBaBbCsAfCoAzFeC`NiHvCnGdFfLrGrShC|J'),
(186, 'UV-156', '', 'vbwkBxae`K_EH}FCkEBeEAiC_H_FmLuCiHoEsKsF{McDkHiAgChK[rJk@hPm@rIi@hISrOs@xHc@bGSkD|LiD|K{CbLeCjIuEhOgDvK');


ALTER TABLE `tbl_asignacionzona`
  ADD CONSTRAINT `fk_tbl_asignacionzona_tbl_personal1` FOREIGN KEY (`id_personal`) REFERENCES `tbl_personal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_asignacionzona_tbl_zona1` FOREIGN KEY (`id_zona`) REFERENCES `tbl_zona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `tbl_detallecompra`
  ADD CONSTRAINT `tbl_detallecompra_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_detallecompra_ibfk_2` FOREIGN KEY (`id_compra`) REFERENCES `tbl_compra` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tbl_detallepedido`
  ADD CONSTRAINT `fk_tbl_detallepedido_tbl_pedido1` FOREIGN KEY (`id_pedido`) REFERENCES `tbl_pedido` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_detallepedido_tbl_producto1` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `tbl_imagen`
  ADD CONSTRAINT `tbl_imagen_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_imagen_ibfk_2` FOREIGN KEY (`id_categoria`) REFERENCES `tbl_categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_imagen_ibfk_3` FOREIGN KEY (`id_negocio`) REFERENCES `tbl_negocio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_imagen_ibfk_4` FOREIGN KEY (`id_oferta`) REFERENCES `tbl_oferta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tbl_movil`
  ADD CONSTRAINT `tbl_movil_ibfk_1` FOREIGN KEY (`id_personal`) REFERENCES `tbl_personal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_movil_ibfk_2` FOREIGN KEY (`id_usuarioplaystore`) REFERENCES `tbl_usuarioplaystore` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tbl_negocio`
  ADD CONSTRAINT `fk_tbl_negocio_tbl_cliente1` FOREIGN KEY (`id_cliente`) REFERENCES `tbl_cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_negocio_tbl_zona1` FOREIGN KEY (`id_zona`) REFERENCES `tbl_zona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `tbl_oferta`
  ADD CONSTRAINT `fk_tbl_oferta_tbl_producto` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `tbl_ofertamovil`
  ADD CONSTRAINT `tbl_ofertamovil_ibfk_1` FOREIGN KEY (`id_movil`) REFERENCES `tbl_movil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_ofertamovil_ibfk_2` FOREIGN KEY (`id_oferta`) REFERENCES `tbl_oferta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tbl_pedido`
  ADD CONSTRAINT `fk_tbl_pedido_tbl_negocio1` FOREIGN KEY (`id_negocio`) REFERENCES `tbl_negocio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_pedido_tbl_personal1` FOREIGN KEY (`id_promotor`) REFERENCES `tbl_personal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tbl_pedido_tbl_personal2` FOREIGN KEY (`id_entregador`) REFERENCES `tbl_personal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `tbl_posicion`
  ADD CONSTRAINT `tbl_posicion_ibfk_1` FOREIGN KEY (`id_personal`) REFERENCES `tbl_personal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tbl_producto`
  ADD CONSTRAINT `fk_tbl_producto_tbl_categoria1` FOREIGN KEY (`id_categoria`) REFERENCES `tbl_categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `tbl_trazo`
  ADD CONSTRAINT `fk_tbl_trazo_tbl_zona1` FOREIGN KEY (`id_zona`) REFERENCES `tbl_zona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `tbl_usuario`
  ADD CONSTRAINT `fk_tbl_usuario_tbl_personal1` FOREIGN KEY (`id_personal`) REFERENCES `tbl_personal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
