/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : diplastdb

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2015-08-10 22:56:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_asignacionzona
-- ----------------------------
DROP TABLE IF EXISTS `tbl_asignacionzona`;
CREATE TABLE `tbl_asignacionzona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_zona` int(11) NOT NULL,
  `id_personal` int(11) NOT NULL,
  `dia` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_asignacionzona_tbl_zona1_idx` (`id_zona`),
  KEY `fk_tbl_asignacionzona_tbl_personal1_idx` (`id_personal`),
  CONSTRAINT `fk_tbl_asignacionzona_tbl_personal1` FOREIGN KEY (`id_personal`) REFERENCES `tbl_personal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_asignacionzona_tbl_zona1` FOREIGN KEY (`id_zona`) REFERENCES `tbl_zona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_asignacionzona
-- ----------------------------
INSERT INTO `tbl_asignacionzona` VALUES ('1', '8', '2', 'Lunes');
INSERT INTO `tbl_asignacionzona` VALUES ('2', '12', '2', 'Martes');
INSERT INTO `tbl_asignacionzona` VALUES ('3', '17', '2', 'Miercoles');
INSERT INTO `tbl_asignacionzona` VALUES ('4', '1', '1', 'Lunes');
INSERT INTO `tbl_asignacionzona` VALUES ('5', '2', '1', 'Martes');
INSERT INTO `tbl_asignacionzona` VALUES ('6', '3', '1', 'Miercoles');

-- ----------------------------
-- Table structure for tbl_categoria
-- ----------------------------
DROP TABLE IF EXISTS `tbl_categoria`;
CREATE TABLE `tbl_categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `fechacreacion` date NOT NULL,
  `fechamodificacion` date NOT NULL,
  `estado` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_categoria
-- ----------------------------
INSERT INTO `tbl_categoria` VALUES ('17', 'Cubiertos', 'Se presenta Cucharas, Cuhillos, Cubiertos', '0000-00-00', '2015-04-14', '1');
INSERT INTO `tbl_categoria` VALUES ('18', 'Envases', 'Vasos y otros', '0000-00-00', '0000-00-00', '1');
INSERT INTO `tbl_categoria` VALUES ('19', 'Popotes', 'Bombillas plásticas', '0000-00-00', '0000-00-00', '1');
INSERT INTO `tbl_categoria` VALUES ('20', 'Vasos desechables', 'Vasos variados en forma y tamaño', '0000-00-00', '0000-00-00', '1');
INSERT INTO `tbl_categoria` VALUES ('23', 'Plato Termico', 'Platos de plastofor', '0000-00-00', '0000-00-00', '1');
INSERT INTO `tbl_categoria` VALUES ('24', 'Charolas', 'Material Plástico', '0000-00-00', '0000-00-00', '1');
INSERT INTO `tbl_categoria` VALUES ('25', 'Producto Termicos', 'Productos Termicos con con Diseño:  se presentan Productos con Diseños en vasos platos y otros', '0000-00-00', '0000-00-00', '1');
INSERT INTO `tbl_categoria` VALUES ('26', 'Contenedores', 'Material Plástico u Plastofor', '0000-00-00', '0000-00-00', '1');
INSERT INTO `tbl_categoria` VALUES ('27', 'Bolsas', 'De distintos materiales', '0000-00-00', '0000-00-00', '1');
INSERT INTO `tbl_categoria` VALUES ('28', 'Papel', 'Distintos tamaños', '0000-00-00', '0000-00-00', '1');
INSERT INTO `tbl_categoria` VALUES ('29', 'Varios', 'De distintos tipos', '0000-00-00', '0000-00-00', '1');
INSERT INTO `tbl_categoria` VALUES ('30', 'Botellas', 'Distintos tamaños', '0000-00-00', '0000-00-00', '1');
INSERT INTO `tbl_categoria` VALUES ('31', 'Vita film', 'Distintas medidas', '0000-00-00', '0000-00-00', '1');

-- ----------------------------
-- Table structure for tbl_cliente
-- ----------------------------
DROP TABLE IF EXISTS `tbl_cliente`;
CREATE TABLE `tbl_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(300) NOT NULL DEFAULT '0',
  `codigo` int(11) DEFAULT '0',
  `nombre` varchar(50) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `sexo` char(1) NOT NULL,
  `fechanacimiento` date DEFAULT NULL,
  `ci` varchar(15) DEFAULT NULL,
  `direccion` varchar(300) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `celular` varchar(20) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `fechacreacion` date NOT NULL,
  `fechamodificacion` date NOT NULL,
  `estado` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_cliente
-- ----------------------------
INSERT INTO `tbl_cliente` VALUES ('172', '0', '0', 'Juanito', 'Arcoiris', 'H', '2015-04-29', '5465468', 'Los Arcos', '33456584', '73216542', 'juanito@gmail.com', '2015-04-14', '2015-07-25', '1');
INSERT INTO `tbl_cliente` VALUES ('173', 'ced7c75d-6b12-427a-9365-aa1522670d5a', '0', 'Dayan', 'Antezana Maldonado', 'M', '1989-08-05', '7728987', 'Los Lotes', '3340439', '79050606', '2@2.com', '2015-08-06', '2015-08-06', '1');
INSERT INTO `tbl_cliente` VALUES ('174', '5caabdc4-a732-4812-92cb-7e475e4e1422', '0', 'Maria', 'Prado', 'F', '1989-08-05', '7728987', 'Los  Penecos', '3340439', '72180342', '2@2.com', '2015-08-06', '2015-08-06', '1');
INSERT INTO `tbl_cliente` VALUES ('175', 'c3582f8a-8acc-4889-b6ad-61ac9e124802', '0', 'Marcela', 'Ajata', 'F', '1984-08-05', '7728987', 'Los Lotes', '3340439', '75052653', '2@2.com', '2015-08-06', '2015-08-06', '1');

-- ----------------------------
-- Table structure for tbl_compra
-- ----------------------------
DROP TABLE IF EXISTS `tbl_compra`;
CREATE TABLE `tbl_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `observacion` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_compra
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_detallecompra
-- ----------------------------
DROP TABLE IF EXISTS `tbl_detallecompra`;
CREATE TABLE `tbl_detallecompra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) NOT NULL,
  `id_compra` int(11) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `costo` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_producto` (`id_producto`),
  KEY `id_compra` (`id_compra`),
  CONSTRAINT `tbl_detallecompra_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_detallecompra_ibfk_2` FOREIGN KEY (`id_compra`) REFERENCES `tbl_compra` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_detallecompra
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_detallepedido
-- ----------------------------
DROP TABLE IF EXISTS `tbl_detallepedido`;
CREATE TABLE `tbl_detallepedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descuento` double DEFAULT NULL,
  `cantidad` int(11) NOT NULL,
  `montototal` double NOT NULL,
  `id_pedido` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_detallepedido_tbl_pedido1_idx` (`id_pedido`),
  KEY `fk_tbl_detallepedido_tbl_producto1_idx` (`id_producto`),
  CONSTRAINT `fk_tbl_detallepedido_tbl_pedido1` FOREIGN KEY (`id_pedido`) REFERENCES `tbl_pedido` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_detallepedido_tbl_producto1` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_detallepedido
-- ----------------------------
INSERT INTO `tbl_detallepedido` VALUES ('4', '0', '4', '1320', '1', '24');
INSERT INTO `tbl_detallepedido` VALUES ('5', '0', '7', '182', '1', '42');
INSERT INTO `tbl_detallepedido` VALUES ('6', '0', '5', '235', '1', '72');
INSERT INTO `tbl_detallepedido` VALUES ('7', '0', '3', '450', '1', '22');
INSERT INTO `tbl_detallepedido` VALUES ('8', '0', '3', '57', '1', '60');
INSERT INTO `tbl_detallepedido` VALUES ('9', '0', '3', '75', '2', '29');
INSERT INTO `tbl_detallepedido` VALUES ('10', '0', '2', '52', '2', '42');
INSERT INTO `tbl_detallepedido` VALUES ('11', '0', '2', '38', '2', '60');
INSERT INTO `tbl_detallepedido` VALUES ('12', '0', '2', '500', '2', '23');
INSERT INTO `tbl_detallepedido` VALUES ('13', '0', '2', '46', '2', '88');
INSERT INTO `tbl_detallepedido` VALUES ('19', '0', '2', '50', '3', '31');
INSERT INTO `tbl_detallepedido` VALUES ('20', '0', '2', '52', '3', '41');
INSERT INTO `tbl_detallepedido` VALUES ('21', '0', '5', '95', '3', '60');
INSERT INTO `tbl_detallepedido` VALUES ('22', '0', '10', '230', '4', '88');
INSERT INTO `tbl_detallepedido` VALUES ('23', '0', '10', '1000', '4', '46');

-- ----------------------------
-- Table structure for tbl_imagen
-- ----------------------------
DROP TABLE IF EXISTS `tbl_imagen`;
CREATE TABLE `tbl_imagen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `direccion` varchar(200) NOT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `id_negocio` int(11) DEFAULT NULL,
  `id_oferta` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_producto` (`id_producto`),
  KEY `id_categoria` (`id_categoria`),
  KEY `id_negocio` (`id_negocio`),
  KEY `id_oferta` (`id_oferta`),
  CONSTRAINT `tbl_imagen_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_imagen_ibfk_2` FOREIGN KEY (`id_categoria`) REFERENCES `tbl_categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_imagen_ibfk_3` FOREIGN KEY (`id_negocio`) REFERENCES `tbl_negocio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_imagen_ibfk_4` FOREIGN KEY (`id_oferta`) REFERENCES `tbl_oferta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_imagen
-- ----------------------------
INSERT INTO `tbl_imagen` VALUES ('48', 'categoria17.jpg', null, '17', null, null);
INSERT INTO `tbl_imagen` VALUES ('49', 'categoria18.jpg', null, '18', null, null);
INSERT INTO `tbl_imagen` VALUES ('50', 'categoria19.jpg', null, '19', null, null);
INSERT INTO `tbl_imagen` VALUES ('51', 'producto22.jpg', '22', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('52', 'categoria20.jpg', null, '20', null, null);
INSERT INTO `tbl_imagen` VALUES ('53', 'producto23.jpg', '23', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('56', 'producto24.jpg', '24', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('57', 'categoria23.jpg', null, '23', null, null);
INSERT INTO `tbl_imagen` VALUES ('58', 'categoria24.png', null, '24', null, null);
INSERT INTO `tbl_imagen` VALUES ('59', 'categoria25.jpg', null, '25', null, null);
INSERT INTO `tbl_imagen` VALUES ('60', 'categoria26.png', null, '26', null, null);
INSERT INTO `tbl_imagen` VALUES ('61', 'producto25.jpg', '25', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('62', 'categoria27.png', null, '27', null, null);
INSERT INTO `tbl_imagen` VALUES ('63', 'producto26.jpg', '26', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('64', 'categoria28.png', null, '28', null, null);
INSERT INTO `tbl_imagen` VALUES ('65', 'producto27.jpg', '27', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('66', 'categoria29.jpg', null, '29', null, null);
INSERT INTO `tbl_imagen` VALUES ('67', 'categoria30.jpg', null, '30', null, null);
INSERT INTO `tbl_imagen` VALUES ('68', 'categoria31.jpg', null, '31', null, null);
INSERT INTO `tbl_imagen` VALUES ('69', 'producto28.jpg', '28', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('70', 'producto29.png', '29', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('71', 'producto30.jpg', '30', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('72', 'producto31.png', '31', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('73', 'producto32.jpg', '32', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('74', 'producto33.jpg', '33', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('75', 'producto34.png', '34', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('76', 'producto35.png', '35', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('77', 'producto36.jpg', '36', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('78', 'producto37.jpg', '37', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('79', 'producto38.jpg', '38', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('80', 'producto39.jpg', '39', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('81', 'producto40.png', '40', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('82', 'producto41.png', '41', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('83', 'producto42.png', '42', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('84', 'producto43.png', '43', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('85', 'producto44.jpg', '44', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('86', 'producto45.jpg', '45', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('87', 'producto46.jpg', '46', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('88', 'producto47.jpg', '47', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('89', 'producto48.png', '48', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('90', 'producto49.png', '49', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('91', 'producto50.jpg', '50', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('92', 'producto51.png', '51', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('93', 'producto52.jpg', '52', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('94', 'producto53.png', '53', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('95', 'producto54.jpg', '54', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('96', 'producto55.png', '55', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('97', 'producto56.png', '56', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('98', 'producto57.jpg', '57', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('99', 'producto58.jpg', '58', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('100', 'producto59.jpg', '59', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('101', 'producto60.jpg', '60', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('102', 'producto61.jpg', '61', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('103', 'producto62.jpg', '62', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('104', 'producto63.jpg', '63', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('105', 'producto64.jpg', '64', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('106', 'producto65.png', '65', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('107', 'producto.jpg', null, null, null, null);
INSERT INTO `tbl_imagen` VALUES ('108', 'producto66.png', '66', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('109', 'producto67.jpg', '67', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('110', 'producto68.png', '68', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('111', 'producto69.png', '69', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('112', 'producto70.png', '70', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('113', 'producto71.jpg', '71', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('114', 'producto.png', null, null, null, null);
INSERT INTO `tbl_imagen` VALUES ('115', 'producto72.png', '72', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('116', 'producto73.png', '73', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('117', 'producto74.png', '74', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('118', 'producto75.png', '75', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('119', 'producto76.png', '76', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('120', 'producto77.png', '77', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('121', 'producto78.png', '78', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('122', 'producto79.png', '79', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('123', 'producto80.png', '80', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('124', 'producto81.png', '81', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('125', 'producto82.jpg', '82', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('126', 'producto83.jpg', '83', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('127', 'producto84.jpg', '84', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('128', 'producto85.png', '85', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('129', 'producto86.png', '86', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('130', 'producto87.png', '87', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('131', 'producto88.png', '88', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('132', 'producto89.png', '89', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('133', 'producto90.png', '90', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('134', 'producto91.png', '91', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('135', 'producto92.png', '92', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('136', 'producto93.png', '93', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('137', 'producto94.png', '94', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('138', 'producto95.png', '95', null, null, null);
INSERT INTO `tbl_imagen` VALUES ('147', 'producto.jpg', null, null, null, null);
INSERT INTO `tbl_imagen` VALUES ('148', 'producto.jpg', null, null, null, null);
INSERT INTO `tbl_imagen` VALUES ('149', 'producto.jpg', null, null, null, null);
INSERT INTO `tbl_imagen` VALUES ('150', 'negocio123_0.png', null, null, '123', null);

-- ----------------------------
-- Table structure for tbl_movil
-- ----------------------------
DROP TABLE IF EXISTS `tbl_movil`;
CREATE TABLE `tbl_movil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imei` varchar(50) NOT NULL,
  `gcmid` varchar(300) NOT NULL,
  `id_personal` int(11) DEFAULT NULL,
  `id_usuarioplaystore` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_personal` (`id_personal`),
  KEY `id_usuarioplaystore` (`id_usuarioplaystore`),
  CONSTRAINT `tbl_movil_ibfk_1` FOREIGN KEY (`id_personal`) REFERENCES `tbl_personal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_movil_ibfk_2` FOREIGN KEY (`id_usuarioplaystore`) REFERENCES `tbl_usuarioplaystore` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_movil
-- ----------------------------
INSERT INTO `tbl_movil` VALUES ('1', 'SADFASGFDSGSADHHAFSDGSDF', 'APA91bFNsxyYKebgn02NnPn_F07npdCNhXI-CHzVdFFJQGNxkmES-6bZBOKW1Copy_scfIgYZZ4E5tMJB6AFGaW5c6giSRlUnpcxWiJgGv2fNDq-RfeZAyV3nJi8VLLgXGb4L207dNrte0yjLYk26bejU4xMZN3Dfg', '2', null);
INSERT INTO `tbl_movil` VALUES ('2', 'SADFASGFDSGSADHHAFSDGSDF', 'APA91bEpw5N772rGTJKydDp7oFDNJz8vN5rMZMR_bWsckesgkekkPK2ze5AsZFjlgitLdRzUJ0o-3eu78V0WG4gwCuo5ALBKOaWtsOn3km6F6UjBf2l0mLNWqZLaM6f_Oly0K0sopOmbw_3HuwA3JcDlMW6Rqsxx_A', '1', null);
INSERT INTO `tbl_movil` VALUES ('3', 'SADFASGFDSGSADHHAFSDGSDF', 'APA91bFNsxyYKebgn02NnPn_F07npdCNhXI-CHzVdFFJQGNxkmES-6bZBOKW1Copy_scfIgYZZ4E5tMJB6AFGaW5c6giSRlUnpcxWiJgGv2fNDq-RfeZAyV3nJi8VLLgXGb4L207dNrte0yjLYk26bejU4xMZN3Dfg', '2', null);
INSERT INTO `tbl_movil` VALUES ('4', 'SADFASGFDSGSADHHAFSDGSDF', 'APA91bEpw5N772rGTJKydDp7oFDNJz8vN5rMZMR_bWsckesgkekkPK2ze5AsZFjlgitLdRzUJ0o-3eu78V0WG4gwCuo5ALBKOaWtsOn3km6F6UjBf2l0mLNWqZLaM6f_Oly0K0sopOmbw_3HuwA3JcDlMW6Rqsxx_A', '1', null);
INSERT INTO `tbl_movil` VALUES ('5', 'SADFASGFDSGSADHHAFSDGSDF', 'APA91bFNsxyYKebgn02NnPn_F07npdCNhXI-CHzVdFFJQGNxkmES-6bZBOKW1Copy_scfIgYZZ4E5tMJB6AFGaW5c6giSRlUnpcxWiJgGv2fNDq-RfeZAyV3nJi8VLLgXGb4L207dNrte0yjLYk26bejU4xMZN3Dfg', '2', null);

-- ----------------------------
-- Table structure for tbl_negocio
-- ----------------------------
DROP TABLE IF EXISTS `tbl_negocio`;
CREATE TABLE `tbl_negocio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(300) NOT NULL DEFAULT '0',
  `nombre` varchar(45) NOT NULL,
  `razonsocial` varchar(45) DEFAULT NULL,
  `nit` varchar(15) DEFAULT NULL,
  `direccion` varchar(300) DEFAULT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `observacion` varchar(200) DEFAULT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_zona` int(11) NOT NULL,
  `fechacreacion` date NOT NULL,
  `fechamodificacion` date NOT NULL,
  `estado` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_tbl_negocio_tbl_cliente1_idx` (`id_cliente`),
  KEY `fk_tbl_negocio_tbl_zona1_idx` (`id_zona`),
  CONSTRAINT `fk_tbl_negocio_tbl_cliente1` FOREIGN KEY (`id_cliente`) REFERENCES `tbl_cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_negocio_tbl_zona1` FOREIGN KEY (`id_zona`) REFERENCES `tbl_zona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_negocio
-- ----------------------------
INSERT INTO `tbl_negocio` VALUES ('120', '0', 'sfasdf', '3453', '4534', 'dfgsdf', '-17.76863141454526', '-63.211552798748016', 'sdf', '172', '8', '2015-04-14', '2015-04-14', '1');
INSERT INTO `tbl_negocio` VALUES ('121', '0123cbee-83f9-4db4-93a8-820f3e2d5106', 'Barajas', 'Venta De Insumos ', '1010101001', 'Banzer ', '-17.782939479036', '-63.212466090918', 'portón negro\n', '173', '58', '2015-08-06', '2015-08-06', '1');
INSERT INTO `tbl_negocio` VALUES ('122', 'ccb7f3f5-e20e-4eb4-82cf-655643ddea6d', 'Maracas', 'Venta Por Abarrotes', '3340101010', 'Los Penocos ', '-17.855496251569', '-63.183125071228', 'portón amarrillo', '174', '131', '2015-08-06', '2015-08-06', '1');
INSERT INTO `tbl_negocio` VALUES ('123', '22b853cf-662b-4071-be7c-ce57d6a77935', 'Barba', 'Abarrotes', '101P1001', 'San Francisco ', '-17.855135637884', '-63.182113878429', 'portón negro', '175', '131', '2015-08-06', '2015-08-06', '1');

-- ----------------------------
-- Table structure for tbl_oferta
-- ----------------------------
DROP TABLE IF EXISTS `tbl_oferta`;
CREATE TABLE `tbl_oferta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `fecha` date DEFAULT NULL,
  `tipo` char(1) DEFAULT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `fechacreacion` date NOT NULL,
  `fechamodificacion` date NOT NULL,
  `estado` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_tbl_oferta_tbl_producto_idx` (`id_producto`),
  CONSTRAINT `fk_tbl_oferta_tbl_producto` FOREIGN KEY (`id_producto`) REFERENCES `tbl_producto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_oferta
-- ----------------------------
INSERT INTO `tbl_oferta` VALUES ('1', 'Oferta de Primavera', 'Gran descuento por Vasos al 50%', '2015-08-05', 'P', '25', '2015-08-06', '2015-08-06', '3');

-- ----------------------------
-- Table structure for tbl_ofertamovil
-- ----------------------------
DROP TABLE IF EXISTS `tbl_ofertamovil`;
CREATE TABLE `tbl_ofertamovil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` varchar(45) NOT NULL,
  `id_movil` int(11) NOT NULL,
  `id_oferta` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_movil` (`id_movil`),
  KEY `id_oferta` (`id_oferta`),
  CONSTRAINT `tbl_ofertamovil_ibfk_1` FOREIGN KEY (`id_movil`) REFERENCES `tbl_movil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_ofertamovil_ibfk_2` FOREIGN KEY (`id_oferta`) REFERENCES `tbl_oferta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_ofertamovil
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_pedido
-- ----------------------------
DROP TABLE IF EXISTS `tbl_pedido`;
CREATE TABLE `tbl_pedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` varchar(50) NOT NULL,
  `fechapedido` datetime DEFAULT NULL,
  `fechaentrega` datetime DEFAULT NULL,
  `estado` varchar(10) NOT NULL,
  `montototal` double NOT NULL,
  `create` datetime NOT NULL,
  `change` datetime DEFAULT NULL,
  `createby` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `id_negocio` int(11) NOT NULL,
  `id_promotor` int(11) DEFAULT NULL,
  `id_entregador` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_pedido_tbl_negocio1_idx` (`id_negocio`),
  KEY `fk_tbl_pedido_tbl_personal1_idx` (`id_promotor`),
  KEY `fk_tbl_pedido_tbl_personal2_idx` (`id_entregador`),
  CONSTRAINT `fk_tbl_pedido_tbl_negocio1` FOREIGN KEY (`id_negocio`) REFERENCES `tbl_negocio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_pedido_tbl_personal1` FOREIGN KEY (`id_promotor`) REFERENCES `tbl_personal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_pedido_tbl_personal2` FOREIGN KEY (`id_entregador`) REFERENCES `tbl_personal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_pedido
-- ----------------------------
INSERT INTO `tbl_pedido` VALUES ('1', '193f5a68-e7b2-4b11-943d-1ea9bd2cc08b', '2015-07-25 00:08:41', null, 'CANCELADO', '2244', '2015-07-25 00:08:44', '2015-07-25 00:14:49', '2', '2', '120', '2', null);
INSERT INTO `tbl_pedido` VALUES ('2', 'ac5745a9-6744-4113-bcd8-72843f55def9', '2015-07-25 00:15:35', null, 'PENDIENTE', '711', '2015-07-25 00:16:21', null, '2', null, '120', '2', null);
INSERT INTO `tbl_pedido` VALUES ('3', '9d105766-3ddf-4fa1-972d-b231c680c3f6', '2015-07-25 00:17:11', null, 'PENDIENTE', '197', '2015-07-25 00:17:13', '2015-07-25 00:18:14', '2', '2', '120', '2', null);
INSERT INTO `tbl_pedido` VALUES ('4', '63b10dd9-baf6-4074-9119-668340cd8a3f', '2015-08-06 11:44:54', null, 'ENTREGADO', '1230', '2015-08-06 11:45:20', '2015-08-06 11:46:11', '2', '2', '123', '2', null);

-- ----------------------------
-- Table structure for tbl_personal
-- ----------------------------
DROP TABLE IF EXISTS `tbl_personal`;
CREATE TABLE `tbl_personal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `sexo` char(1) DEFAULT NULL,
  `fechanacimiento` date DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(30) DEFAULT NULL,
  `cargo` varchar(45) DEFAULT NULL,
  `ci` varchar(15) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `tipo` char(1) DEFAULT NULL,
  `create` datetime DEFAULT NULL,
  `change` datetime DEFAULT NULL,
  `createby` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `estado` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_personal
-- ----------------------------
INSERT INTO `tbl_personal` VALUES ('1', 'Freddy', 'Quispe Fernandez', null, null, 'B/ Villa Mercedez', '75625642', 'Entregador', '654321', 'freddy@gmail.com', 'P', null, null, null, null, '0');
INSERT INTO `tbl_personal` VALUES ('2', 'Adan', 'Condori Callisaya', null, null, 'B/ Pedro Diez', '65431', 'Administrador', '6549872', 'adan@gmail.com', 'P', null, null, null, null, '0');

-- ----------------------------
-- Table structure for tbl_posicion
-- ----------------------------
DROP TABLE IF EXISTS `tbl_posicion`;
CREATE TABLE `tbl_posicion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `fechahora` datetime NOT NULL,
  `texto` varchar(200) DEFAULT NULL,
  `id_personal` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_personal` (`id_personal`),
  CONSTRAINT `tbl_posicion_ibfk_1` FOREIGN KEY (`id_personal`) REFERENCES `tbl_personal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_posicion
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_producto
-- ----------------------------
DROP TABLE IF EXISTS `tbl_producto`;
CREATE TABLE `tbl_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(50) DEFAULT NULL,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `medida` varchar(30) DEFAULT NULL,
  `costo` double DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `stock` int(11) NOT NULL,
  `porcentage` double DEFAULT NULL,
  `id_categoria` int(11) NOT NULL,
  `create` datetime DEFAULT NULL,
  `change` datetime DEFAULT NULL,
  `createby` int(11) DEFAULT NULL,
  `changeby` int(11) DEFAULT NULL,
  `estado` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_tbl_producto_tbl_categoria1_idx` (`id_categoria`),
  CONSTRAINT `fk_tbl_producto_tbl_categoria1` FOREIGN KEY (`id_categoria`) REFERENCES `tbl_categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_producto
-- ----------------------------
INSERT INTO `tbl_producto` VALUES ('22', '1000', 'Cubierto INIX', 'Material: Poliestireno INIX Medidas: Largo 16.5 Marca: Color: Negro Presentación: Caja 1  100 Unidades', '16 cm', '100', '150', '160', '0.5', '17', null, '2015-04-14 00:00:00', null, null, '0');
INSERT INTO `tbl_producto` VALUES ('23', '1001', 'Cucharas', 'Material: Poliestireno Medidas: Largo 13.5cm Marca: Fantasy Color: Blanco Presentación: Caja 1.  24 Unidades ', '13 cm', '200', '250', '665', '0.2', '17', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('24', '1002', 'Cuchillo', 'Material: Poliestireno Medidas: Largo 17 cm Marca: Fantasy Color: Blanco Presentación: Caja 1  24 Unidades', '17 cm', '300', '330', '773', '0.1', '17', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('25', '1005', 'Vaso Termico L', 'Vaso térmico de 20 cm con diseño de Frutas', '20 cm x 10 cm', '0', '22', '244', '0', '25', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('26', '1006', 'Vaso Termico M', 'Vaso termico de Frutas de 10 cm x 5 cm', '10 cm x 5 cm', '0', '18', '350', '0', '25', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('27', '1007', 'Tapas ', 'Tapas para vasos Termicos', '5 cm', '0', '8', '500', '0', '25', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('28', '1009', 'Botella Cooler', 'Botella con tomador para liquidos, con formas. de 30 cm :  1 Caja  contiene 12 Unidades ', '30 cm', '0', '25', '400', '0', '25', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('29', '2001', 'Envase 1 litro Reyma', 'Piezas por paquete: 25; Paquete por caja: 12; Unidad por caja: 300; Capacidad de ml: 1065', '1065 ml', '0', '25', '240', '0', '18', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('30', '1011', 'Vita Film L', 'Cinta de plastico de 50 cm  transparente  1 caja tiene 6 Unidades', '50 cm', '0', '80', '300', '0', '31', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('31', '2002', 'Envase 1 Lt. Vamsa', 'Piezas por paquete: 25; Paquete por caja: 12; Unidad por caja:	300; Capacidad de ml:	1065', '1065 ml', '0', '25', '292', '0', '18', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('32', '1012', 'Vita Film M.  EGA ', 'Materia: Película plástica grado alimenticio. Medidas:  38cm,  1200 Mts. ( 5 Kg ) Marca: EGA PAC Color: Transparente. Presentación: 1 CAJA  6 Unidades', '38 cm', '0', '85', '350', '0', '31', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('33', '1013', 'Vita Film S.', 'Vita Film pequeño de 15 cm', '15 cm', '0', '56', '294', '0', '31', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('34', '2003', 'Envase 1/2 Lt. Reyma', 'Piezas por paquete: 25 Paquete por caja: 20 Unidad por caja: 500 Capacidad de ml: 500', '500 ml', '0', '15', '300', '0', '18', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('35', '2004', 'Envase 1/2 Lt. Vamsa', 'Piezas por paquete: 25 Paquete por caja: 12 Unidad por caja: 300 Capacidad de ml: 600', '600 ml', '0', '15', '300', '0', '18', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('36', '1014', 'Botella S', 'Botella Pequeña, tranparente de 15 cm', '15 cm', '0', '50', '346', '0', '30', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('37', '1015', 'Botellon', 'Botellon de 20 litros con medida de 50 cm  por unidad ', '50 cm', '0', '80', '380', '0', '30', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('38', '1016', 'Botellon Mediano', 'Botellon de 5 Litros de 30  cm . venta por unidad', '30 cm', '0', '25', '345', '0', '30', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('39', '1016', 'Botellon Mediano', 'Botellon de 5 Litros de 30  cm . venta por unidad', '30 cm', '0', '25', '347', '0', '30', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('40', '2005', 'Popote corrugado estuchado', '2000 piezas por caja, blanco y natural.', '22 cm', '0', '28', '300', '0', '19', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('41', '2006', 'Popote corto granel estuchado', '2000 piezas por caja, blanco y natural.', '21 cm', '0', '26', '289', '0', '19', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('42', '2007', 'Popote en caja corrugado estuchado', '500 piezas por caja / 4 cajas por caja de empaque, blanco y natural.', '22 cm', '0', '26', '298', '0', '19', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('43', '2008', 'Popote en caja corto estuchado', '500 piezas por caja / 4 cajas por caja de empaque, blanco y natural.', '21 cm', '0', '24', '300', '0', '19', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('44', '1019', 'Hoja de Papel Aluminio', 'Hojas de papel aluminio de 30 de ancho', '30 cm', '0', '30', '498', '0', '29', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('45', '1020', 'Hojas de papel aluminio', 'Capacillos rojos No. 72', '5 cm', '0', '5', '350', '0', '29', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('46', '1021', 'Blondas', 'Blondas para mesa venta por caja. 1 Caja tiene 100 unidades ', '10 cm', '0', '100', '290', '0', '29', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('47', '1023', 'Tapas para bebidas calientes', 'Material: POLIPRO Medidas: Varias Marca: SOLO CUP y DART Color: Blanca Presentacón:  Caja 10/100   1000 PZ', '8 cm', '0', '60', '300', '0', '29', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('48', '2009', 'Popote en caja largo estuchado', '500 piezas por caja / 4 cajas por caja de empaque, blanco y natural.', '26 cm', '0', '21', '300', '0', '19', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('49', '2010', 'Popote en caja rayado y natural', '150 piezas por caja, rayado y natural.', '21 cm', '0', '28', '300', '0', '19', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('50', '1025', 'Red para cabello Negro', 'Red para cabello de color  negro', '5 cm', '0', '10', '300', '0', '29', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('51', '2011', 'Popote en caja rayado y natural', '150 piezas por caja, rayado y natural.', '26 cm', '0', '26', '300', '0', '19', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('52', '2012', 'Popote estuchado papel celofan PRIMO', 'Material: Plástico virgen. Medidas: 21 cm Marca: PRIMO Color: Natural. Presentación: Caja 2000 piezas.', '21 cm', '0', '35', '300', '0', '19', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('53', '2013', 'Popote largo estuchado', '2000 piezas por caja, blanco y natural.', '26 cm', '0', '32', '300', '0', '19', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('54', '1027', 'Agitador para café', 'Agitador para café de color marron de 15  cm. 1 Bolsa contiene 100 unidades', '15 cm', '0', '45', '346', '0', '29', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('55', '2014', 'Popote Neon granel corto', '1,700 pzas. Aproximadamente por paquete. color: verde, amarillo, naranja, rojo y azul', '21 cm', '0', '29', '297', '0', '19', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('56', '2015', 'Popote Neon granel largo', '1,350 pzas. Aproximadamente por paquete. color: verde, amarillo, naranja, rojo y azul', '26 cm', '0', '32', '300', '0', '19', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('57', '2016', 'Popote Sipp', 'Popote Sipp', '25 cm', '0', '34', '300', '0', '19', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('58', '2017', 'Vaso 10 Oz JAGUAR', 'Material: Polipropileno. Medidas: 10 oz. Marca: JAGUAR Color: Natural. Presentación: Caja 20/50   1000 piezas.', '10 oz', '0', '18', '295', '0', '20', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('59', '1029', 'Higiénico fapsa hd360', 'Higiénico fapsa hd360', '10 cm', '0', '30', '300', '0', '28', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('60', '2018', 'Vaso 10 Oz REYMA', 'Material: Polipropileno. Medidas: 10 oz. Marca: REYMA Color: Natural. Presentación: Caja 20/50   1000 piezas.', '10 oz', '0', '19', '293', '0', '20', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('61', '1030', 'Higiénico predut hd 200', 'Higiénico predut hd 200 1 Caja contiene 6 Rollos', '20 cm', '0', '60', '350', '0', '28', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('62', '2019', 'Vaso 12 Oz REYMA', 'Material: Polipropileno. Medidas: 12 oz. Marca: REYMA Color: Natural. Presentación: Caja 20/50   1000 piezas.', '12 oz', '0', '34', '300', '0', '20', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('63', '1031', 'Bolsa baja densidad rollo', 'MATERIAL: BAJA DENSIDAD MEDIDAS: 40 X 60 CALIBRES :  150 COLOR: NATURAL PRESENTACION:  ROLLO', '40 cm x 60 cm', '0', '80', '345', '0', '27', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('64', '2020', 'Vaso para bebidas calientes 4 oz', 'Material: Papel encerado. Medidas: 4 oz. Marca: SOLO CUP Color: Tonalidades en cafe. Presentación:  Caja 20/50   1000 PZ', '4 oz', '0', '29', '300', '0', '20', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('65', '2021', 'Vaso Poliestireno Molde Cristal 10', 'Vaso transparente, ideal para gelatina en tus fiestas infantiles.  Piezas por paquete	50 Paquete por caja	20 Unidad por caja	1000 Capacidad de ml	75', '10 cm', '0', '26', '300', '0', '20', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('66', '2022', 'Vaso Poliestireno no. 5 A', 'Piezas por paquete	50 Paquete por caja	20 Unidad por caja	1000 Capacidad de ml	84', '5 cm', '0', '22', '300', '0', '20', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('67', '1034', 'Bolsa Fuelle Alta Densidad Kilogramos 25×50', ' En fuelle de alta densidad se pueden manejar colores y medidas especiales sobre pedido, consultarlo con su agente de ventas * Aplica restricciones.', '25cm x 50cm', '0', '35', '350', '0', '27', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('68', '2023', 'Vaso Polipropileno Bicolor No. 16', 'Piezas por paquete	25 Paquete por caja	20 Unidad por caja	500 Capacidad de ml	525', '10 cm', '0', '29', '300', '0', '20', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('69', '2024', 'Vaso Polipropileno no. 12', 'Piezas por paquete	50 Paquete por caja	20 Unidad por caja	1000 Capacidad de ml	311', '12 cm', '0', '35', '300', '0', '20', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('70', '1035', 'Bolsa Fuelle Baja Densidad Negro Kilogramos', 'Bolsa Fuelle Baja Densidad Negro Kilogramos', '50 cm', '0', '18', '500', '0', '27', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('71', '1038', 'Bolsa negra alta densidad', '40 x 48 Pulgada (NEGRA) 24 X 24 Pulgadas ( NATURAL) CALIBRE: 100 COLOR: NEGRO Y NATURAL PRESENTACION: ROLLO SELLO ESTRELLA', '40 x 48 Pulgada', '0', '50', '500', '0', '27', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('72', '2025', 'Plato pastelero 006', 'Piezas por paquete	25 Paquete por caja	20 Unidad por caja	500 Capacidad de ml	200', '25 cm', '0', '47', '300', '0', '23', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('73', '2026', 'Plato pozolero PH10', 'Piezas por paquete	25 Paquete por caja	20 Unidad por caja	500 Capacidad de ml	630', '22 cm', '0', '54', '300', '0', '23', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('74', '2027', 'Plato Térmico 10 división', 'Piezas por paquete	25 Paquete por caja	20 Unidad por caja	500 Capacidad de ml	600', '24 cm', '0', '52', '300', '0', '23', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('75', '1040', 'Contenedor de poliestireno R500', ' Piezas por paquete	50 Paquete por caja	2 Unidad por caja	100 Capacidad de ml	4000', '30 cm', '0', '150', '500', '0', '26', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('76', '2028', 'Plato Térmico 9 c2 división', 'Piezas por paquete	25 Paquete por caja	20 Unidad por caja	500 Capacidad de ml	700', '24x28', '0', '59', '300', '0', '23', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('77', '1041', 'Contenedor 7×7 div doble bisagra', 'Contenedor 7×7 div doble bisagra', '10 cm x 8 cm', '0', '15', '500', '0', '26', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('78', '1043', 'Contenedor 7×7 liso doble bisagra', 'Contenedor 7×7 liso doble bisagra', '10cm x 8cm', '0', '35', '497', '0', '26', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('79', '1044', 'Contenedor 8×8 div', 'Contenedor 8×8 div', '8cm x 8cm', '0', '35', '500', '0', '27', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('80', '1045', 'Contenedor 8×8 liso', 'Contenedor 8×8 liso color Blanco', '8cm x 8cm', '0', '26', '500', '0', '26', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('81', '1047', 'Contenedor 9×9 div', 'Contenedor 9×9 div color blanco liso', '8cm x 8cm', '0', '28', '500', '0', '26', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('82', '1049', 'Contenedor Base Negra CPP2415N', 'MATERIAL: POLIESTRILENO CONTENEDOR BASE NEGRA CPP2415N MEDIDAS: LARGO: 24m ANCHO: 15cm ALTO: 6cm CALIBRE: 120 COLOR: BASE NEGRA /DOMO NATURAL PRESENTACION: CAJA 100 PZ AS', '24 cm x 8cm', '0', '45', '500', '0', '26', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('83', '1050', 'Contenedor Base Negra FH 1613-23N', 'MATERIAL: POLIESTRILENO CONTENEDOR BASE NEGRA FH 1613-23N MEDIDAS: LARGO: 16m ANCHO: 13cm ALTO: 5cm CALIBRE: 120 COLOR: BASE NEGRA /DOMO NATURAL PRESENTACION: CAJA 250 PZ AS', '16cm x 8cm', '0', '35', '500', '0', '26', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('84', '1051', 'Contenedor Base Negra Redondo', 'MATERIAL: POLIESTRILENO CONTENEDOR BASE NEGRA  FH17N MEDIDAS: DIAMETRO: 17 cm CALIBRE: 120 COLOR: BASE NEGRA /DOMO NATURAL PRESENTACION: CAJA 150 PZ AS', '10cm', '0', '25', '500', '0', '26', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('85', '1052', 'Contenedor de poliestireno R10', 'piezas por paquete	25 Paquete por caja	4 Unidad por caja	100 Capacidad de ml	1500', '20cm', '0', '56', '500', '0', '26', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('86', '1053', 'Contenedor de poliestireno R12', ' Piezas por paquete	250 Paquete por caja	1 Unidad por caja	250 Capacidad de ml	650', '15cm', '0', '42', '496', '0', '26', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('87', '1054', 'Charola 007 reyma', ' Piezas por paquete	50 Paquete por caja	10 Unidad por caja	500 Capacidad de ml	600', '10cm', '0', '25', '492', '0', '24', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('88', '1055', 'Charola 1014', ' Piezas por paquete	200 Paquete por caja	1 Unidad por caja	200 Capacidad de ml	1060', '10cm', '0', '23', '488', '0', '24', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('89', '1056', 'Charola 4P', ' Piezas por paquete	300 Paquete por caja	1 Unidad por caja	300 Capacidad de ml	700', '10cm', '0', '53', '500', '0', '24', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('90', '1057', 'Charola 5D', ' Piezas por paquete	125 Paquete por caja	4 Unidad por caja	500 Capacidad de ml	850', '10cm', '0', '42', '500', '0', '24', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('91', '1058', 'Charola 8S', ' Piezas por paquete	500 Paquete por caja	1 Unidad por caja	500 Capacidad de ml	520', '10cm', '0', '35', '500', '0', '24', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('92', '1059', 'Charola 9H', ' Piezas por paquete	200 Paquete por caja	1 Unidad por caja	200 Capacidad de ml	1350', '10cm', '0', '15', '500', '0', '24', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('93', '2029', 'Plato Térmico 8 división', 'Piezas por paquete	20 Paquete por caja	25 Unidad por caja	500 Capacidad de ml	500', '25 cm', '0', '53', '298', '0', '23', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('94', '1060', 'Charola 9', ' Piezas por paquete	200 Paquete por caja	1 Unidad por caja	200 Capacidad de ml	1220', '10cm', '0', '42', '500', '0', '24', null, null, null, null, '0');
INSERT INTO `tbl_producto` VALUES ('95', '2030', 'Plato Térmico 9 división', 'Piezas por paquete	25 Paquete por caja	20 Unidad por caja	500 Capacidad de ml	700', '32 cm', '0', '64', '300', '0', '23', null, null, null, null, '0');

-- ----------------------------
-- Table structure for tbl_proveedor
-- ----------------------------
DROP TABLE IF EXISTS `tbl_proveedor`;
CREATE TABLE `tbl_proveedor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `razonsocial` varchar(50) DEFAULT NULL,
  `nit` varchar(50) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(30) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_proveedor
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_trazo
-- ----------------------------
DROP TABLE IF EXISTS `tbl_trazo`;
CREATE TABLE `tbl_trazo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `indice` int(11) NOT NULL,
  `id_zona` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_trazo_tbl_zona1_idx` (`id_zona`),
  CONSTRAINT `fk_tbl_trazo_tbl_zona1` FOREIGN KEY (`id_zona`) REFERENCES `tbl_zona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_trazo
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_usuario
-- ----------------------------
DROP TABLE IF EXISTS `tbl_usuario`;
CREATE TABLE `tbl_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) NOT NULL,
  `contrasena` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `conectado` char(1) DEFAULT NULL,
  `id_personal` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_usuario_tbl_personal1_idx` (`id_personal`),
  CONSTRAINT `fk_tbl_usuario_tbl_personal1` FOREIGN KEY (`id_personal`) REFERENCES `tbl_personal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_usuario
-- ----------------------------
INSERT INTO `tbl_usuario` VALUES ('1', 'freddy', '123456', 'freddy', 'S', '1');
INSERT INTO `tbl_usuario` VALUES ('2', 'adan', '123456', 'adan@gmail.com', 'S', '2');

-- ----------------------------
-- Table structure for tbl_usuarioplaystore
-- ----------------------------
DROP TABLE IF EXISTS `tbl_usuarioplaystore`;
CREATE TABLE `tbl_usuarioplaystore` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  `fechanacimiento` date DEFAULT NULL,
  `sexo` char(1) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_usuarioplaystore
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_zona
-- ----------------------------
DROP TABLE IF EXISTS `tbl_zona`;
CREATE TABLE `tbl_zona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `poligono` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=187 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_zona
-- ----------------------------
INSERT INTO `tbl_zona` VALUES ('1', 'UV-001', '', 'lymkBzhs`Km@ocAhQbPpDvEnBpEe@jH[tEBvEn@jLgL`@');
INSERT INTO `tbl_zona` VALUES ('2', 'UV-002', '', 'nsnkB|br`KsBuEeDaEuEiEsEmE_DsD`@wDz@oD~BoFnDiEfE_DdC_Ax@`@v@rEjI|P~EjF`A|A|@rDcEhAsDjBwBtCc@HQPCN@L');
INSERT INTO `tbl_zona` VALUES ('3', 'UV-003', '', '|gokB|lq`Ku@uCeB}CcDoD_A_BqG{Mu@eEOgAv@w@|AOzMgB`J_A`FUrFUhAHb@d@jClj@i@n@gI`@aIr@gCXeBU');
INSERT INTO `tbl_zona` VALUES ('4', 'UV-004', '', 'lfpkBtgq`K}B_j@Rw@r@YxQUhHC}@dEqAnHk@tCmArDiChEY|AJpAdAhEU^oL|@UU');
INSERT INTO `tbl_zona` VALUES ('5', 'UV-005', '', 'vupkBxeq`KiAeGhA}CxBiE`BeEhAqG|@kFNcAp@MpBPdG|ArFxAdFdApD`@bCJf@d@w@dLaBvQmAbLOXa@Gk@i@e@w@[w@k@YiBkBoBoByCcA');
INSERT INTO `tbl_zona` VALUES ('6', 'PU-C', '', 'larkBztq`KnFon@`AUhB`@hAZ`BIx@@h@JuFpn@}Bf@}BOmAYm@Q');
INSERT INTO `tbl_zona` VALUES ('7', 'UV-006', '', 'rorkBrvq`KrFao@zCXpB~@`BdA|AbCx@dCb@zDShCgC~ZwAi@qAw@sBm@wBEwBN}@L');
INSERT INTO `tbl_zona` VALUES ('8', 'UV-C', '', '|mpkB|pt`KeSFoJmAgJmEcEcHqCgJyAaKqAaK?uLhBmLxAuEbFwFdGuAjPoBfWwBvUgBrDGhClAzBlBfA`CXdEvEjg@fEve@iA`@qM`@cFz@gJpC{EtA');
INSERT INTO `tbl_zona` VALUES ('9', 'UV-007', '', '`brkB`bs`KsFwn@f@Yn@X~ClCzDbArEQ|Ea@nDCpChAtBdBd@nBfFhX[`A');
INSERT INTO `tbl_zona` VALUES ('10', 'UV-008', '', 'lgrkBfdt`KkDu`@~f@wDnATjHv^wl@hF');
INSERT INTO `tbl_zona` VALUES ('11', 'UV-009', '', 'hdskBrgu`Kc[q_@Ve@pk@mEp@rD]zJiCxHoFjG');
INSERT INTO `tbl_zona` VALUES ('12', 'UV-010', '', '|rqkBvsu`Kw@}Lq@sHqCeEy@sH~JwBpNk@`AXzZv_@uEhDsFtA_IHyOp@');
INSERT INTO `tbl_zona` VALUES ('13', 'UV-011', '', 'lspkBhwu`KSa@uAcb@l@UvESpFaA|GmBbAvFx@pC`BrBb@lG_@z@b@Th@fJKn@oUjA');
INSERT INTO `tbl_zona` VALUES ('14', 'UV-012', '', 'zqokBnou`Kc@g@dDcZNQVExLJvGCPFPZ|Ar`@Mh@e@RuDY');
INSERT INTO `tbl_zona` VALUES ('15', 'UV-013', '', 'h|nkB~hu`KwHcByBsAu@cArDup@xAOzClNhBfEzCfD|BfBjDnAhCr@vBLb@`@_@|CuA`Mi@`Gc@PqL}B');
INSERT INTO `tbl_zona` VALUES ('16', 'UV-014', '', 'tdnkBhtt`Ku@yCCm@[o@{DkSeAaGKoDr@UrVa@hA`E_BFuDll@YjBkCsD');
INSERT INTO `tbl_zona` VALUES ('17', 'UV-015', '', 'txlkB~as`KwVmHcHuBoBmCSwKHuFbAc@^@fZvG`HrBrX`J`AfBPlQc@ZkE?');
INSERT INTO `tbl_zona` VALUES ('18', 'UV-016', '', 'rwmkBlrr`Koa@aNu]{HmAiAd@cJhBoQpByJx@Yfu@lTd@ZZz@');
INSERT INTO `tbl_zona` VALUES ('19', 'UV-017', '', 'dvlkB`up`K_WkHi@{@tGyQtGyLpC{CnA@hHhGvJzIvQrUJlA}B`Gq@nD[dC');
INSERT INTO `tbl_zona` VALUES ('20', 'UV-018', '', 'dvlkB`co`KImAlF_FfG_EbHuDlHsClAj@xEvIvDlLlFfZDv@Kj@a@j@kE~BeFvEe@@');
INSERT INTO `tbl_zona` VALUES ('21', 'UV-019', '', 'tpnkBz`p`KeGo[qDwMsFyJR_AnQ}DzMyBtDr]pDl^cCX');
INSERT INTO `tbl_zona` VALUES ('22', 'UV-023', '', '|cqkBpvo`K?SFY|@uE^w@VSb@Un@K~@AbFz@nB`@lBXtBLrBEnFu@`Cm@Z?XNb@pDH`BD|AC`DKrBK^OZa@PSBS?sAQmKgAyCo@yCs@uI{BwCo@QKGK');
INSERT INTO `tbl_zona` VALUES ('23', 'UV-020', '', 'djokBv{o`K_Ju|@lEa@lFW`FE`F?~An]vAt[Sd@YV');
INSERT INTO `tbl_zona` VALUES ('24', 'UV-021', '', 'rbpkBtwo`KsDuz@|BLrAj@fA|@dAnA~@vAxBjDx@nBC^F^NPZJfAdIvAnDfDnD`FhFh@|@DrBWpAmAv@}FIeGT}CHmFd@');
INSERT INTO `tbl_zona` VALUES ('25', 'UV-022', '', 'nipkBhcn`KiB}BcBqAyAe@rReI`@H^`@@bCEfEGjEW`Du@nB_@Ls@?]O');
INSERT INTO `tbl_zona` VALUES ('26', 'UV-025', '', '`fskBtwq`KfAqMfAiLCkDT]~EgCnGmDpJmFtCaB|@En@`@rAfCfCdIxEdOzAhFOj@g@^eNtDcQtEsGdBoGz@}AHe@w@');
INSERT INTO `tbl_zona` VALUES ('27', 'UV-024', '', 'xhskBzqp`KgAcCiAyAqCiBmDkAsFy@Ke@R_KmAkQPoEz@}H`AmDhA]zJvE~K`IdG`GdGxGtIhM]pAqGjDqHdE{D|B_EjB}@D');
INSERT INTO `tbl_zona` VALUES ('28', 'UV-026', '', 'hmskB`yr`KsBcLmBaLVm@zKeBdNsDxI}BnQwEt@p@jErIjFfLhFvOlEbPmAVeMmEyE_B}EiA}TCyWR');
INSERT INTO `tbl_zona` VALUES ('29', 'UV-027', '', 'v{skBrzt`KkC{Ez@uKi@aGoDmRsB{MdGUpKKxWG|GtApTzHpAbAdAfB`@rHWxDg@vDgHrTwAr@eSNc\\L');
INSERT INTO `tbl_zona` VALUES ('30', 'UV-028', '', 'xfskBniu`KzGoGdCeDjBqEtCrEf@QrTOlNCfPKJj@_CpHuDdJuG`L}K~IuGdFcAb@m@]aFeG_IeJuMqO');
INSERT INTO `tbl_zona` VALUES ('31', 'UV-029', '', 'rlrkB~mw`K}GyS}EgOwFuPTg@fVcArHc@zCw@rCiA|CqB`EzEhN`P|KtMWjAgKxG_MbGqMxDqMxC');
INSERT INTO `tbl_zona` VALUES ('32', 'UV-030', '', 'bvpkBjsw`Km@k[a@gWEiDdCYvJa@xLi@x@^nBtFbBdF~A`F|AzEnEzMdDpKc@fAsEp@wNv@mFDkFK');
INSERT INTO `tbl_zona` VALUES ('33', 'UV-031', '', 'dfokBfkw`KhCgYjBcSfAwJzAXzNtCzEx@|EZ`@p@JvDRbKPxLf@rUI`AcFI_e@sD');
INSERT INTO `tbl_zona` VALUES ('34', 'UV-032', '', 'pdnkBxdw`K|AyVxBc]`@{G^YjFxBfGnA`LvBpCr@U~GuAjOaBvNqBbPeA`@qIs@mJsAsEa@');
INSERT INTO `tbl_zona` VALUES ('35', 'UV-033', '', 'hankBlfw`KeE{@sDgAmGqD}I_KsGcKwBmERs@dMkH|HiExFgDdI}GfCuBbBnCdBvBg@|LwA|SgAjPu@pL');
INSERT INTO `tbl_zona` VALUES ('36', 'UV-034', '', 'pslkBf{u`K_EoIeCiGm@iBVy@vAg@zAKdLc@|EWhHwAjHsCvIqFh@JnBhHuJbIkN|HmLzGaEvB');
INSERT INTO `tbl_zona` VALUES ('37', 'UV-035', '', '|dlkB~}t`KwA{FiCoJTeA~Am@`CWjBPpG`AnGlApAJxAItA[jAi@nAoAbA}AfAaCzBeBfCq@tDq@d@^jA|FrAnGRhA{@r@eGxDsGdDaGzAgPx@uK\\');
INSERT INTO `tbl_zona` VALUES ('38', 'UV-036', '', 't|kkBret`KiBsLyAsNa@iJAqEx@?pJlCrQbFnP~EjEh@`ITd@dFd@bE{FjAoDnAeAtAiBtDiBnBeCn@gBH{FgAmIaBaDOqDv@');
INSERT INTO `tbl_zona` VALUES ('39', 'UV-037', '', 'lzikBbxq`K|@cL~@sNh@m@p@AlJbFjK`GzHfEnFfDMrHFpO]F_IyB}LwDqJoCeIuC');
INSERT INTO `tbl_zona` VALUES ('40', 'UV-038', '', 'fjkkBnrq`K}MoH_LkGaJaFs@kAg@qBW}@P_D^gEl@sG`@wBdBw@zKtC~MzD|NhEmApH}AbLsAnP');
INSERT INTO `tbl_zona` VALUES ('41', 'UV-039', '', 'rfjkBnpo`KnAuFfBsFbGiNfH{K`BuBjBKzMpHfNnI`HhEAv@cGtImGtLkFxOi@ReSqFqMsDaGuC');
INSERT INTO `tbl_zona` VALUES ('42', 'UV-040', '', 'fklkB|yn`KqM_IsNoIwFsDAsBrLqK`KsH~LuG`GqBbC^hE`HnKbOxEjIEv@uNdHkIvF}H`H');
INSERT INTO `tbl_zona` VALUES ('43', 'UV-041', '', 'xsmkBt}m`K_HcLiGsI}AqB}CeGDcBdCeAhMkDlCc@pMWhS@~AfAb@~Mr@nJZvGc@`@uLdBqHbBmKdC');
INSERT INTO `tbl_zona` VALUES ('44', 'UV-042', '', 'r}nkBrrm`KaAsLk@eHIgJr@eAnF@dIZlCbB~ApCbArEt@fF^rFL`DmMVkELwER');
INSERT INTO `tbl_zona` VALUES ('45', 'UV-043', '', 't|okBdzm`Kq@wMcAeLoCwKb@g@fP|@rFNtCJx@d@x@vAB|AE~EIbGm@vCiBbCiHtCcHzC');
INSERT INTO `tbl_zona` VALUES ('46', 'UV-043B', '', 'zmrkBlgo`KsH{Y_HuWiJi]VClRb@zOlBhLbCr@^`@~@@z@sFzQ_G|QmBbGe@fCWfCa@zEUxE');
INSERT INTO `tbl_zona` VALUES ('47', 'UV-044', '', '`wrkB~hn`KvDmLrDoLfCeI|@u@`BM`E|@xJlDnJ|EvHhF@xASjAuGzIyH`LoHpKkAQ{E}DsHeFuJkF');
INSERT INTO `tbl_zona` VALUES ('48', 'UV-045', '', 'nttkB`wo`K{BgKsBkF_GgGaDuBH}@pF_IrE}GxDkFnA@pEbEpE|FxFbJtCvFVxBGlCi@fBmA`BcClAuBbA{EdCuF`C');
INSERT INTO `tbl_zona` VALUES ('49', 'UV-046', '', 'hlukBxeq`K_GuJqBgGmFkPuAcEbAq@~NkHdLyFvBTzBnCnJhM|FvHN~Ae@vAgLzHcRtM');
INSERT INTO `tbl_zona` VALUES ('50', 'UV-047', '', 'v}ukBjhr`KoD}LaAaDgAmCtC{BpLqItLgI`GgEnGbJpDvFxDfHbDpJbBdGExA]l@oOZyOXaJNiIv@');
INSERT INTO `tbl_zona` VALUES ('51', 'UV-048', '', 'tivkBvis`KaCaLwBmIaAoDNq@fHu@rP]dPYrGGzA^jAjAz@nFVrDZdK?xKo@p@s@ZmKu@sOmAaHk@{Fc@');
INSERT INTO `tbl_zona` VALUES ('52', 'UV-049', '', 'zavkBbvt`KjDsK~AiHb@_P`@SdKt@lS~A`L`Av@r@d@bAHtAUhFm@fJwClCgBZwI|@yMpAuP`BkDQWmA');
INSERT INTO `tbl_zona` VALUES ('53', 'UV-050', '', '`sukBzvu`KrEqKfEkMv@[fCf@nJs@rO{ArLiAdGg@b@XP`AsAbKmDtPsFvQqEdKsBl@gBW}MuIeJgGgF_DgFeC');
INSERT INTO `tbl_zona` VALUES ('54', 'UV-051', '', '~wukBvex`KyKwMaPgRaGcHJc@rHkH|JyJvGmHnBMjCx@vN`J|IzFfG~D`AlB?zBsEvIkHfLcJhKiErDeA@');
INSERT INTO `tbl_zona` VALUES ('55', 'UV-052', '', 'd`skBpvy`KmFc`@mCyQk@aFb@u@zIgBdNqDbM{FpQqKn@AzCdDzG|HfL|MjM~NgNtJsJpGqJdF_KfEgKnDgVrF');
INSERT INTO `tbl_zona` VALUES ('56', 'UV-053', '', 'l}qkBzzy`KkB{UsBuVe@wHf@w@fHw@|I{AnDk@r@l@tBjOzBhPrB|N|@jGk@|B_A^_AR_Jp@cIPsAA{@]');
INSERT INTO `tbl_zona` VALUES ('57', 'UV-054', '', 'hypkB|xy`Kg@wY_@wTKuE`@u@`KVhGLxD?`A`AlAnNbBlTz@tKXxDi@rAw@NwF?{JCgG?_BcA');
INSERT INTO `tbl_zona` VALUES ('58', 'UV-055', '', 'pfokB|zy`KOaTUuXMmQl@w@zJh@|QfB|LbBz@jAZhMb@pV^`Si@t@gPIgMGwN_@');
INSERT INTO `tbl_zona` VALUES ('59', 'UV-056', '', 't}mkBlwx`KdA}Nt@kKl@sFp@a@`Dt@dD|@nBj@pAHrHD`C@d@d@JlJFtMNjOLdMDvGcALsGmCiImGaG}GsFiGyAgB');
INSERT INTO `tbl_zona` VALUES ('60', 'UV-057', '', 'j}lkBhxw`KvD}D`DuC`A]l@a@vF|@nEr@hEj@V^Nd@EzAWxCm@jHmAjQQVa@A_IeImG_GyJsI');
INSERT INTO `tbl_zona` VALUES ('61', 'UV-058', '', '|ylkBlww`KwIeFsH}EuGqBeHmAkIkCcB{@@YrQmKhK}FbDiB`B@vEpIlFpIhFdHtCtDFvA_DnDsFdF');
INSERT INTO `tbl_zona` VALUES ('62', 'UV-059', '', 'j`kkBlzv`KwIoQwGkPcDeMTiBpAgBlCkAbEo@~EJfMtAhJdAnAN|C~FvFzKjDnGShAsPnJeMdHoDtBqAG');
INSERT INTO `tbl_zona` VALUES ('63', 'UV-059A', '', 'j{jkBtzv`KcXcHiUgFwPmM`CaBjKeBtGaAzLeHnAkAn@`@hAdElBjGjFzN|HhQ');
INSERT INTO `tbl_zona` VALUES ('64', 'UV-060', '', 'ddjkB`_u`KoByJgA_H?qAt@k@rQ~A`LfArQbBxA`Ah@pEr@vEQPeCSeKiAwMmAyEVwBb@gBv@eDdB');
INSERT INTO `tbl_zona` VALUES ('65', 'UV-061', '', 'j|ikBh`t`Ky@iHs@cKMgE@}Av@o@|Eb@xN~AlMzApHz@pBXf@p@j@rFr@pH|@pI^nDe@PkPwAuLeAgIy@kGo@wAmA');
INSERT INTO `tbl_zona` VALUES ('66', 'UV-062', '', 'xwikBzxr`KIkEN}G`@oF`@{AzAF~GpBhHpBtHzBhG`BtEzA~@^IpA_@fCRvBt@dDn@nC@v@_@`@cC]mMwA{KkAgH{@oFw@yAkA');
INSERT INTO `tbl_zona` VALUES ('67', 'UV-063', '', 'lxikBtdt`KeQvDkOdDcEz@CyA[iTJoCMyH@{DG}DpB{@t@Bm@cFKgCScGAkEZ}BhAuHn@oDjAX~KtCtJvCfBbA~@fDe@rIE|Ki@bBc@`@`A`CZj@XpDrBbWA`A');
INSERT INTO `tbl_zona` VALUES ('68', 'UV-064', '', 'nohkBvqt`KeJfByCj@aAg@Y_Cw@sKBaMXqMmBgE_BsC}Bw@qEaB`@yAzD_O~E}PdDqKlD~@nMrDfBj@c@xE_@zCaAvFCbCNpFZhGf@jDi@HgCx@ATPlB@fHH`FAx@UrARjDJlKJpE');
INSERT INTO `tbl_zona` VALUES ('69', 'UV-065', '', 'fsikBryq`K_LaDyM_EoOgE_GkBGc@zCaLbCmJpAkEt@S|K~CxPbFpLnDbAfAn@hBq@lLu@jJg@vCcA`@');
INSERT INTO `tbl_zona` VALUES ('70', 'UV-066', '', 'zyikB|sp`KqMsDeMuDsNiEYcADiAfBcHlCkJn@gBn@KvQfFnJfClH`Cl@hAPdAu@fFg@rFo@~G_@z@');
INSERT INTO `tbl_zona` VALUES ('71', 'UV-067', '', 'x_jkBjso`KwPaFuMoD_GiBKe@r@oC`DcMlEuOpDyK~A}EvB{HhAsD|InFrL`HdLlHfEvCGlDcE`GyFzJmEtKsD|Lo@pB');
INSERT INTO `tbl_zona` VALUES ('72', 'UV-331', '', 'zbgkBp`u`KaNrCwDx@_Oe@iHIoImBcNmAgEcA}E_CwOuGSmRc@yBKyMp@{I|B^`HHrBdBpMP~CBtAn@zACnS[h[J|FtAnAtCVna@HtK');
INSERT INTO `tbl_zona` VALUES ('73', 'UV-068', '', '~hgkBbwr`KgMaFyRuHoMiET{@pAsEzDuNnCkJ`CaItDlAbQtEpV|G~An@O~AiGvSgCbJ{BzIu@bB');
INSERT INTO `tbl_zona` VALUES ('74', 'UV-069', '', 'xzgkBdcq`K_SsFgSaG}FeBLw@hDyPzCuM|@cDbAZ`MtEhLdEvI|EbG`DF|@kCzJaDtLcAtC');
INSERT INTO `tbl_zona` VALUES ('75', 'UV-070', '', 'tghkBbbp`KcJ}E}F_DmG_CkOsFmCiAbAkDpD}LdEoO`CkHzKlK~IbJbH~Gt@fAfBpAbCfBDv@eCdJwCxKs@b@');
INSERT INTO `tbl_zona` VALUES ('76', 'UV-071', '', '`thkBpao`KqEeDoM}MiLkL_FgFBWhJyQtKeTdFgJjA}AhAr@pIbFjI~EfLzGn@j@kC`FcGzQmDnKiClJaFtR');
INSERT INTO `tbl_zona` VALUES ('77', 'UV-073', '', 'rhfkBxop`KePsEmPcFoGcBdA_D`H{TzD{LfBcJlLlEzPzG`GdCmAzDaExOkDhPgApF');
INSERT INTO `tbl_zona` VALUES ('78', 'UV-072', '', '|uekBl_r`KqReFqQwE{Ag@fDyNzFuVxA_GdSjFfQfFnChAgFtQeE|OeB~G');
INSERT INTO `tbl_zona` VALUES ('79', 'UV-074', '', 'lyfkBz}n`KmRqHyNuFeCiAlDoL~HsXt@a@xIzEfK~FlLjG_@rA{BrH_CnI{CtK');
INSERT INTO `tbl_zona` VALUES ('80', 'UV-075', '', 'zggkB|vm`KgPyI}KoG{D{B@{@hEqI`IoOzC{FfA}AWm@n@y@dB[|B}B~DoA~F~ChBCpC|AtOjIqA`DeGvL}HjOeK`S');
INSERT INTO `tbl_zona` VALUES ('81', 'UV-332', '', 'd}ckBxpt`K]kTUkAUyUmHC_ONgLNnCjId@vETvEKlFtGpApHfDjJdD');
INSERT INTO `tbl_zona` VALUES ('82', 'UV-333', '', 'd{ckBtas`K_RV{MNgDBwBkGE}Qv@wJZ{E|QrBtU~BtHzAaCjKmC`Ky@zAFlD');
INSERT INTO `tbl_zona` VALUES ('83', 'UV-076', '', 'xjdkBfpq`KaUsE_LsAqNmA~AeGdA}GtDoXhBkMlNfD`RxFhRxFcGlR}FbW');
INSERT INTO `tbl_zona` VALUES ('84', 'UV-077', '', 'p{dkBb~o`KmY{IwRkF_EoAh@aC~AoGzDaNbF}Q`PxFzVrJlJrDKl@uE~NiEhNeB|F');
INSERT INTO `tbl_zona` VALUES ('85', 'UV-078', '', 'xoekBpin`K{Y{K}MsF}GeCbDiOfFaObEaOnDnBtO~IbOlI`ItECr@wDzMaEvMsBnH');
INSERT INTO `tbl_zona` VALUES ('86', 'UV-079', '', 'bafkB|}l`KmUcM{PkJkG}DhKcSrLwT`DwFn@uDbX|OxZ`RyBpCiCrBkCdB_AxA}GvMyGzM}BnE');
INSERT INTO `tbl_zona` VALUES ('87', 'UV-079A', '', 'fjgkBjjk`K{TyMeU{MoKoG~AwFzEsL`DoHj^nR|KdGk@p@mEfINZTjHn@rFXzCZhB');
INSERT INTO `tbl_zona` VALUES ('88', 'UV-106', '', 'b{wkBfbr`KoEkOaHyNmHwJSi@tBaBvLuI|HsFdEuCbC|ChDpElFbJlFzHnKxN`CpDnCtGd@jBa@~@cPjCsKlAaMXoFCo@w@');
INSERT INTO `tbl_zona` VALUES ('89', 'UV-107', '', 'pyykBh|s`K}WqBmR{AkIs@eAiBGiKQeIu@iJ]{C^y@pNYtEO`QyB`IyA`Ab@hBzObCzT`BhOw@vA');
INSERT INTO `tbl_zona` VALUES ('90', 'UV-108', '', 'vvwkBvmx`KmNqKaMkJmDqC`@y@bFkJnGcL`DsGbIiTpBbA`UdLrOfIvHzDeA~CoDdIBfAoB`EaCpBkK`ImG|EwJlH');
INSERT INTO `tbl_zona` VALUES ('91', 'UV-109', '', '~tvkBtfy`KeJwK{IsKM{AVgAvFgGbGaHdCeD`K|H|N`LlFhEaKpH}JnHgFnDw@?');
INSERT INTO `tbl_zona` VALUES ('92', 'UV-110', '', 'bzukBxnz`KuGqHkI{JgKgL}BuCnL_HnKkHdG}ExBDfF|FlI~JrDhEG`AuC`CkEbIaKbTc@b@');
INSERT INTO `tbl_zona` VALUES ('93', 'UV-111', '', 'nltkBzx{`KiFaOaDqH{E~CuCfBiEoPsDyKeDoId@mAzP{DhPiFzJcE|@N|QnShKjMpCdDIf@wOnMyOjM}E`E');
INSERT INTO `tbl_zona` VALUES ('94', 'UV-112', '', 'byskBfw{`KkR`BwCHaJgD_DwBiLAd@kB{@mHEwFqAuOe@iG^kAx@_@nMSnKy@dCh@vAjBhExKxCxKhGrU');
INSERT INTO `tbl_zona` VALUES ('95', 'UV-113', '', 'zarkBzq{`K_UIOo@}GuAwEeAYwO_@cSAqBh@{@fLIlPHpAh@~@~AxAvOj@dHDhFr@bF]jB');
INSERT INTO `tbl_zona` VALUES ('96', 'UV-114', '', '~rykBb|q`KgDyHcEeHwPsU_DwFgDkF_KqMsDsE_Be@zBkAzFiEzKqHrEgDjEpDdK`JnQbQlKvI|EdElNfLrIrGvHxGcHpCoOzFuDpAcEp@iP|BwEj@');
INSERT INTO `tbl_zona` VALUES ('97', 'UV-115', '', 'h_zkB|xs`KoEoa@cBqOc@aExB_@|IuAjK{AhFoAvLqEzKgErIfHdKvIjErDkBlKcB~Ig@zCuJ{BsHuA_C`@wJlG{RbNcHrE}Bx@');
INSERT INTO `tbl_zona` VALUES ('98', 'UV-116', '', 'tr{kBn|u`KwVa@}CQwQgDuFaAG]dCqRh@kM@aOl@_BlBcBbHsE|O_LpLaItCq@nLnB`FvAaCxUiDn[oBlOcEpU');
INSERT INTO `tbl_zona` VALUES ('99', 'UV-117', '', '~{zkB~lw`KeHwBqD}@wFfA}HpAg@a@cEcFkEkFkBcCbDeGhDyIvCgKj@cBlCZdNvBjFz@hD^|LJdFR`@`AmCzH{EhMyDfKyBvF');
INSERT INTO `tbl_zona` VALUES ('100', 'UV-118A', '', 'tczkBr_y`KmMqKcTmPwIwH`B}AfDkDxGcO~@qBxAP|GtIxEtFtEtFtEhClItCEf@yDzJiDtJ_BfE');
INSERT INTO `tbl_zona` VALUES ('101', 'UV-118', '', '`czkBz`y`KsHzF{DxB}IlFgCtA{FUwCgDsB{DsJmM}H_GuDaD|@iAhJqHxIsGjBoAlI|GfNfLhHtFrIfH');
INSERT INTO `tbl_zona` VALUES ('102', 'UV-119', '', 'j|wkBdsz`KaL_NgKmLuHmJCw@`AcAjJ_HrImGbE{Cv@^bKxHhCtBjGbItAvBG^dDdFMv@qBfCq@~BmAvBeJlH}E|Ds@X');
INSERT INTO `tbl_zona` VALUES ('103', 'UV-120', '', 't|vkB~t{`K}NmP_NwOIg@b@gAlFcLdHiOrBeB|AgAbAJjJxKxJpLpJrLE`AmIlJmIlJiFlF');
INSERT INTO `tbl_zona` VALUES ('104', 'UV-121', '', '~{vkBxw{`KeMnNaLnL_L|L{HiNaHgMgIu@wKuA}BaAdNeKvKyIpB]~KeJvGkFf@VjDvDnIlJbMnN');
INSERT INTO `tbl_zona` VALUES ('105', 'UV-122', '', '|o|kBxev`KwKeC}GeBcEcAlB}IzDaVjE}_@tCsV|K`CxOnD~IfCvDrAoHlSoJjYeIpT}DhK');
INSERT INTO `tbl_zona` VALUES ('106', 'UV-123', '', 'bx{kBdxw`KuKaDiIwBwAk@pFyOdHsRpDyIrA`@nKfCfItBN^sEzL_HnR_DxI');
INSERT INTO `tbl_zona` VALUES ('107', 'UV-124', '', 'tu{kBx|y`KkPwGaNyHyJuHmBeB`EaKnDoHzBqFvHvAxQxCtOzCpKhCuExJiFrKyIvQ');
INSERT INTO `tbl_zona` VALUES ('108', 'UV-125', '', 'lszkBp`{`KoKaJgMwKuJoImFeFdJyFlFoClGmEzCoBt@oBlDdDjEnDlGbFfJvFlMnFiAjDoB`DmCdC}GnF_KdI');
INSERT INTO `tbl_zona` VALUES ('109', 'UV-126', '', 'vgykBpd|`KiN}P_MaNuIkKJm@zK}JlGwFH{Bx@aBxDkCbEF~E~D`NzLxJxIxH`H~AzAwI~GcLvIqMvJ');
INSERT INTO `tbl_zona` VALUES ('110', 'UV-127', '', 'nfxkBfd}`KaMaOiM{NwIwKlFgGvIoJtJqKjIjJfLdNxI~JtFnGoJzHiLjKmFjF');
INSERT INTO `tbl_zona` VALUES ('111', 'UV-129', '', '~s}kBlmv`KoNiBkKqAkDg@lIuVxIkVrJmX`CaHdAc@fKxCnMfElJfDsJpRmLbU_NpX');
INSERT INTO `tbl_zona` VALUES ('112', 'UV-130', '', 'fv|kBpdx`KqQ{FsHsBtBeGToCjE{L|E_NtBoFbAHbJdAnNdBnGz@E|@yLlViHfOkDvG');
INSERT INTO `tbl_zona` VALUES ('113', 'UV-131', '', 'z__lB~yv`KeM{B_IuAaFcAyHgBCk@rLsUvJ}RzJ{RbE{H|N~EhMzDdInCqFzKiSb_@mIhO');
INSERT INTO `tbl_zona` VALUES ('114', 'UV-132', '', 'pw}kBbtx`KaG{BoEo@cFaCgGsC_Be@lBqAdCeFtDiIpFyK~FkLtBcE`P|CfKnBpI~A}HjMgJfOqPvW');
INSERT INTO `tbl_zona` VALUES ('115', 'UV-133', '', 'fk`lBhdw`KuRyC{OqB}Cu@tHeObKkRrIwOtHkNnWdIxRvG}OfUeJhNmLpP');
INSERT INTO `tbl_zona` VALUES ('116', 'UV-134', '', 'by~kBjcy`KcRyGcL_ElK{O`MmS`LwQpTpClRvC_MlQoM~Q}NrR');
INSERT INTO `tbl_zona` VALUES ('117', 'UV-135', '', 'ti}kBn{z`KwF{DeG_BqLaFkLmF}GsD|GqOpIkPlEgJxZlK`YfKvG~BeIrHwIfMaJlN');
INSERT INTO `tbl_zona` VALUES ('118', 'UV-136', '', '|c|kBxo|`K{IeIkBuDkIkHgHoGeHiGiD{C|HcGxQwNtDqH~JnEhNvGlJjDvEbBhEhCeHlLqJhOqLdQ');
INSERT INTO `tbl_zona` VALUES ('119', 'UV-137', '', 'b}zkBz`~`K{PoSaT{VeMyNlTsP`TiPrPdO|E|Ej@[jDzCvCnEbExDbF|E{LrP{QnX');
INSERT INTO `tbl_zona` VALUES ('120', 'UV-138', '', 'tuykBty~`K{JuLaN}O_SmUnKaKvIaIfHyFlWjZxJpLzInKvDfEoG|EeKvEoLjF');
INSERT INTO `tbl_zona` VALUES ('121', 'UV-186', '', 'xnalBzow`KeM_EmKiDoCcAvJwNtKkPpIaMtEeHbIpC`Cz@aDtLmGnTqHtV');
INSERT INTO `tbl_zona` VALUES ('122', 'UV-187', '', 'tn`lB`cy`KwMuA{Fo@aIgDiFiBjIcLpHqKbH{JnJwLvMbE|NzEmHrPcK~R');
INSERT INTO `tbl_zona` VALUES ('123', 'UV-188', '', 'ry_lB|bz`K}GwCaHoCmK{DcK}DbIwHrIgItAeB~GtBnGrCnJx@hLpAkGrNsEzKaDfD');
INSERT INTO `tbl_zona` VALUES ('124', 'UV-188A', '', 'jo~kB~b{`KeF}CuIoFkIeFjF_IlDoFxDcF`HiGrMzEhIzCdNzFnDbBIv@kEfKyMdEyQlF');
INSERT INTO `tbl_zona` VALUES ('125', 'UV-166', '', 'vhzkB|~o`KiSyPgTaRiEuDdXaSfPeL`IcG|InTpGhNnBzD`BpAbCtBnAxCwJnHgIlG{JnH');
INSERT INTO `tbl_zona` VALUES ('126', 'UV-170', '', '~z{kBrkq`KcNqLaP_NmIcHgFwEvKgIvJqHnKqIfBnFGbFbDlIvGtOhHfRwCzC{DbE');
INSERT INTO `tbl_zona` VALUES ('127', 'UV-167', '', 'h~zkB`jn`KcFyLcEgJ}AiErRiN|NiK|K{H~A`MdBlNtArLf@lEeHjDkH`D{EtBqJpB');
INSERT INTO `tbl_zona` VALUES ('128', 'UV-171', '', 'th|kB`}p`K}FmNmMc]RwEyDoK}DgDaCmCsCcGvOaDfGiChPuHxHfRh@QxH|Qz@|Ef@rD~@pDzC~HhDnJcGrC_LxFwFrCeDhD');
INSERT INTO `tbl_zona` VALUES ('129', 'UV-175', '', 'hk|kBbzq`KqIeH{CgChD_EbNwN~OaIrLcGfBrEjHdQzBpI}ExCeSpLcMdH');
INSERT INTO `tbl_zona` VALUES ('130', 'UV-176', '', '~v}kBv`s`KmF_EaFkEyIcIsEuDdBwAvHoEpMmHlL_H~FnOtJrLoIlI}KvI');
INSERT INTO `tbl_zona` VALUES ('131', 'UV-180', '', '|t~kB~ys`KuDyAkFkEcH}GaEmDlJmIfJoIjDcD`HlInArAjBiAlFxEhJxIoK|GmQ|K');
INSERT INTO `tbl_zona` VALUES ('132', 'UV-184', '', 'lm`lBlrt`KmZsJwPuF}G_CxOuJlF}ClFmDjFfElOxG`IpC{HlU');
INSERT INTO `tbl_zona` VALUES ('133', 'UV-184A', '', 'l|alBdgu`KoSuG}NkEkIsClDaLzDoKdBiEhIjC~H~B~FSpJs@kAlNq@~Gw@lJSpC');
INSERT INTO `tbl_zona` VALUES ('134', 'UV-168', '', 'zu|kBbln`K{EiIyDwIcCqTcByNoAcL|K}HnKyHhRgNfCnT~A`QbCrTzAbMxAdNkK`DcUfH');
INSERT INTO `tbl_zona` VALUES ('135', 'UV-172', '', '~n}kBdcp`KiHsPsDqPoH{QpN}EbNwDfOuFvJ_C|E`PlFpP`DdKiCp@_Cz@oGlDiIvCoEfB}QhM');
INSERT INTO `tbl_zona` VALUES ('136', 'UV-173', '', 'le_lBjco`KgGaRaGeRqBmGpEyAfNkBdDI~Bj@bDm@tC`BrBtCfHzO~ErKz@xBgCxAmBz@uL|AiOjB');
INSERT INTO `tbl_zona` VALUES ('137', 'UV-174', '', 'rp`lB~vn`KeIuQcG}MoCmDcCgAtLkF`KqEzDeCrI|LtJlNhE`IsLjG}QhJ');
INSERT INTO `tbl_zona` VALUES ('138', 'UV-092', '', 'hqukBlxn`K{LyRcIwIQw@|CaGzK{MbHeItBuEvA}OT_DrEzFtGdJ~ExGbCfEjHfHLjAlC`CYt@sL|KmSzQyKvJ');
INSERT INTO `tbl_zona` VALUES ('139', 'UV-091A', '', '|utkBfvm`KkKeHeG{CrFkNjHkQ~FsNfE{K`DZdFxAtF`D`CtCKrDo@`KShD}AtDmC|DqDnDaK~LaEfG');
INSERT INTO `tbl_zona` VALUES ('140', 'UV-080', '', 'rdskB`|l`KaQ_DyOkB}N]_@g@KiCd@_GfAeDpBwAjDu@`Ho@jQqB`OoBpHq@OhBeFz[oAxGq@\\');
INSERT INTO `tbl_zona` VALUES ('141', 'UV-091', '', 'n_tkBhgm`KeIwCmHcCy@qBtAiJfC}OpB}LbASnLqAtIeAlEg@xFST^a@tAmJlVuF|M_G|N');
INSERT INTO `tbl_zona` VALUES ('142', 'UV-104', '', 'xawkB`jm`KoAsAEoB_MaTcIeL{GkJ}AaCTcNLgJLyIP[lER|C`@tCvAdCzBlFhGzC`D|ChD|HfIdF~EfGhEx@Zl@~@hBxCyHpKgLlOcBvAsBR{@pA');
INSERT INTO `tbl_zona` VALUES ('143', 'UV-150', '', 'xbxkBhel`K}C_EBg@cJcHcOgOyCuDzKyLfJuJ~L_NvG_Hn@ERUrB|FnGxOUl@nD`JlCrGlA`DeLfMoNlO');
INSERT INTO `tbl_zona` VALUES ('144', 'UV-149', '', '|hykBh_k`KoEmLwFmMoHkRkAmCbLcF~J}EnJeEbDjHnFtMzD`JfCpF{JvH{NdN');
INSERT INTO `tbl_zona` VALUES ('145', 'UV-148', '', '|nxkBjki`Ki@Q[FQGmFsMyFsOqCiH~MiGdJqEnJkEnEdKzDdKbH~OcJ~DoIfEwJzE');
INSERT INTO `tbl_zona` VALUES ('146', 'UV-147', '', '`uwkBtej`K{MiPgL_NaIoJ|FiCbIqDtL{FxG_D|DfJjGvOrCvGnA`DkI`IaIzI_AjA');
INSERT INTO `tbl_zona` VALUES ('147', 'UV-145', '', 'pnvkBnxh`KjFaNrIsRlDeJtAkCbCzF|DhJhCvGiNpGiQdI');
INSERT INTO `tbl_zona` VALUES ('148', 'UV-146', '', 'ptwkBlej`KiNcPcN{OuFkHkHMiCES`@m@fXe@xOGbDXJnCHnD^rCjAlCfBfKlLzDgEtJqK');
INSERT INTO `tbl_zona` VALUES ('149', 'UV-101', '', 'fmvkBbyh`KkFQyCKK[^qNTkI~@u`@~KKhP]vD~IwLxXoDdJeEfKa@|@');
INSERT INTO `tbl_zona` VALUES ('150', 'UV-097', '', '`yukB`pk`KiGyDgDqAoGkA_DWPuIZ}JLyDfIb@jH^jF`@OtHa@tS');
INSERT INTO `tbl_zona` VALUES ('151', 'UV-098', '', 'vzukB|ij`KkJe@{Im@qCQNmNZuMZoMHmAf@MbL^jFTxDTI`Ee@rRe@rO');
INSERT INTO `tbl_zona` VALUES ('152', 'UV-099', '', 'p~ukB`xh`KcL_@qG[kEUXmKb@sPb@oQLqEpGI|JMpFCSfJa@nPo@`W');
INSERT INTO `tbl_zona` VALUES ('153', 'UV-100', '', 'jbvkBb{f`K}MVgLNwI_Bl@}Ub@wTT_JvILbM^dMZQbHa@zOg@hQ');
INSERT INTO `tbl_zona` VALUES ('154', 'UV-102', '', 'lbwkB~yf`KqNRiML^wSb@iQf@wLhE|JbHzPfGfO');
INSERT INTO `tbl_zona` VALUES ('155', 'UV-093', '', 'd}tkBtdk`KmHRsLvA}N`CKOp@wEb@gH`@qJb@wJP[pLn@xIj@xJt@UhH_@dM');
INSERT INTO `tbl_zona` VALUES ('156', 'UV-094', '', 'h_ukBvfj`KqOcA}Kq@_F]LiEpAwVn@qNd@WvLr@zOf@bAj@KxPm@`Y');
INSERT INTO `tbl_zona` VALUES ('157', 'UV-095', '', 'z`ukBnuh`KiQo@yLy@g@w@?aSDgNpJqA@w@VmFb@}FxGd@lN~BYnNa@hQk@|R');
INSERT INTO `tbl_zona` VALUES ('158', 'UV-081', '', 'jsskBzkk`KqNbBgKlAgSbCeJz@qBQq@[@aAr@mEpAqBrAkBPmBT_FpJXzJd@hGZ|If@nFRlDPp@bAVdA');
INSERT INTO `tbl_zona` VALUES ('159', 'UV-082', '', 'j|qkBh}i`KyFYkHa@oF]wDOvAdFpCfKjDtMhCpJzBzITgAOo@v@aCvBsCVwCZcJj@eMT}F');
INSERT INTO `tbl_zona` VALUES ('160', 'UV-083', '', 'j|qkBr|i`KoFSgFWaFWqG]OUwAiFmBmHqBmH{AqFe@qBLGvGa@dFUfJa@xFUrGWfBEZP?hASvI[fKa@fJWbF');
INSERT INTO `tbl_zona` VALUES ('161', 'UV-086', '', 'ntskBz`k`KyHWcKe@mI_@eHa@gIYwCQUc@n@sNn@uNr@gQv@wXz@o@hACfKh@dKh@tJb@dLl@dDTd@XBh@WtG]zG]nI]xIc@jJk@hMY~FM|@');
INSERT INTO `tbl_zona` VALUES ('162', 'UV-084B', '', 'tvpkBhqh`KyCJmADUUu@yCy@wC{@aDq@mCqBqGkBwFeAsC[mAe@qBe@sCGi@R_Az@WxE]xD]hE]hGg@`@COjOc@bQs@~P');
INSERT INTO `tbl_zona` VALUES ('163', 'UV-084', '', 'p~qkBlmh`KsFXgGXsGXiFR{ETY]ZsGb@iL`@yLZcPLaDrDBdFNbFJ|EFxJPK|EIxJOnMSbQU`@');
INSERT INTO `tbl_zona` VALUES ('164', 'UV-087', '', 't|rkBfnh`KqEQeEQ_FUaEUe@q@HqGHaJNyNHcIFyBd@QpFRjPf@~@FItFU`L_@nPU|J');
INSERT INTO `tbl_zona` VALUES ('165', 'UV-089', '', 't|skB~qh`K_H[eIe@}Ig@JuK`@kKNeIT}JLuCTI`GNxGLdELhCHRPAnGAdKAjJEzK@xE');
INSERT INTO `tbl_zona` VALUES ('166', 'UV-085B', '', 'pvokB~of`KmCyJcCyIeCkJqBaHqBkHI{APy@zFRrJZlKHjKLvKJT^KzFSjLSzMWzJaDVgGh@_Kz@');
INSERT INTO `tbl_zona` VALUES ('167', 'UV-085', '', 'rbrkBflf`KyGKcIMuGMwEGgCCRiMP}KNiMJ_FfILxIVfIPxDNT^ItIIzLMtP');
INSERT INTO `tbl_zona` VALUES ('168', 'UV-088', '', '``skBznf`KqGGyFKyD[_EUJyJF_MJ_JNoJtGLhIPdIVDlAYlLWxL]jO');
INSERT INTO `tbl_zona` VALUES ('169', 'UV-090', '', 'j_tkB~ef`KyEGuFOiGQuDIReIPuKPcIDaD~FHlIR`ITSdNWpN');
INSERT INTO `tbl_zona` VALUES ('170', 'UV-096', '', '~ztkBhnf`K_F_@oDe@cFiBgEqAPoKTkKTiLrEFzGPjFLpBJQhJYvL]dP');
INSERT INTO `tbl_zona` VALUES ('171', 'UV-142', '', 'l~xkBflg`KcFzByHnDoHjDgJdEiFmM{CqHaDgIaAaClF_CrGwCnGwC|KgFjDlIhDfI|EvL');
INSERT INTO `tbl_zona` VALUES ('172', 'UV-143', '', 'njxkB`ef`KeI|DkFbCeF~BeGnCaEdBmEyK_C_G_EqL_EqJ}BmGxEGxID~HC|HC|DLjAXrAt@lA`AzAvDhEdK~BlF');
INSERT INTO `tbl_zona` VALUES ('173', 'UV-226', '', 'fypkBfwd`KwLEwMOyJOkK]qAWkEDmFDsAA{A_H{B}HkB_H{BkIiBkGsAiEb@MfFIjLElIIvDErG?pJEtKCzLMjFECtIC`NApOG~At@~F`@`B');
INSERT INTO `tbl_zona` VALUES ('174', 'UV-227', '', 'tcrkBvyd`KkIQeJSuGSuFSyAqJGgHG{MFoNAyE~JMjLSzBCzEz@bF~@IjGIxIYpUIrK');
INSERT INTO `tbl_zona` VALUES ('175', 'UV-228', '', 'zbskBr{d`KoFKoGO}FOkDKDyKB{IReKJ_MJ_GrDNdITnJZrAFS`J]fKMfQEvL');
INSERT INTO `tbl_zona` VALUES ('176', 'UV-229', '', 'dqskBj|d`KsFGmDOHqMBoLPaJXuJRsEtER|FTfFHGzCUvHI`D[xCa@hAuAtBsAfDkA~P');
INSERT INTO `tbl_zona` VALUES ('177', 'UV-229A', '', 'r}tkBb_e`KiGOcHM_HOqK]}COn@yHp@sJtC_GhA}Al@kNf@{OrISpHUrHMfCG^|@w@jYQlNSnKWjI');
INSERT INTO `tbl_zona` VALUES ('178', 'UV-100A', '', 'jevkBpae`KiFGeHOmFQqHM_DQTqITuLPcIRqIPmKPqG|CxHpCnG|FfNjGhOpE~KdAlC');
INSERT INTO `tbl_zona` VALUES ('179', 'UV-234', '', '~_ukBvxb`KcFN_FPyENuFJwCH@{IE{DXwD|@iGjBiM`AKjAGzCbHbDfIv@dArEvKfCdG');
INSERT INTO `tbl_zona` VALUES ('180', 'UV-233', '', 'p|skBrbc`K}EOiFO_GUf@gNTuKXmIXwJN{HfCi@jEZlIh@bEZo@~BaAdE{@`Gw@nGMnAW|G@rIQ~H');
INSERT INTO `tbl_zona` VALUES ('181', 'UV-232', '', 'feskB~`c`KqFQwEOsEMaEKaAGZ}BHqKDiLDqFjEaC|GiEjImEjDaBFrA]lMc@vOa@vNKjD');
INSERT INTO `tbl_zona` VALUES ('182', 'UV-231', '', 'zfrkBv~b`KiEm@oEw@y@SmGHoFFyABHmAtCw@vDcF~DgGzAiC~EyBlGqDAvGGdJEdKS|A');
INSERT INTO `tbl_zona` VALUES ('183', 'UV-263', '', 'xqpkB|zb`KuDd@eG@_GDgHDgFBoEByB@kAeEoCeGaCwDr@o@zDm@jEo@dEs@dEq@vDi@lEy@xEs@zFeAxApJ`AbG~AhI');
INSERT INTO `tbl_zona` VALUES ('184', 'UV-230', '', 'xiqkBpub`K{B`BqDDiJ^c@mEyBmKgBuKjDu@tF}@hGaAxC_B|AgCn@r@dC`FjDbHrAvCkBlCuBhD');
INSERT INTO `tbl_zona` VALUES ('185', 'UV-276', '', 'petkBbfa`KqHg@}F]wBQC{@sFr@eAhAsH|DmE~B}EjCiEbCgFpCwEnC}@j@]OkC}E}DuHkAwBj@uBX_Be@_Do@eE_C}DdHaE`HgEh@~@nDmBlEoCvBgAxBaBbCsAfCoAzFeC`NiHvCnGdFfLrGrShC|J');
INSERT INTO `tbl_zona` VALUES ('186', 'UV-156', '', 'vbwkBxae`K_EH}FCkEBeEAiC_H_FmLuCiHoEsKsF{McDkHiAgChK[rJk@hPm@rIi@hISrOs@xHc@bGSkD|LiD|K{CbLeCjIuEhOgDvK');
DROP TRIGGER IF EXISTS `creardetalle`;
DELIMITER ;;
CREATE TRIGGER `creardetalle` AFTER INSERT ON `tbl_detallepedido` FOR EACH ROW BEGIN
UPDATE tbl_producto SET stock=stock - NEW.cantidad WHERE id=NEW.id_producto;
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `cancelarpedido`;
DELIMITER ;;
CREATE TRIGGER `cancelarpedido` AFTER UPDATE ON `tbl_detallepedido` FOR EACH ROW begin
update tbl_producto set stock=stock+old.cantidad where id=old.id_producto;
end
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `eliminardetalle`;
DELIMITER ;;
CREATE TRIGGER `eliminardetalle` AFTER DELETE ON `tbl_detallepedido` FOR EACH ROW begin
update tbl_producto set stock=stock+OLD.cantidad where id=OLD.id_producto;
end
;;
DELIMITER ;
