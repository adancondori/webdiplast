<?php

class ReporteController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'ajax', 'reporteproductosmasvendidos', 'reportenegociomaspedidos', 'reportenegociomasalto'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Oferta;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Oferta'])) {
            $model->attributes = $_POST['Oferta'];

            if ($model->save()) {

                //  ENVIO DE NOTIFICACIONES POR GOOGLE CLOUD MESSENGER77
                $LM = Movil::model()->findAll();
                $ids = array();
                for ($i = 0; $i < sizeof($LM); $i++) {
                    $ids[] = $LM[$i]->getAttribute('gcmid');
                }
                $this->send_notification($ids, array("msg" => $model->getAttribute('nombre'), "oferta" => json_encode($model->getAttributes())));
                //----------------------------------------------------

                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Oferta'])) {
            $model->attributes = $_POST['Oferta'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Oferta');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Oferta('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Oferta']))
            $model->attributes = $_GET['Oferta'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Oferta the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Oferta::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Oferta $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'oferta-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAjax() {

        if (isset($_GET['modo'])) {
            $modo = $_GET['modo'];
        } else {
            $modo = $_POST['modo'];
        }
        switch ($modo) {
            case 'reporte':
                $this->reporte();
                break;

            case 'reporte2':
                $this->reporte2();
                break;

            case 'nuevo':

                $this->nuevo();
                break;

            case 'guardar':

                $this->guardar();
                break;

            case 'cargar':

                $this->cargar();
                break;

            case 'modificar':

                $this->modificar();
                break;

            default:
                break;
        }
    }

    private function reporte() {
        $LC = "";

        $this->render('reportechart', array(
            'ofertas' => $LC)
        );
    }

    private function reporte2() {
        $LC = "";

        $this->render('reportenegocio', array(
            'negocios' => $LC)
        );
    }

    private function nuevo() {

        $O = new Oferta();
        $this->render('formulario', array(
            'oferta' => $O)
        );
    }

    private function guardar() {

        $model = new Oferta();
        if (isset($_POST['Oferta'])) {
            $model->attributes = $_POST['Oferta'];
//            echo "<pre>";
//            var_dump($model);
//            echo "</pre>";
//            exit();
            $model->save();
            //  ENVIO DE NOTIFICACIONES POR GOOGLE CLOUD MESSENGER77
            $LM = Movil::model()->findAll();
            $ids = array();
            for ($i = 0; $i < sizeof($LM); $i++) {
                $ids[] = $LM[$i]->getAttribute('gcmid');
            }
        }
        $this->listar();
    }

    private function cargar() {

        $id = $_POST['id'];
        $O = $this->loadModel($id);
        $this->render('formulario', array(
            'oferta' => $O)
        );
    }

    private function modificar() {

        $id = $_POST['id'];
        $model = $this->loadModel($id);
        if (isset($_POST['Oferta'])) {
            $model->attributes = $_POST['Oferta'];
            $model->save();
        }
    }

    public function actionReporteproductosmasvendidos() {
        $sql = "SELECT dp.id_producto , SUM(dp.cantidad) AS cantidad
                FROM tbl_detallepedido AS dp
                GROUP BY dp.id_producto
                ORDER BY SUM(dp.cantidad) DESC
                LIMIT 0 , 10";
        $detalles = Detallepedido::model()->findAllBySql($sql);
        //$data = CHtml::listData($detalles, 'id_producto', 'cantidad');
        $return_array = array();
        foreach ($detalles as $detalle) {
            $return_array[] = array(
                'nombre' => $detalle->idProducto->nombre,
                'cantidad' => $detalle->cantidad,
            );
        }

        echo CJSON::encode($return_array);
    }

    public function actionReportenegociomaspedidos() {
        $sql = "SELECT p.id_negocio, count(1) as id_entregador
                from tbl_pedido p
                GROUP BY p.id_negocio
                ORDER BY COUNT(p.id_negocio) DESC
                LIMIT 0 , 10";
        $detalles = Pedido::model()->findAllBySql($sql);
        //$data = CHtml::listData($detalles, 'id_producto', 'cantidad');
        $return_array = array();
        foreach ($detalles as $detalle) {
            $return_array[] = array(
                'nombre' => $detalle->idNegocio->nombre,
                'cantidad' => $detalle->id_entregador,
            );
        }
        echo CJSON::encode($return_array);
    }

    public function actionReportenegociomasalto() {
        $sql = "SELECT p.id_negocio, SUM(p.montototal) as montototal
                from tbl_pedido p
                GROUP BY p.id_negocio
                ORDER BY SUM(p.montototal) DESC
                LIMIT 0 , 10";
        $detalles = Pedido::model()->findAllBySql($sql);
        //$data = CHtml::listData($detalles, 'id_producto', 'cantidad');
        $return_array = array();
        foreach ($detalles as $detalle) {
            $return_array[] = array(
                'nombre' => $detalle->idNegocio->nombre,
                'monto' => $detalle->montototal,
            );
        }
        echo CJSON::encode($return_array);
    }

}
