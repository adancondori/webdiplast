<?php
include "SincController.php";
class CategoriaController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $PATH = "assets/upload/server/php/files/";

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'ajax','enviarCorreo'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Categoria;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Categoria'])) {
            $model->attributes = $_POST['Categoria'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Categoria'])) {
            $model->attributes = $_POST['Categoria'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Categoria');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Categoria('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Categoria']))
            $model->attributes = $_GET['Categoria'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Categoria the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Categoria::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Categoria $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'categoria-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAjax() {

        if (isset($_GET['modo'])) {
            $modo = $_GET['modo'];
        } else {
            $modo = $_POST['modo'];
        }
        switch ($modo) {
            case 'listar':

                $this->listar();
                break;

            case 'nuevo':

                $this->nuevo();
                break;

            case 'guardar':

                $this->guardar();
                break;

            case 'cargar':

                $this->cargar();
                break;

            case 'modificar':

                $this->modificar();
                break;

            case 'galeria':

                $this->galeria();
                break;

            default:
                break;
        }
    }

    public function listar() {

        $LC = Categoria::model()->findAll();
        $LI = array();
        foreach ($LC as $cat) {
            $img = Imagen::model()->findByAttributes(array('id_categoria' => $cat->id));
            if ($img) {
                $LI[] = $img->direccion;
            } else {
                $LI[] = '';
            }
        }
        $this->render('listar', array(
            'categorias' => $LC, 'imagenes' => $LI)
        );
    }

    private function nuevo() {

        $C = new Categoria();
        $this->render('formulario', array(
            'categoria' => $C)
        );
    }

    private function guardar() {
        if ($_POST['id']) {
            $this->modificar();
        } else {
            $model = new Categoria();
            if (isset($_POST['Categoria'])) {
                $model->attributes = $_POST['Categoria'];
                $fecha = date('Y-m-d');
                $model->fechacreacion = $fecha;
                $model->fechamodificacion = $fecha;
                $model->save();
            }
            $idC = $model->getPrimaryKey();
            //  Guardando imagen
            $extension = end(explode('.', $_FILES['imagen']['name']));
            $nombre = 'categoria' . $idC . '.' . $extension;
            if ($_FILES['imagen']["error"] <= 0) {
                move_uploaded_file($_FILES['imagen']['tmp_name'], $this->PATH . $nombre);
                $imagen = new Imagen();
                $imagen->setAttribute('direccion', $nombre);
                $imagen->setAttribute('id_categoria', $idC);
                $imagen->save();
            }
        }
        $this->listar();
    }

    private function cargar() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
        } else {
            $id = $_GET['id'];
        }
        $P = $this->loadModel($id);
        $this->render('formulario', array(
            'categoria' => $P)
        );
    }

    private function modificar() {

        $id = $_POST['id'];
        $model = $this->loadModel($id);
        if (isset($_POST['Categoria'])) {
            $model->attributes = $_POST['Categoria'];
                $fecha = date('Y-m-d');
                $model->fechamodificacion = $fecha;
            $model->save();
            if ($_FILES['imagen']["error"] <= 0) {
                $extension = end(explode('.', $_FILES['imagen']['name']));
                $nombre = 'categoria' . $id . '.' . $extension;
                move_uploaded_file($_FILES['imagen']['tmp_name'], $this->PATH . $nombre);
            }
        }
    }

    private function galeria() {

        $LC = Categoria::model()->findAll();
        $LP = Producto::model()->findAll();
        $LI = array();
        foreach ($LP as $prod) {
            $img = Imagen::model()->findByAttributes(array('id_producto' => $prod->id));
            if ($img) {
                $LI[] = $img->direccion;
            } else {
                $LI[] = '';
            }
        }
        $this->render('galeria', array(
            'categorias' => $LC, 'productos' => $LP, 'imagenes' => $LI)
        );
    }

                                          //'ADAN CONDORI','Hola como estas','adan.condoric@gmail.com','Este es el Contenido'
    public static function actionEnviarCorreo(){//$nombre_asunto,$Subject,$correos_comas,$body_contenido){
        /*$dFechaC=Yii::app()->locale->dateFormatter->format('yyyy-MM-dd HH:mm', time());
            
        //Plantilla 
        $mail = new YiiMailer();
        $mail->setFrom('zurieliasd@gmail.com',$nombre);//QUIEN ESTA ENVIANDO
            $arrayPara=split(",", $correos_comas);//PARA QUIEN
        $mail->setTo($arrayPara);//PARA QUIENES
        $mail->setSubject($Subject);
        $mail->setReplyTo('');//sDe para responder directamente
        
        $mail->setBody($body_contenido);
        $mail->setSmtp('smtp.gmail.com', 465, 'ssl', true, 'zurieliasd@gmail.com', '200871137'); 
        $send = $mail->send();
        var_dump($send); 
        if ($send)
        {   
            $mail->clear(); 
            return true;
        }else{
            return false;
        }*/
        $sinc = SincController::SendCorreoNotificacion();
    }

}
