<?php

class NegocioController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'ajax'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Negocio;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Negocio'])) {
            $model->attributes = $_POST['Negocio'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Negocio'])) {
            $model->attributes = $_POST['Negocio'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Negocio');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Negocio('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Negocio']))
            $model->attributes = $_GET['Negocio'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Negocio the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Negocio::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Negocio $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'negocio-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAjax() {

        if ($_GET['modo']) {
            $modo = $_GET['modo'];
        } else {
            $modo = $_POST['modo'];
        }

        switch ($modo) {
            case 'listar':

                $this->listar();
                break;

            case 'nuevo':

                $this->nuevo();
                break;

            case 'guardar':

                $this->guardar();
                break;

            case 'cargar':

                $this->cargar();
                break;

            case 'modificar':

                $this->modificar();
                break;

            case 'importar':

                $this->importar();
                break;

            case 'exportar':

                $this->exportar();
                break;

            default:
                break;
        }
    }

    private function listar() {

        $LN = Negocio::model()->findAll();

        $this->render('listar', array(
            'negocios' => $LN)
        );
    }

    private function nuevo() {

        $P = new Cliente();
        $LZ = Zona::model()->findAll();
        $this->render('formulario', array(
            'cliente' => $P, 'zonas' => $LZ)
        );
    }

    private function guardar() {

        $model = new Cliente();
        if (isset($_POST['Cliente']) && isset($_POST['Negocio'])) {
            $model->attributes = $_POST['Cliente'];
            $model->save();

            $negocio = new Negocio();
            $negocio->attributes = $_POST['Negocio'];
            $negocio->setAttribute('id_cliente', $model->getPrimaryKey());
            $fecha = date('Y-m-d');
            $negocio->fechacreacion = $fecha;
            $negocio->fechamodificacion = $fecha;
            $negocio->save();

            $imagen0 = new Imagen();
            if (isset($_POST['Imagen0'])) {
                $imagen0->attributes = $_POST['Imagen0'];
                $imagen0->setAttribute('id_negocio', $negocio->getPrimaryKey());
                $imagen0->save();
            }
            $imagen1 = new Imagen();
            if (isset($_POST['Imagen1'])) {
                $imagen1->attributes = $_POST['Imagen1'];
                $imagen1->setAttribute('id_negocio', $negocio->getPrimaryKey());
                $imagen1->save();
            }
            $imagen2 = new Imagen();
            if (isset($_POST['Imagen2'])) {
                $imagen2->attributes = $_POST['Imagen2'];
                $imagen2->setAttribute('id_negocio', $negocio->getPrimaryKey());
                $imagen2->save();
            }
        } else {
            echo "error";
        }
    }

    private function cargar() {
        $id = $_GET['id'];
        $N = $this->loadModel($id);
        $LZ = Zona::model()->findAll();

        $LI = Imagen::model()->findAllByAttributes(array('id_negocio' => $id));

        $this->render('formulario', array(
            'negocio' => $N, 'zonas' => $LZ, 'imagenes' => $LI)
        );
    }

    private function modificar() {

        $id = $_POST['id'];
        $model = $this->loadModel($id);
        if (isset($_POST['Negocio'])) {
            $model->attributes = $_POST['Negocio'];
            $fecha = date('Y-m-d');
            $model->fechamodificacion = $fecha;

            $localizacion = split(",", $_POST['localizacion']);
            $model->setAttribute('latitud', $localizacion[0]);
            $model->setAttribute('longitud', $localizacion[1]);

            $model->save();
        }
        $this->listar();
    }

    private function importar() {

        if ($_FILES['archivo']['size'] > 0) {
            $file = $_FILES['archivo']['tmp_name'];
            $handle = fopen($file, "r");
            $cont = 0;
            $lista = array();
            while (( $data = fgetcsv($handle, 1000, "|")) !== FALSE) {
                if ($cont == 0) {
                    $cont++;
                    continue;
                }
                $sw = Negocio::model()->exists('uuid = :uuid', array(":uuid" => $data[0]));
                if (!$sw) {
                    $P = new Negocio();
                    $P->cargar($data);
                    $P->save();
                    $lista[] = $P;
                }
            }
            fclose($handle);
        }
//        $this->redirect(array('producto/ajax?modo=planilla','negocios' => $lista));
        $this->render('//producto/planilla', array(
            'negocios' => $lista)
        );
    }

    private function exportar() {

        $f = fopen("negocios.csv", "w");
        $sep = "|";

        $LP = Negocio::model()->findAll();
        $linea = 'uuid' . $sep . 'nombre' . $sep . 'razon social' . $sep . 'nit' . $sep . 'direccion' . $sep . 'latitud' . $sep . 'longitud' . $sep . 'observacion' . $sep . 'cliente' . $sep . "zona\n";
        fwrite($f, $linea);
        foreach ($LP as $P) {
            $observacion = str_replace("\n", " ", $P->observacion);
            $observacion = utf8_decode($observacion);
            $linea = $P->uuid . $sep . utf8_decode($P->nombre) . $sep
                    . utf8_decode($P->razonsocial) . $sep . $P->nit . $sep . utf8_decode($P->direccion) . $sep . $P->latitud . $sep
                    . $P->longitud . $sep . $observacion . $sep . $P->idCliente->uuid . $sep . $P->id_zona 
                    . $sep . $P->fechacreacion
                    . $sep . $P->fechamodificacion
                    . $sep . $P->estado. "\n";
            fwrite($f, $linea);
        }
        fclose($f);
        $this->download_file('negocios.csv');
    }

    function download_file($archivo, $downloadfilename = null) {

        if (file_exists($archivo)) {
            $downloadfilename = $downloadfilename !== null ? $downloadfilename : basename($archivo);
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . $downloadfilename);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($archivo));

            ob_clean();
            flush();
            readfile($archivo);
            exit;
        }
    }

}
