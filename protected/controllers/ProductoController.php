<?php

class ProductoController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $PATH = "assets/upload/server/php/files/";

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'ajax', 'planilla'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Producto;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Producto'])) {
            $model->attributes = $_POST['Producto'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Producto'])) {
            $model->attributes = $_POST['Producto'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Producto');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Producto('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Producto']))
            $model->attributes = $_GET['Producto'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Producto the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Producto::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Producto $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'producto-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAjax() {

        if (isset($_GET['modo'])) {
            $modo = $_GET['modo'];
        } else {
            $modo = $_POST['modo'];
        }

        switch ($modo) {
            case 'listar':

                $this->listar();
                break;

            case 'nuevo':

                $this->nuevo();
                break;

            case 'guardar':

                $this->guardar();
                break;

            case 'cargar':

                $this->cargar();
                break;

            case 'eliminar':

                $this->eliminar();
                break;

            case 'modificar':

                $this->modificar();
                break;

            case 'planilla':

                $this->planilla();
                break;

            case 'importar':

                $this->importar();
                break;

            case 'exportar':

                $this->exportar();
                break;

            case 'stock':

                $this->stock();
                break;

            case 'costo':

                $this->costo();
                break;

            default:
                break;
        }
    }

    private function listar() {
        if (isset($_GET['id_categoria'])) {
            $id_categoria = $_GET['id_categoria'];
            $LC = Producto::model()->findAllByAttributes(array('id_categoria' => $id_categoria));
        } else {
            $LC = Producto::model()->findAll();
        }
        $this->render('listar', array(
            'productos' => $LC)
        );
    }

    private function nuevo() {

        $idcategoria = $_GET['idcategoria'];
        $C = Categoria::model()->findByPk($idcategoria);
        $P = new Producto();
        $this->render('formulario', array(
            'producto' => $P, 'categoria' => $C)
        );
    }

    private function guardar() {
        if ($_POST['id']) {
            $this->modificar();
        } else { //  GUARDAR NUEVO
            $model = new Producto();
            if (isset($_POST['Producto'])) {
                $model->attributes = $_POST['Producto'];
                $fecha = date('Y-m-d');
                $model->create = $fecha;
                $model->change = $fecha;
                $model->save();
            }
            $idC = $model->getPrimaryKey();
            // Guardando imagen
            $extension = end(explode('.', $_FILES['imagen']['name']));
            $nombre = 'producto' . $idC . '.' . $extension;
            if ($_FILES['imagen']["error"] <= 0) {
                move_uploaded_file($_FILES['imagen']['tmp_name'], $this->PATH . $nombre);
                $imagen = new Imagen();
                $imagen->setAttribute('direccion', $nombre);
                $imagen->setAttribute('id_producto', $idC);
                $imagen->save();
            }
        }
        $this->redirect(array('categoria/ajax?modo=listar'));
    }

    private function cargar() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
        } else {
            $id = $_GET['id'];
        }
        $P = $this->loadModel($id);
        $idcategoria = $P->id_categoria;
        $C = Categoria::model()->findByPk($idcategoria);
        $this->render('formulario', array(
            'producto' => $P, 'categoria' => $C)
        );
    }

    private function eliminar() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
        } else {
            $id = $_GET['id'];
        }
        $P = $this->loadModel($id);
        $count = Detallepedido::model()->countByAttributes(array("id_producto" => $id));
        if (!$count) {
            Producto::model()->deleteByPk($id);
        }
        $idcategoria = $P->id_categoria;
//        $C = Categoria::model()->findByPk($idcategoria);

        $LC = Producto::model()->findAllByAttributes(array('id_categoria' => $idcategoria));
        $this->render('listar', array(
            'productos' => $LC)
        );
    }

    private function modificar() {
        $id = $_POST['id'];
        $model = $this->loadModel($id);
        if (isset($_POST['Producto'])) {
            $model->attributes = $_POST['Producto'];
            $fecha = date('Y-m-d');
            $model->change = $fecha;
            $model->save();
            if ($_FILES['imagen']["error"] <= 0) {
                $extension = end(explode('.', $_FILES['imagen']['name']));
                $nombre = 'producto' . $id . '.' . $extension;
                move_uploaded_file($_FILES['imagen']['tmp_name'], $this->PATH . $nombre);
            }
        }
    }

    private function planilla() {

        if (isset($_GET["clientes"])) {
            $clientes = $_GET["clientes"];
            $this->render('planilla', array('clientes' => $clientes));
        } else if (isset($_GET["negocios"])) {
            $negocios = $_GET["negocios"];
            $this->render('planilla', array('negocios' => $negocios));
        } else {
            $this->render('planilla', array());
        }
    }

    private function importar() {

        if ($_FILES['archivo']['size'] > 0) {
            $file = $_FILES['archivo']['tmp_name'];
            $handle = fopen($file, "r");
            $cont = 0;
            $lista = array();
            while (( $data = fgetcsv($handle, 1000, "|")) !== FALSE) {
                if ($cont == 0) {
                    $cont++;
                    continue;
                }
                $sw = Producto::model()->exists('codigo = :codigo', array(":codigo" => $data[0]));
                if (!$sw) {
                    $P = new Producto();
                    $P->cargar($data);
                    $P->save();
                    $lista[] = $P;
                }
            }
            fclose($handle);
        }
        $this->render('planilla', array('productos' => $lista));
    }

    private function stock() {

        if ($_FILES['archivo']['size'] > 0) {
            $file = $_FILES['archivo']['tmp_name'];
            $handle = fopen($file, "r");
            $cont = 0;
            $lista = array();
            while (( $data = fgetcsv($handle, 1000, "|")) !== FALSE) {
                if ($cont == 0) {
                    $cont++;
                    continue;
                }
                $P = Producto::model()->findByAttributes(array("codigo" => $data[0]));
                if ($P) {
                    $stock = $P->stock + intval($data[1]);
                    $P->setAttribute("stock", $stock);
                    $P->update();
                    $lista[] = $P;
                }
            }
            fclose($handle);
        }
        $this->render('planilla', array('productosStock' => $lista));
    }

    private function costo() {

        if ($_FILES['archivo']['size'] > 0) {
            $file = $_FILES['archivo']['tmp_name'];
            $handle = fopen($file, "r");
            $cont = 0;
            $lista = array();
            while (( $data = fgetcsv($handle, 1000, "|")) !== FALSE) {
                if ($cont == 0) {
                    $cont++;
                    continue;
                }
                $P = Producto::model()->findByAttributes(array("codigo" => $data[0]));
                if ($P) {
                    $P->setAttribute("costo", doubleval($data[1]));
                    $P->setAttribute("porcentage", doubleval($data[2]));
                    $precio = doubleval($data[1]) + (doubleval($data[1]) * doubleval($data[2]));
                    $P->setAttribute("precio", $precio);
                    $P->update();
                    $lista[] = $P;
                }
            }
            fclose($handle);
        }
        $this->render('planilla', array('productosCosto' => $lista));
    }

    private function exportar() {

        $f = fopen("productos.csv", "w");
        $sep = "|";

        $LP = Producto::model()->findAll();
        $linea = 'codigo' . $sep . 'nombre' . $sep . 'descripcion' . $sep . 'medida' . $sep . 'precio' . $sep . 'stock' . $sep . "categoria\n";
        fwrite($f, $linea);
        foreach ($LP as $P) {
            $linea = $P->codigo . $sep . utf8_decode($P->nombre) . $sep . utf8_decode($P->descripcion) . $sep
                    . $P->medida . $sep . $P->precio . $sep . $P->stock . $sep . $P->id_categoria . "\n";
            fwrite($f, $linea);
        }
        fclose($f);
        $this->download_file('productos.csv');

        $this->render('planilla', array());
    }

    function download_file($archivo, $downloadfilename = null) {

        if (file_exists($archivo)) {
            $downloadfilename = $downloadfilename !== null ? $downloadfilename : basename($archivo);
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . $downloadfilename);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($archivo));

            ob_clean();
            flush();
            readfile($archivo);
            exit;
        }
    }

}
