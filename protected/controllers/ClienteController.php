<?php

class ClienteController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'ajax'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Cliente;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Cliente'])) {
            $model->attributes = $_POST['Cliente'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Cliente'])) {
            $model->attributes = $_POST['Cliente'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Cliente');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Cliente('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Cliente']))
            $model->attributes = $_GET['Cliente'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Cliente the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Cliente::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Cliente $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cliente-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionAjax() {

        if ($_GET['modo']) {
            $modo = $_GET['modo'];
        } else {
            $modo = $_POST['modo'];
        }

        switch ($modo) {
            case 'listar':

                $this->listar();
                break;

            case 'nuevo':

                $this->nuevo();
                break;

            case 'upload':

                $this->upload();
                break;

            case 'guardar':

                $this->guardar();
                break;

            case 'cargar':

                $this->cargar();
                break;

            case 'modificar':

                $this->modificar();
                break;

            case 'importar':

                $this->importar();
                break;

            case 'exportar':

                $this->exportar();
                break;

            default:
                break;
        }
    }

    private function listar() {

        $LC = Cliente::model()->findAll();

        $this->render('listar', array(
            'clientes' => $LC)
        );
    }

    private function nuevo() {

        $P = new Cliente();
        $LZ = Zona::model()->findAll();
        $this->render('formulario', array(
            'cliente' => $P, 'zonas' => $LZ)
        );
    }

    private function upload() {

        $this->render('upload', array());
    }

    private function guardar() {

        $model = new Cliente();
        if (isset($_POST['Cliente']) && isset($_POST['Negocio'])) {
            $model->attributes = $_POST['Cliente'];
            $fecha = date('Y-m-d');
            $model->fechacreacion = $fecha;
            $model->fechamodificacion = $fecha;
            $model->save();

            $localizacion = split(",", $_POST['localizacion']);

            $negocio = new Negocio();
            $negocio->attributes = $_POST['Negocio'];
            $negocio->setAttribute('latitud', $localizacion[0]);
            $negocio->setAttribute('longitud', $localizacion[1]);
            $negocio->setAttribute('id_cliente', $model->getPrimaryKey());
            $negocio->fechacreacion = $fecha;
            $negocio->fechamodificacion = $fecha;
            $negocio->save();

            $imagenes = split(",", $_POST['imagenes']);
            $imagen0 = new Imagen();
            if (isset($imagenes[0])) {
                $imagen0->setAttribute('direccion', $imagenes[0]);
                $imagen0->setAttribute('id_negocio', $negocio->getPrimaryKey());
                $imagen0->save();
            }
            $imagen1 = new Imagen();
            if (isset($imagenes[1])) {
                $imagen1->setAttribute('direccion', $imagenes[1]);
                $imagen1->setAttribute('id_negocio', $negocio->getPrimaryKey());
                $imagen1->save();
            }
            $imagen2 = new Imagen();
            if (isset($imagenes[2])) {
                $imagen2->setAttribute('direccion', $imagenes[2]);
                $imagen2->setAttribute('id_negocio', $negocio->getPrimaryKey());
                $imagen2->save();
            }
        } else {
            echo "error";
        }
        $this->listar();
    }

    private function cargar() {

        $id = $_GET['id'];
        $P = $this->loadModel($id);
        $this->render('formulariomod', array(
            'cliente' => $P)
        );
    }

    private function modificar() {

        $id = $_POST['id'];
        $model = $this->loadModel($id);
        if (isset($_POST['Cliente'])) {
            $model->attributes = $_POST['Cliente'];
            $fecha = date('Y-m-d');
            $model->fechamodificacion = $fecha;
            $model->save();
        }
        $this->listar();
    }

    private function importar() {

        if ($_FILES['archivo']['size'] > 0) {
            $file = $_FILES['archivo']['tmp_name'];
            $handle = fopen($file, "r");
            $cont = 0;
            $lista = array();
            while (( $data = fgetcsv($handle, 1000, "|")) !== FALSE) {
                if ($cont == 0) {
                    $cont++;
                    continue;
                }
                $sw = Cliente::model()->exists('uuid = :uuid', array(":uuid" => $data[1]));
                if (!$sw) {
                    $P = new Cliente();
                    $P->cargar($data);
                    $fecha = date('Y-m-d');
                    $P->fechacreacion = $fecha;
                    $P->fechamodificacion = $fecha;
                    $P->save();
                    $lista[] = $P;
                }
            }

            fclose($handle);
        }
//        $this->render(array('producto/ajax'),'clientes' => $lista));
        $this->render('//producto/planilla', array(
            'clientes' => $lista)
        );
    }

    private function exportar() {

        $f = fopen("clientes.csv", "w");
        $sep = "|";

        $LP = Cliente::model()->findAll();
        $linea = 'codigo' . $sep . 'uuid' . $sep . 'nombre' . $sep . 'apellidos' . $sep . 'sexo' . $sep . 'fecha de nacimiento' . $sep . 'ci' . $sep . 'direccion' . $sep . 'telefono' . $sep . 'celular' . $sep . "email\n";
        fwrite($f, $linea);
        foreach ($LP as $P) {
            $linea = $P->codigo . $sep . $P->uuid . $sep . utf8_decode($P->nombre) . $sep
                    . utf8_decode($P->apellidos) . $sep . $P->sexo . $sep . $P->fechanacimiento . $sep . $P->ci . $sep
                    . $P->direccion . $sep . $P->telefono . $sep . $P->celular . $sep . $P->email . "\n";
            fwrite($f, $linea);
        }
        fclose($f);
        $this->download_file('clientes.csv');
    }

    function download_file($archivo, $downloadfilename = null) {

        if (file_exists($archivo)) {
            $downloadfilename = $downloadfilename !== null ? $downloadfilename : basename($archivo);
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . $downloadfilename);
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($archivo));

            ob_clean();
            flush();
            readfile($archivo);
            exit;
        }
    }

}
