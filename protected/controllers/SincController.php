<?php

class SincController {

    /**
     * @return array action filters
     */
    public $PATH_IMAGE = 'assets/upload/server/php/files/';

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'ajax'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView() {
        echo 'Hola mundo acc';
    }

    public function monitoreo() {
        try {
            $LP = json_decode($_POST['posiciones']);
            for ($i = 0; $i < sizeof($LP); $i++) {
                $P = new Posicion();
                $P->cargarObject($LP[$i]);
                $P->save();
            }
            $resp['msg'] = 'ok';
            echo json_encode($resp);
        } catch (Exception $e) {
            $resp['msg'] = 'error1';
            echo json_encode($resp);
        }
    }

    public function login() {

        $usr = $_GET['usr'];
        $pwd = $_GET['pwd'];

        $rec = Usuario::model()->findByAttributes(array('usuario' => $usr, 'contrasena' => $pwd));

        if (!isset($rec['id'])) {
            $resp['msg'] = 'error';
        } else {
            $resp['msg'] = 'ok';
            $P = Personal::model()->findByPk($rec['id_personal']);
            $resp['json'] = $P->attributes;
            $LP = Producto::model()->findAll();
            $precios = array();
            foreach ($LP as $pro) {
                $precios[] = array("id" => $pro->id, "precio" => $pro->precio, "stock" => $pro->stock);
            }
            $resp['precios'] = $precios;
        }
        echo json_encode($resp);
    }

    public function sinccategoria() {

        $lrec = Categoria::model()->findAll();
        $resp['msg'] = 'ok';
        $lc = array();
        for ($i = 0; $i < sizeof($lrec); $i++) {
            $lc[$i] = $lrec[$i]->attributes;
        }
        $resp['json'] = $lc;
        echo json_encode($resp);
    }

    public function sincproducto() {

        $lrec = Producto::model()->findAll();
        $resp['msg'] = 'ok';
        $lc = array();
        for ($i = 0; $i < sizeof($lrec); $i++) {
            $lc[$i] = $lrec[$i]->attributes;
        }
        $resp['json'] = $lc;
        echo json_encode($resp);
    }

    public function sinczona() {

        $lrec = Zona::model()->findAll();
        $resp['msg'] = 'ok';
        $lc = array();
        for ($i = 0; $i < sizeof($lrec); $i++) {
            $lc[$i] = $lrec[$i]->attributes;
        }
        $resp['json'] = $lc;
        echo json_encode($resp);
    }

    public function sinccliente() {
        $idpersonal = $_GET['id_personal'];
        $sql = "SELECT * FROM `tbl_cliente` WHERE id IN (SELECT DISTINCT(id_cliente) FROM `tbl_negocio` WHERE id_zona in (SELECT DISTINCT(id_zona) FROM `tbl_asignacionzona` WHERE id_personal=$idpersonal))";
        $lrec = Cliente::model()->findAllBySql($sql);
        $resp['msg'] = 'ok';
        $lc = array();
        for ($i = 0; $i < sizeof($lrec); $i++) {
            $lc[$i] = $lrec[$i]->attributes;
        }
        $resp['json'] = $lc;
        echo json_encode($resp);
    }

    public function sincnegocio() {
        $idpersonal = $_GET['id_personal'];
        $sql = "SELECT * FROM `tbl_negocio` WHERE id_zona in (SELECT DISTINCT(id_zona) FROM `tbl_asignacionzona` WHERE id_personal=$idpersonal)";
        $lrec = Negocio::model()->findAllBySql($sql);
        $resp['msg'] = 'ok';
        $lc = array();
        for ($i = 0; $i < sizeof($lrec); $i++) {
            $lc[$i] = $lrec[$i]->attributes;
        }
        $resp['json'] = $lc;
        echo json_encode($resp);
    }

    public function sincimagen() {

        $lrec = Imagen::model()->findAll();
        $resp['msg'] = 'ok';
        $lc = array();
        for ($i = 0; $i < sizeof($lrec); $i++) {
            $lc[$i] = $lrec[$i]->attributes;
        }
        $resp['json'] = $lc;
        echo json_encode($resp);
    }

    public function sincimagenproducto() {

        $lrec = Imagen::model()->findAllBySql("select * from tbl_imagen where id_producto is not null");
        $resp['msg'] = 'ok';
        $lc = array();
        for ($i = 0; $i < sizeof($lrec); $i++) {
            $lc[$i] = $lrec[$i]->attributes;
        }
        $resp['json'] = $lc;
        echo json_encode($resp);
    }

    public function sincimagennegocio() {
        $idpersonal = $_GET['id_personal'];
        $lrec = Imagen::model()->findAllBySql("select * from tbl_imagen where id_negocio in (SELECT id FROM `tbl_negocio` WHERE id_zona in (SELECT DISTINCT(id_zona) FROM `tbl_asignacionzona` WHERE id_personal=$idpersonal))");
        $resp['msg'] = 'ok';
        $lc = array();
        for ($i = 0; $i < sizeof($lrec); $i++) {
            $lc[$i] = $lrec[$i]->attributes;
        }
        $resp['json'] = $lc;
        echo json_encode($resp);
    }

    public function enviarpedido() {
        $OP = json_decode($_POST['pedido']);
        $LD = $OP->detalles;
        //----------------------------------------------------------------------
        $detalle2 = '';
        $detalle3 = '';
        foreach ($LD as $detalle) {
            $P = Producto::model()->findByAttributes(array("id" => $detalle->id_producto));
            $newstock = intval($P->stock) - intval($detalle->cantidad);
            if ($newstock < 0) {
                $detalle2 .= "\n$P->nombre - Cantidad: $detalle->cantidad - Stock: $P->stock";
                $detalle3 .= "\n$P->nombre - Cantidad: $detalle->cantidad - Cantidad faltante: ".($newstock*-1);
            }
        }
        //----------------------------------------------------------------------
        if (!isset($_POST['stock'])) {//
            // Consultando disponibilidad de Stock
            if ($detalle2 != '') {
                $resp['msg'] = 'stock';
                $resp['detalle'] = $detalle2;
                echo json_encode($resp);
                return;
            }
            // ------
        } else {
            if ($detalle3 != '') {
                $this->SendCorreoNotificacion($detalle3);
                sleep(1);
            }
        }

        $sw = Pedido::model()->exists("numero = :numero", array(":numero" => $OP->numero));
        if ($sw) {
            $this->modificarpedidouuid($OP->id_promotor, $OP->numero, $OP->montototal, $LD);
            return;
        }

        $N = Negocio::model()->findByAttributes(array('uuid' => $OP->uuidnegocio));

        $P = new Pedido();
        $P->cargarObject($OP);
        $P->setAttribute("estado", Pedido::PENDIENTE);
        $P->setAttribute("id_negocio", $N->id);

        date_default_timezone_set('America/La_Paz');
        $hoy = date('Y-m-d H:i:s');

        $P->setAttribute('create', $hoy);
        $P->setAttribute('createby', $P->getAttribute('id_promotor'));
        $P->save();

        $idP = $P->getPrimaryKey();

        for ($i = 0; $i < sizeof($LD); $i++) {
            $DP = new Detallepedido();
            $DP->cargarObject($LD[$i]);
            $DP->setAttribute('id_pedido', $idP);
            $DP->save();
        }
        $resp['msg'] = 'ok';
        echo json_encode($resp);
    }

    public function modificarpedidouuid($idpersonal, $numero, $monto, $LD) {
//        $idpersonal = $_POST['id_personal'];
//        $numero = $_POST['numero'];
//        $monto = $_POST['monto'];
//        $LD = json_decode($_POST['detalles']);

        date_default_timezone_set('America/La_Paz');
        $hoy = date('Y-m-d H:i:s');

        $LP = Pedido::model()->findAllByAttributes(array('numero' => $numero));
        $P = $LP[0];
        $P->setAttribute('montototal', $monto);
        $P->setAttribute('changeby', $idpersonal);
        $P->setAttribute('change', $hoy);
        $P->update();


        Detallepedido::model()->deleteAllByAttributes(array('id_pedido' => $P->getAttribute('id')));

        for ($i = 0; $i < sizeof($LD); $i++) {
            $DP = new Detallepedido();
            $DP->cargarObject($LD[$i]);
            $DP->setAttribute('id_pedido', $P->getAttribute('id'));
            $DP->save();
        }
        $resp['msg'] = 'ok';
        echo json_encode($resp);
    }

    public function modificarpedido() {
        $idpersonal = $_POST['id_personal'];
        $numero = $_POST['numero'];
        $monto = $_POST['monto'];
        $LD = json_decode($_POST['detalles']);

        if (!isset($_POST['stock'])) {
            // Consultando disponibilidad de Stock
            $detalle2 = '';
            foreach ($LD as $detalle) {
                $P = Producto::model()->findByAttributes(array("id" => $detalle->id_producto));
                $newstock = intval($P->stock) - intval($detalle->cantidad);
                if ($newstock < 0) {
                    $detalle2 .= "\n$P->nombre - Cantidad: $detalle->cantidad - Stock: $P->stock";
                }
            }
            if ($detalle2 != '') {
                $resp['msg'] = 'stock';
                $resp['detalle'] = $detalle2;
                echo json_encode($resp);
                return;
            }
            // ------
        }


        date_default_timezone_set('America/La_Paz');
        $hoy = date('Y-m-d H:i:s');

        $LP = Pedido::model()->findAllByAttributes(array('numero' => $numero));
        $P = $LP[0];
        $P->setAttribute('montototal', $monto);
        $P->setAttribute('changeby', $idpersonal);
        $P->setAttribute('change', $hoy);
        $P->update();


        Detallepedido::model()->deleteAllByAttributes(array('id_pedido' => $P->getAttribute('id')));

        for ($i = 0; $i < sizeof($LD); $i++) {
            $DP = new Detallepedido();
            $DP->cargarObject($LD[$i]);
            $DP->setAttribute('id_pedido', $P->getAttribute('id'));
            $DP->save();
        }
        $resp['msg'] = 'ok';
        echo json_encode($resp);
    }

    public function cancelarpedido() {
        $idpersonal = $_POST['id_personal'];
        $numero = $_POST['numero'];

        $LP = Pedido::model()->findAllByAttributes(array('numero' => $numero));
        $P = $LP[0];

        date_default_timezone_set('America/La_Paz');
        $hoy = date('Y-m-d H:i:s');

        $P->setAttribute('changeby', $idpersonal);
        $P->setAttribute('change', $hoy);
        $P->setAttribute('estado', Pedido::CANCELADO);
        $P->update();

        $LDP = Detallepedido::model()->findAllByAttributes(array('id_pedido' => $P->getAttribute('id')));
        for ($i = 0; $i < sizeof($LDP); $i++) {
            $DP = $LDP[$i];
            $DP->update();
        }

        $resp['msg'] = 'ok';
        echo json_encode($resp);
    }

    public function registrarcliente() {
        try {
            $OC = json_decode($_POST['cliente']);
            $ON = json_decode($_POST['negocio']);
            $C = new Cliente();
            $C->cargarObject($OC);
            $C->setAttribute('', uniqid());
            $fecha = date('Y-m-d');
            $C->fechacreacion = $fecha;
            $C->fechamodificacion = $fecha;
            $C->estado = 1;
            $C->save();
            $N = new Negocio();
            $N->cargarObject($ON);
            $N->fechacreacion = $fecha;
            $N->fechamodificacion = $fecha;
            $N->estado = 1;
            $N->setAttribute('id_cliente', $C->getPrimaryKey());
            $N->save();

            $resp['msg'] = 'ok';
            echo json_encode($resp);
        } catch (Exception $e) {
            echo print_r($e);
            $resp['msg'] = 'error1';
            echo json_encode($resp);
        }
    }

    public function registrarnegocio() {
        $ON = json_decode($_POST['negocio']);
        $uuid = $_POST['uuid'];

        $LC = Cliente::model()->findAllByAttributes(array('codigo' => $uuid));
        $C = $LC[0];

        $N = new Negocio();
        $N->cargarObject($ON);
        $N->id_cliente = $C->id;
        $N->save();

        $resp['msg'] = 'ok';
        echo json_encode($resp);
    }

    public function sincpedido() {
        $id_personal = $_GET['id_personal'];
        $Per = Personal::model()->findByPk($id_personal);

        if ($Per->getAttribute('tipo') == 'P') { // Promotor
            $LP = Pedido::model()->findAllByAttributes(array('id_promotor' => $Per->getAttribute('id')));
        } else { // Entregador
            $sql = "SELECT * FROM `tbl_pedido` WHERE estado = 'pendiente' and id_negocio IN (SELECT id FROM `tbl_negocio` WHERE id_zona IN (SELECT DISTINCT(id_zona) FROM `tbl_asignacionzona` WHERE id_personal=$id_personal))";
            $LP = Pedido::model()->findAllBySql($sql);
        }
        $array1 = array();
        for ($i = 0; $i < sizeof($LP); $i++) {
            $Ped = $LP[$i]->attributes;
            $LDP = Detallepedido::model()->findAllByAttributes(array('id_pedido' => $Ped['id']));
            $array2 = array();
            for ($j = 0; $j < sizeof($LDP); $j++) {
                $array2[$j] = $LDP[$j]->attributes;
            }
            $Ped['detalles'] = $array2;
            $array1[$i] = $Ped;
        }
        $resp['msg'] = 'ok';
        $resp['pedidos'] = $array1;
        echo json_encode($resp);
    }

    public function registrarmovil() {
        $imei = $_GET['imei'];
        $gcmid = $_GET['gcmid'];
        $id_personal = $_GET['idpersonal'];

        $M = new Movil();
        $M->setAttribute('imei', $imei);
        $M->setAttribute('gcmid', $gcmid);
        $M->setAttribute('id_personal', $id_personal);

        $M->save();

        $resp['msg'] = 'ok';
        echo json_encode($resp);
    }

    public function registrarimagennegocio() {

        $resp['msg'] = 'ok';

        $LI = json_decode($_POST['imagenes']);
        $uuid = $_POST['uuid_negocio'];
        $N = Negocio::model()->findByAttributes(array('uuid' => $uuid));
        for ($i = 0; $i < count($LI); $i++) {
            $data = base64_decode($LI[$i]);
            $image = imagecreatefromstring($data);
            $nombre = 'negocio' . $N->id . '_' . $i . '.png';
            $sw = imagepng($image, $this->PATH_IMAGE . $nombre);
            if ($sw) {
                $imagen = new Imagen();
                $imagen->setAttribute('direccion', $nombre);
                $imagen->setAttribute('id_negocio', $N->id);
                $imagen->save();
            } else {
                $resp['msg'] = 'error';
                break;
            }
        }
        echo json_encode($resp);
    }

    public function entregarpedido() {
        $idpersonal = $_POST['id_personal'];
        $numero = $_POST['numero'];

        $LP = Pedido::model()->findAllByAttributes(array('numero' => $numero));
        $P = $LP[0];

        date_default_timezone_set('America/La_Paz');
        $hoy = date('Y-m-d H:i:s');

        $P->setAttribute('changeby', $idpersonal);
        $P->setAttribute('change', $hoy);
        $P->setAttribute('estado', Pedido::ENTREGADO);
        $P->update();
        /*
          $LDP = Detallepedido::model()->findAllByAttributes(array('id_pedido' => $P->getAttribute('id')));
          for ($i = 0; $i < sizeof($LDP); $i++) {
          $DP = $LDP[$i];
          $DP->update();
          } */

        $resp['msg'] = 'ok';
        echo json_encode($resp);
    }

    //----------------------------------------------------------
    //---------------- Envio de Activación  --------------------
    //---------------- Begin -----------------------------------
    //----------------------------------------------------------


    public function SendCorreoNotificacion($detalle2) {
        //-----------------------------------------

        $dFechaC = Yii::app()->locale->dateFormatter->format('yyyy-MM-dd HH:mm', time());
        $sNombreCompleto = "Estimado: Freddy Quispe Fernandez";

        /* $mail = new YiiMailer();
          //$mail->clearLayout();//if layout is already set in config
          $mail->setFrom('from@example.com', 'John Doe');
          $mail->setTo(Yii::app()->params['adminEmail']);
          $mail->setSubject('Mail subject');
          $mail->setBody('Simple message');
          $mail->send(); */
        //Plantilla 
        $mail = new YiiMailer();
        $mail->setFrom('zurieliasd@gmail.com', 'DIPLAST S.R.L.'); //QUIEN ESTA ENVIANDO
        $arrayPara = split(",", "freddy553g@gmail.com,adan.condoric@gmail.com"); //PARA QUIEN
        $mail->setTo($arrayPara); //PARA QUIENES
        $mail->setSubject("Stock Insuficiente.");
        $mail->setReplyTo(''); //sDe para responder directamente
//        $mail->setBody("Estimado personal de Diplast, el producto Bolsas con Código 12000, requiere repocicion de Productos. ya que se encuentra fuera de StocK.");
        $contenido = '<table border="0" cellpadding="0" cellspacing="0" height="100%" style="min-width:348px" width="100%">
   <tbody>
      <tr height="32px"> 		</tr>
      <tr align="center">
         <td width="32px"> 				&nbsp;</td>
         <td>
            <table border="0" cellpadding="0" cellspacing="0" style="max-width:600px">
               <tbody>
                  <tr>
                     <td> 								&nbsp;</td>
                  </tr>
                  <tr height="16"> 						</tr>
                  <tr>
                     <td>
                        <table bgcolor="#2196F3" border="0" cellpadding="0" cellspacing="0" style="background-color:#2196F3;  min-width:332px;max-width:600px;border:1px solid #e0e0e0;border-bottom:0;border-top-left-radius:3px;border-top-right-radius:3px" width="100%">
                           <tbody>
                              <tr>
                                 <td colspan="3" height="72px"> 												&nbsp;</td>
                              </tr>
                              <tr>
                                 <td width="32px"> 												&nbsp;</td>
                                 <td style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:24px;color:#ffffff;line-height:1.25"> 												Diplast S.R.L. </td>
                                 <td width="32px"> 												&nbsp;</td>
                              </tr>
                              <tr>
                                 <td colspan="3" height="18px"> 												&nbsp;</td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        <table bgcolor="#FAFAFA" border="0" cellpadding="0" cellspacing="0" style="min-width:332px;max-width:600px;border:1px solid #f0f0f0;border-bottom:1px solid #c0c0c0;border-top:0;border-bottom-left-radius:3px;border-bottom-right-radius:3px" width="100%">
                           <tbody>
                              <tr height="16px">
                                 <td rowspan="3" width="32px"> 												&nbsp;</td>
                                 <td> 												&nbsp;</td>
                                 <td rowspan="3" width="32px"> 												&nbsp;</td>
                              </tr>
                              <tr>
                                 <td>
                                    <table border="0" cellpadding="0" cellspacing="0" style="min-width:300px">
                                       <tbody>
                                          <tr>
                                             <td style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5"> 																Estimado Personal de Diplast S.R.L. .</td>
                                          </tr>
                                          <tr>
                                             <td style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5">
                                                <p> 																	
                                                Los siguientes productos requieren actualizar el Stock:<br> 
                                                '.$detalle2.'<br> 
                                                </p>
                                                <p></p>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:13px;color:#202020;line-height:1.5"> 																Atentamente,<br /> 																El equipo de Diplast S.R.L. .</td>
                                          </tr>
                                          <tr>
                                             <td style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:12px;color:#b9b9b9;line-height:1.5"> 																Para obtener m&aacute;s informaci&oacute;n, visita nuestra web <a href="http://www.diplastsrl.net/" style="text-decoration:none;color:#4285f4" target="_blank">Diplast S.R.L. </a>.</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <tr height="32px"> 										</tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <tr height="16"> 						</tr>
                  <tr>
                     <td style="max-width:600px;font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:10px;color:#bcbcbc;line-height:1.5"> 								&nbsp;</td>
                  </tr>
               </tbody>
            </table>
         </td>
         <td width="32px"> 				&nbsp;</td>
      </tr>
      <tr height="32px"> 		</tr>
   </tbody>
</table>';
        $mail->setBody($contenido); //
        $mail->setSmtp('smtp.gmail.com', 465, 'ssl', true, 'zurieliasd@gmail.com', '200871137');
        //$mail->setSmtp('smtp.gmail.com', 465, 'ssl', true, 'v8testapp@gmail.com', 'LuLuP3.123');
        if ($mail->send()) {
            $mail->clear();
        } else {
        }
    }

    public function actionPrueba() {
        $this->SendCorreoNotificacion();
    }

}
