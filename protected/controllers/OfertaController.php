<?php

class OfertaController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'ajax', 'borrar'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Oferta;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Oferta'])) {
            $model->attributes = $_POST['Oferta'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Oferta'])) {
            $model->attributes = $_POST['Oferta'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();
        $this->redirect(array('listar'));
    }

    public function actionBorrar($id) {
        $this->loadModel($id)->delete();
        $LC = Oferta::model()->findAll();

        $this->render('listar', array(
            'ofertas' => $LC)
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Oferta');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Oferta('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Oferta']))
            $model->attributes = $_GET['Oferta'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Oferta the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Oferta::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Oferta $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'oferta-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function send_notification($registatoin_ids, $message) {

        $url = 'https://android.googleapis.com/gcm/send';

        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $message,
        );

        $headers = array(
            'Authorization: key=AIzaSyBADa-A2Jqkq2C4ZlgOdZs98dFE0q00_N0',
            'Content-Type: application/json'
        );
        //print_r($headers);
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);
        //echo $result;
    }

    public function actionAjax() {

        if (isset($_GET['modo'])) {
            $modo = $_GET['modo'];
        } else {
            $modo = $_POST['modo'];
        }
        switch ($modo) {
            case 'listar':

                $this->listar();
                break;

            case 'nuevo':

                $this->nuevo();
                break;

            case 'guardar':

                $this->guardar();
                break;

            case 'guardarenviar':

                $this->guardarEnviar();
                break;

            case 'cargar':

                $this->cargar();
                break;

            case 'modificar':

                $this->modificar();
                break;

            default:
                break;
        }
    }

    private function listar() {

        $LC = Oferta::model()->findAll();

        $this->render('listar', array(
            'ofertas' => $LC)
        );
    }

    private function nuevo() {
        $LP = Producto::model()->findAll();
        $O = new Oferta();
        $O->id=0;
        $this->render('formulario', array(
            'oferta' => $O, 'productos' => $LP)
        );
    }

    private function guardar() {

        $model = new Oferta();
        if (isset($_POST['Oferta'])) {
            $model->attributes = $_POST['Oferta'];
            $fecha = date('Y-m-d');
            $model->fechacreacion = $fecha;
            $model->tipo = "P";
            $model->fechamodificacion = $fecha;
            $model->estado = Oferta::NOENVIADO;
            if ($model->id>0){
                $model->isNewRecord=false;   
            }
            $model->save();
        }
        $this->listar();
    }

    private function guardarEnviar() {

        $model = new Oferta();
        if (isset($_POST['Oferta'])) {
            $model->attributes = $_POST['Oferta'];
            $fecha = date('Y-m-d');
            $model->fechacreacion = $fecha;
            $model->tipo = "P";
            $model->fechamodificacion = $fecha;
            $model->estado = Oferta::ENVIADO;
            if ($model->id>0)
                $model->isNewRecord=false;
            $model->save();
            //  ENVIO DE NOTIFICACIONES POR GOOGLE CLOUD MESSENGER77
            $LM = Movil::model()->findAll();
            $ids = array();
            for ($i = 0; $i < sizeof($LM); $i++) {
                $ids[] = $LM[$i]->getAttribute('gcmid');
            }
            $this->send_notification($ids, array("msg" => $model->getAttribute('nombre'), "oferta" => json_encode($model->getAttributes())));
            //----------------------------------------------------
        }
        $this->listar();
    }

    private function cargar() {

        $id = $_GET['id'];
        $O = $this->loadModel($id);
        $LP = Producto::model()->findAll();
        $this->render('formulario', array(
            'oferta' => $O, 'productos' => $LP)
        );
    }

    private function modificar() {

        $id = $_POST['id'];
        $model = $this->loadModel($id);
        if (isset($_POST['Oferta'])) {
            $model->attributes = $_POST['Oferta'];
            $model->save();
        }
    }

    public function actionGetproducto() {
        $res = array();
        $term = Yii::app()->getRequest()->getParam('term', false);
        if ($term) {
            // test table is for the sake of this example
            $sql = 'SELECT id, nombre FROM tbl_producto where LCASE(nombre) LIKE :nombre';
            $cmd = Yii::app()->db->createCommand($sql);
            $cmd->bindValue(":nombre", "%" . strtolower($term) . "%", PDO::PARAM_STR);
            $res = $cmd->queryAll();
        }
        echo CJSON::encode($res);
        Yii::app()->end();
    }

}
